<?php

/* common/sidebar/sidebar-left.html */
class __TwigTemplate_f1b826c11960dc1516a52d7356755937a1ea78e9364b4141375648059f34e855 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<style>
\t.color-w{
\t\tcolor:white;
\t}
\t.effect:hover span{ 
\t\topacity:0.5;
\t}
\t.sidenav a {
\t\tfont-family: AkkuratPro_regular;
\t\tposition: relative;
\t}
\t.small-l{
\t\tfont-size:14px!important;
\t\tcolor:#000000!important;
\t\tfont-weight:bold!important;
\t}
\t.small-s{
\t\tfont-size:13px!important;
\t\tfont-weight:bold;
\t\tcolor:#000000!important;
\t\tpadding-left:40px!important;
\t}
\t.small-s:hover{
\t\tbackground-color:#4E4F51;
\t\tcolor:#ffffff;
\t}
\t.small-s:hover span{
\t\tcolor:#ffffff!important;
\t\topacity:1;
\t}
\t.big-l{
\t\tfont-size:16px!important;
\t\tcolor:#000000!important;
\t\tfont-weight:bold!important;
\t\tborder-top: 1px solid #c7c1c1;
\t}
\t.big-submenu{
\t\tfont-size:16px!important;
\t\tcolor:#ffffff!important;
\t\tfont-weight:bold!important;
\t\tpadding: 0 8px 0 32px;
\t}
\t.arrow-menu{
\t\tposition: absolute;
\t\tright: 10px;
\t\twidth: 14px;
\t\tmargin-top: 3px;
\t}
\t.romb-menu{
\t\twidth: 9px;
    \tmargin-top: -4px;
    \tmargin-right:5px;
\t}
\t.tags{
\t\tposition: relative;
\t\tdisplay:inline-block;
\t\tmargin-right: 8px;
    \tmargin-bottom: 7px;
    \tfont-weight:bold;
\t}
\t.tags>.close-tag{
\t\tposition: absolute;
\t\ttop: -10px;
    \tright: -8px;
    \tfont-size: 15px;
    \tcursor:pointer;
\t}
\t.container-tags{
\t\tpadding:8px 8px 8px 32px;
\t}
\t.tag{
\t\tfont-family: AkkuratPro_regular;
\t\tbackground: -webkit-linear-gradient(#09B5A5, #B9E9E5);
\t\t-webkit-background-clip: text;
\t\t-webkit-text-fill-color: transparent;
\t\tcolor:#008e82;
\t}
\t.filters{
\t\tposition:relative;
\t\theight:100px;
\t\tz-index:3;
\t}
\t.see-more{
    \tcolor: #05aa9a;
    \tfont-family: AkkuratPro_regular;
\t}
\t.button-search{
\t\topacity:0;
\t\ttransition: all 1s ease;
\t\twidth:80%;
\t}
\t.button-search:hover{
\t\topacity:1;
\t\tborder:1px solid #d8d8d8;
\t}
\t.submenu{
\t\tbackground-color: #4E4F51;
\t    height: 130px;
\t    width: 250px;
\t    position: absolute;
\t    top: 0;
\t    left: 100%;
\t    margin-right: -1px;
\t    z-index: 4;
\t    padding:14px 0 8px 0;
\t}
\t.submenu a{
\t\ttext-decoration: none;
\t\tborder: 0;
\t}
\t.submenu>a{
\t\tdisplay:none;
\t}
\t.contain-filt{
\t\theight:1px;
\t\tborder:none;
\t\tposition: relative
\t}
.label-success{
\tbackground-color:#4e4f51;
}
.sidenav {
    height: 100%!important;
    overflow: initial;
    border-right: 1px solid #c7c1c1;
}
.sidenav-hide{
\topacity:0;
\twidth:0;
}
.sidenav-appear{
\topacity:1;
\twidth:250px;
}
.sidenav-appear-mov{
\topacity:1;
\twidth:100%;
}
.hidden-tmp{
\tdisplay:none;
}
.hidden-tmp-none{
\tdisplay:block;
}
.submenu.ps-container>.ps-scrollbar-y-rail>.ps-scrollbar-y {
    background-color: white!important;
}
ps-container.ps-active-x>.ps-scrollbar-x-rail, .ps-container.ps-active-y>.ps-scrollbar-y-rail {
    /* background of the nav scroll */
}

\tprogress::-moz-progress-bar { background-color:#5cb85c!important;width:100%;color:red!important; }
\tprogress::-webkit-progress-bar { background-color:#ffffff!important;}
\tprogress::-webkit-progress-value { background-color:#0CB4A5!important;width:100%;color:red!important; }
\tprogress { background-color:#5cb85c!important;width:100%;color:blue!important;height: 10px; }
</style>
<div id=\"boxMenu\" class=\"sidenav sidenav-hide\">
<div>
</div>
\t<div class=\"container-tags\">
\t</div>
\t<a href=\"\" class=\"small-l\"><p>Fashion Trends 2017</p></a>
\t<a href=\"\" class=\"small-l\"><p>Tecnología Hip</p></a>
\t<a href=\"\" class=\"small-l\"><p>Decor</p></a>
\t<a href=\"javascript:void(0)\" class=\"closebtn\" onclick=\"closeNav()\">&times;</a>
\t<a href=\"#\" class=\"big-l\">GRUPOS 
\t\t<img src=\"";
        // line 168
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/dropdown.png\" class=\"arrow-menu\" alt=\"\">
\t\t<div class=\"filters var1\" data-type=\"groups\">
\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t<img src=\"";
        // line 171
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t<span>Fashion Trends 2017</span>
\t\t\t\t</a>
\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t<img src=\"";
        // line 175
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t<span>Tecnología Hip</span>
\t\t\t\t
\t\t\t\t</a>
\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t<img src=\"";
        // line 180
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t<span>Decor</span>
\t\t\t\t</a>
\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t<img src=\"";
        // line 184
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t<span>Camping</span>
\t\t\t\t</a>
\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t<img src=\"";
        // line 188
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t<span>Branding</span>
\t\t\t\t</a>
\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t<img src=\"";
        // line 192
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t<span>Playa</span>
\t\t\t\t</a>
\t\t</div>\t\t\t

\t</a>
\t<a href=\"#\" class=\"big-l\" data-special=\"category\">CATEGORÍAS
\t\t<img src=\"";
        // line 199
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/dropdown.png\" class=\"arrow-menu\" alt=\"\">
\t\t<div class=\"contain-filt\">
\t\t\t<div class=\"submenu\">
\t\t\t";
        // line 202
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subcategories"]) ? $context["subcategories"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["data"]) {
            // line 203
            echo "\t\t\t\t<div class=\"itm hidden-tmp sub-m-";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\"  data-type=\"categorys\">
\t\t\t\t\t<p class=\"big-submenu text-uppercase name-sub\">PROMOCIONALES</p>
\t\t\t\t\t";
            // line 205
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["data"]);
            foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
                // line 206
                echo "\t\t\t\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t\t\t\t<img src=\"";
                // line 207
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "public/assets/img/diamant-w.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t\t\t\t<span class=\"color-w\"\">";
                // line 208
                echo twig_escape_filter($this->env, $context["list"], "html", null, true);
                echo "</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 210
            echo "\t
\t\t\t\t</div>

\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 214
        echo "\t\t\t</div>\t\t\t
\t\t</div>
\t\t
\t\t<div class=\"filters var1\" data-type=\"categorys\">
\t\t
\t\t\t";
        // line 219
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categorys"]) ? $context["categorys"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 220
            echo "\t\t\t\t<a href=\"\" class=\"small-s effect subcategory\" data-name=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\">
\t\t\t\t\t<img src=\"";
            // line 221
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t\t<span>";
            // line 222
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</span>
\t\t\t\t</a>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 225
        echo "\t\t</div>
\t</a>
\t<a href=\"#\" class=\"big-l\">
\t\t<span class=\"glyphicon glyphicon-search\"></span>
\t\t<input type=\"text\"  class=\"button-search\" name=\"\">
\t</a>
\t<a href=\"#\" class=\"big-l\">CALIDAD
\t\t<img src=\"";
        // line 232
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/dropdown.png\" class=\"arrow-menu\" alt=\"\">
\t\t<div class=\"filters var1\" data-type=\"quality\">
\t\t\t";
        // line 234
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["qualities"]) ? $context["qualities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["quality"]) {
            // line 235
            echo "\t\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t\t<img src=\"";
            // line 236
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t\t<span>";
            // line 237
            echo twig_escape_filter($this->env, $this->getAttribute($context["quality"], "name", array()), "html", null, true);
            echo "</span>
\t\t\t\t</a>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quality'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 240
        echo "\t\t</div>\t
\t</a>
\t<a href=\"\" class=\"big-l\">ORIGEN
\t\t<img src=\"";
        // line 243
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/dropdown.png\" class=\"arrow-menu\" alt=\"\">
\t\t<div class=\"filters var1\" data-type=\"country\">
\t\t\t";
        // line 245
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 246
            echo "\t\t\t\t<a href=\"\" class=\"small-s effect\">
\t\t\t\t\t<img src=\"";
            // line 247
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/assets/img/diamant.png\" class=\"romb-menu\" alt=\"\">
\t\t\t\t\t<span>";
            // line 248
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "country", array()), "html", null, true);
            echo "</span>
\t\t\t\t</a>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 251
        echo "\t\t</div>\t
\t</a>
\t<a href=\"\" class=\"big-l\">COSTO
\t\t<img src=\"";
        // line 254
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/dropdown.png\" class=\"arrow-menu\" alt=\"\">
\t</a>
</div>
<script type=\"text/javascript\">

</script>";
    }

    public function getTemplateName()
    {
        return "common/sidebar/sidebar-left.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 254,  380 => 251,  371 => 248,  367 => 247,  364 => 246,  360 => 245,  355 => 243,  350 => 240,  341 => 237,  337 => 236,  334 => 235,  330 => 234,  325 => 232,  316 => 225,  307 => 222,  303 => 221,  296 => 220,  292 => 219,  285 => 214,  276 => 210,  267 => 208,  263 => 207,  260 => 206,  256 => 205,  250 => 203,  246 => 202,  240 => 199,  230 => 192,  223 => 188,  216 => 184,  209 => 180,  201 => 175,  194 => 171,  188 => 168,  19 => 1,);
    }
}
/* */
/* <style>*/
/* 	.color-w{*/
/* 		color:white;*/
/* 	}*/
/* 	.effect:hover span{ */
/* 		opacity:0.5;*/
/* 	}*/
/* 	.sidenav a {*/
/* 		font-family: AkkuratPro_regular;*/
/* 		position: relative;*/
/* 	}*/
/* 	.small-l{*/
/* 		font-size:14px!important;*/
/* 		color:#000000!important;*/
/* 		font-weight:bold!important;*/
/* 	}*/
/* 	.small-s{*/
/* 		font-size:13px!important;*/
/* 		font-weight:bold;*/
/* 		color:#000000!important;*/
/* 		padding-left:40px!important;*/
/* 	}*/
/* 	.small-s:hover{*/
/* 		background-color:#4E4F51;*/
/* 		color:#ffffff;*/
/* 	}*/
/* 	.small-s:hover span{*/
/* 		color:#ffffff!important;*/
/* 		opacity:1;*/
/* 	}*/
/* 	.big-l{*/
/* 		font-size:16px!important;*/
/* 		color:#000000!important;*/
/* 		font-weight:bold!important;*/
/* 		border-top: 1px solid #c7c1c1;*/
/* 	}*/
/* 	.big-submenu{*/
/* 		font-size:16px!important;*/
/* 		color:#ffffff!important;*/
/* 		font-weight:bold!important;*/
/* 		padding: 0 8px 0 32px;*/
/* 	}*/
/* 	.arrow-menu{*/
/* 		position: absolute;*/
/* 		right: 10px;*/
/* 		width: 14px;*/
/* 		margin-top: 3px;*/
/* 	}*/
/* 	.romb-menu{*/
/* 		width: 9px;*/
/*     	margin-top: -4px;*/
/*     	margin-right:5px;*/
/* 	}*/
/* 	.tags{*/
/* 		position: relative;*/
/* 		display:inline-block;*/
/* 		margin-right: 8px;*/
/*     	margin-bottom: 7px;*/
/*     	font-weight:bold;*/
/* 	}*/
/* 	.tags>.close-tag{*/
/* 		position: absolute;*/
/* 		top: -10px;*/
/*     	right: -8px;*/
/*     	font-size: 15px;*/
/*     	cursor:pointer;*/
/* 	}*/
/* 	.container-tags{*/
/* 		padding:8px 8px 8px 32px;*/
/* 	}*/
/* 	.tag{*/
/* 		font-family: AkkuratPro_regular;*/
/* 		background: -webkit-linear-gradient(#09B5A5, #B9E9E5);*/
/* 		-webkit-background-clip: text;*/
/* 		-webkit-text-fill-color: transparent;*/
/* 		color:#008e82;*/
/* 	}*/
/* 	.filters{*/
/* 		position:relative;*/
/* 		height:100px;*/
/* 		z-index:3;*/
/* 	}*/
/* 	.see-more{*/
/*     	color: #05aa9a;*/
/*     	font-family: AkkuratPro_regular;*/
/* 	}*/
/* 	.button-search{*/
/* 		opacity:0;*/
/* 		transition: all 1s ease;*/
/* 		width:80%;*/
/* 	}*/
/* 	.button-search:hover{*/
/* 		opacity:1;*/
/* 		border:1px solid #d8d8d8;*/
/* 	}*/
/* 	.submenu{*/
/* 		background-color: #4E4F51;*/
/* 	    height: 130px;*/
/* 	    width: 250px;*/
/* 	    position: absolute;*/
/* 	    top: 0;*/
/* 	    left: 100%;*/
/* 	    margin-right: -1px;*/
/* 	    z-index: 4;*/
/* 	    padding:14px 0 8px 0;*/
/* 	}*/
/* 	.submenu a{*/
/* 		text-decoration: none;*/
/* 		border: 0;*/
/* 	}*/
/* 	.submenu>a{*/
/* 		display:none;*/
/* 	}*/
/* 	.contain-filt{*/
/* 		height:1px;*/
/* 		border:none;*/
/* 		position: relative*/
/* 	}*/
/* .label-success{*/
/* 	background-color:#4e4f51;*/
/* }*/
/* .sidenav {*/
/*     height: 100%!important;*/
/*     overflow: initial;*/
/*     border-right: 1px solid #c7c1c1;*/
/* }*/
/* .sidenav-hide{*/
/* 	opacity:0;*/
/* 	width:0;*/
/* }*/
/* .sidenav-appear{*/
/* 	opacity:1;*/
/* 	width:250px;*/
/* }*/
/* .sidenav-appear-mov{*/
/* 	opacity:1;*/
/* 	width:100%;*/
/* }*/
/* .hidden-tmp{*/
/* 	display:none;*/
/* }*/
/* .hidden-tmp-none{*/
/* 	display:block;*/
/* }*/
/* .submenu.ps-container>.ps-scrollbar-y-rail>.ps-scrollbar-y {*/
/*     background-color: white!important;*/
/* }*/
/* ps-container.ps-active-x>.ps-scrollbar-x-rail, .ps-container.ps-active-y>.ps-scrollbar-y-rail {*/
/*     /* background of the nav scroll *//* */
/* }*/
/* */
/* 	progress::-moz-progress-bar { background-color:#5cb85c!important;width:100%;color:red!important; }*/
/* 	progress::-webkit-progress-bar { background-color:#ffffff!important;}*/
/* 	progress::-webkit-progress-value { background-color:#0CB4A5!important;width:100%;color:red!important; }*/
/* 	progress { background-color:#5cb85c!important;width:100%;color:blue!important;height: 10px; }*/
/* </style>*/
/* <div id="boxMenu" class="sidenav sidenav-hide">*/
/* <div>*/
/* </div>*/
/* 	<div class="container-tags">*/
/* 	</div>*/
/* 	<a href="" class="small-l"><p>Fashion Trends 2017</p></a>*/
/* 	<a href="" class="small-l"><p>Tecnología Hip</p></a>*/
/* 	<a href="" class="small-l"><p>Decor</p></a>*/
/* 	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>*/
/* 	<a href="#" class="big-l">GRUPOS */
/* 		<img src="{{ base_url() }}public/assets/img/dropdown.png" class="arrow-menu" alt="">*/
/* 		<div class="filters var1" data-type="groups">*/
/* 			<a href="" class="small-s effect">*/
/* 				<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 				<span>Fashion Trends 2017</span>*/
/* 				</a>*/
/* 			<a href="" class="small-s effect">*/
/* 				<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 				<span>Tecnología Hip</span>*/
/* 				*/
/* 				</a>*/
/* 			<a href="" class="small-s effect">*/
/* 				<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 				<span>Decor</span>*/
/* 				</a>*/
/* 			<a href="" class="small-s effect">*/
/* 				<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 				<span>Camping</span>*/
/* 				</a>*/
/* 			<a href="" class="small-s effect">*/
/* 				<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 				<span>Branding</span>*/
/* 				</a>*/
/* 			<a href="" class="small-s effect">*/
/* 				<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 				<span>Playa</span>*/
/* 				</a>*/
/* 		</div>			*/
/* */
/* 	</a>*/
/* 	<a href="#" class="big-l" data-special="category">CATEGORÍAS*/
/* 		<img src="{{ base_url() }}public/assets/img/dropdown.png" class="arrow-menu" alt="">*/
/* 		<div class="contain-filt">*/
/* 			<div class="submenu">*/
/* 			{% for key,data in  subcategories %}*/
/* 				<div class="itm hidden-tmp sub-m-{{ key }}"  data-type="categorys">*/
/* 					<p class="big-submenu text-uppercase name-sub">PROMOCIONALES</p>*/
/* 					{% for list in data %}*/
/* 						<a href="" class="small-s effect">*/
/* 							<img src="{{ base_url() }}public/assets/img/diamant-w.png" class="romb-menu" alt="">*/
/* 							<span class="color-w"">{{ list }}</span>*/
/* 						</a>*/
/* 					{% endfor %}	*/
/* 				</div>*/
/* */
/* 			{% endfor %}*/
/* 			</div>			*/
/* 		</div>*/
/* 		*/
/* 		<div class="filters var1" data-type="categorys">*/
/* 		*/
/* 			{% for category in categorys %}*/
/* 				<a href="" class="small-s effect subcategory" data-name="{{ category.name }}" data-id="{{ category.idCategory}}">*/
/* 					<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 					<span>{{ category.name }}</span>*/
/* 				</a>*/
/* 			{% endfor %}*/
/* 		</div>*/
/* 	</a>*/
/* 	<a href="#" class="big-l">*/
/* 		<span class="glyphicon glyphicon-search"></span>*/
/* 		<input type="text"  class="button-search" name="">*/
/* 	</a>*/
/* 	<a href="#" class="big-l">CALIDAD*/
/* 		<img src="{{ base_url() }}public/assets/img/dropdown.png" class="arrow-menu" alt="">*/
/* 		<div class="filters var1" data-type="quality">*/
/* 			{% for quality in qualities %}*/
/* 				<a href="" class="small-s effect">*/
/* 					<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 					<span>{{ quality.name }}</span>*/
/* 				</a>*/
/* 			{% endfor %}*/
/* 		</div>	*/
/* 	</a>*/
/* 	<a href="" class="big-l">ORIGEN*/
/* 		<img src="{{ base_url() }}public/assets/img/dropdown.png" class="arrow-menu" alt="">*/
/* 		<div class="filters var1" data-type="country">*/
/* 			{% for country in countries %}*/
/* 				<a href="" class="small-s effect">*/
/* 					<img src="{{ base_url() }}public/assets/img/diamant.png" class="romb-menu" alt="">*/
/* 					<span>{{ country.country }}</span>*/
/* 				</a>*/
/* 			{% endfor %}*/
/* 		</div>	*/
/* 	</a>*/
/* 	<a href="" class="big-l">COSTO*/
/* 		<img src="{{ base_url() }}public/assets/img/dropdown.png" class="arrow-menu" alt="">*/
/* 	</a>*/
/* </div>*/
/* <script type="text/javascript">*/
/* */
/* </script>*/
