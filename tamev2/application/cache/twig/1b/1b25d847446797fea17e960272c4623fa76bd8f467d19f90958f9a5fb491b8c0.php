<?php

/* shop/diseno.html */
class __TwigTemplate_1b4604cb27e88fe3a40c469898c74bd0ab8edf3eac9553900cf5fd4b24ded329 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
\t<div class=\"row\">
\t  <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 selectColor\">
\t    <p>SELECCIÓN DE COLORES</p>
\t    <p>Elige los colores en los que quieres que <br>venga tu producto</p>
\t    <div id=\"contentColors\">
\t      <div class=\"circle_color\" style=\"background-color: black\"></div>
\t      <div class=\"circle_color\" style=\"background-color: red\"></div>
\t      <div class=\"circle_color\" style=\"background-color: yellow\"></div>
\t      <div class=\"circle_color\" style=\"background-color: green\"></div>
\t      <div class=\"circle_color\" style=\"background-color: blue\"></div>
\t      <div class=\"circle_color\" style=\"background-color: purple\"></div>
\t      <div class=\"circle_color\" style=\"background-color: pink\"></div>
\t      <div class=\"circle_color\" style=\"background-color: orange\"></div>
\t    </div>
\t    <p>SOLICITUD DE COLORES</p>

\t    <!-- Color Picker -->
\t    <div id=\"inline-demo\"></div>
\t    <!-- End Color Picker -->
\t    
\t    <hr>
\t    <p style=\"display: inline-block;\">COMENTARIOS</p><img class=\"upFile\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/up_file.png\" alt=\"up_file\">
\t  </div>
\t  <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 impresion\">
\t    <p>IMPRESIÓN</p>
\t    <p>Producto con logo impreso</p>
\t    <div class=\"form-group\">
\t      <label for=\"\">Tipo de impresión</label>
\t      <select>
\t        <option value=\"\">SELECCIONAR</option>
\t        <option value=\"\">Opción 1</option>
\t        <option value=\"\">Opción 2</option>
\t      </select>
\t    </div>
\t    <div class=\"form-group\">
\t      <label for=\"\">Numero de tintas</label>
\t      <select>
\t        <option value=\"\">SELECCIONAR</option>
\t        <option value=\"\">Opción 1</option>
\t        <option value=\"\">Opción 2</option>
\t      </select>
\t    </div>
\t    <hr>
\t    <p style=\"display: inline-block;\">COMENTARIOS</p><img class=\"upFile\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/up_file.png\" alt=\"up_file\">
\t  </div>
\t</div>

\t<script src=\"";
        // line 49
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/appPersonalizar.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "shop/diseno.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 49,  68 => 45,  43 => 23,  19 => 1,);
    }
}
/* */
/* 	<div class="row">*/
/* 	  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 selectColor">*/
/* 	    <p>SELECCIÓN DE COLORES</p>*/
/* 	    <p>Elige los colores en los que quieres que <br>venga tu producto</p>*/
/* 	    <div id="contentColors">*/
/* 	      <div class="circle_color" style="background-color: black"></div>*/
/* 	      <div class="circle_color" style="background-color: red"></div>*/
/* 	      <div class="circle_color" style="background-color: yellow"></div>*/
/* 	      <div class="circle_color" style="background-color: green"></div>*/
/* 	      <div class="circle_color" style="background-color: blue"></div>*/
/* 	      <div class="circle_color" style="background-color: purple"></div>*/
/* 	      <div class="circle_color" style="background-color: pink"></div>*/
/* 	      <div class="circle_color" style="background-color: orange"></div>*/
/* 	    </div>*/
/* 	    <p>SOLICITUD DE COLORES</p>*/
/* */
/* 	    <!-- Color Picker -->*/
/* 	    <div id="inline-demo"></div>*/
/* 	    <!-- End Color Picker -->*/
/* 	    */
/* 	    <hr>*/
/* 	    <p style="display: inline-block;">COMENTARIOS</p><img class="upFile" src="{{ base_url() }}public/assets/img/up_file.png" alt="up_file">*/
/* 	  </div>*/
/* 	  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 impresion">*/
/* 	    <p>IMPRESIÓN</p>*/
/* 	    <p>Producto con logo impreso</p>*/
/* 	    <div class="form-group">*/
/* 	      <label for="">Tipo de impresión</label>*/
/* 	      <select>*/
/* 	        <option value="">SELECCIONAR</option>*/
/* 	        <option value="">Opción 1</option>*/
/* 	        <option value="">Opción 2</option>*/
/* 	      </select>*/
/* 	    </div>*/
/* 	    <div class="form-group">*/
/* 	      <label for="">Numero de tintas</label>*/
/* 	      <select>*/
/* 	        <option value="">SELECCIONAR</option>*/
/* 	        <option value="">Opción 1</option>*/
/* 	        <option value="">Opción 2</option>*/
/* 	      </select>*/
/* 	    </div>*/
/* 	    <hr>*/
/* 	    <p style="display: inline-block;">COMENTARIOS</p><img class="upFile" src="{{ base_url() }}public/assets/img/up_file.png" alt="up_file">*/
/* 	  </div>*/
/* 	</div>*/
/* */
/* 	<script src="{{ base_url() }}public/assets/js/appPersonalizar.js"></script>*/
/* */
