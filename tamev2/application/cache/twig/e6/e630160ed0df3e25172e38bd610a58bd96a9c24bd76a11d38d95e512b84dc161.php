<?php

/* dashboard/admin/supplier-preview.html */
class __TwigTemplate_2180e2031baf45e642fdf207259ba71f98da9dad5b2c54992ed5a2f2b23064b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/supplier-preview.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-6 text-center\">
            <h1 class=\"title-dashboard text-uppercase\">Vista Previa</h1>
        </div>
        <div class=\"col-sm-6 text-right\">
            <a href=\"";
        // line 18
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/add\" class=\"btn btn-sm btn-success\">
                <i class=\"fa fa-plus-circle fa-right--10\"></i>
                Agregar
            </a>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <h4 class=\"margin-clear\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "name", array()), "html", null, true);
        echo "</h4>
                    <p class=\"text-muted\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "description", array()), "html", null, true);
        echo "</p>
                </div>
                <table class=\"table\">
                    <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Calificación</th>
                    </tr>
                    ";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 38
            echo "                        <tr>
                            <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 5, array(), "array"), "html", null, true);
            echo "</td>
                            <td>
                                <img src=\"";
            // line 42
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 5, array(), "array"), "html", null, true);
            echo ".jpg\" alt=\"\" height=\"50\">
                            </td>
                            <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 2, array(), "array"), "html", null, true);
            echo "</td>
                            <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 0, array(), "array"), "html", null, true);
            echo "</td>
                        </tr>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "                </table>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/supplier-preview.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 48,  125 => 45,  121 => 44,  112 => 42,  107 => 40,  103 => 39,  100 => 38,  83 => 37,  70 => 27,  66 => 26,  55 => 18,  47 => 12,  44 => 11,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-6 text-center">*/
/*             <h1 class="title-dashboard text-uppercase">Vista Previa</h1>*/
/*         </div>*/
/*         <div class="col-sm-6 text-right">*/
/*             <a href="{{ base_url() }}dashboard/admin/catalog/add" class="btn btn-sm btn-success">*/
/*                 <i class="fa fa-plus-circle fa-right--10"></i>*/
/*                 Agregar*/
/*             </a>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <h4 class="margin-clear">{{ supplier.name }}</h4>*/
/*                     <p class="text-muted">{{ supplier.description }}</p>*/
/*                 </div>*/
/*                 <table class="table">*/
/*                     <tr>*/
/*                         <th>#</th>*/
/*                         <th>Item</th>*/
/*                         <th>Imagen</th>*/
/*                         <th>Nombre</th>*/
/*                         <th>Calificación</th>*/
/*                     </tr>*/
/*                     {% for product in products %}*/
/*                         <tr>*/
/*                             <td>{{ loop.index }}</td>*/
/*                             <td>{{ product[5] }}</td>*/
/*                             <td>*/
/*                                 <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ supplier.directory }}/{{ product[5] }}.jpg" alt="" height="50">*/
/*                             </td>*/
/*                             <td>{{ product[2] }}</td>*/
/*                             <td>{{ product[0] }}</td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
