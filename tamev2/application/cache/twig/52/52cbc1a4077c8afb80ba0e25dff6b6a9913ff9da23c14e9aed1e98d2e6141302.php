<?php

/* dashboard/admin/login.html */
class __TwigTemplate_f1fd1ea9ae469d5f1d7e2774385fd239d1eb24aa3061c0a6e4b380942232ad9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/login.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'sidebar' => array($this, 'block_sidebar'),
            'navbar' => array($this, 'block_navbar'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        echo "    <style>
        body {
            background: url(\"";
        // line 5
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/office_1.png\") no-repeat center;
            background-size: cover;
        }
    </style>
";
    }

    // line 10
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 11
    public function block_navbar($context, array $blocks = array())
    {
    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "\t<div class=\"container-fluid bg-dark\" style=\"height: 150px\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-12 text-center margin-top--50\">
\t\t\t\t<img src=\"";
        // line 16
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/logo-tt.svg\" height=\"50\">
\t\t\t\t<p class=\"color-white font-weight--300\">Areá de administración</p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4\">
\t\t\t\t<h3 class=\"margin-bottom--20\">Iniciar Sesión</h3>
\t\t\t\t<form action=\"";
        // line 25
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/home/actionLogin\" method=\"\" accept-charset=\"utf-8\">
\t\t\t\t\t<label for=\"user\">Usuario</label>
\t\t\t\t\t<input id=\"user\" type=\"text\" name=\"user\" class=\"form-control margin-bottom--10\">
\t\t\t\t\t<label for=\"pass\">Contraseña</label>
\t\t\t\t\t<input id=\"pass\" type=\"password\" name=\"pass\" class=\"form-control margin-bottom--20\">
\t\t\t\t\t<button class=\"btn btn-default btn-block\">Ingresar</button>
\t\t\t\t</form>
\t\t\t\t<a href=\"#\" class=\"btn btn-link btn-block margin-top--20\">Recuperar contraseña</a>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 25,  65 => 16,  60 => 13,  57 => 12,  52 => 11,  47 => 10,  38 => 5,  34 => 3,  31 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/*     <style>*/
/*         body {*/
/*             background: url("{{ base_url() }}public/assets/img/office_1.png") no-repeat center;*/
/*             background-size: cover;*/
/*         }*/
/*     </style>*/
/* {% endblock %}*/
/* {% block sidebar %}{% endblock %}*/
/* {% block navbar %}{% endblock %}*/
/* {% block body%}*/
/* 	<div class="container-fluid bg-dark" style="height: 150px">*/
/* 		<div class="row">*/
/* 			<div class="col-sm-12 text-center margin-top--50">*/
/* 				<img src="{{ base_url() }}public/assets/img/logo-tt.svg" height="50">*/
/* 				<p class="color-white font-weight--300">Areá de administración</p>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<div class="container">*/
/* 		<div class="row">*/
/* 			<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">*/
/* 				<h3 class="margin-bottom--20">Iniciar Sesión</h3>*/
/* 				<form action="{{ base_url() }}dashboard/admin/home/actionLogin" method="" accept-charset="utf-8">*/
/* 					<label for="user">Usuario</label>*/
/* 					<input id="user" type="text" name="user" class="form-control margin-bottom--10">*/
/* 					<label for="pass">Contraseña</label>*/
/* 					<input id="pass" type="password" name="pass" class="form-control margin-bottom--20">*/
/* 					<button class="btn btn-default btn-block">Ingresar</button>*/
/* 				</form>*/
/* 				<a href="#" class="btn btn-link btn-block margin-top--20">Recuperar contraseña</a>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
