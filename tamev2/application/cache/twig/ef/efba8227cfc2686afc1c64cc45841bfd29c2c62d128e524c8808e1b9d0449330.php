<?php

/* dashboard/admin/catalog-add.html */
class __TwigTemplate_40be98d0a3d95bc53b19819389d2b4274a2eed7043287236e4215737479ca3f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/catalog-add.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
\t#bodyWrapper {
\t\theight: 100%;
\t\tbackground-color: #fff;
\t} 
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "\t<div class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t<h1 class=\"title-dashboard\">Agregar nuevo producto a mi catálogo</h1>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12\">
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 19
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog\" class=\"btn btn-danger\">
\t\t\t\t\t\t<i class=\"fa fa-times-circle  margin-right--5\"></i>
\t\t\t\t\t\tCancelar
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t";
        // line 26
        if ( !twig_test_empty($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "validation", array()))) {
            // line 27
            echo "\t\t\t\t\t\t\t<div class=\"alert alert-danger\">
\t\t\t\t\t\t\t\t<p>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "validation", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "info", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
                // line 32
                echo "\t\t\t\t\t\t\t\t\tLa fila ";
                echo twig_escape_filter($this->env, $context["info"], "html", null, true);
                echo " esta vacía
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "\t\t\t\t\t\t";
        }
        // line 35
        echo "\t\t\t\t\t\t<form action=\"";
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/upload_test\" method=\"post\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">
\t\t\t\t\t\t\t<label>Archivo Excel</label>
\t\t\t\t\t\t\t<input id=\"fileExcel\" type=\"file\" name=\"excel\" class=\"form-control margin-bottom--10\">
\t\t\t\t\t\t\t<label>Carpeta de Imágenes</label>
\t\t\t\t\t\t\t<input id=\"dirImage\" type=\"file\" name=\"zip\" class=\"form-control\">
\t\t\t\t\t\t\t<p class=\"small text-muted margin-bottom--10\">Las imágenes no deben estar en ninguna sub carpeta.</p>
\t\t\t\t\t\t\t<button class=\"btn btn-success\">Subir</button>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/catalog-add.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 35,  90 => 34,  81 => 32,  77 => 31,  71 => 28,  68 => 27,  66 => 26,  56 => 19,  47 => 12,  44 => 11,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/* 	#bodyWrapper {*/
/* 		height: 100%;*/
/* 		background-color: #fff;*/
/* 	} */
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* 	<div class="container-fluid">*/
/* 		<div class="row">*/
/* 			<div class="col-sm-12 text-center">*/
/* 				<h1 class="title-dashboard">Agregar nuevo producto a mi catálogo</h1>*/
/* 			</div>*/
/* 			<div class="col-sm-12">*/
/* 				<p>*/
/* 					<a href="{{ base_url() }}dashboard/admin/catalog" class="btn btn-danger">*/
/* 						<i class="fa fa-times-circle  margin-right--5"></i>*/
/* 						Cancelar*/
/* 					</a>*/
/* 				</p>*/
/* 				<div class="panel panel-default">*/
/* 					<div class="panel-body">*/
/* 						{% if error.validation is not empty %}*/
/* 							<div class="alert alert-danger">*/
/* 								<p>{{ error.validation }}</p>*/
/* 							</div>*/
/* */
/* 								{% for info in error.info %}*/
/* 									La fila {{ info }} esta vacía*/
/* 								{% endfor %}*/
/* 						{% endif %}*/
/* 						<form action="{{ base_url() }}dashboard/admin/catalog/upload_test" method="post" accept-charset="utf-8" enctype="multipart/form-data">*/
/* 							<label>Archivo Excel</label>*/
/* 							<input id="fileExcel" type="file" name="excel" class="form-control margin-bottom--10">*/
/* 							<label>Carpeta de Imágenes</label>*/
/* 							<input id="dirImage" type="file" name="zip" class="form-control">*/
/* 							<p class="small text-muted margin-bottom--10">Las imágenes no deben estar en ninguna sub carpeta.</p>*/
/* 							<button class="btn btn-success">Subir</button>*/
/* 						</form>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
