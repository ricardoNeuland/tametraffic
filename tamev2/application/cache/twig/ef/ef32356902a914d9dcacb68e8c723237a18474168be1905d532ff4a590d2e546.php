<?php

/* shop/empaque.html */
class __TwigTemplate_ab7f9ee5304ee755efa67534ce7e39215ead65363b6139647b0c18cd42d1aede extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
\t<div class=\"row\">
\t  <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 selectColor impresion\">
\t    <p>PRESENTACIÓN</p>
\t    <p>Como vas a presentar tu producto</p>
\t    <div class=\"form-group\">
\t      <label for=\"\">Tipo de impresión</label>
\t      <select>
\t        <option value=\"\">SELECCIONAR</option>
\t        <option value=\"\">Opción 1</option>
\t        <option value=\"\">Opción 2</option>
\t      </select>
\t    </div>
\t    <hr>
\t    <p style=\"display: inline-block;\">COMENTARIOS</p><img class=\"upFile\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/up_file.png\" alt=\"up_file\">
\t  </div>
\t  <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 impresion\">
\t    <p>CAJA MAESTRA</p>
\t    <p>Elige un empaque que se acomode a tu logística y control de inventarios</p>
\t    <div class=\"form-group\">
\t    \t<label for=\"\">Numero de piezas</label>
\t    \t<input type=\"text\" placeholder=\"213\">
\t    </div>
\t    <div class=\"form-group\">
\t      <label for=\"\">Mezcla de productos</label>
\t      <select>
\t        <option value=\"\">SELECCIONAR</option>
\t        <option value=\"\">Opción 1</option>
\t        <option value=\"\">Opción 2</option>
\t      </select>
\t    </div>
\t    <hr>
\t    <p style=\"display: inline-block;\">COMENTARIOS</p><img class=\"upFile\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/up_file.png\" alt=\"up_file\">
\t  </div>
\t</div>

\t<script src=\"";
        // line 37
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/appPersonalizar.js\"></script>";
    }

    public function getTemplateName()
    {
        return "shop/empaque.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 37,  56 => 33,  35 => 15,  19 => 1,);
    }
}
/* */
/* 	<div class="row">*/
/* 	  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 selectColor impresion">*/
/* 	    <p>PRESENTACIÓN</p>*/
/* 	    <p>Como vas a presentar tu producto</p>*/
/* 	    <div class="form-group">*/
/* 	      <label for="">Tipo de impresión</label>*/
/* 	      <select>*/
/* 	        <option value="">SELECCIONAR</option>*/
/* 	        <option value="">Opción 1</option>*/
/* 	        <option value="">Opción 2</option>*/
/* 	      </select>*/
/* 	    </div>*/
/* 	    <hr>*/
/* 	    <p style="display: inline-block;">COMENTARIOS</p><img class="upFile" src="{{ base_url() }}public/assets/img/up_file.png" alt="up_file">*/
/* 	  </div>*/
/* 	  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 impresion">*/
/* 	    <p>CAJA MAESTRA</p>*/
/* 	    <p>Elige un empaque que se acomode a tu logística y control de inventarios</p>*/
/* 	    <div class="form-group">*/
/* 	    	<label for="">Numero de piezas</label>*/
/* 	    	<input type="text" placeholder="213">*/
/* 	    </div>*/
/* 	    <div class="form-group">*/
/* 	      <label for="">Mezcla de productos</label>*/
/* 	      <select>*/
/* 	        <option value="">SELECCIONAR</option>*/
/* 	        <option value="">Opción 1</option>*/
/* 	        <option value="">Opción 2</option>*/
/* 	      </select>*/
/* 	    </div>*/
/* 	    <hr>*/
/* 	    <p style="display: inline-block;">COMENTARIOS</p><img class="upFile" src="{{ base_url() }}public/assets/img/up_file.png" alt="up_file">*/
/* 	  </div>*/
/* 	</div>*/
/* */
/* 	<script src="{{ base_url() }}public/assets/js/appPersonalizar.js"></script>*/
