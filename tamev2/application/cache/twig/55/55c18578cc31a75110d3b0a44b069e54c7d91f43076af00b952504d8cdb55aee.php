<?php

/* shop/otros.html */
class __TwigTemplate_f083515fbe95c9655cd43925c03678c670b0582a034abc45feba1054f04d47ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
\t<div class=\"row\">
\t  <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 selectColor impresion\">
\t    <p>OTROS</p>
\t    <p>En el caso de que quieras otro tipo de personalización</p>
\t    <hr>
\t    <p style=\"display: inline-block;\">COMENTARIOS</p><img class=\"upFile\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/up_file.png\" alt=\"up_file\">
\t  </div>
\t</div>

\t<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/appPersonalizar.js\"></script>";
    }

    public function getTemplateName()
    {
        return "shop/otros.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 11,  27 => 7,  19 => 1,);
    }
}
/* */
/* 	<div class="row">*/
/* 	  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 selectColor impresion">*/
/* 	    <p>OTROS</p>*/
/* 	    <p>En el caso de que quieras otro tipo de personalización</p>*/
/* 	    <hr>*/
/* 	    <p style="display: inline-block;">COMENTARIOS</p><img class="upFile" src="{{ base_url() }}public/assets/img/up_file.png" alt="up_file">*/
/* 	  </div>*/
/* 	</div>*/
/* */
/* 	<script src="{{ base_url() }}public/assets/js/appPersonalizar.js"></script>*/
