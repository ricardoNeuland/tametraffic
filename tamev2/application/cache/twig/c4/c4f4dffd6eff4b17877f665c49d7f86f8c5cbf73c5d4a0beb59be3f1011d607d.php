<?php

/* common/base-shop.html */
class __TwigTemplate_2b4cf39e52548c5016e2ac63636ce5186af4e909a7d89bf91a2931c8f9a29ce9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"shortcut icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/favicon.ico\" type=\"image/png\">
    <title>";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
    <!-- Bootstrap -->
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/bootstrap.min.css\" rel=\"stylesheet\">
    <!-- Font-awesome -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/font-awesome.min.css\" rel=\"stylesheet\">
    <!-- CSS/SASS -->
    <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/catalogoTienda.css\" rel=\"stylesheet\">
    <!-- Perfect Scrollbar -->
    <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/perfect-scrollbar.min.css\" rel=\"stylesheet\">  
    <!-- Sweetalert2 -->
    <link href=\"https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.css\" rel=\"stylesheet\">
    <link id=\"jquiCSS\" rel=\"stylesheet\" href=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css\" type=\"text/css\" media=\"all\">
    <!-- Colorpicker -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/evol-colorpicker.min.css\" rel=\"stylesheet\">
  </head>

  <body ng-app=\"appTame\">

    <nav class=\"navbar navbar-default\">
      <div class=\"container-fluid\">
        <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
            <div class=\"container\" onclick=\"sandwichMovil(this)\">
              <div class=\"bar1\"></div>
              <div class=\"bar2\"></div>
              <div class=\"bar3\"></div>
            </div>
          </button>
          <a class=\"navbar-brand\" href=\"#\"><img id=\"logoNav\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/logo.png\" alt=\"logo\"></a>
        </div>
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
          <form class=\"navbar-form navbar-left\">
            <div class=\"input-group tsearch\">
              <span class=\"input-group-addon\" id=\"basic-addon1\"><span class=\"glyphicon glyphicon-search\"></span></span>
              <input type=\"text\" class=\"form-control\" aria-describedby=\"basic-addon1\">
            </div>
          </form>
          <center>
            <ul class=\"nav navbar-nav aligNav\">
              <li class=\"alignCenter\"><a href=\"#\">PRODUCTOS</a></li>
              <li class=\"alignCenter\"><a href=\"#\">NOSOTROS</a></li>
              <li class=\"alignCenter\"><a href=\"#\">BLOG</a></li>
            </ul>
            <ul class=\"nav navbar-nav navbar-right\">
              <li><a class=\"alignLogin\" href=\"#\" id=\"registrar\">Regístrate <span id=\"separatorLogin\">|</span></a></li>
              <li><a class=\"alignLogin\" href=\"#\" id=\"ingresar\">Ingresar</a></li>
              <a class=\"navbar-brand\" href=\"#\"><img id=\"logoLogin\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconLogin.png\" alt=\"logo\"></a>
            </ul>
          </center>
        </div>
      </div>
    </nav>

    ";
        // line 61
        $this->loadTemplate("common/sidebar/sidebar-left.html", "common/base-shop.html", 61)->display($context);
        // line 62
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
        // line 64
        echo "
    <footer>
      <a href=\"#\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>
      <a href=\"#\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a>
      <a href=\"#\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a>
      <a href=\"#\"><i class=\"fa fa-youtube-play\" aria-hidden=\"true\"></i></a>
      <a href=\"#\"><i class=\"fa fa-google-plus\" aria-hidden=\"true\"></i></a>
      <a href=\"#\"><i class=\"fa fa-pinterest-p\" aria-hidden=\"true\"></i></a>
    </footer>

    <!-- Modals -->
    <div class=\"modal fade\" id=\"modal_registrar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center><h4 class=\"modal-title\" id=\"myModalLabel\">Regístrate</h4></center>
          </div>
          <div class=\"modal-body\">
            <form action=\"#\" id=\"form_registrar\">
              <div class=\"form-group\">
                <label for=\"\">Usuario</label><br>
                <input type=\"text\" name=\"user\" required>
              </div>
              <div class=\"form-group\">
                <label for=\"\">Contraseña</label><br>
                <input type=\"password\" name=\"psw\" required>
              </div>
              <center>
                <button type=\"submit\" id=\"btn_registrar\">ENVIAR</button>
              </center>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_ingresar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center><h4 class=\"modal-title\" id=\"myModalLabel\">Ingresar</h4></center>
          </div>
          <div class=\"modal-body\">
            <form action=\"#\" method=\"POST\" id=\"form_registrar\">
              <div class=\"form-group\">
                <label for=\"\">Usuario</label><br>
                <input type=\"text\" name=\"user\" required>
              </div>
              <div class=\"form-group\">
                <label for=\"\">Contraseña</label><br>
                <input type=\"password\" name=\"psw\" required>
              </div>
              <center>
                <button type=\"submit\" id=\"btn_registrar\">ENVIAR</button>
              </center>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_lista\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center><h4 class=\"modal-title\" id=\"myModalLabel\">Nueva lista</h4></center>
          </div>
          <div class=\"modal-body\">
            <form action=\"#\" method=\"POST\" id=\"form_registrar\">
              <div class=\"form-group\">
                <label for=\"\">Nombre</label><br>
                <input type=\"text\" name=\"name\" required>
              </div>
              <div class=\"form-group\">
                <label for=\"\">Descripción</label><br>
                <textarea name=\"description\" rows=\"10\"></textarea>
              </div>
              <button type=\"button\" id=\"btn_registrar\" data-dismiss=\"modal\">CANCELAR</button>
              <button type=\"submit\" id=\"btn_registrar\" style=\"float: right;\">GUARDAR</button>  
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_personalizar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center><h4 class=\"modal-title\" id=\"myModalLabel\">PERSONALIZAR</h4></center>
          </div>
          <div class=\"modal-body\">
            <form action=\"#\" method=\"POST\">
              <center>
                <p>
                  Queremos que este producto sea lo que estas buscando, envíanos <br>lo que quieres personalizar
                </p>
                <nav id=\"navPersonalizar\">
                  <a href=\"#diseno\">DISEÑO</a>
                  <a href=\"#empaque\">EMPAQUE</a>
                  <a href=\"#otros\">OTROS</a>
                </nav>
              </center>

              <!-- Content angular -->
              ";
        // line 175
        echo "
                <div ng-view></div>
              ";
        echo "

              <div class=\"row\">
                <div class=\"col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2\">
                  <center>
                    <button type=\"button\" id=\"btn_registrar\" data-dismiss=\"modal\">CANCELAR</button>
                    <button type=\"submit\" id=\"btn_registrar\">GUARDAR</button> 
                  </center>
                </div>
              </div> 
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_comentarios\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center><h4 class=\"modal-title\" id=\"myModalLabel\">SELECCIÓN DE COLORES</h4></center>
          </div>
          <div class=\"modal-body\">
            <form action=\"#\" method=\"POST\">
              <div class=\"row\">
                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                  <div class=\"form-group formComentarios\">
                    <p>Comentarios</p>
                    <textarea name=\"\" id=\"\" rows=\"7\"></textarea>
                  </div>
                  <center>
                    <button type=\"button\" id=\"btn_registrar\" data-dismiss=\"modal\" style=\"float: left;\">CANCELAR</button>
                    <img class=\"upFile comentUpFile\" src=\"";
        // line 208
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/up_file.png\" alt=\"up_file\">
                    <button type=\"submit\" id=\"btn_registrar\" style=\"float: right;\">GUARDAR</button> 
                  </center>
                </div>
              </div> 
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_pedido\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <h4 class=\"modal-title\" id=\"myModalLabel\">MI CONTENEDOR</h4>
            <p>Total 3 productos</p>
          </div>
          <div class=\"modal-body\">
            
            <div class=\"scrollbar\" id=\"style-3\">
              <div class=\"row anchoRow\">
                <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey\">
                  <img src=\"http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg\" alt=\"comparaPic1\">
                </div>
                <div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor\">
                  <p>Anki Kozmo</p>
                  <p>Tecnología I Niños</p>
                  <p>\$ 43.80 USD I 100 pz min.</p>
                  <p>China Muestra Express Personalizable</p>
                </div>
              </div>
              <div class=\"row anchoRow\">
                <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey\">
                  <img src=\"http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg\" alt=\"comparaPic1\">
                </div>
                <div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor\">
                  <p>Anki Kozmo</p>
                  <p>Tecnología I Niños</p>
                  <p>\$ 43.80 USD I 100 pz min.</p>
                  <p>China Muestra Express Personalizable</p>
                </div>
              </div>
              <div class=\"row anchoRow\">
                <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey\">
                  <img src=\"http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg\" alt=\"comparaPic1\">
                </div>
                <div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor\">
                  <p>Anki Kozmo</p>
                  <p>Tecnología I Niños</p>
                  <p>\$ 43.80 USD I 100 pz min.</p>
                  <p>China Muestra Express Personalizable</p>
                </div>
              </div>
              <div class=\"row anchoRow\">
                <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey\">
                  <img src=\"http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg\" alt=\"comparaPic1\">
                </div>
                <div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor\">
                  <p>Anki Kozmo</p>
                  <p>Tecnología I Niños</p>
                  <p>\$ 43.80 USD I 100 pz min.</p>
                  <p>China Muestra Express Personalizable</p>
                </div>
              </div>
            </div>

          </div>
          <div class=\"modal-footer\">
            <center>
              <a href=\"http://yankuserver.com/tamev2/catalogo/contenedorProducto\"><button>IR A CONTENEDOR</button></a><br>
              <a href=\"http://yankuserver.com/tamev2/catalogo\">Seguir seleccionando</a>
            </center>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_guardar_contenedor\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center><h4 class=\"modal-title\" id=\"myModalLabel\">Contenedor</h4></center>
          </div>
          <div class=\"modal-body\">
            <form action=\"#\" method=\"POST\" id=\"form_registrar\">
              <div class=\"form-group\">
                <label for=\"\">Nombre</label><label for=\"\"><a href=\"#\">Ver mis contenedores</a></label><br>
                <input type=\"text\" name=\"name\" required>
              </div>
              <div class=\"form-group\">
                <label for=\"\">Descripción</label><br>
                <textarea name=\"description\" rows=\"10\"></textarea>
              </div>
              <button type=\"button\" id=\"btn_registrar\" data-dismiss=\"modal\">CANCELAR</button>
              <button type=\"submit\" id=\"btn_registrar\" style=\"float: right;\">GUARDAR</button>  
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_calendar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center>
              <h4 class=\"modal-title\" id=\"myModalLabel\">FECHA DE ENTREGA</h4>
              <p>Fecha estimada en la cual se entregaría el pedido</p>
            </center>
          </div>
          <div class=\"modal-body\">
            <center>
              <div class=\"calendar\"></div>
            </center>
          </div>
          <div class=\"modal-footer\">
            <center>
              <button id=\"btnSave\">Guardar</button>
            </center>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade modal_calendar_right\" id=\"modal_calendar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center>
              <h4 class=\"modal-title\" id=\"myModalLabel\">FECHA DE ENTREGA</h4>
              <p>Fecha estimada en la cual se entregaría el pedido</p>
            </center>
          </div>
          <div class=\"modal-body\">
            <center>
              <div class=\"calendar\"></div>
            </center>
          </div>
          <div class=\"modal-footer\">
            <center>
              <button id=\"btnSave\" class=\"btnNavC\">Guardar</button>
            </center>
          </div>
        </div>
      </div>
    </div>

    <div class=\"modal fade\" id=\"modal_comentario\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <center><h4 class=\"modal-title\" id=\"myModalLabel\">Comentarios</h4></center>
          </div>
          <div class=\"modal-body\">
            <form action=\"#\" method=\"POST\" id=\"form_registrar\">        
              <div class=\"form-group\">
                <textarea name=\"description\" rows=\"10\"></textarea>
              </div>
              <button type=\"button\" id=\"btn_registrar\" data-dismiss=\"modal\">CANCELAR</button>
              <button type=\"submit\" id=\"btn_registrar\" style=\"float: right;\">GUARDAR</button>  
            </form>
          </div>
        </div>
      </div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js\" type=\"text/javascript\"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src=\"";
        // line 385
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/bootstrap.min.js\"></script>
    <!-- Perfect Scrollbar -->
    <script src=\"";
        // line 387
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/perfect-scrollbar.jquery.min.js\"></script>
    <script src=\"";
        // line 388
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/perfect-scrollbar.min.js\"></script>
    <script src=\"https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.js\"></script>
    <!-- operaciones -->
    <script src=\"";
        // line 391
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/operaciones.js\"></script>
    <script src=\"";
        // line 392
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/operaciones2.js\"></script>
    <!-- operaciones colors palette -->
    <script src=\"";
        // line 394
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/evol-colorpicker.min.js\"></script>
    <!-- Angular -->
    <script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js\"></script>
    <script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js\"></script>
    <!-- modules -->
    <script src=\"";
        // line 399
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/modules/root.js\"></script>

  </body>
</html>
";
    }

    // line 62
    public function block_content($context, array $blocks = array())
    {
        // line 63
        echo "    ";
    }

    public function getTemplateName()
    {
        return "common/base-shop.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  487 => 63,  484 => 62,  475 => 399,  467 => 394,  462 => 392,  458 => 391,  452 => 388,  448 => 387,  443 => 385,  263 => 208,  225 => 175,  114 => 64,  111 => 62,  109 => 61,  99 => 54,  78 => 36,  60 => 21,  52 => 16,  47 => 14,  42 => 12,  37 => 10,  32 => 8,  28 => 7,  20 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/*   <head>*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <link rel="shortcut icon" href="{{ base_url() }}public/assets/img/favicon.ico" type="image/png">*/
/*     <title>{{ title }}</title>*/
/*     <!-- Bootstrap -->*/
/*     <link href="{{ base_url() }}public/assets/css/bootstrap.min.css" rel="stylesheet">*/
/*     <!-- Font-awesome -->*/
/*     <link href="{{ base_url() }}public/assets/css/font-awesome.min.css" rel="stylesheet">*/
/*     <!-- CSS/SASS -->*/
/*     <link href="{{ base_url() }}public/assets/css/catalogoTienda.css" rel="stylesheet">*/
/*     <!-- Perfect Scrollbar -->*/
/*     <link href="{{ base_url() }}public/assets/css/perfect-scrollbar.min.css" rel="stylesheet">  */
/*     <!-- Sweetalert2 -->*/
/*     <link href="https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.css" rel="stylesheet">*/
/*     <link id="jquiCSS" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css" type="text/css" media="all">*/
/*     <!-- Colorpicker -->*/
/*     <link href="{{ base_url() }}public/assets/css/evol-colorpicker.min.css" rel="stylesheet">*/
/*   </head>*/
/* */
/*   <body ng-app="appTame">*/
/* */
/*     <nav class="navbar navbar-default">*/
/*       <div class="container-fluid">*/
/*         <div class="navbar-header">*/
/*           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">*/
/*             <div class="container" onclick="sandwichMovil(this)">*/
/*               <div class="bar1"></div>*/
/*               <div class="bar2"></div>*/
/*               <div class="bar3"></div>*/
/*             </div>*/
/*           </button>*/
/*           <a class="navbar-brand" href="#"><img id="logoNav" src="{{ base_url() }}public/assets/img/logo.png" alt="logo"></a>*/
/*         </div>*/
/*         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/*           <form class="navbar-form navbar-left">*/
/*             <div class="input-group tsearch">*/
/*               <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>*/
/*               <input type="text" class="form-control" aria-describedby="basic-addon1">*/
/*             </div>*/
/*           </form>*/
/*           <center>*/
/*             <ul class="nav navbar-nav aligNav">*/
/*               <li class="alignCenter"><a href="#">PRODUCTOS</a></li>*/
/*               <li class="alignCenter"><a href="#">NOSOTROS</a></li>*/
/*               <li class="alignCenter"><a href="#">BLOG</a></li>*/
/*             </ul>*/
/*             <ul class="nav navbar-nav navbar-right">*/
/*               <li><a class="alignLogin" href="#" id="registrar">Regístrate <span id="separatorLogin">|</span></a></li>*/
/*               <li><a class="alignLogin" href="#" id="ingresar">Ingresar</a></li>*/
/*               <a class="navbar-brand" href="#"><img id="logoLogin" src="{{ base_url() }}public/assets/img/iconLogin.png" alt="logo"></a>*/
/*             </ul>*/
/*           </center>*/
/*         </div>*/
/*       </div>*/
/*     </nav>*/
/* */
/*     {% include 'common/sidebar/sidebar-left.html' %}*/
/*     {% block content %}*/
/*     {% endblock %}*/
/* */
/*     <footer>*/
/*       <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>*/
/*       <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>*/
/*       <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>*/
/*       <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>*/
/*       <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>*/
/*       <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>*/
/*     </footer>*/
/* */
/*     <!-- Modals -->*/
/*     <div class="modal fade" id="modal_registrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center><h4 class="modal-title" id="myModalLabel">Regístrate</h4></center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <form action="#" id="form_registrar">*/
/*               <div class="form-group">*/
/*                 <label for="">Usuario</label><br>*/
/*                 <input type="text" name="user" required>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label for="">Contraseña</label><br>*/
/*                 <input type="password" name="psw" required>*/
/*               </div>*/
/*               <center>*/
/*                 <button type="submit" id="btn_registrar">ENVIAR</button>*/
/*               </center>*/
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center><h4 class="modal-title" id="myModalLabel">Ingresar</h4></center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <form action="#" method="POST" id="form_registrar">*/
/*               <div class="form-group">*/
/*                 <label for="">Usuario</label><br>*/
/*                 <input type="text" name="user" required>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label for="">Contraseña</label><br>*/
/*                 <input type="password" name="psw" required>*/
/*               </div>*/
/*               <center>*/
/*                 <button type="submit" id="btn_registrar">ENVIAR</button>*/
/*               </center>*/
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_lista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center><h4 class="modal-title" id="myModalLabel">Nueva lista</h4></center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <form action="#" method="POST" id="form_registrar">*/
/*               <div class="form-group">*/
/*                 <label for="">Nombre</label><br>*/
/*                 <input type="text" name="name" required>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label for="">Descripción</label><br>*/
/*                 <textarea name="description" rows="10"></textarea>*/
/*               </div>*/
/*               <button type="button" id="btn_registrar" data-dismiss="modal">CANCELAR</button>*/
/*               <button type="submit" id="btn_registrar" style="float: right;">GUARDAR</button>  */
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_personalizar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center><h4 class="modal-title" id="myModalLabel">PERSONALIZAR</h4></center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <form action="#" method="POST">*/
/*               <center>*/
/*                 <p>*/
/*                   Queremos que este producto sea lo que estas buscando, envíanos <br>lo que quieres personalizar*/
/*                 </p>*/
/*                 <nav id="navPersonalizar">*/
/*                   <a href="#diseno">DISEÑO</a>*/
/*                   <a href="#empaque">EMPAQUE</a>*/
/*                   <a href="#otros">OTROS</a>*/
/*                 </nav>*/
/*               </center>*/
/* */
/*               <!-- Content angular -->*/
/*               {% verbatim %}*/
/*                 <div ng-view></div>*/
/*               {% endverbatim %}*/
/* */
/*               <div class="row">*/
/*                 <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">*/
/*                   <center>*/
/*                     <button type="button" id="btn_registrar" data-dismiss="modal">CANCELAR</button>*/
/*                     <button type="submit" id="btn_registrar">GUARDAR</button> */
/*                   </center>*/
/*                 </div>*/
/*               </div> */
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_comentarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center><h4 class="modal-title" id="myModalLabel">SELECCIÓN DE COLORES</h4></center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <form action="#" method="POST">*/
/*               <div class="row">*/
/*                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                   <div class="form-group formComentarios">*/
/*                     <p>Comentarios</p>*/
/*                     <textarea name="" id="" rows="7"></textarea>*/
/*                   </div>*/
/*                   <center>*/
/*                     <button type="button" id="btn_registrar" data-dismiss="modal" style="float: left;">CANCELAR</button>*/
/*                     <img class="upFile comentUpFile" src="{{ base_url() }}public/assets/img/up_file.png" alt="up_file">*/
/*                     <button type="submit" id="btn_registrar" style="float: right;">GUARDAR</button> */
/*                   </center>*/
/*                 </div>*/
/*               </div> */
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_pedido" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <h4 class="modal-title" id="myModalLabel">MI CONTENEDOR</h4>*/
/*             <p>Total 3 productos</p>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             */
/*             <div class="scrollbar" id="style-3">*/
/*               <div class="row anchoRow">*/
/*                 <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey">*/
/*                   <img src="http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg" alt="comparaPic1">*/
/*                 </div>*/
/*                 <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor">*/
/*                   <p>Anki Kozmo</p>*/
/*                   <p>Tecnología I Niños</p>*/
/*                   <p>$ 43.80 USD I 100 pz min.</p>*/
/*                   <p>China Muestra Express Personalizable</p>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="row anchoRow">*/
/*                 <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey">*/
/*                   <img src="http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg" alt="comparaPic1">*/
/*                 </div>*/
/*                 <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor">*/
/*                   <p>Anki Kozmo</p>*/
/*                   <p>Tecnología I Niños</p>*/
/*                   <p>$ 43.80 USD I 100 pz min.</p>*/
/*                   <p>China Muestra Express Personalizable</p>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="row anchoRow">*/
/*                 <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey">*/
/*                   <img src="http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg" alt="comparaPic1">*/
/*                 </div>*/
/*                 <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor">*/
/*                   <p>Anki Kozmo</p>*/
/*                   <p>Tecnología I Niños</p>*/
/*                   <p>$ 43.80 USD I 100 pz min.</p>*/
/*                   <p>China Muestra Express Personalizable</p>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="row anchoRow">*/
/*                 <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 borderGrey">*/
/*                   <img src="http://yankuserver.com/tamev2/public/assets/img/comparaPic1.jpg" alt="comparaPic1">*/
/*                 </div>*/
/*                 <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 infoContenedor">*/
/*                   <p>Anki Kozmo</p>*/
/*                   <p>Tecnología I Niños</p>*/
/*                   <p>$ 43.80 USD I 100 pz min.</p>*/
/*                   <p>China Muestra Express Personalizable</p>*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/* */
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <center>*/
/*               <a href="http://yankuserver.com/tamev2/catalogo/contenedorProducto"><button>IR A CONTENEDOR</button></a><br>*/
/*               <a href="http://yankuserver.com/tamev2/catalogo">Seguir seleccionando</a>*/
/*             </center>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_guardar_contenedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center><h4 class="modal-title" id="myModalLabel">Contenedor</h4></center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <form action="#" method="POST" id="form_registrar">*/
/*               <div class="form-group">*/
/*                 <label for="">Nombre</label><label for=""><a href="#">Ver mis contenedores</a></label><br>*/
/*                 <input type="text" name="name" required>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label for="">Descripción</label><br>*/
/*                 <textarea name="description" rows="10"></textarea>*/
/*               </div>*/
/*               <button type="button" id="btn_registrar" data-dismiss="modal">CANCELAR</button>*/
/*               <button type="submit" id="btn_registrar" style="float: right;">GUARDAR</button>  */
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_calendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center>*/
/*               <h4 class="modal-title" id="myModalLabel">FECHA DE ENTREGA</h4>*/
/*               <p>Fecha estimada en la cual se entregaría el pedido</p>*/
/*             </center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <center>*/
/*               <div class="calendar"></div>*/
/*             </center>*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <center>*/
/*               <button id="btnSave">Guardar</button>*/
/*             </center>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade modal_calendar_right" id="modal_calendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center>*/
/*               <h4 class="modal-title" id="myModalLabel">FECHA DE ENTREGA</h4>*/
/*               <p>Fecha estimada en la cual se entregaría el pedido</p>*/
/*             </center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <center>*/
/*               <div class="calendar"></div>*/
/*             </center>*/
/*           </div>*/
/*           <div class="modal-footer">*/
/*             <center>*/
/*               <button id="btnSave" class="btnNavC">Guardar</button>*/
/*             </center>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/*     <div class="modal fade" id="modal_comentario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*       <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*           <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <center><h4 class="modal-title" id="myModalLabel">Comentarios</h4></center>*/
/*           </div>*/
/*           <div class="modal-body">*/
/*             <form action="#" method="POST" id="form_registrar">        */
/*               <div class="form-group">*/
/*                 <textarea name="description" rows="10"></textarea>*/
/*               </div>*/
/*               <button type="button" id="btn_registrar" data-dismiss="modal">CANCELAR</button>*/
/*               <button type="submit" id="btn_registrar" style="float: right;">GUARDAR</button>  */
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* */
/* */
/*     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->*/
/*     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>*/
/*     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>*/
/*     <!-- Include all compiled plugins (below), or include individual files as needed -->*/
/*     <script src="{{ base_url() }}public/assets/js/bootstrap.min.js"></script>*/
/*     <!-- Perfect Scrollbar -->*/
/*     <script src="{{ base_url() }}public/assets/js/perfect-scrollbar.jquery.min.js"></script>*/
/*     <script src="{{ base_url() }}public/assets/js/perfect-scrollbar.min.js"></script>*/
/*     <script src="https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.js"></script>*/
/*     <!-- operaciones -->*/
/*     <script src="{{ base_url() }}public/assets/js/operaciones.js"></script>*/
/*     <script src="{{ base_url() }}public/assets/js/operaciones2.js"></script>*/
/*     <!-- operaciones colors palette -->*/
/*     <script src="{{ base_url() }}public/assets/js/evol-colorpicker.min.js"></script>*/
/*     <!-- Angular -->*/
/*     <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>*/
/*     <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>*/
/*     <!-- modules -->*/
/*     <script src="{{ base_url() }}public/assets/js/modules/root.js"></script>*/
/* */
/*   </body>*/
/* </html>*/
/* */
