<?php

/* dashboard/admin/supplier-add.html */
class __TwigTemplate_ac8c404b2464c30d102af4d94fae516ad88d79345df73cf39d8df43e7d95c3ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/supplier-add.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
\t#bodyWrapper {
\t\theight: 100%;
\t\tbackground-color: #fff;
\t}
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "\t<div class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-12 text-center\">
\t\t\t\t<h1 class=\"title-dashboard\">Agregar Supplier y Catálogo</h1>
\t\t\t\t<br><br>
\t\t\t</div>\t\t\t
\t\t\t<div class=\"col-sm-10 col-md-offset-1\">\t\t\t\t
\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t";
        // line 21
        if ( !twig_test_empty($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "validation", array()))) {
            // line 22
            echo "\t\t\t\t\t\t\t<div class=\"alert alert-danger\">
\t\t\t\t\t\t\t\t<p>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "validation", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "info", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
                // line 27
                echo "\t\t\t\t\t\t\t\t\tLa fila ";
                echo twig_escape_filter($this->env, $context["info"], "html", null, true);
                echo " esta vacia
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "\t\t\t\t\t\t";
        }
        // line 30
        echo "\t\t\t\t\t\t<form action=\"";
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/upload_test\" method=\"post\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-md-offset-1 col-lg-10\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<label>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\tArchivo Excel
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t<input id=\"fileExcel\" type=\"file\" name=\"excel\" class=\"form-control margin-bottom--10\">\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-offset-1 col-lg-10\">\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t\t<label>Carpeta de Imagenes</label>
\t\t\t\t\t\t\t\t<input id=\"dirImage\" type=\"file\" name=\"zip\" class=\"form-control\">
\t\t\t\t\t\t\t\t<p class=\"small col-lg-12 text-muted margin-bottom-10\">Las imagenes no deben estar en niguna subcarpeta.</p>
\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-lg-12 text-center\">\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t\t<button class=\"col-lg-3 col-md-offset-2 text-center btn btn-success\">Subir</button>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 51
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier\" class=\"btn btn-danger col-lg-3 col-md-offset-2 text-center\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times-circle  margin-right--5\"></i>Cancelar
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/supplier-add.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 51,  85 => 30,  82 => 29,  73 => 27,  69 => 26,  63 => 23,  60 => 22,  58 => 21,  47 => 12,  44 => 11,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/* 	#bodyWrapper {*/
/* 		height: 100%;*/
/* 		background-color: #fff;*/
/* 	}*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* 	<div class="container-fluid">*/
/* 		<div class="row">*/
/* 			<div class="col-md-12 text-center">*/
/* 				<h1 class="title-dashboard">Agregar Supplier y Catálogo</h1>*/
/* 				<br><br>*/
/* 			</div>			*/
/* 			<div class="col-sm-10 col-md-offset-1">				*/
/* 				<div class="panel panel-default">*/
/* 					<div class="panel-body">*/
/* 						{% if error.validation is not empty %}*/
/* 							<div class="alert alert-danger">*/
/* 								<p>{{ error.validation }}</p>*/
/* 							</div>*/
/* */
/* 								{% for info in error.info %}*/
/* 									La fila {{ info }} esta vacia*/
/* 								{% endfor %}*/
/* 						{% endif %}*/
/* 						<form action="{{ base_url() }}dashboard/admin/supplier/upload_test" method="post" accept-charset="utf-8" enctype="multipart/form-data">							*/
/* 							<div class="col-md-offset-1 col-lg-10">*/
/* 								<br>*/
/* 								<label>*/
/* 									<br>*/
/* 									Archivo Excel*/
/* 									<br>*/
/* 								</label>*/
/* 								<input id="fileExcel" type="file" name="excel" class="form-control margin-bottom--10">	*/
/* 							</div>*/
/* 							<div class="col-md-offset-1 col-lg-10">								*/
/* 								<br><br>*/
/* 								<label>Carpeta de Imagenes</label>*/
/* 								<input id="dirImage" type="file" name="zip" class="form-control">*/
/* 								<p class="small col-lg-12 text-muted margin-bottom-10">Las imagenes no deben estar en niguna subcarpeta.</p>*/
/* 								<br><br>*/
/* 							</div>							*/
/* 							<div class="col-lg-12 text-center">								*/
/* 								<br><br>*/
/* 								<button class="col-lg-3 col-md-offset-2 text-center btn btn-success">Subir</button>*/
/* 								<p>*/
/* 									<a href="{{ base_url() }}dashboard/admin/supplier" class="btn btn-danger col-lg-3 col-md-offset-2 text-center">*/
/* 										<i class="fa fa-times-circle  margin-right--5"></i>Cancelar*/
/* 									</a>*/
/* 								</p>*/
/* 								<br><br>*/
/* 								<br><br>*/
/* 							</div>							*/
/* 						</form>*/
/* 					</div>*/
/* 				</div>				*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
