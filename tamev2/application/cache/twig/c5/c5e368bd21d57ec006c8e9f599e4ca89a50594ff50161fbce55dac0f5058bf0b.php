<?php

/* dashboard/admin/product-pdf.html */
class __TwigTemplate_a92f65a603f3c8f166eff307c111fdbd70db0c35aea49ae3c9ee353b97a0766c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
<head>
    <meta charset=\"UTF-8\">    
    <title>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title", array()), "html", null, true);
        echo "</title>
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/favicon.ico\" />        
    <style>
        .gallery div img {
            width: 250px;
        }
        .gallery div {
            margin: 0 auto;
            padding: 0;
        }        
        hr {
            color: #a9a9a9;
            height: 1px;
        }        
        p {
            font-family: helvetica;
            font-size: 11px;
            font-style: normal;
            font-weight: normal;
            line-height: normal;
            text-rendering: optimizeLegibility;
            color: #666;
            padding: 0;
            margin: 0;
        }
        h1, h2, h3, h4 {
            color: #333;
            margin: 0 auto;
            padding: 0;
            line-height: normal;            
        }
    </style>  
</head>
<body>
    <div>        
        <div class=\"row\">
            <table border=\"0\">
               <tr>
                 <td>
                   <div class=\"gallery\">
                       <div>
                           <img src=\"";
        // line 46
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/uploads/supplier/catalog/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo ".jpg\">
                       </div>            
                       ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["gallery"]) ? $context["gallery"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["img"]) {
            // line 49
            echo "                       <div>
                            <img src=\"";
            // line 50
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["img"], "name", array()), "html", null, true);
            echo "\">
                       </div>
                       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['img'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "                   </div>
                 </td>                         
                 <td>
                   <div class=\"summary\">
                       <div>                                   
                           <h1 class=\"product-name\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "</h1>
                           <h2 class=\"product-title\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "header", array()), "html", null, true);
        echo "</h2>
                           <h3 class=\"product-score\">Calificacion: ";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "score", array()), "html", null, true);
        echo "</h3>
                           <p class=\"product-price\">\$";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "</p>
                           <p class=\"product-description\">";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "html", null, true);
        echo "</p>                                                  
                           <p>
                               <strong>Cantidad minima de pedido:</strong> ";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "minimumOrder", array()), "html", null, true);
        echo "<br>
                               <strong>Item:</strong> ";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo "<br>
                               <strong>Supplier ID: </strong> ";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_custome", array()), "html", null, true);
        echo "<br>
                               <strong>Supplier Name:</strong> <a href=\"";
        // line 67
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/products/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_id", array()), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_name", array()), "html", null, true);
        echo " </a><br>
                               <strong>Calidad:</strong> ";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quality", array()), "html", null, true);
        echo "<br>
                               <strong>Origen:</strong> ";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "origin2", array()), "html", null, true);
        echo "<br>
                               <strong>Procedencia:</strong> ";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "origin", array()), "html", null, true);
        echo "<br>
                               <strong>Presentación:</strong> ";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "presentation", array()), "html", null, true);
        echo "
                           </p>
                           <br>
                               <h4>COLORES</h4>
                               ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["colors"]) ? $context["colors"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
            echo "          
                                   <div style=\"background-color: ";
            // line 76
            echo twig_escape_filter($this->env, $context["color"], "html", null, true);
            echo "; padding: 10px;\"></div>
                               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                           
                            <br>
                            <h4>CARACTERISTICAS</h4>
                               <p>
                                  ";
        // line 81
        if (twig_test_empty($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()))) {
            // line 82
            echo "                                       <span class=\"text-muted\">No hay una descripción</span>
                                  ";
        } else {
            // line 84
            echo "                                      ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()), "html", null, true);
            echo "
                                  ";
        }
        // line 86
        echo "                               </p>                                       
                           <h4>ESPECIFICACIONES</h4>
                           <p>
                               ";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "especifications", array()), "html", null, true);
        echo "<br><br>
                               <strong>Dimensiones:</strong> ";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "dimensions", array()), "html", null, true);
        echo "<br>
                               <strong>Peso:</strong> ";
        // line 91
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "weight", array()), "html", null, true);
        echo "<br><br>                                
                           </p>                           
                           <h4>EMPAQUE</h4>
                           <p>
                               <strong>No. piezas:</strong> ";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_numPieces", array()), "html", null, true);
        echo "<br>
                               <strong>Especificaciones: </strong> ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_especifications", array()), "html", null, true);
        echo "<br>
                               <strong>Alto:</strong> ";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_height", array()), "html", null, true);
        echo "cm<br>
                               <strong>Ancho:</strong> ";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_width", array()), "html", null, true);
        echo "cm<br>
                               <strong>Largo:</strong> ";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_large", array()), "html", null, true);
        echo "cm<br>
                               <strong>Peso:</strong> ";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_weight", array()), "html", null, true);
        echo "kg
                           </p>                           
                           <h4>LOGISTICA</h4>
                           <p>
                               <strong>Tiempo de producción:</strong> ";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_productionTime", array()), "html", null, true);
        echo "<br>
                               <strong>FOB:</strong> ";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fob", array()), "html", null, true);
        echo "<br>
                               <strong>Tiempo de entrega FOB:</strong> ";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fobDeliveryTime", array()), "html", null, true);
        echo "<br>
                               <strong>Muestra fisica:</strong> ";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sample", array()), "html", null, true);
        echo "<br>
                           </p>                           
                           <h4>PRECIO</h4>
                           <p>
                               <strong>Precio s/IVA:</strong> \$";
        // line 111
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo "<br>
                               <strong>Moneda:</strong> ";
        // line 112
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "<br>
                               <strong>Precio sugerido:</strong> \$";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_suggested", array()), "html", null, true);
        echo "<br>
                               <strong>Precio TT:</strong> \$";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo "<br>
                               ";
        // line 115
        $context["utilidad"] = ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()) - $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()));
        // line 116
        echo "                               <strong>Utilidad:</strong> \$";
        echo twig_escape_filter($this->env, (isset($context["utilidad"]) ? $context["utilidad"] : null), "html", null, true);
        echo "
                           </p>                                   
                       </div> 
                   </div>
                 </td>          
               </tr>        
            </table>
        </div>
    </div>    
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/product-pdf.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  286 => 116,  284 => 115,  280 => 114,  276 => 113,  272 => 112,  268 => 111,  261 => 107,  257 => 106,  253 => 105,  249 => 104,  242 => 100,  238 => 99,  234 => 98,  230 => 97,  226 => 96,  222 => 95,  215 => 91,  211 => 90,  207 => 89,  202 => 86,  196 => 84,  192 => 82,  190 => 81,  184 => 77,  176 => 76,  170 => 75,  163 => 71,  159 => 70,  155 => 69,  151 => 68,  143 => 67,  139 => 66,  135 => 65,  131 => 64,  126 => 62,  120 => 61,  116 => 60,  112 => 59,  108 => 58,  101 => 53,  88 => 50,  85 => 49,  81 => 48,  72 => 46,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="es">*/
/* <head>*/
/*     <meta charset="UTF-8">    */
/*     <title>{{ page.title }}</title>*/
/*     <link rel="shortcut icon" type="image/x-icon" href="{{ base_url() }}public/assets/img/favicon.ico" />        */
/*     <style>*/
/*         .gallery div img {*/
/*             width: 250px;*/
/*         }*/
/*         .gallery div {*/
/*             margin: 0 auto;*/
/*             padding: 0;*/
/*         }        */
/*         hr {*/
/*             color: #a9a9a9;*/
/*             height: 1px;*/
/*         }        */
/*         p {*/
/*             font-family: helvetica;*/
/*             font-size: 11px;*/
/*             font-style: normal;*/
/*             font-weight: normal;*/
/*             line-height: normal;*/
/*             text-rendering: optimizeLegibility;*/
/*             color: #666;*/
/*             padding: 0;*/
/*             margin: 0;*/
/*         }*/
/*         h1, h2, h3, h4 {*/
/*             color: #333;*/
/*             margin: 0 auto;*/
/*             padding: 0;*/
/*             line-height: normal;            */
/*         }*/
/*     </style>  */
/* </head>*/
/* <body>*/
/*     <div>        */
/*         <div class="row">*/
/*             <table border="0">*/
/*                <tr>*/
/*                  <td>*/
/*                    <div class="gallery">*/
/*                        <div>*/
/*                            <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ product.s_directory }}/{{ product.item }}.jpg">*/
/*                        </div>            */
/*                        {% for img in gallery %}*/
/*                        <div>*/
/*                             <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ product.s_directory }}/{{ img.name }}">*/
/*                        </div>*/
/*                        {% endfor %}*/
/*                    </div>*/
/*                  </td>                         */
/*                  <td>*/
/*                    <div class="summary">*/
/*                        <div>                                   */
/*                            <h1 class="product-name">{{ product.name }}</h1>*/
/*                            <h2 class="product-title">{{ product.header }}</h2>*/
/*                            <h3 class="product-score">Calificacion: {{ product.score }}</h3>*/
/*                            <p class="product-price">${{ product.price_tt }} {{ product.price_currency }}</p>*/
/*                            <p class="product-description">{{ product.description }}</p>                                                  */
/*                            <p>*/
/*                                <strong>Cantidad minima de pedido:</strong> {{ product.minimumOrder }}<br>*/
/*                                <strong>Item:</strong> {{ product.item }}<br>*/
/*                                <strong>Supplier ID: </strong> {{ product.s_custome }}<br>*/
/*                                <strong>Supplier Name:</strong> <a href="{{ base_url() }}dashboard/admin/supplier/products/{{ product.s_id }}"> {{ product.s_name }} </a><br>*/
/*                                <strong>Calidad:</strong> {{ product.quality }}<br>*/
/*                                <strong>Origen:</strong> {{ product.origin2 }}<br>*/
/*                                <strong>Procedencia:</strong> {{ product.origin }}<br>*/
/*                                <strong>Presentación:</strong> {{ product.presentation }}*/
/*                            </p>*/
/*                            <br>*/
/*                                <h4>COLORES</h4>*/
/*                                {% for color in colors %}          */
/*                                    <div style="background-color: {{ color }}; padding: 10px;"></div>*/
/*                                {% endfor %}                           */
/*                             <br>*/
/*                             <h4>CARACTERISTICAS</h4>*/
/*                                <p>*/
/*                                   {% if product.characteristics is empty %}*/
/*                                        <span class="text-muted">No hay una descripción</span>*/
/*                                   {% else %}*/
/*                                       {{ product.characteristics }}*/
/*                                   {% endif %}*/
/*                                </p>                                       */
/*                            <h4>ESPECIFICACIONES</h4>*/
/*                            <p>*/
/*                                {{ product.especifications }}<br><br>*/
/*                                <strong>Dimensiones:</strong> {{ product.dimensions }}<br>*/
/*                                <strong>Peso:</strong> {{ product.weight }}<br><br>                                */
/*                            </p>                           */
/*                            <h4>EMPAQUE</h4>*/
/*                            <p>*/
/*                                <strong>No. piezas:</strong> {{ product.packing_numPieces }}<br>*/
/*                                <strong>Especificaciones: </strong> {{ product.packing_especifications }}<br>*/
/*                                <strong>Alto:</strong> {{ product.packing_height }}cm<br>*/
/*                                <strong>Ancho:</strong> {{ product.packing_width }}cm<br>*/
/*                                <strong>Largo:</strong> {{ product.packing_large }}cm<br>*/
/*                                <strong>Peso:</strong> {{ product.packing_weight }}kg*/
/*                            </p>                           */
/*                            <h4>LOGISTICA</h4>*/
/*                            <p>*/
/*                                <strong>Tiempo de producción:</strong> {{ product.logistics_productionTime }}<br>*/
/*                                <strong>FOB:</strong> {{ product.logistics_fob }}<br>*/
/*                                <strong>Tiempo de entrega FOB:</strong> {{ product.logistics_fobDeliveryTime }}<br>*/
/*                                <strong>Muestra fisica:</strong> {{ product.sample }}<br>*/
/*                            </p>                           */
/*                            <h4>PRECIO</h4>*/
/*                            <p>*/
/*                                <strong>Precio s/IVA:</strong> ${{ product.price_withoutTax}}<br>*/
/*                                <strong>Moneda:</strong> {{ product.price_currency }}<br>*/
/*                                <strong>Precio sugerido:</strong> ${{ product.price_suggested }}<br>*/
/*                                <strong>Precio TT:</strong> ${{ product.price_tt }}<br>*/
/*                                {% set utilidad = product.price_tt - product.price_withoutTax %}*/
/*                                <strong>Utilidad:</strong> ${{ utilidad }}*/
/*                            </p>                                   */
/*                        </div> */
/*                    </div>*/
/*                  </td>          */
/*                </tr>        */
/*             </table>*/
/*         </div>*/
/*     </div>    */
/* </body>*/
/* </html>*/
