<?php

/* dashboard/admin/catalog.html */
class __TwigTemplate_d9a8103187c446ccdb9e861d5162735f111fcfdcc8029d7897b30b9d91896f43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/catalog.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div  class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-12 margin-bottom--30\">
            <h1 class=\"title-dashboard text-center\">
                CATÁLOGO
            </h1>
            <h3 class=\"text-center\">
                ";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["products_count"]) ? $context["products_count"] : null), "html", null, true);
        echo "
                ";
        // line 20
        if (((isset($context["products_count"]) ? $context["products_count"] : null) > 1)) {
            // line 21
            echo "                    Productos
                ";
        } else {
            // line 23
            echo "                    Producto
                ";
        }
        // line 25
        echo "            </h3>
        </div>
        <div class=\"col-sm-12 text-right\">
            <div class=\"row margin-bottom--30\">
                <div class=\"col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4\">
\t                <form action=\"";
        // line 30
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search\" method=\"get\" accept-charset=\"utf-8\">\t
\t                    <div class=\"input-group\">
\t                    \t<input name=\"q\" type=\"text\" class=\"form-control\" placeholder=\"Buscar todo\">
\t                    \t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t</span>
\t\t                </div>
\t                </form>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                    <select name=\"category\" id=\"category\" class=\"form-control\">
                        ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 46
            echo "                            <option data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "slug", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "                    </select>
                </div>
                <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                    <select name=\"subcategory\" id=\"subcategory\" class=\"form-control\">
                        <option value=\"\">Subcategoría</option>\t\t\t\t\t
                    </select>
                </div>
                <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                    <select name=\"quality\" id=\"quality\" class=\"form-control\">
                        <option value=\"\">Calidad</option>
                        ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["qualities"]) ? $context["qualities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["quality"]) {
            // line 59
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["quality"], "slug", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["quality"], "name", array()), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quality'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                    </select>
                </div>
                <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                    <select name=\"\" id=\"other\" class=\"form-control\">
                        <option value=\"\">Otro</option>
                        <option value=\"valor\">Valor</option>
                        <option value=\"popularidad\">Popularidad</option>
                        <option value=\"creadoen\">Fecha de admisión</option>
                        <option value=\"entregado\">Entregados</option>
                        <option value=\"menosvalor\">Menos Valor</option>
                        <option value=\"popular\">Menos Popular</option>
                        <option value=\"costo\">Costo</option>
                    </select>
                </div>
            </div>
        </div>
        <div class=\"col-sm-12\">
            <p class=\"text-left\">
                <a href=\"";
        // line 79
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/add\" class=\"btn btn-info\">
                    <i class=\"fa fa-plus-circle margin-right--5\"></i>
                    Agregar
                </a>
            </p>
        </div>
\t\t<div class=\"col-lg-12\">
\t\t\t<p class=\"text-left\">
                <button class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#add-cat\">
                    <i class=\"fa fa-plus-circle margin-right--5\"></i>
                    Agregar categoria/subcategoria
                </button>\t\t\t
\t\t\t</p>
\t\t</div>
\t\t<div class=\"modal fade\" id=\"add-cat\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
\t\t  <div class=\"modal-dialog modal-lg\">
\t\t\t<div class=\"modal-content\">
\t\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-8 col-lg-offset-2\">
\t\t\t\t\t  <!-- Nav tabs -->
\t\t\t\t\t  <ul class=\"nav nav-tabs modal-add text-center margin-top--50\" role=\"tablist\">
\t\t\t\t\t\t<li role=\"presentation\" class=\"active\">
\t\t\t\t\t\t\t<a href=\"#categoryAdd\" aria-controls=\"categoryAdd\" role=\"tab\" data-toggle=\"tab\">Agregar categoria</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li role=\"presentation\">
\t\t\t\t\t\t\t<a href=\"#subcategoryAdd\" aria-controls=\"subcategoryAdd\" role=\"tab\" data-toggle=\"tab\">Agregar subcategoria</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t  </ul>

\t\t\t\t\t  <!-- Tab panes -->
\t\t\t\t\t  <div class=\"tab-content\">
\t\t\t\t\t\t<div role=\"tabpanel\" class=\"tab-pane active\" id=\"categoryAdd\">
\t\t\t\t\t\t\t<div class=\"row margin-top--20\">

\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">

\t\t\t\t\t\t\t\t\t<h1 class=\"title-dashboard\">Agregar Categoría</h1>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">

\t\t\t\t\t\t\t\t\t<div class=\"panel panel-default\">

\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">

\t\t\t\t\t\t\t\t\t\t\t<form action=\"";
        // line 124
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/add\" method=\"post\" id=\"addcategories\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">

\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"name\">Nombre</label>

\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"name\" name=\"name\" type=\"text\" required class=\"form-control\">

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"slug\">Filtro</label>

\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"slug\" name=\"slug\" type=\"text\" required class=\"form-control\">

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-info\">Agregar</button>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t</form>

\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">

\t\t\t\t\t\t\t\t\t<div class=\"panel panel-default\">

\t\t\t\t\t\t\t\t\t\t";
        // line 159
        if (((isset($context["categories_count"]) ? $context["categories_count"] : null) > 0)) {
            // line 160
            echo "
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-hover table-category\">

\t\t\t\t\t\t\t\t\t\t\t<tr>

\t\t\t\t\t\t\t\t\t\t\t\t<th>Nombre</th>

\t\t\t\t\t\t\t\t\t\t\t\t<th>Filtro</th>

\t\t\t\t\t\t\t\t\t\t\t\t<th>Acciones</th>

\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t";
            // line 173
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
                // line 174
                echo "
\t\t\t\t\t\t\t\t\t\t\t<tr class=\"btn-tr\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"form-group\"><input class=\"cat-name\" data-id=\"";
                // line 177
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\" class=\"form-control\" disabled data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "name", array()), "html", null, true);
                echo "\" type=\"text\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "name", array()), "html", null, true);
                echo "\"></td>

\t\t\t\t\t\t\t\t\t\t\t\t<td><input class=\"cat-slug\" data-id=\"";
                // line 179
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\" class=\"form-control\" disabled data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "slug", array()), "html", null, true);
                echo "\" type=\"text\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "slug", array()), "html", null, true);
                echo "\"></td>

\t\t\t\t\t\t\t\t\t\t\t\t<td>

\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-info edit-category\" data-id=\"";
                // line 183
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\tEditar

\t\t\t\t\t\t\t\t\t\t\t\t\t</button>

\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-danger del-category\" data-id=\"";
                // line 189
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\tEliminar

\t\t\t\t\t\t\t\t\t\t\t\t\t</button>\t\t\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 200
            echo "
\t\t\t\t\t\t\t\t\t\t</table>

\t\t\t\t\t\t\t\t\t\t";
        } else {
            // line 204
            echo "
\t\t\t\t\t\t\t\t\t\t\t<p class=\"h3 margin-bottom--20 text-muted text-center\">No hay categorias registradas</p>

\t\t\t\t\t\t\t\t\t\t";
        }
        // line 208
        echo "
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div role=\"tabpanel\" class=\"tab-pane\" id=\"subcategoryAdd\">
\t\t\t\t\t\t\t<div class=\"row margin-top--20\">

\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">

\t\t\t\t\t\t\t\t\t<h1 class=\"title-dashboard\">Agregar Subcategoría</h1>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">

\t\t\t\t\t\t\t\t\t<div class=\"panel panel-default\">

\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">

\t\t\t\t\t\t\t\t\t\t\t<form action=\"";
        // line 229
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/add_subcategory\" id=\"addsubcategories\" method=\"post\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">

\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"category\">Categoría</label>

\t\t\t\t\t\t\t\t\t\t\t\t\t<select name=\"category\" id=\"category-list\" class=\"form-control\" required>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\" disabled>Seleccionar una opción</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 239
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 240
            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            // line 241
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 244
        echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>

\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"name\">Nombre</label>

\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"name-sub\" type=\"text\" class=\"form-control\" required>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"slug\">Filtro</label>

\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"slug-sub\" type=\"text\" class=\"form-control\" required>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\">Agregar</button>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t</form>

\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">

\t\t\t\t\t\t\t\t\t<div class=\"panel panel-default\">

\t\t\t\t\t\t\t\t\t\t";
        // line 282
        if (((isset($context["subcategories_count"]) ? $context["subcategories_count"] : null) > 0)) {
            // line 283
            echo "
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-hover table-subcategory\">

\t\t\t\t\t\t\t\t\t\t\t<tr>

\t\t\t\t\t\t\t\t\t\t\t\t<th>Nombre</th>

\t\t\t\t\t\t\t\t\t\t\t\t<th>Filtro</th>

\t\t\t\t\t\t\t\t\t\t\t\t<th>Categoría</th>

\t\t\t\t\t\t\t\t\t\t\t\t<th>Acciones</th>

\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t";
            // line 298
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["subcategories"]) ? $context["subcategories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["subcategory"]) {
                // line 299
                echo "
\t\t\t\t\t\t\t\t\t\t\t<tr class=\"btn-tr\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"subcat-name\" data-id=\"";
                // line 303
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" class=\"form-control\" disabled data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sname", array()), "html", null, true);
                echo "\" type=\"text\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sname", array()), "html", null, true);
                echo "\">\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"subcat-slug\" data-id=\"";
                // line 307
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" class=\"form-control\" disabled data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sslug", array()), "html", null, true);
                echo "\" type=\"text\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sslug", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select  data-id=\"";
                // line 311
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" disabled class=\"form-control subcat-main\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\">Seleccionar una opción</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 315
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 316
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 317
                    if (($this->getAttribute($context["subcategory"], "cid", array()) == $this->getAttribute($context["category"], "idCategory", array()))) {
                        // line 318
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option selected value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "cid", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "cname", array()), "html", null, true);
                        echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 321
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                        echo "</option>\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 324
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 326
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>\t\t\t\t\t\t\t\t\t\t\t\t
<!--<input class=\"subcat-main\"  class=\"form-control\" disabled data-value=\"";
                // line 328
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "cid", array()), "html", null, true);
                echo "\"   type=\"text\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "cname", array()), "html", null, true);
                echo "\">-->
\t\t\t\t\t\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t\t\t\t\t\t<td>

\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-info edit-subcategory\" data-id=\"";
                // line 333
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\tEditar

\t\t\t\t\t\t\t\t\t\t\t\t\t</button>

\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<button class=\"btn btn-danger\" data-toggle=\"modal\" data-id=\"";
                // line 339
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" data-target=\"#delete\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\tEliminar

\t\t\t\t\t\t\t\t\t\t\t\t\t</button>-->
\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-danger del-subcategory\" data-id=\"";
                // line 344
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\tEliminar

\t\t\t\t\t\t\t\t\t\t\t\t\t</button>\t\t\t\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 355
            echo "
\t\t\t\t\t\t\t\t\t\t</table>

\t\t\t\t\t\t\t\t\t\t";
        } else {
            // line 359
            echo "
\t\t\t\t\t\t\t\t\t\t<p class=\"h3 margin-bottom--20 text-muted text-center\">No hay subcategorias registradas</p>

\t\t\t\t\t\t\t\t\t\t";
        }
        // line 363
        echo "
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>\t\t\t\t
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t  </div>
\t\t</div>
\t\t<!---- area modal ---->
\t\t<div class=\"modal fade\" id=\"delete\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalDelete\">

\t\t\t<div class=\"modal-dialog modal-sm\" role=\"document\">

\t\t\t\t<div class=\"modal-content\">

\t\t\t\t\t<div class=\"modal-body\">

\t\t\t\t\t\t<h3 class=\"modal-title\">Confrimación</h3>

\t\t\t\t\t\t<p>Confirma que deseas eliminar la categoría <span id=\"catName\"></span></p>

\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"modal-footer\">

\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>

\t\t\t\t\t\t<a id=\"deleteUrl\" href=\"\" class=\"btn btn-danger\">Confirmar</a>

\t\t\t\t\t</div>

\t\t\t\t</div>

\t\t\t</div>

\t\t</div>\t\t
\t\t<!---- end modal  ---->
        <div class=\"col-sm-12\">
            <div class=\"row\">

                ";
        // line 408
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 409
            echo "                <div class=\"col-sm-12 col-lg-4\">
                    <div class=\"row\">
                        <!--
                        <div class=\"col-sm-6\" style=\"background: url(";
            // line 412
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo ".jpg) no-repeat center; background-size: cover\"></div>
                        -->
                        <div class=\"col-sm-12 text-center\">
                            <div class=\"panel panel-default\">
                                <a href=\"";
            // line 416
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "dashboard/admin/catalog/product/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "idProduct", array()), "html", null, true);
            echo "\" class=\"decoration-none\">
                                <div class=\"panel-heading panel-main\" style=\"background: url(";
            // line 417
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo ".jpg) no-repeat center;background-size: cover;\">
                                    <div class=\"panel-main-shadow\">

                                    </div>
                                    <div style=\"position: relative\">
                                        <p class=\"h3 text-uppercase color-white\">";
            // line 422
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</p>
                                        <p class=\"color-white\">";
            // line 423
            echo twig_escape_filter($this->env, character_limiter($this->getAttribute($context["product"], "description", array()), 100));
            echo "...</p>
                                    </div>
                                </div>
                                </a>
                                <div class=\"panel-body panel-product\">
                                    <p class=\"h2 margin-clear\">";
            // line 428
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "score", array()), "html", null, true);
            echo "</p>
                                    <hr>
                                    <div class=\"row margin-bottom--20\">
                                     \t<a href=\"";
            // line 431
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "dashboard/admin/catalog/product/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "idProduct", array()), "html", null, true);
            echo "\" class=\"decoration-none\">
                                        <div class=\"col-sm-12 col-md-6\">
                                            <b>ITEM</b><br>
                                            ";
            // line 434
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo "
                                        </div>
                                        </a>
                                        <a href=\"";
            // line 437
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "/tamev2/dashboard/admin/supplier/products/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\" class=\"decoration-none\">
                                        <div class=\"col-sm-12 col-md-6\">
                                            <b>SUPPLIER</b><br>
                                            ";
            // line 440
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "supplier_customeid", array()), "html", null, true);
            echo "
                                        </div>
                                        </a>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-sm-12 col-md-6\">
                                            <i class=\"fa fa-thumbs-up fa-lg\"></i> 42
                                        </div>
                                        <div class=\"col-sm-12 col-md-6\">
                                            <i class=\"fa fa-truck fa-lg\"></i> 8
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </a>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 459
        echo "            </div>
        </div>
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-4 col-lg-offset-4 text-center\" style=\"padding-top:20px;padding-bottom:20px;\">
\t\t\t\t\t<a href=\"";
        // line 464
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog?page=";
        echo twig_escape_filter($this->env, (isset($context["contador"]) ? $context["contador"] : null), "html", null, true);
        echo "\" class=\"btn btn-success\">Ver más productos</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
    </div>
</div>
";
    }

    // line 471
    public function block_script($context, array $blocks = array())
    {
        // line 472
        echo "<script>
\t
\tvar category = \"";
        // line 474
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
        echo "\";
\tvar subcategory = \"";
        // line 475
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\";
\tvar quality = \"";
        // line 476
        echo twig_escape_filter($this->env, (isset($context["quality"]) ? $context["quality"] : null), "html", null, true);
        echo "\";
\t
\t\$(\"#category\").val(category);
\t\$(\"#subcategory\").val(subcategory);
\t\$(\"#quality\").val(quality);
\t
\tvar subcategoryOptions = \$(\"#subcategory option\").length;
\t
\tif (category.length > 0) {
\t\t\tgetSubcategories();
\t}
\t
    \$(\"#category\").on(\"change\", function() {

\t\tvar category = \$(\"#category option:selected\");
\t\tlocation.href = \"";
        // line 491
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category.val();
    });
    
    function getSubcategories() {
\t    var category = \$(\"#category option:selected\");

        \$.get(\"/tamev2/dashboard/admin/catalog/subcategory/\" + category.data(\"id\"), function(data) {

            var options = '<option value=\"\">Subcategoría</option>';
            \$.each( data, function( key, value ) {
                options += '<option value=\"' + value.sslug + '\"> ' + value.sname + '</option>';
            });

            \$(\"#subcategory\").html(options)
            \$(\"#subcategory\").val(\"";
        // line 505
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\");
        });
    }
    
    \$(\"#subcategory\").on(\"change\", function() {

\t\tvar category = \$(\"#category option:selected\").val();
\t\tvar subcategory = \$(\"#subcategory option:selected\");
\t\tvar quality= \"";
        // line 513
        echo twig_escape_filter($this->env, (isset($context["quality"]) ? $context["quality"] : null), "html", null, true);
        echo "\";

\t\tif (quality.length > 0) {
\t\t\tlocation.href = \"";
        // line 516
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&subcategory=\" + subcategory.val() + \"&quality=\" + quality;
\t\t} else {
\t\t\tlocation.href = \"";
        // line 518
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&subcategory=\" + subcategory.val();
\t\t}
    });
    
    \$(\"#quality\").on(\"change\", function() {

\t\tvar category = \"";
        // line 524
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
        echo "\";
\t\tvar subcategory = \"";
        // line 525
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\";
\t\tvar quality= \$(\"#quality option:selected\");

\t\tif (category.length > 0) {
\t\t\tif (subcategory.length > 0) {
\t    \t\tlocation.href = \"";
        // line 530
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&subcategory=\" + subcategory + \"&quality=\" + quality.val(); 
\t    \t} else {
        \t\tlocation.href = \"";
        // line 532
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&quality=\" + quality.val();
        \t}
        } else {
\t        location.href = \"";
        // line 535
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?quality=\" + quality.val();
        }
    });
\t
\t\$('body').on('click','.del-category',function(){
\tvar  button = \$(this);
\t var id     = button.data('id');
\t\tswal({
\t\t  title: '¿Deseas eliminar esta categoría?',
\t\t  text: \"\",
\t\t  type: 'warning',
\t\t  showCancelButton: true,
\t\t  confirmButtonColor: '#3085d6',
\t\t  cancelButtonColor: '#d33',
\t\t  confirmButtonText: 'Eliminar!'
\t\t}).then(function() {
\t\t
\t\t\t\$.get(\"";
        // line 552
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/delete/\" + id, function(data) {

\t\t\t\tif(data==1){
\t\t\t\tbutton.parent().parent().remove();
\t\t\t\t}
\t\t\t\telse{
\t\t\t\t\tswal(
\t\t\t\t\t  'Lo sentimos algo fallo?',
\t\t\t\t\t  'Por favor contacta al administrador o intenta mas tarde.',
\t\t\t\t\t  'error'
\t\t\t\t\t)\t\t\t\t
\t\t\t\t}
\t\t\t});\t\t
\t\t\t
\t\t  swal(
\t\t\t'Eliminado!',
\t\t\t'Este cambio no es reversible',
\t\t\t'success'
\t\t  )
\t\t})\t\t
\t});
\t
\t\$('body').on('click','.del-subcategory',function(){
\tvar  button = \$(this);
\t var id     = button.data('id');
\t\tswal({
\t\t  title: '¿Deseas eliminar esta subcategoría?',
\t\t  text: \"\",
\t\t  type: 'warning',
\t\t  showCancelButton: true,
\t\t  confirmButtonColor: '#3085d6',
\t\t  cancelButtonColor: '#d33',
\t\t  confirmButtonText: 'Eliminar!'
\t\t}).then(function() {
\t\t
\t\t\t\$.get(\"";
        // line 587
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/delete_subcategory/\" + id, function(data) {
\t\t\t\t
\t\t\t\tif(data==1){
\t\t\t\tbutton.parent().parent().remove();
\t\t\t\t}
\t\t\t\telse{
\t\t\t\t\tswal(
\t\t\t\t\t  'Lo sentimos algo fallo?',
\t\t\t\t\t  'Por favor contacta al administrador o intenta mas tarde.',
\t\t\t\t\t  'error'
\t\t\t\t\t)\t\t\t\t
\t\t\t\t}
\t\t\t});\t\t
\t\t\t
\t\t  swal(
\t\t\t'Eliminado!',
\t\t\t'Este cambio no es reversible',
\t\t\t'success'
\t\t  )
\t\t})\t
\t});
\t
\tstatus = 0;
\t\$('body').on('click','.edit-category',function(){
\t\tvar button = \$(this);
\t\tif( status%2 == 0 ){
\t\t\tedit_label = button.text('Guardar');
\t\t\tbutton.parent().parent().find('.cat-name').prop('disabled',false);
\t\t\tbutton.parent().parent().find('.cat-slug').prop('disabled',false);\t\t\t
\t\t\t
\t\t}
\t\telse{

\t\t\tvar name_id = button.parent().parent().find('.cat-name').data('id');
\t\t\tvar slug_id = button.parent().parent().find('.cat-name').data('id');
\t\t\t
\t\t\tvar name = button.parent().parent().find('.cat-name').val();
\t\t\tvar slug = button.parent().parent().find('.cat-slug').val();
\t\t\t
\t\t\t\tdataInf = {
\t\t\t\t\t\t\tid_name :name_id, 
\t\t\t\t\t\t\tid_slug :slug_id,
\t\t\t\t\t\t\tname    :name,
\t\t\t\t\t\t\tslug    :slug
\t\t\t\t\t\t  };

\t\t\t\t\$.post(\"";
        // line 633
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/update/\",dataInf,function(data) {
\t\t\t\t\tif( data==1 ){
\t\t\t\t\t\tedit_label = button.text('Editar');\t
\t\t\t\t\t\tbutton.parent().parent().find('.cat-name').prop('disabled',true);
\t\t\t\t\t\tbutton.parent().parent().find('.cat-slug').prop('disabled',true);\t\t\t\t\t
\t\t\t\t\t\tswal(
\t\t\t\t\t\t  'Cambio exitoso',
\t\t\t\t\t\t  '',
\t\t\t\t\t\t  'success'
\t\t\t\t\t\t)\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\telse if( data == 2){
\t\t\t\t\t\tedit_label = button.text('Editar');\t
\t\t\t\t\t\tbutton.parent().parent().find('.cat-name').prop('disabled',true);
\t\t\t\t\t\tbutton.parent().parent().find('.cat-slug').prop('disabled',true);
\t\t\t\t\t}else{
\t\t\t\t\t\tswal(
\t\t\t\t\t\t  'Lo sentimos, intenta mas tarde.',
\t\t\t\t\t\t  '',
\t\t\t\t\t\t  'error'
\t\t\t\t\t\t)\t\t\t\t
\t\t\t\t\t}
\t\t\t\t});\t
\t\t}
\t\tstatus++;
\t});
\t
\t
\t\$('body').on('click','.edit-subcategory',function(){
\t\tvar button = \$(this);
\t\tif( status%2 == 0 ){
\t\t\tedit_label = button.text('Guardar');
\t\t\tbutton.parent().parent().find('.subcat-name').prop('disabled',false);
\t\t\tbutton.parent().parent().find('.subcat-slug').prop('disabled',false);\t
\t\t\tbutton.parent().parent().find('.subcat-main').prop('disabled',false);\t\t\t\t
\t\t\tconsole.log(\"no guardar\");
\t\t}
\t\telse{
\t\t\tconsole.log(\"guardar\");
\t\t\tvar name_sub_id = button.parent().parent().find('.subcat-name').data('id');
\t\t\tvar slug_sub_id   = button.parent().parent().find('.subcat-slug').data('id');
\t\t\tvar id_sub_id     = button.parent().parent().find('.subcat-main').data('id');\t\t\t
\t\t\t
\t\t\tvar name = button.parent().parent().find('.subcat-name').val();
\t\t\tvar slug = button.parent().parent().find('.subcat-slug').val();
\t\t\tvar id   = button.parent().parent().find('.subcat-main option:selected').val();
\t\t\t
\t\t\t\tdataInf = {
\t\t\t\t\t\t\tindice  : id_sub_id,
\t\t\t\t\t\t\tid_name : name_sub_id, 
\t\t\t\t\t\t\tid_slug : slug_sub_id,
\t\t\t\t\t\t\tid      : id,
\t\t\t\t\t\t\tname    :name,
\t\t\t\t\t\t\tslug    :slug
\t\t\t\t\t\t  };

\t\t\t\t\$.post(\"";
        // line 689
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/update_subcategory\",dataInf,function(data) {
\t\t\t\t\tif( data==1 ){
\t\t\t\t\t\tedit_label = button.text('Editar');\t
\t\t\t\t\t\tbutton.parent().parent().find('.subcat-name').prop('disabled',true);
\t\t\t\t\t\tbutton.parent().parent().find('.subcat-slug').prop('disabled',true);\t
\t\t\t\t\t\tbutton.parent().parent().find('.subcat-main').prop('disabled',true);\t\t\t\t\t\t\t
\t\t\t\t\t\tswal(
\t\t\t\t\t\t  'Cambio exitoso',
\t\t\t\t\t\t  '',
\t\t\t\t\t\t  'success'
\t\t\t\t\t\t)\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\telse if( data == 2){
\t\t\t\t\t\tedit_label = button.text('Editar');\t
\t\t\t\t\t\tbutton.parent().parent().find('.subcat-name').prop('disabled',true);
\t\t\t\t\t\tbutton.parent().parent().find('.subcat-slug').prop('disabled',true);
\t\t\t\t\t\tbutton.parent().parent().find('.subcat-main').prop('disabled',true);
\t\t\t\t\t}else{
\t\t\t\t\t\tswal(
\t\t\t\t\t\t  'Lo sentimos, intenta mas tarde.',
\t\t\t\t\t\t  '',
\t\t\t\t\t\t  'error'
\t\t\t\t\t\t)\t\t\t\t
\t\t\t\t\t}
\t\t\t\t});\t
\t\t}
\t\tstatus++;
\t});
\t
\t\$('#addcategories').on('submit',function(e){
\t\te.preventDefault();
\t\tform = \$(this);
\t\tserializ = form.serialize();
\t\t\$.post(\"";
        // line 722
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/add/\",form.serialize(),function(data) {
\t\t  var table = \$('.table-category>tbody tr:nth-child(1)');
\t\t  var name = \$('#addcategories :input[name=name]').val();
\t\t  var slug = \$('#addcategories :input[name=slug]').val();
\t\t  var th1 = \"<input class='cat-name' data-id='\"+data+\"' disabled data-value='\"+name+\"' type='text' value='\"+name+\"'>\";
\t\t  var th2 = \"<input class='cat-slug' data-id='\"+data+\"' disabled data-value='\"+slug+\"' type='text' value='\"+slug+\"'>\";
\t\t  var th3 = \"<button class='btn btn-info edit-category' data-id=\"+data+\">Editar</button>  <button class='btn btn-danger del-category' data-id=\"+data+\">Eliminar</button>\";
\t\t  \$('.table-category>tbody tr:nth-child(1)').after(\"<tr class='btn-tr'><td class='form-group'>\"+th1+\"</td><td>\"+th2+\"</td><td>\"+th3+\"</td></tr>\");
\t\t \$('#addcategories')[0].reset();
\t\tif( data != 0 ){
\t\t\tswal(
\t\t\t  'Se ha guardado tu categoria correctamente.',
\t\t\t  '',
\t\t\t  'success'
\t\t\t)\t\t
\t\t}else{
\t\t\tswal(
\t\t\t  'Lo sentimos, intenta mas tarde.',
\t\t\t  '',
\t\t\t  'error'
\t\t\t)\t\t
\t\t
\t\t}
\t\t});\t\t\t
\t\t
\t});
\t
\t\$('#addsubcategories').on('submit',function(e){
\t\te.preventDefault();
\t\tform = \$(this);
\t\t\$.post(form.attr('action'),form.serialize(),function(data) {
\t\t var table = \$('.table-subcategory>tbody tr:nth-child(1)');
\t\t  
\t\t  var name = \$('#addsubcategories :input[name=name-sub]').val();
\t\t  var slug = \$('#addsubcategories :input[name=slug-sub]').val();
\t\t  var cat_id = \$('#addsubcategories select option:selected').val();
\t\t  var cat_text = \$('#addsubcategories select option:selected').text();
\t\t  
\t\t  var texto_select = \$('#category-list option:selected').text();
\t\t  var id_select = \$('#category-list option:selected').val();\t\t  
\t\t  
\t\t  var th1 = \"<input class='subcat-name' data-id='\"+data+\"' disabled data-value='\"+name+\"' type='text' value='\"+name+\"'>\";
\t\t  var th2 = \"<input class='subcat-slug' data-id='\"+data+\"' disabled data-value='\"+slug+\"' type='text' value='\"+slug+\"'>\";
\t\t  var th3 = \"<select data-id='\"+data+\"' disabled class='form-control subcat-main'><option value='\"+id_select+\"'>\"+texto_select+\"</option></select>\";
\t\t\t\t  
\t\t  var th4 = \"<button class='btn btn-info edit-subcategory' data-id='\"+data+\"'>Editar</button> <button class='btn btn-danger del-subcategory' data-id='\"+data+\"'>Eliminar</button>\";
\t\t  \$('.table-subcategory>tbody tr:nth-child(1)').after(\"<tr class='btn-tr'><td>\"+th1+\"</td><td>\"+th2+\"</td><td>\"+th3+\"</td><td>\"+th4+\"</td></tr>\");
\t\t 

\t\t \$('#category-list').find('option').clone().appendTo('.table-subcategory tbody tr:nth-child(2) td:nth-child(3) select');
\t\t 
\t\tif( data != 0 ){
\t\t\tswal(
\t\t\t  'Se ha guardado tu subcategoria correctamente.',
\t\t\t  '',
\t\t\t  'success'
\t\t\t)\t\t
\t\t}else{
\t\t\tswal(
\t\t\t  'Lo sentimos, intenta mas tarde.',
\t\t\t  '',
\t\t\t  'error'
\t\t\t)\t\t
\t\t
\t\t}
\t\t});
\t\t
\t\t
\t});\t
    /*\$(\"#other\").on(\"change\", function() {

\t\tvar category = \"";
        // line 793
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
        echo "\";
\t\tvar subcategory = \"";
        // line 794
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\";
\t\tvar quality= \$(\"#quality option:selected\");
\t\tvar other= \$(\"#quality option:selected\");
\t\talert(\"En construcción\");
\t\tif (category.length > 0) {
\t\t\tif (subcategory.length > 0) {
\t    \t\tlocation.href = \"";
        // line 800
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&subcategory=\" + subcategory + \"&quality=\" + quality.val(); 
\t    \t} else {
        \t\tlocation.href = \"";
        // line 802
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&quality=\" + quality.val();
        \t}
        } else {
\t        location.href = \"";
        // line 805
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?quality=\" + quality.val();
        }
    });\t*/
    
</script>
<style>

</style>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/catalog.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1150 => 805,  1144 => 802,  1139 => 800,  1130 => 794,  1126 => 793,  1052 => 722,  1016 => 689,  957 => 633,  908 => 587,  870 => 552,  850 => 535,  844 => 532,  839 => 530,  831 => 525,  827 => 524,  818 => 518,  813 => 516,  807 => 513,  796 => 505,  779 => 491,  761 => 476,  757 => 475,  753 => 474,  749 => 472,  746 => 471,  733 => 464,  726 => 459,  701 => 440,  693 => 437,  687 => 434,  679 => 431,  673 => 428,  665 => 423,  661 => 422,  649 => 417,  643 => 416,  632 => 412,  627 => 409,  623 => 408,  576 => 363,  570 => 359,  564 => 355,  547 => 344,  539 => 339,  530 => 333,  520 => 328,  516 => 326,  509 => 324,  500 => 321,  491 => 318,  489 => 317,  486 => 316,  482 => 315,  475 => 311,  464 => 307,  453 => 303,  447 => 299,  443 => 298,  426 => 283,  424 => 282,  384 => 244,  373 => 241,  370 => 240,  366 => 239,  353 => 229,  330 => 208,  324 => 204,  318 => 200,  301 => 189,  292 => 183,  281 => 179,  272 => 177,  267 => 174,  263 => 173,  248 => 160,  246 => 159,  208 => 124,  160 => 79,  140 => 61,  129 => 59,  125 => 58,  113 => 48,  100 => 46,  96 => 45,  78 => 30,  71 => 25,  67 => 23,  63 => 21,  61 => 20,  57 => 19,  48 => 12,  45 => 11,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div  class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-12 margin-bottom--30">*/
/*             <h1 class="title-dashboard text-center">*/
/*                 CATÁLOGO*/
/*             </h1>*/
/*             <h3 class="text-center">*/
/*                 {{ products_count }}*/
/*                 {% if products_count > 1 %}*/
/*                     Productos*/
/*                 {% else %}*/
/*                     Producto*/
/*                 {% endif %}*/
/*             </h3>*/
/*         </div>*/
/*         <div class="col-sm-12 text-right">*/
/*             <div class="row margin-bottom--30">*/
/*                 <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">*/
/* 	                <form action="{{ base_url() }}dashboard/admin/catalog/search" method="get" accept-charset="utf-8">	*/
/* 	                    <div class="input-group">*/
/* 	                    	<input name="q" type="text" class="form-control" placeholder="Buscar todo">*/
/* 	                    	<span class="input-group-btn">*/
/* 								<button class="btn btn-default" type="submit">*/
/* 									<i class="fa fa-search"></i>*/
/* 								</button>*/
/* 							</span>*/
/* 		                </div>*/
/* 	                </form>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                     <select name="category" id="category" class="form-control">*/
/*                         {% for category in categories %}*/
/*                             <option data-id="{{ category.idCategory }}" value="{{ category.slug }}">{{ category.name }}</option>*/
/*                         {% endfor %}*/
/*                     </select>*/
/*                 </div>*/
/*                 <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                     <select name="subcategory" id="subcategory" class="form-control">*/
/*                         <option value="">Subcategoría</option>					*/
/*                     </select>*/
/*                 </div>*/
/*                 <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                     <select name="quality" id="quality" class="form-control">*/
/*                         <option value="">Calidad</option>*/
/*                         {% for quality in qualities %}*/
/*                             <option value="{{ quality.slug }}">{{ quality.name }}</option>*/
/*                         {% endfor %}*/
/*                     </select>*/
/*                 </div>*/
/*                 <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                     <select name="" id="other" class="form-control">*/
/*                         <option value="">Otro</option>*/
/*                         <option value="valor">Valor</option>*/
/*                         <option value="popularidad">Popularidad</option>*/
/*                         <option value="creadoen">Fecha de admisión</option>*/
/*                         <option value="entregado">Entregados</option>*/
/*                         <option value="menosvalor">Menos Valor</option>*/
/*                         <option value="popular">Menos Popular</option>*/
/*                         <option value="costo">Costo</option>*/
/*                     </select>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <p class="text-left">*/
/*                 <a href="{{ base_url() }}dashboard/admin/catalog/add" class="btn btn-info">*/
/*                     <i class="fa fa-plus-circle margin-right--5"></i>*/
/*                     Agregar*/
/*                 </a>*/
/*             </p>*/
/*         </div>*/
/* 		<div class="col-lg-12">*/
/* 			<p class="text-left">*/
/*                 <button class="btn btn-success" data-toggle="modal" data-target="#add-cat">*/
/*                     <i class="fa fa-plus-circle margin-right--5"></i>*/
/*                     Agregar categoria/subcategoria*/
/*                 </button>			*/
/* 			</p>*/
/* 		</div>*/
/* 		<div class="modal fade" id="add-cat" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">*/
/* 		  <div class="modal-dialog modal-lg">*/
/* 			<div class="modal-content">*/
/* 				<div class="row">*/
/* 				<div class="col-lg-8 col-lg-offset-2">*/
/* 					  <!-- Nav tabs -->*/
/* 					  <ul class="nav nav-tabs modal-add text-center margin-top--50" role="tablist">*/
/* 						<li role="presentation" class="active">*/
/* 							<a href="#categoryAdd" aria-controls="categoryAdd" role="tab" data-toggle="tab">Agregar categoria</a>*/
/* 						</li>*/
/* 						<li role="presentation">*/
/* 							<a href="#subcategoryAdd" aria-controls="subcategoryAdd" role="tab" data-toggle="tab">Agregar subcategoria</a>*/
/* 						</li>*/
/* 					  </ul>*/
/* */
/* 					  <!-- Tab panes -->*/
/* 					  <div class="tab-content">*/
/* 						<div role="tabpanel" class="tab-pane active" id="categoryAdd">*/
/* 							<div class="row margin-top--20">*/
/* */
/* 								<div class="col-sm-12 text-center">*/
/* */
/* 									<h1 class="title-dashboard">Agregar Categoría</h1>*/
/* */
/* 								</div>*/
/* 								<div class="col-sm-12">*/
/* */
/* 									<div class="panel panel-default">*/
/* */
/* 										<div class="panel-body">*/
/* */
/* 											<form action="{{ base_url() }}dashboard/admin/category/add" method="post" id="addcategories" accept-charset="utf-8" enctype="multipart/form-data">*/
/* */
/* 												<div class="form-group">*/
/* */
/* 													<label for="name">Nombre</label>*/
/* */
/* 													<input id="name" name="name" type="text" required class="form-control">*/
/* */
/* 												</div>*/
/* */
/* 												<div class="form-group">*/
/* */
/* 													<label for="slug">Filtro</label>*/
/* */
/* 													<input id="slug" name="slug" type="text" required class="form-control">*/
/* */
/* 												</div>*/
/* */
/* 												<div class="form-group">*/
/* */
/* 													<button class="btn btn-info">Agregar</button>*/
/* */
/* 												</div>*/
/* */
/* 											</form>*/
/* */
/* 										</div>*/
/* */
/* 									</div>*/
/* */
/* 								</div>	*/
/* 								<div class="col-sm-12">*/
/* */
/* 									<div class="panel panel-default">*/
/* */
/* 										{% if categories_count > 0 %}*/
/* */
/* 										<table class="table table-hover table-category">*/
/* */
/* 											<tr>*/
/* */
/* 												<th>Nombre</th>*/
/* */
/* 												<th>Filtro</th>*/
/* */
/* 												<th>Acciones</th>*/
/* */
/* 											</tr>*/
/* */
/* 											{% for categorie in categories %}*/
/* */
/* 											<tr class="btn-tr">*/
/* 												*/
/* 												<td class="form-group"><input class="cat-name" data-id="{{ categorie.idCategory }}" class="form-control" disabled data-value="{{ categorie.name }}" type="text" value="{{ categorie.name }}"></td>*/
/* */
/* 												<td><input class="cat-slug" data-id="{{ categorie.idCategory }}" class="form-control" disabled data-value="{{ categorie.slug }}" type="text" value="{{ categorie.slug }}"></td>*/
/* */
/* 												<td>*/
/* */
/* 													<button class="btn btn-info edit-category" data-id="{{ categorie.idCategory }}">*/
/* */
/* 														Editar*/
/* */
/* 													</button>*/
/* */
/* 													<button class="btn btn-danger del-category" data-id="{{ categorie.idCategory }}">*/
/* */
/* 														Eliminar*/
/* */
/* 													</button>													*/
/* */
/* 												</td>*/
/* */
/* 											</tr>*/
/* */
/* 											{% endfor %}*/
/* */
/* 										</table>*/
/* */
/* 										{% else %}*/
/* */
/* 											<p class="h3 margin-bottom--20 text-muted text-center">No hay categorias registradas</p>*/
/* */
/* 										{% endif %}*/
/* */
/* 									</div>*/
/* */
/* 								</div>								*/
/* 							</div>*/
/* 						</div>*/
/* 						<div role="tabpanel" class="tab-pane" id="subcategoryAdd">*/
/* 							<div class="row margin-top--20">*/
/* */
/* 								<div class="col-sm-12 text-center">*/
/* */
/* 									<h1 class="title-dashboard">Agregar Subcategoría</h1>*/
/* */
/* 								</div>*/
/* 								*/
/* 								<div class="col-sm-12">*/
/* */
/* 									<div class="panel panel-default">*/
/* */
/* 										<div class="panel-body">*/
/* */
/* 											<form action="{{ base_url() }}dashboard/admin/category/add_subcategory" id="addsubcategories" method="post" accept-charset="utf-8" enctype="multipart/form-data">*/
/* */
/* 												<div class="form-group">*/
/* */
/* 													<label for="category">Categoría</label>*/
/* */
/* 													<select name="category" id="category-list" class="form-control" required>*/
/* */
/* 														<option value="0" disabled>Seleccionar una opción</option>*/
/* */
/* 														{% for category in categories %}*/
/* */
/* 														<option value="{{ category.idCategory }}">{{ category.name }}</option>*/
/* */
/* 														{% endfor %}*/
/* */
/* 													</select>*/
/* */
/* 												</div>*/
/* 												*/
/* 												<div class="form-group">*/
/* */
/* 													<label for="name">Nombre</label>*/
/* */
/* 													<input name="name-sub" type="text" class="form-control" required>*/
/* */
/* 												</div>*/
/* */
/* 												<div class="form-group">*/
/* */
/* 													<label for="slug">Filtro</label>*/
/* */
/* 													<input name="slug-sub" type="text" class="form-control" required>*/
/* */
/* 												</div>*/
/* */
/* 												<div class="form-group">*/
/* */
/* 													<button type="submit" class="btn btn-info">Agregar</button>*/
/* */
/* 												</div>*/
/* */
/* 											</form>*/
/* */
/* 										</div>*/
/* */
/* 									</div>*/
/* */
/* 								</div>*/
/* 								<div class="col-sm-12">*/
/* */
/* 									<div class="panel panel-default">*/
/* */
/* 										{% if subcategories_count > 0 %}*/
/* */
/* 										<table class="table table-hover table-subcategory">*/
/* */
/* 											<tr>*/
/* */
/* 												<th>Nombre</th>*/
/* */
/* 												<th>Filtro</th>*/
/* */
/* 												<th>Categoría</th>*/
/* */
/* 												<th>Acciones</th>*/
/* */
/* 											</tr>*/
/* */
/* 											{% for subcategory in subcategories %}*/
/* */
/* 											<tr class="btn-tr">*/
/* 												*/
/* 												<td>*/
/* 													<input class="subcat-name" data-id="{{ subcategory.sid }}" class="form-control" disabled data-value="{{ subcategory.sname }}" type="text" value="{{ subcategory.sname }}">												*/
/* 												</td>*/
/* */
/* 												<td>*/
/* 													<input class="subcat-slug" data-id="{{ subcategory.sid }}" class="form-control" disabled data-value="{{ subcategory.sslug }}" type="text" value="{{ subcategory.sslug }}">*/
/* 												</td>*/
/* */
/* 												<td>*/
/* 													<select  data-id="{{ subcategory.sid }}" disabled class="form-control subcat-main">*/
/* */
/* 														<option value="0">Seleccionar una opción</option>*/
/* */
/* 														{% for category in categories %}*/
/* */
/* 															{% if subcategory.cid == category.idCategory %}*/
/* 																<option selected value="{{ subcategory.cid }}">{{ subcategory.cname }}</option>*/
/* 																*/
/* 															{% else %}*/
/* 																<option value="{{ category.idCategory }}">{{ category.name }}</option>														*/
/* 																*/
/* 															{% endif %}*/
/* 																*/
/* 														{% endfor %}*/
/* */
/* 													</select>												*/
/* <!--<input class="subcat-main"  class="form-control" disabled data-value="{{ subcategory.cid }}"   type="text" value="{{ subcategory.cname }}">-->*/
/* 												</td>*/
/* */
/* 												<td>*/
/* */
/* 													<button class="btn btn-info edit-subcategory" data-id="{{ subcategory.sid }}">*/
/* */
/* 														Editar*/
/* */
/* 													</button>*/
/* */
/* 													<!--<button class="btn btn-danger" data-toggle="modal" data-id="{{ subcategory.sid }}" data-target="#delete">*/
/* */
/* 														Eliminar*/
/* */
/* 													</button>-->*/
/* 													<button class="btn btn-danger del-subcategory" data-id="{{ subcategory.sid }}">*/
/* */
/* 														Eliminar*/
/* */
/* 													</button>														*/
/* */
/* 												</td>*/
/* */
/* 											</tr>*/
/* */
/* 											{% endfor %}*/
/* */
/* 										</table>*/
/* */
/* 										{% else %}*/
/* */
/* 										<p class="h3 margin-bottom--20 text-muted text-center">No hay subcategorias registradas</p>*/
/* */
/* 										{% endif %}*/
/* */
/* 									</div>*/
/* */
/* 								</div>								*/
/* 							</div>*/
/* 						</div>*/
/* 					  </div>				*/
/* 				</div>*/
/* 				</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		  </div>*/
/* 		</div>*/
/* 		<!---- area modal ---->*/
/* 		<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="modalDelete">*/
/* */
/* 			<div class="modal-dialog modal-sm" role="document">*/
/* */
/* 				<div class="modal-content">*/
/* */
/* 					<div class="modal-body">*/
/* */
/* 						<h3 class="modal-title">Confrimación</h3>*/
/* */
/* 						<p>Confirma que deseas eliminar la categoría <span id="catName"></span></p>*/
/* */
/* 					</div>*/
/* */
/* 					<div class="modal-footer">*/
/* */
/* 						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>*/
/* */
/* 						<a id="deleteUrl" href="" class="btn btn-danger">Confirmar</a>*/
/* */
/* 					</div>*/
/* */
/* 				</div>*/
/* */
/* 			</div>*/
/* */
/* 		</div>		*/
/* 		<!---- end modal  ---->*/
/*         <div class="col-sm-12">*/
/*             <div class="row">*/
/* */
/*                 {% for product in products %}*/
/*                 <div class="col-sm-12 col-lg-4">*/
/*                     <div class="row">*/
/*                         <!--*/
/*                         <div class="col-sm-6" style="background: url({{ base_url() }}public/uploads/supplier/catalog/{{ product.directory }}/{{ product.item }}.jpg) no-repeat center; background-size: cover"></div>*/
/*                         -->*/
/*                         <div class="col-sm-12 text-center">*/
/*                             <div class="panel panel-default">*/
/*                                 <a href="{{ base_url() }}dashboard/admin/catalog/product/{{ product.idProduct }}" class="decoration-none">*/
/*                                 <div class="panel-heading panel-main" style="background: url({{ base_url() }}public/uploads/supplier/catalog/{{ product.directory }}/{{ product.item }}.jpg) no-repeat center;background-size: cover;">*/
/*                                     <div class="panel-main-shadow">*/
/* */
/*                                     </div>*/
/*                                     <div style="position: relative">*/
/*                                         <p class="h3 text-uppercase color-white">{{ product.name }}</p>*/
/*                                         <p class="color-white">{{ character_limiter(product.description, 100)|e }}...</p>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 </a>*/
/*                                 <div class="panel-body panel-product">*/
/*                                     <p class="h2 margin-clear">{{ product.score }}</p>*/
/*                                     <hr>*/
/*                                     <div class="row margin-bottom--20">*/
/*                                      	<a href="{{ base_url() }}dashboard/admin/catalog/product/{{ product.idProduct }}" class="decoration-none">*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <b>ITEM</b><br>*/
/*                                             {{ product.item }}*/
/*                                         </div>*/
/*                                         </a>*/
/*                                         <a href="{{ base_url }}/tamev2/dashboard/admin/supplier/products/{{ product.id }}" class="decoration-none">*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <b>SUPPLIER</b><br>*/
/*                                             {{ product.supplier_customeid }}*/
/*                                         </div>*/
/*                                         </a>*/
/*                                     </div>*/
/*                                     <div class="row">*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <i class="fa fa-thumbs-up fa-lg"></i> 42*/
/*                                         </div>*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <i class="fa fa-truck fa-lg"></i> 8*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 </a>*/
/*                 {% endfor %}*/
/*             </div>*/
/*         </div>*/
/* 		<div class="col-lg-12">*/
/* 			<div class="row">*/
/* 				<div class="col-lg-4 col-lg-offset-4 text-center" style="padding-top:20px;padding-bottom:20px;">*/
/* 					<a href="{{ base_url() }}dashboard/admin/catalog?page={{ contador }}" class="btn btn-success">Ver más productos</a>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/* 	*/
/* 	var category = "{{ category }}";*/
/* 	var subcategory = "{{ subcategory }}";*/
/* 	var quality = "{{ quality }}";*/
/* 	*/
/* 	$("#category").val(category);*/
/* 	$("#subcategory").val(subcategory);*/
/* 	$("#quality").val(quality);*/
/* 	*/
/* 	var subcategoryOptions = $("#subcategory option").length;*/
/* 	*/
/* 	if (category.length > 0) {*/
/* 			getSubcategories();*/
/* 	}*/
/* 	*/
/*     $("#category").on("change", function() {*/
/* */
/* 		var category = $("#category option:selected");*/
/* 		location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category.val();*/
/*     });*/
/*     */
/*     function getSubcategories() {*/
/* 	    var category = $("#category option:selected");*/
/* */
/*         $.get("/tamev2/dashboard/admin/catalog/subcategory/" + category.data("id"), function(data) {*/
/* */
/*             var options = '<option value="">Subcategoría</option>';*/
/*             $.each( data, function( key, value ) {*/
/*                 options += '<option value="' + value.sslug + '"> ' + value.sname + '</option>';*/
/*             });*/
/* */
/*             $("#subcategory").html(options)*/
/*             $("#subcategory").val("{{ subcategory }}");*/
/*         });*/
/*     }*/
/*     */
/*     $("#subcategory").on("change", function() {*/
/* */
/* 		var category = $("#category option:selected").val();*/
/* 		var subcategory = $("#subcategory option:selected");*/
/* 		var quality= "{{ quality }}";*/
/* */
/* 		if (quality.length > 0) {*/
/* 			location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&subcategory=" + subcategory.val() + "&quality=" + quality;*/
/* 		} else {*/
/* 			location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&subcategory=" + subcategory.val();*/
/* 		}*/
/*     });*/
/*     */
/*     $("#quality").on("change", function() {*/
/* */
/* 		var category = "{{ category }}";*/
/* 		var subcategory = "{{ subcategory }}";*/
/* 		var quality= $("#quality option:selected");*/
/* */
/* 		if (category.length > 0) {*/
/* 			if (subcategory.length > 0) {*/
/* 	    		location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&subcategory=" + subcategory + "&quality=" + quality.val(); */
/* 	    	} else {*/
/*         		location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&quality=" + quality.val();*/
/*         	}*/
/*         } else {*/
/* 	        location.href = "{{ base_url() }}dashboard/admin/catalog/search?quality=" + quality.val();*/
/*         }*/
/*     });*/
/* 	*/
/* 	$('body').on('click','.del-category',function(){*/
/* 	var  button = $(this);*/
/* 	 var id     = button.data('id');*/
/* 		swal({*/
/* 		  title: '¿Deseas eliminar esta categoría?',*/
/* 		  text: "",*/
/* 		  type: 'warning',*/
/* 		  showCancelButton: true,*/
/* 		  confirmButtonColor: '#3085d6',*/
/* 		  cancelButtonColor: '#d33',*/
/* 		  confirmButtonText: 'Eliminar!'*/
/* 		}).then(function() {*/
/* 		*/
/* 			$.get("{{ base_url() }}dashboard/admin/category/delete/" + id, function(data) {*/
/* */
/* 				if(data==1){*/
/* 				button.parent().parent().remove();*/
/* 				}*/
/* 				else{*/
/* 					swal(*/
/* 					  'Lo sentimos algo fallo?',*/
/* 					  'Por favor contacta al administrador o intenta mas tarde.',*/
/* 					  'error'*/
/* 					)				*/
/* 				}*/
/* 			});		*/
/* 			*/
/* 		  swal(*/
/* 			'Eliminado!',*/
/* 			'Este cambio no es reversible',*/
/* 			'success'*/
/* 		  )*/
/* 		})		*/
/* 	});*/
/* 	*/
/* 	$('body').on('click','.del-subcategory',function(){*/
/* 	var  button = $(this);*/
/* 	 var id     = button.data('id');*/
/* 		swal({*/
/* 		  title: '¿Deseas eliminar esta subcategoría?',*/
/* 		  text: "",*/
/* 		  type: 'warning',*/
/* 		  showCancelButton: true,*/
/* 		  confirmButtonColor: '#3085d6',*/
/* 		  cancelButtonColor: '#d33',*/
/* 		  confirmButtonText: 'Eliminar!'*/
/* 		}).then(function() {*/
/* 		*/
/* 			$.get("{{ base_url() }}dashboard/admin/category/delete_subcategory/" + id, function(data) {*/
/* 				*/
/* 				if(data==1){*/
/* 				button.parent().parent().remove();*/
/* 				}*/
/* 				else{*/
/* 					swal(*/
/* 					  'Lo sentimos algo fallo?',*/
/* 					  'Por favor contacta al administrador o intenta mas tarde.',*/
/* 					  'error'*/
/* 					)				*/
/* 				}*/
/* 			});		*/
/* 			*/
/* 		  swal(*/
/* 			'Eliminado!',*/
/* 			'Este cambio no es reversible',*/
/* 			'success'*/
/* 		  )*/
/* 		})	*/
/* 	});*/
/* 	*/
/* 	status = 0;*/
/* 	$('body').on('click','.edit-category',function(){*/
/* 		var button = $(this);*/
/* 		if( status%2 == 0 ){*/
/* 			edit_label = button.text('Guardar');*/
/* 			button.parent().parent().find('.cat-name').prop('disabled',false);*/
/* 			button.parent().parent().find('.cat-slug').prop('disabled',false);			*/
/* 			*/
/* 		}*/
/* 		else{*/
/* */
/* 			var name_id = button.parent().parent().find('.cat-name').data('id');*/
/* 			var slug_id = button.parent().parent().find('.cat-name').data('id');*/
/* 			*/
/* 			var name = button.parent().parent().find('.cat-name').val();*/
/* 			var slug = button.parent().parent().find('.cat-slug').val();*/
/* 			*/
/* 				dataInf = {*/
/* 							id_name :name_id, */
/* 							id_slug :slug_id,*/
/* 							name    :name,*/
/* 							slug    :slug*/
/* 						  };*/
/* */
/* 				$.post("{{ base_url() }}dashboard/admin/category/update/",dataInf,function(data) {*/
/* 					if( data==1 ){*/
/* 						edit_label = button.text('Editar');	*/
/* 						button.parent().parent().find('.cat-name').prop('disabled',true);*/
/* 						button.parent().parent().find('.cat-slug').prop('disabled',true);					*/
/* 						swal(*/
/* 						  'Cambio exitoso',*/
/* 						  '',*/
/* 						  'success'*/
/* 						)					*/
/* 					}*/
/* 					else if( data == 2){*/
/* 						edit_label = button.text('Editar');	*/
/* 						button.parent().parent().find('.cat-name').prop('disabled',true);*/
/* 						button.parent().parent().find('.cat-slug').prop('disabled',true);*/
/* 					}else{*/
/* 						swal(*/
/* 						  'Lo sentimos, intenta mas tarde.',*/
/* 						  '',*/
/* 						  'error'*/
/* 						)				*/
/* 					}*/
/* 				});	*/
/* 		}*/
/* 		status++;*/
/* 	});*/
/* 	*/
/* 	*/
/* 	$('body').on('click','.edit-subcategory',function(){*/
/* 		var button = $(this);*/
/* 		if( status%2 == 0 ){*/
/* 			edit_label = button.text('Guardar');*/
/* 			button.parent().parent().find('.subcat-name').prop('disabled',false);*/
/* 			button.parent().parent().find('.subcat-slug').prop('disabled',false);	*/
/* 			button.parent().parent().find('.subcat-main').prop('disabled',false);				*/
/* 			console.log("no guardar");*/
/* 		}*/
/* 		else{*/
/* 			console.log("guardar");*/
/* 			var name_sub_id = button.parent().parent().find('.subcat-name').data('id');*/
/* 			var slug_sub_id   = button.parent().parent().find('.subcat-slug').data('id');*/
/* 			var id_sub_id     = button.parent().parent().find('.subcat-main').data('id');			*/
/* 			*/
/* 			var name = button.parent().parent().find('.subcat-name').val();*/
/* 			var slug = button.parent().parent().find('.subcat-slug').val();*/
/* 			var id   = button.parent().parent().find('.subcat-main option:selected').val();*/
/* 			*/
/* 				dataInf = {*/
/* 							indice  : id_sub_id,*/
/* 							id_name : name_sub_id, */
/* 							id_slug : slug_sub_id,*/
/* 							id      : id,*/
/* 							name    :name,*/
/* 							slug    :slug*/
/* 						  };*/
/* */
/* 				$.post("{{ base_url() }}dashboard/admin/category/update_subcategory",dataInf,function(data) {*/
/* 					if( data==1 ){*/
/* 						edit_label = button.text('Editar');	*/
/* 						button.parent().parent().find('.subcat-name').prop('disabled',true);*/
/* 						button.parent().parent().find('.subcat-slug').prop('disabled',true);	*/
/* 						button.parent().parent().find('.subcat-main').prop('disabled',true);							*/
/* 						swal(*/
/* 						  'Cambio exitoso',*/
/* 						  '',*/
/* 						  'success'*/
/* 						)					*/
/* 					}*/
/* 					else if( data == 2){*/
/* 						edit_label = button.text('Editar');	*/
/* 						button.parent().parent().find('.subcat-name').prop('disabled',true);*/
/* 						button.parent().parent().find('.subcat-slug').prop('disabled',true);*/
/* 						button.parent().parent().find('.subcat-main').prop('disabled',true);*/
/* 					}else{*/
/* 						swal(*/
/* 						  'Lo sentimos, intenta mas tarde.',*/
/* 						  '',*/
/* 						  'error'*/
/* 						)				*/
/* 					}*/
/* 				});	*/
/* 		}*/
/* 		status++;*/
/* 	});*/
/* 	*/
/* 	$('#addcategories').on('submit',function(e){*/
/* 		e.preventDefault();*/
/* 		form = $(this);*/
/* 		serializ = form.serialize();*/
/* 		$.post("{{ base_url() }}dashboard/admin/category/add/",form.serialize(),function(data) {*/
/* 		  var table = $('.table-category>tbody tr:nth-child(1)');*/
/* 		  var name = $('#addcategories :input[name=name]').val();*/
/* 		  var slug = $('#addcategories :input[name=slug]').val();*/
/* 		  var th1 = "<input class='cat-name' data-id='"+data+"' disabled data-value='"+name+"' type='text' value='"+name+"'>";*/
/* 		  var th2 = "<input class='cat-slug' data-id='"+data+"' disabled data-value='"+slug+"' type='text' value='"+slug+"'>";*/
/* 		  var th3 = "<button class='btn btn-info edit-category' data-id="+data+">Editar</button>  <button class='btn btn-danger del-category' data-id="+data+">Eliminar</button>";*/
/* 		  $('.table-category>tbody tr:nth-child(1)').after("<tr class='btn-tr'><td class='form-group'>"+th1+"</td><td>"+th2+"</td><td>"+th3+"</td></tr>");*/
/* 		 $('#addcategories')[0].reset();*/
/* 		if( data != 0 ){*/
/* 			swal(*/
/* 			  'Se ha guardado tu categoria correctamente.',*/
/* 			  '',*/
/* 			  'success'*/
/* 			)		*/
/* 		}else{*/
/* 			swal(*/
/* 			  'Lo sentimos, intenta mas tarde.',*/
/* 			  '',*/
/* 			  'error'*/
/* 			)		*/
/* 		*/
/* 		}*/
/* 		});			*/
/* 		*/
/* 	});*/
/* 	*/
/* 	$('#addsubcategories').on('submit',function(e){*/
/* 		e.preventDefault();*/
/* 		form = $(this);*/
/* 		$.post(form.attr('action'),form.serialize(),function(data) {*/
/* 		 var table = $('.table-subcategory>tbody tr:nth-child(1)');*/
/* 		  */
/* 		  var name = $('#addsubcategories :input[name=name-sub]').val();*/
/* 		  var slug = $('#addsubcategories :input[name=slug-sub]').val();*/
/* 		  var cat_id = $('#addsubcategories select option:selected').val();*/
/* 		  var cat_text = $('#addsubcategories select option:selected').text();*/
/* 		  */
/* 		  var texto_select = $('#category-list option:selected').text();*/
/* 		  var id_select = $('#category-list option:selected').val();		  */
/* 		  */
/* 		  var th1 = "<input class='subcat-name' data-id='"+data+"' disabled data-value='"+name+"' type='text' value='"+name+"'>";*/
/* 		  var th2 = "<input class='subcat-slug' data-id='"+data+"' disabled data-value='"+slug+"' type='text' value='"+slug+"'>";*/
/* 		  var th3 = "<select data-id='"+data+"' disabled class='form-control subcat-main'><option value='"+id_select+"'>"+texto_select+"</option></select>";*/
/* 				  */
/* 		  var th4 = "<button class='btn btn-info edit-subcategory' data-id='"+data+"'>Editar</button> <button class='btn btn-danger del-subcategory' data-id='"+data+"'>Eliminar</button>";*/
/* 		  $('.table-subcategory>tbody tr:nth-child(1)').after("<tr class='btn-tr'><td>"+th1+"</td><td>"+th2+"</td><td>"+th3+"</td><td>"+th4+"</td></tr>");*/
/* 		 */
/* */
/* 		 $('#category-list').find('option').clone().appendTo('.table-subcategory tbody tr:nth-child(2) td:nth-child(3) select');*/
/* 		 */
/* 		if( data != 0 ){*/
/* 			swal(*/
/* 			  'Se ha guardado tu subcategoria correctamente.',*/
/* 			  '',*/
/* 			  'success'*/
/* 			)		*/
/* 		}else{*/
/* 			swal(*/
/* 			  'Lo sentimos, intenta mas tarde.',*/
/* 			  '',*/
/* 			  'error'*/
/* 			)		*/
/* 		*/
/* 		}*/
/* 		});*/
/* 		*/
/* 		*/
/* 	});	*/
/*     /*$("#other").on("change", function() {*/
/* */
/* 		var category = "{{ category }}";*/
/* 		var subcategory = "{{ subcategory }}";*/
/* 		var quality= $("#quality option:selected");*/
/* 		var other= $("#quality option:selected");*/
/* 		alert("En construcción");*/
/* 		if (category.length > 0) {*/
/* 			if (subcategory.length > 0) {*/
/* 	    		location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&subcategory=" + subcategory + "&quality=" + quality.val(); */
/* 	    	} else {*/
/*         		location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&quality=" + quality.val();*/
/*         	}*/
/*         } else {*/
/* 	        location.href = "{{ base_url() }}dashboard/admin/catalog/search?quality=" + quality.val();*/
/*         }*/
/*     });	*//* */
/*     */
/* </script>*/
/* <style>*/
/* */
/* </style>*/
/* {% endblock %}*/
