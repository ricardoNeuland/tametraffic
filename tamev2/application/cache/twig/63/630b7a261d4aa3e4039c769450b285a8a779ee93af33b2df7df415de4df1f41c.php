<?php

/* dashboard/admin/product-edit.html */
class __TwigTemplate_5a838daf502f4cc5251bb0d5edd3aa1a9041605fa350c3678087c8a43c6cb87c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/product-edit.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"row\">
                <div class=\"col-sm-10 col-sm-offset-1\">
                    <form action=\"\">
                        <div class=\"form-group\">
                            <label for=\"name\">Nombre</label>
                            <input id=\"name\" name=\"name\" type=\"text\" class=\"form-control\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"price\">Precio</label>
                            <input id=\"price\" name=\"price\" type=\"text\" class=\"form-control\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo "\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"score\">Calificación</label>
                            <input id=\"score\" name=\"score\" type=\"text\" class=\"form-control\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "score", array()), "html", null, true);
        echo "\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"header\">Header</label>
                            <input id=\"header\" name=\"header\" type=\"text\" class=\"form-control\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "header", array()), "html", null, true);
        echo "\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"description\">Descripción</label>
                            <textarea name=\"description\" id=\"description\" cols=\"30\" rows=\"10\" class=\"form-control\">
                                ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "html", null, true);
        echo "
                            </textarea>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"\"></label>
                            <input id=\"\" name=\"\" type=\"text\" class=\"form-control\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo "\">
                        </div>
                    </form>
                </div>
                <div class=\"col-lg-8\">
                    <p class=\"text-muted\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "c_name", array()), "html", null, true);
        echo "</p>
                    <h2 class=\"margin-clear margin-bottom--10\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "</h2>
                    <h4 class=\"margin-clear margin-bottom--10 text-muted\">\$";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "</h4>
                    <p class=\"lead margin-clear margin-bottom--10\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "header", array()), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-lg-4 text-right\">
                    <p class=\"h1\">";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "score", array()), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-sm-12\">
                    <p>";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "html", null, true);
        echo "</p>
                    <hr>
                    <p>
                        <b>Cantidad minima de pedido: </b> ";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "minimumOrder", array()), "html", null, true);
        echo "<br>
                        <b>Item: </b> ";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo "<br>
                        <b>Supplier ID: </b> ";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_custome", array()), "html", null, true);
        echo "<br>
                        <b>Calidad: </b> ";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quality", array()), "html", null, true);
        echo "<br>
                        <b>Origen: </b> ";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "origin", array()), "html", null, true);
        echo "<br>
                        <b>Presentación: </b> ";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "presentation", array()), "html", null, true);
        echo "
                    </p>
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapseCharacteristics\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        CARACTERISTICAS <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapseCharacteristics\">
                        ";
        // line 70
        if (twig_test_empty($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()))) {
            // line 71
            echo "                        <span class=\"text-muted\">No hay una descripción</span>
                        ";
        } else {
            // line 73
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()), "html", null, true);
            echo "
                        ";
        }
        // line 75
        echo "                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapseEspecifications\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        ESPECIFICACIONES <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapseEspecifications\">
                        ";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "especifications", array()), "html", null, true);
        echo "<br><br>
                        <b>Dimensiones: </b> ";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "dimensions", array()), "html", null, true);
        echo "<br>
                        <b>Peso: </b> ";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "weight", array()), "html", null, true);
        echo "<br><br>

                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapsePacking\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        EMPAQUE <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapsePacking\">
                        <b>No. piezas: </b> ";
        // line 91
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_numPieces", array()), "html", null, true);
        echo "<br>
                        <b>Especificaciones: </b> ";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_especifications", array()), "html", null, true);
        echo "<br>
                        <b>Alto: </b> ";
        // line 93
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_height", array()), "html", null, true);
        echo "cm<br>
                        <b>Ancho: </b> ";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_width", array()), "html", null, true);
        echo "cm<br>
                        <b>Largo: </b> ";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_large", array()), "html", null, true);
        echo "cm<br>
                        <b>Peso:</b> ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_weight", array()), "html", null, true);
        echo "kg
                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapseLogistic\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        LOGISTICA <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapseLogistic\">
                        <b>Tiempo de producción: </b> ";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_productionTime", array()), "html", null, true);
        echo "<br>
                        <b>FOB: </b> ";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fob", array()), "html", null, true);
        echo "<br>
                        <b>Tiempo de entrega FOB: </b> ";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fobDeliveryTime", array()), "html", null, true);
        echo "<br>
                        <b>Muestra fisica: </b> ";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sample", array()), "html", null, true);
        echo "<br>
                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapsePrice\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        PRECIO <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapsePrice\">
                        <b>Precio s/IVA: </b> \$";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo "<br>
                        <b>Moneda: </b> ";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "<br>
                        <b>Precio sugerido: </b> \$";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_suggested", array()), "html", null, true);
        echo "<br>
                        <b>Precio TT: </b> \$";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo "<br>
                        ";
        // line 117
        $context["utilidad"] = ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()) - $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()));
        // line 118
        echo "                        <b>Utilidad: </b> \$";
        echo twig_escape_filter($this->env, (isset($context["utilidad"]) ? $context["utilidad"] : null), "html", null, true);
        echo "
                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                </div>
                <div class=\"col-sm-12\">
                    <div class=\"row\">
                        ";
        // line 124
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["colors"]) ? $context["colors"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
            // line 125
            echo "                        <div class=\"col-sm-1\">
                            <div class=\"panel\" style=\"height: 20px; background: ";
            // line 126
            echo twig_escape_filter($this->env, $context["color"], "html", null, true);
            echo "\"></div>
                        </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 136
    public function block_script($context, array $blocks = array())
    {
        // line 137
        echo "<script>
    \$(function() {

        \$('#myCarousel').carousel({
            interval: 5000
        });

        //Handles the carousel thumbnails
        \$('[id^=carousel-selector-]').click(function () {
            var id_selector = \$(this).attr(\"id\");
            try {
                var id = /-(\\d+)\$/.exec(id_selector)[1];
                console.log(id_selector, id);
                jQuery('#myCarousel').carousel(parseInt(id));
            } catch (e) {
                console.log('Regex failed!', e);
            }
        });
        // When the carousel slides, auto update the text
        \$('#myCarousel').on('slid.bs.carousel', function (e) {
            var id = \$('.item.active').data('slide-number');
            \$('#carousel-text').html(\$('#slide-content-'+id).html());
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/product-edit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 137,  309 => 136,  299 => 129,  290 => 126,  287 => 125,  283 => 124,  273 => 118,  271 => 117,  267 => 116,  263 => 115,  259 => 114,  255 => 113,  245 => 106,  241 => 105,  237 => 104,  233 => 103,  223 => 96,  219 => 95,  215 => 94,  211 => 93,  207 => 92,  203 => 91,  192 => 83,  188 => 82,  184 => 81,  176 => 75,  170 => 73,  166 => 71,  164 => 70,  155 => 64,  151 => 63,  147 => 62,  143 => 61,  139 => 60,  135 => 59,  129 => 56,  123 => 53,  117 => 50,  111 => 49,  107 => 48,  103 => 47,  95 => 42,  87 => 37,  79 => 32,  72 => 28,  65 => 24,  58 => 20,  48 => 12,  45 => 11,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-12">*/
/*             <div class="row">*/
/*                 <div class="col-sm-10 col-sm-offset-1">*/
/*                     <form action="">*/
/*                         <div class="form-group">*/
/*                             <label for="name">Nombre</label>*/
/*                             <input id="name" name="name" type="text" class="form-control" value="{{ product.name }}">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="price">Precio</label>*/
/*                             <input id="price" name="price" type="text" class="form-control" value="{{ product.price_withoutTax }}">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="score">Calificación</label>*/
/*                             <input id="score" name="score" type="text" class="form-control" value="{{ product.score }}">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="header">Header</label>*/
/*                             <input id="header" name="header" type="text" class="form-control" value="{{ product.header }}">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="description">Descripción</label>*/
/*                             <textarea name="description" id="description" cols="30" rows="10" class="form-control">*/
/*                                 {{ product.description }}*/
/*                             </textarea>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for=""></label>*/
/*                             <input id="" name="" type="text" class="form-control" value="{{ product.price_withoutTax }}">*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*                 <div class="col-lg-8">*/
/*                     <p class="text-muted">{{ product.c_name }}</p>*/
/*                     <h2 class="margin-clear margin-bottom--10">{{ product.name }}</h2>*/
/*                     <h4 class="margin-clear margin-bottom--10 text-muted">${{ product.price_withoutTax }} {{ product.price_currency }}</h4>*/
/*                     <p class="lead margin-clear margin-bottom--10">{{ product.header }}</p>*/
/*                 </div>*/
/*                 <div class="col-lg-4 text-right">*/
/*                     <p class="h1">{{ product.score }}</p>*/
/*                 </div>*/
/*                 <div class="col-sm-12">*/
/*                     <p>{{ product.description }}</p>*/
/*                     <hr>*/
/*                     <p>*/
/*                         <b>Cantidad minima de pedido: </b> {{ product.minimumOrder }}<br>*/
/*                         <b>Item: </b> {{ product.item }}<br>*/
/*                         <b>Supplier ID: </b> {{ product.s_custome }}<br>*/
/*                         <b>Calidad: </b> {{ product.quality }}<br>*/
/*                         <b>Origen: </b> {{ product.origin }}<br>*/
/*                         <b>Presentación: </b> {{ product.presentation }}*/
/*                     </p>*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapseCharacteristics" aria-expanded="false" aria-controls="collapseExample">*/
/*                         CARACTERISTICAS <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapseCharacteristics">*/
/*                         {% if product.characteristics is empty %}*/
/*                         <span class="text-muted">No hay una descripción</span>*/
/*                         {% else %}*/
/*                         {{ product.characteristics }}*/
/*                         {% endif %}*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapseEspecifications" aria-expanded="false" aria-controls="collapseExample">*/
/*                         ESPECIFICACIONES <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapseEspecifications">*/
/*                         {{ product.especifications }}<br><br>*/
/*                         <b>Dimensiones: </b> {{ product.dimensions }}<br>*/
/*                         <b>Peso: </b> {{ product.weight }}<br><br>*/
/* */
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapsePacking" aria-expanded="false" aria-controls="collapseExample">*/
/*                         EMPAQUE <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapsePacking">*/
/*                         <b>No. piezas: </b> {{ product.packing_numPieces }}<br>*/
/*                         <b>Especificaciones: </b> {{ product.packing_especifications }}<br>*/
/*                         <b>Alto: </b> {{ product.packing_height }}cm<br>*/
/*                         <b>Ancho: </b> {{ product.packing_width }}cm<br>*/
/*                         <b>Largo: </b> {{ product.packing_large }}cm<br>*/
/*                         <b>Peso:</b> {{ product.packing_weight }}kg*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapseLogistic" aria-expanded="false" aria-controls="collapseExample">*/
/*                         LOGISTICA <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapseLogistic">*/
/*                         <b>Tiempo de producción: </b> {{ product.logistics_productionTime }}<br>*/
/*                         <b>FOB: </b> {{ product.logistics_fob }}<br>*/
/*                         <b>Tiempo de entrega FOB: </b> {{ product.logistics_fobDeliveryTime }}<br>*/
/*                         <b>Muestra fisica: </b> {{ product.sample }}<br>*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapsePrice" aria-expanded="false" aria-controls="collapseExample">*/
/*                         PRECIO <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapsePrice">*/
/*                         <b>Precio s/IVA: </b> ${{ product.price_withoutTax}}<br>*/
/*                         <b>Moneda: </b> {{ product.price_currency }}<br>*/
/*                         <b>Precio sugerido: </b> ${{ product.price_suggested }}<br>*/
/*                         <b>Precio TT: </b> ${{ product.price_tt }}<br>*/
/*                         {% set utilidad = product.price_tt - product.price_withoutTax %}*/
/*                         <b>Utilidad: </b> ${{ utilidad }}*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                 </div>*/
/*                 <div class="col-sm-12">*/
/*                     <div class="row">*/
/*                         {% for color in colors %}*/
/*                         <div class="col-sm-1">*/
/*                             <div class="panel" style="height: 20px; background: {{ color }}"></div>*/
/*                         </div>*/
/*                         {% endfor %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/*     $(function() {*/
/* */
/*         $('#myCarousel').carousel({*/
/*             interval: 5000*/
/*         });*/
/* */
/*         //Handles the carousel thumbnails*/
/*         $('[id^=carousel-selector-]').click(function () {*/
/*             var id_selector = $(this).attr("id");*/
/*             try {*/
/*                 var id = /-(\d+)$/.exec(id_selector)[1];*/
/*                 console.log(id_selector, id);*/
/*                 jQuery('#myCarousel').carousel(parseInt(id));*/
/*             } catch (e) {*/
/*                 console.log('Regex failed!', e);*/
/*             }*/
/*         });*/
/*         // When the carousel slides, auto update the text*/
/*         $('#myCarousel').on('slid.bs.carousel', function (e) {*/
/*             var id = $('.item.active').data('slide-number');*/
/*             $('#carousel-text').html($('#slide-content-'+id).html());*/
/*         });*/
/*     });*/
/* </script>*/
/* {% endblock %}*/
