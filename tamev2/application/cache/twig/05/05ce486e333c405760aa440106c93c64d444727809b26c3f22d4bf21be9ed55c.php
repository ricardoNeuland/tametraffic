<?php

/* auth/login.html */
class __TwigTemplate_17e4e11cd8093a5b1c2572316f04e1e79df4782771bc45c18caa09da4210d115 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("common/base-shop.html", "auth/login.html", 2);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-shop.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<div style=\"padding-top:74px;\"></div>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-offset-3 col-sm-6\">
\t\t\t\t<style type=\"text/css\">
\t\t\t\t\t.errn{
\t\t\t\t\t\tfont-family: AkkuratPro_regular;
\t\t\t\t\t\tcolor:red;
\t\t\t\t\t\tfont-size:12px;
\t\t\t\t\t}
\t\t\t\t</style>
\t\t\t\t";
        // line 15
        echo form_open("auth/login/validate", "class=\"\"");
        echo "
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Username</label>
\t\t\t\t\t\t<input type=\"email\" name=\"username\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["user"]) ? $context["user"] : null), "html", null, true);
        echo "\" class=\"form-control\" required>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Password</label>
\t\t\t\t\t\t<input type=\"password\" name=\"password\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["pass"]) ? $context["pass"] : null), "html", null, true);
        echo "\" class=\"form-control\" required>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group errn\">
\t\t\t\t\t\t";
        // line 25
        echo (isset($context["error"]) ? $context["error"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-default\">Inicia sesión</button>
\t\t\t\t\t</div>
 \t\t\t\t";
        // line 30
        echo form_close();
        echo "

\t\t\t</div>
\t\t</div>
\t</div>
\t";
    }

    public function getTemplateName()
    {
        return "auth/login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 30,  63 => 25,  57 => 22,  50 => 18,  44 => 15,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* */
/* 	{% extends 'common/base-shop.html' %}*/
/* 	{% block content %}*/
/* 	<div style="padding-top:74px;"></div>*/
/* 	<div class="container">*/
/* 		<div class="row">*/
/* 			<div class="col-sm-offset-3 col-sm-6">*/
/* 				<style type="text/css">*/
/* 					.errn{*/
/* 						font-family: AkkuratPro_regular;*/
/* 						color:red;*/
/* 						font-size:12px;*/
/* 					}*/
/* 				</style>*/
/* 				{{ form_open('auth/login/validate', 'class=""')|raw }}*/
/* 					<div class="form-group">*/
/* 						<label>Username</label>*/
/* 						<input type="email" name="username" value="{{ user }}" class="form-control" required>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Password</label>*/
/* 						<input type="password" name="password" value="{{ pass }}" class="form-control" required>*/
/* 					</div>*/
/* 					<div class="form-group errn">*/
/* 						{{ error|raw }}*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<button type="submit" class="btn btn-default">Inicia sesión</button>*/
/* 					</div>*/
/*  				{{ form_close()|raw }}*/
/* */
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	{% endblock %}*/
