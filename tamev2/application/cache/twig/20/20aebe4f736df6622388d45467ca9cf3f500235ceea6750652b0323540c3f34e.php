<?php

/* dashboard/admin/catalog-products.html */
class __TwigTemplate_1b16079b4782b7134bcb104ce285f194ce0513e5cd62f5063fc6d92be35e8f63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/catalog-products.html", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-6\">
            <h3 class=\"title-dashboard\">
                Catálogo / Productos
            </h3>
        </div>
        <div class=\"col-sm-6 text-right\">

        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <h4 class=\"margin-clear\">
                        ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "name", array()), "html", null, true);
        echo "
                        <a class=\"btn btn-link pull-right\" role=\"button\" data-toggle=\"collapse\" href=\"#supplierInfo\" aria-expanded=\"false\" aria-controls=\"supplierInfo\">
                            Ver más
                        </a>
                    </h4>
                    <p class=\"text-muted\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "description", array()), "html", null, true);
        echo "</p>
                    <div class=\"collapse\" id=\"supplierInfo\">
                        <p>
                            <b>Razón Social: </b> ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "legal_name", array()), "html", null, true);
        echo "
                            <br>
                            <b>Supplier ID: </b> ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idcustome", array()), "html", null, true);
        echo "
                            <br>
                            <b>Fecha de Admisión: </b> ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "registered", array()), "html", null, true);
        echo "
                            <br>
                            <b>Categoría /Giro: </b> ";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "category", array()), "html", null, true);
        echo "
                            <br>
                            <b>Dirección Oficina: </b> ";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_address", array()), "html", null, true);
        echo "
                            <br>
                            <b>Dirección Bodega: </b> ";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_warehouse", array()), "html", null, true);
        echo "
                        </p>
                        <h3> Contacto</h3>
                        <p>
                            <b>Persona Asignada: </b> ";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee", array()), "html", null, true);
        echo "
                            <br>
                            <b>Teléfono Oficina: </b> ";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_phone", array()), "html", null, true);
        echo "
                            <br>
                            <b>Teléfono Móvil: </b> ";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_mobile", array()), "html", null, true);
        echo "
                            <br>
                            <b>Email: </b> ";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_email", array()), "html", null, true);
        echo "
                        </p>
                        <p>
                            <b>Persona Alto Mando: </b> ";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager", array()), "html", null, true);
        echo "
                            <br>
                            <b>Teléfono Oficina: </b> ";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_phone", array()), "html", null, true);
        echo "
                            <br>
                            <b>Teléfono Móvil: </b> ";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_mobile", array()), "html", null, true);
        echo "
                            <br>
                            <b>Email: </b> ";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_email", array()), "html", null, true);
        echo "
                        </p>
                    </div>
                </div>
                <table class=\"table\">
                    <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Calificación</th>
                        <th>Acciones</th>
                    </tr>
                    ";
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 68
            echo "                    <tr>
                        <td>";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo "</td>
                        <td>
                            <img src=\"";
            // line 72
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo ".jpg\" alt=\"\" height=\"50\">
                        </td>
                        <td>";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "ranking", array()), "html", null, true);
            echo "</td>
                        <td>
                            <a href=\"#\" class=\"btn btn-info btn-sm\">
                                <span class=\"fa fa-info-circle margin-right--5\"></span>
                                Ver info
                            </a>
                            <a href=\"\" class=\"btn btn-danger btn-sm\">
                                <span class=\"fa fa-trash margin-right--5\"></span>
                                Eliminar
                            </a>
                        </td>
                    </tr>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "                </table>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/catalog-products.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 88,  187 => 75,  183 => 74,  174 => 72,  169 => 70,  165 => 69,  162 => 68,  145 => 67,  129 => 54,  124 => 52,  119 => 50,  114 => 48,  108 => 45,  103 => 43,  98 => 41,  93 => 39,  86 => 35,  81 => 33,  76 => 31,  71 => 29,  66 => 27,  61 => 25,  55 => 22,  47 => 17,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-6">*/
/*             <h3 class="title-dashboard">*/
/*                 Catálogo / Productos*/
/*             </h3>*/
/*         </div>*/
/*         <div class="col-sm-6 text-right">*/
/* */
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <h4 class="margin-clear">*/
/*                         {{ supplier.name }}*/
/*                         <a class="btn btn-link pull-right" role="button" data-toggle="collapse" href="#supplierInfo" aria-expanded="false" aria-controls="supplierInfo">*/
/*                             Ver más*/
/*                         </a>*/
/*                     </h4>*/
/*                     <p class="text-muted">{{ supplier.description }}</p>*/
/*                     <div class="collapse" id="supplierInfo">*/
/*                         <p>*/
/*                             <b>Razón Social: </b> {{ supplier.legal_name }}*/
/*                             <br>*/
/*                             <b>Supplier ID: </b> {{ supplier.idcustome }}*/
/*                             <br>*/
/*                             <b>Fecha de Admisión: </b> {{ supplier.registered }}*/
/*                             <br>*/
/*                             <b>Categoría /Giro: </b> {{ supplier.category }}*/
/*                             <br>*/
/*                             <b>Dirección Oficina: </b> {{ supplier.office_address }}*/
/*                             <br>*/
/*                             <b>Dirección Bodega: </b> {{ supplier.office_warehouse }}*/
/*                         </p>*/
/*                         <h3> Contacto</h3>*/
/*                         <p>*/
/*                             <b>Persona Asignada: </b> {{ supplier.designee }}*/
/*                             <br>*/
/*                             <b>Teléfono Oficina: </b> {{ supplier.designee_phone }}*/
/*                             <br>*/
/*                             <b>Teléfono Móvil: </b> {{ supplier.designee_mobile }}*/
/*                             <br>*/
/*                             <b>Email: </b> {{ supplier.designee_email }}*/
/*                         </p>*/
/*                         <p>*/
/*                             <b>Persona Alto Mando: </b> {{ supplier.manager }}*/
/*                             <br>*/
/*                             <b>Teléfono Oficina: </b> {{ supplier.manager_phone }}*/
/*                             <br>*/
/*                             <b>Teléfono Móvil: </b> {{ supplier.manager_mobile }}*/
/*                             <br>*/
/*                             <b>Email: </b> {{ supplier.manager_email }}*/
/*                         </p>*/
/*                     </div>*/
/*                 </div>*/
/*                 <table class="table">*/
/*                     <tr>*/
/*                         <th>#</th>*/
/*                         <th>Item</th>*/
/*                         <th>Imagen</th>*/
/*                         <th>Nombre</th>*/
/*                         <th>Calificación</th>*/
/*                         <th>Acciones</th>*/
/*                     </tr>*/
/*                     {% for product in products %}*/
/*                     <tr>*/
/*                         <td>{{ loop.index }}</td>*/
/*                         <td>{{ product.item }}</td>*/
/*                         <td>*/
/*                             <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ product.directory }}/{{ product.item }}.jpg" alt="" height="50">*/
/*                         </td>*/
/*                         <td>{{ product.name }}</td>*/
/*                         <td>{{ product.ranking }}</td>*/
/*                         <td>*/
/*                             <a href="#" class="btn btn-info btn-sm">*/
/*                                 <span class="fa fa-info-circle margin-right--5"></span>*/
/*                                 Ver info*/
/*                             </a>*/
/*                             <a href="" class="btn btn-danger btn-sm">*/
/*                                 <span class="fa fa-trash margin-right--5"></span>*/
/*                                 Eliminar*/
/*                             </a>*/
/*                         </td>*/
/*                     </tr>*/
/*                     {% endfor %}*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
