<?php

/* dashboard/admin/category-add.html */
class __TwigTemplate_d403fa4a3733680b8cf19091659ac3a5d68132a121b1f0fb5c239ffc4729a545 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/category-add.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-12 text-center\">
            <h1 class=\"title-dashboard\">Agregar Categoría</h1>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <form action=\"";
        // line 20
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/add\" method=\"post\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">
                        <div class=\"form-group\">
                            <label for=\"name\">Nombre</label>
                            <input id=\"name\" name=\"name\" type=\"text\" class=\"form-control\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"slug\">Filtro</label>
                            <input id=\"slug\" name=\"slug\" type=\"text\" class=\"form-control\">
                        </div>
                        <div class=\"form-group\">
                            <button class=\"btn btn-info\">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                ";
        // line 38
        if (((isset($context["categories_count"]) ? $context["categories_count"] : null) > 0)) {
            // line 39
            echo "                <table class=\"table table-hover\">
                    <tr>
                        <th>Nombre</th>
                        <th>Filtro</th>
                        <th>Acciones</th>
                    </tr>
                    ";
            // line 45
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
                // line 46
                echo "                    <tr class=\"btn-tr\">
                        <td id=\"cat-name-";
                // line 47
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\" data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "name", array()), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "name", array()), "html", null, true);
                echo "</td>
                        <td id=\"cat-slug-";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\" data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "slug", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "slug", array()), "html", null, true);
                echo "</td>
                        <td>
                            <button class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#edit\" data-id=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\">
                                Editar
                            </button>
                            <button class=\"btn btn-danger\" data-toggle=\"modal\" data-id=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "idCategory", array()), "html", null, true);
                echo "\" data-target=\"#delete\">
                                Eliminar
                            </button>
                        </td>
                    </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "                </table>
                ";
        } else {
            // line 61
            echo "                    <p class=\"h3 margin-bottom--20 text-muted text-center\">No hay categorias registradas</p>
                ";
        }
        // line 63
        echo "            </div>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"edit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Editar Categoría</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"";
        // line 75
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/update\" method=\"post\" accept-charset=\"utf-8\">
                    <div class=\"form-group\">
                        <label for=\"editCatName\">Nombre</label>
                        <input name=\"editCatName\" type=\"text\" id=\"editCatName\" class=\"form-control\">
                    </div>
                    <div class=\"form-group\">
                        <label for=\"editCatSlug\">Slug</label>
                        <input name=\"editCatSlug\" type=\"text\" id=\"editCatSlug\" class=\"form-control\">
                    </div>
                    <div class=\"form-group\">
                        <input type=\"hidden\" name=\"id\" id=\"editCatId\">
                        <button type=\"submit\" class=\"btn btn-success\">Guardar</button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"delete\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalDelete\">
    <div class=\"modal-dialog modal-sm\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <h3 class=\"modal-title\">Confrimación</h3>
                <p>Confirma que deseas eliminar la categoría <span id=\"catName\"></span></p>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                <a id=\"deleteUrl\" href=\"\" class=\"btn btn-danger\">Confirmar</a>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 111
    public function block_script($context, array $blocks = array())
    {
        // line 112
        echo "<script>

    var normalize = (function() {
        var from = \"ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç\",
                to   = \"AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc\",
                mapping = {};

        for(var i = 0, j = from.length; i < j; i++ )
            mapping[ from.charAt( i ) ] = to.charAt( i );

        return function( str ) {
            var ret = [];
            for( var i = 0, j = str.length; i < j; i++ ) {
                var c = str.charAt( i );
                if( mapping.hasOwnProperty( str.charAt( i ) ) )
                    ret.push( mapping[ c ] );
                else
                    ret.push( c );
            }
            return ret.join( '' );
        }

    })();

    \$(function(){
        \$(\"#name\").on(\"keyup\", function() {
            var text = \$(this).val();

            var cleanSpace = text.replace(\" \", \"\");
            cleanSpace = cleanSpace.replace(\"/\", \"\");

            \$(\"#slug\").val(normalize(cleanSpace.toLowerCase()));
        });

        \$('#edit').on('show.bs.modal', function (event) {

            var modal = \$(this)
            var button = \$(event.relatedTarget)

            var id = button.data('id')
            var name = \$(\"#cat-name-\" + id).data('value');
            var slug = \$('#cat-slug-' + id).data('value');

            modal.find('.modal-title').text('Editar ' + name)
            modal.find('.modal-body #editCatName').val(name)
            modal.find('.modal-body #editCatSlug').val(slug)
            modal.find('.modal-body #editCatId').val(id)
        })

        \$('#delete').on('show.bs.modal', function (event) {

            var modal = \$(this)
            var button = \$(event.relatedTarget)

            var id = button.data('id')
            var name = \$(\"#cat-name-\" + id).data('value');

            modal.find('#catName').html(name)
            modal.find('.modal-footer #deleteUrl').attr('href', '";
        // line 170
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/delete/' + id)
        })
    });




</script>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/category-add.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 170,  196 => 112,  193 => 111,  153 => 75,  139 => 63,  135 => 61,  131 => 59,  119 => 53,  113 => 50,  104 => 48,  96 => 47,  93 => 46,  89 => 45,  81 => 39,  79 => 38,  58 => 20,  48 => 12,  45 => 11,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-12 text-center">*/
/*             <h1 class="title-dashboard">Agregar Categoría</h1>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <form action="{{ base_url() }}dashboard/admin/category/add" method="post" accept-charset="utf-8" enctype="multipart/form-data">*/
/*                         <div class="form-group">*/
/*                             <label for="name">Nombre</label>*/
/*                             <input id="name" name="name" type="text" class="form-control">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="slug">Filtro</label>*/
/*                             <input id="slug" name="slug" type="text" class="form-control">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <button class="btn btn-info">Agregar</button>*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 {% if categories_count > 0 %}*/
/*                 <table class="table table-hover">*/
/*                     <tr>*/
/*                         <th>Nombre</th>*/
/*                         <th>Filtro</th>*/
/*                         <th>Acciones</th>*/
/*                     </tr>*/
/*                     {% for categorie in categories %}*/
/*                     <tr class="btn-tr">*/
/*                         <td id="cat-name-{{ categorie.idCategory }}" data-value="{{ categorie.name }}" >{{ categorie.name }}</td>*/
/*                         <td id="cat-slug-{{ categorie.idCategory }}" data-value="{{ categorie.slug }}">{{ categorie.slug }}</td>*/
/*                         <td>*/
/*                             <button class="btn btn-info" data-toggle="modal" data-target="#edit" data-id="{{ categorie.idCategory }}">*/
/*                                 Editar*/
/*                             </button>*/
/*                             <button class="btn btn-danger" data-toggle="modal" data-id="{{ categorie.idCategory }}" data-target="#delete">*/
/*                                 Eliminar*/
/*                             </button>*/
/*                         </td>*/
/*                     </tr>*/
/*                     {% endfor %}*/
/*                 </table>*/
/*                 {% else %}*/
/*                     <p class="h3 margin-bottom--20 text-muted text-center">No hay categorias registradas</p>*/
/*                 {% endif %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*     <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*                 <h4 class="modal-title" id="myModalLabel">Editar Categoría</h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*                 <form action="{{ base_url() }}dashboard/admin/category/update" method="post" accept-charset="utf-8">*/
/*                     <div class="form-group">*/
/*                         <label for="editCatName">Nombre</label>*/
/*                         <input name="editCatName" type="text" id="editCatName" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label for="editCatSlug">Slug</label>*/
/*                         <input name="editCatSlug" type="text" id="editCatSlug" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <input type="hidden" name="id" id="editCatId">*/
/*                         <button type="submit" class="btn btn-success">Guardar</button>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="modalDelete">*/
/*     <div class="modal-dialog modal-sm" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-body">*/
/*                 <h3 class="modal-title">Confrimación</h3>*/
/*                 <p>Confirma que deseas eliminar la categoría <span id="catName"></span></p>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>*/
/*                 <a id="deleteUrl" href="" class="btn btn-danger">Confirmar</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/* */
/*     var normalize = (function() {*/
/*         var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",*/
/*                 to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",*/
/*                 mapping = {};*/
/* */
/*         for(var i = 0, j = from.length; i < j; i++ )*/
/*             mapping[ from.charAt( i ) ] = to.charAt( i );*/
/* */
/*         return function( str ) {*/
/*             var ret = [];*/
/*             for( var i = 0, j = str.length; i < j; i++ ) {*/
/*                 var c = str.charAt( i );*/
/*                 if( mapping.hasOwnProperty( str.charAt( i ) ) )*/
/*                     ret.push( mapping[ c ] );*/
/*                 else*/
/*                     ret.push( c );*/
/*             }*/
/*             return ret.join( '' );*/
/*         }*/
/* */
/*     })();*/
/* */
/*     $(function(){*/
/*         $("#name").on("keyup", function() {*/
/*             var text = $(this).val();*/
/* */
/*             var cleanSpace = text.replace(" ", "");*/
/*             cleanSpace = cleanSpace.replace("/", "");*/
/* */
/*             $("#slug").val(normalize(cleanSpace.toLowerCase()));*/
/*         });*/
/* */
/*         $('#edit').on('show.bs.modal', function (event) {*/
/* */
/*             var modal = $(this)*/
/*             var button = $(event.relatedTarget)*/
/* */
/*             var id = button.data('id')*/
/*             var name = $("#cat-name-" + id).data('value');*/
/*             var slug = $('#cat-slug-' + id).data('value');*/
/* */
/*             modal.find('.modal-title').text('Editar ' + name)*/
/*             modal.find('.modal-body #editCatName').val(name)*/
/*             modal.find('.modal-body #editCatSlug').val(slug)*/
/*             modal.find('.modal-body #editCatId').val(id)*/
/*         })*/
/* */
/*         $('#delete').on('show.bs.modal', function (event) {*/
/* */
/*             var modal = $(this)*/
/*             var button = $(event.relatedTarget)*/
/* */
/*             var id = button.data('id')*/
/*             var name = $("#cat-name-" + id).data('value');*/
/* */
/*             modal.find('#catName').html(name)*/
/*             modal.find('.modal-footer #deleteUrl').attr('href', '{{ base_url() }}dashboard/admin/category/delete/' + id)*/
/*         })*/
/*     });*/
/* */
/* */
/* */
/* */
/* </script>*/
/* {% endblock %}*/
