<?php

/* common/base-admin.html */
class __TwigTemplate_22b58be88d28ea95199bf100864efd7c44c382df2cab00ac3f5e5c36cbb2e3df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'sidebar' => array($this, 'block_sidebar'),
            'navbar' => array($this, 'block_navbar'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
\t<meta charset=\"utf-8\">
\t<title>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title", array()), "html", null, true);
        echo "</title>
\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/favicon.ico\" />\t
\t<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/bootstrap.min.css\">
\t<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/font-awesome.min.css\">
\t<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/app.css\">
\t<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/sweetalert2/5.2.1/sweetalert2.min.css\">
\t<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">\t
\t";
        // line 12
        $this->displayBlock('head', $context, $blocks);
        // line 20
        echo "</head>
<body>
\t<div id=\"wrapper\">
\t";
        // line 23
        $this->displayBlock('sidebar', $context, $blocks);
        // line 61
        echo "\t<div id=\"bodyWrapper\">\t
\t\t";
        // line 62
        $this->displayBlock('navbar', $context, $blocks);
        // line 74
        echo "\t\t
\t\t";
        // line 75
        $this->displayBlock('body', $context, $blocks);
        // line 76
        echo "\t</div>
\t</div>
\t<script type=\"text/javascript\" src=\"";
        // line 78
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/jquery.min.js\"></script>
\t<script src=\"";
        // line 79
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/pace.min.js\"></script>\t
\t<script type=\"text/javascript\">

\t\t\$(document).ready(function(){

\t\t   var retraso=2000, setTimeoutConst;

\t\t   \$('#menu-toggle').hover(function(){
\t\t   \t
\t\t\t\tsetTimeoutConst = setTimeout(function(){

\t\t\t\t\$(\"#wrapper\").toggleClass(\"toggled\");

\t\t\t\t}, retraso);

\t\t   },function(){

\t\t\t\tclearTimeout(setTimeoutConst );
\t\t   })
\t\t   \$(\"#menu-toggle\").click(function(e) {
\t       \t\te.preventDefault();
\t        \t\$(\"#wrapper\").toggleClass(\"toggled\");
\t    \t});
\t\t})

\t</script>
\t<script type=\"text/javascript\" src=\"https://cdn.jsdelivr.net/sweetalert2/5.2.1/sweetalert2.min.js\"></script>
\t<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>\t
\t<script type=\"text/javascript\" src=\"";
        // line 107
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/js/bootstrap.min.js\"></script>\t
\t";
        // line 108
        $this->displayBlock('script', $context, $blocks);
        // line 109
        echo "</body>
</html>";
    }

    // line 12
    public function block_head($context, array $blocks = array())
    {
        // line 13
        echo "\t\t<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/css/simple-sidebar.css\">
\t\t<style>
\t\t\tbody {
\t\t\t\tbackground-color: #ecf0f5
\t\t\t}
\t\t</style>
\t";
    }

    // line 23
    public function block_sidebar($context, array $blocks = array())
    {
        // line 24
        echo "\t\t<div id=\"sidebar-wrapper\">
            <ul class=\"sidebar-nav\">
                <li class=\"sidebar-brand\">
                       <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/logo-tt-white-2.svg\" class=\"img-responsive\">

                </li>
\t\t\t\t<li class=\"sidebar-username\">
\t\t\t\t\tSantiago
\t\t\t\t</li>
                <li>
                    <a href=\"";
        // line 34
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/home\">
\t                    Dashboard
\t                </a>
                </li>
                <li>
                    <a href=\"";
        // line 39
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier\">
                        Supplier Team
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog\">
\t                    Catálogo
\t                </a>
                </li>
                <li>
                    <a href=\"";
        // line 49
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog\">
                        Mensajes
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 54
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog\">
                        Mi cuenta
                    </a>
                </li>
            </ul>
        </div>
\t";
    }

    // line 62
    public function block_navbar($context, array $blocks = array())
    {
        // line 63
        echo "\t\t\t<nav class=\"navbar navbar-dashboard navbar-static-top\">
\t\t\t\t<div class=\"container-fluid\">
\t\t\t\t\t<ul class=\"nav navbar-nav navbar-left\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"#\" id=\"menu-toggle\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-bars\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</nav>
\t\t";
    }

    // line 75
    public function block_body($context, array $blocks = array())
    {
    }

    // line 108
    public function block_script($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "common/base-admin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 108,  215 => 75,  200 => 63,  197 => 62,  186 => 54,  178 => 49,  170 => 44,  162 => 39,  154 => 34,  144 => 27,  139 => 24,  136 => 23,  124 => 13,  121 => 12,  116 => 109,  114 => 108,  110 => 107,  79 => 79,  75 => 78,  71 => 76,  69 => 75,  66 => 74,  64 => 62,  61 => 61,  59 => 23,  54 => 20,  52 => 12,  46 => 9,  42 => 8,  38 => 7,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/* 	<meta charset="utf-8">*/
/* 	<title>{{ page.title }}</title>*/
/* 	<link rel="shortcut icon" type="image/x-icon" href="{{ base_url() }}public/assets/img/favicon.ico" />	*/
/* 	<link rel="stylesheet" href="{{ base_url() }}public/assets/css/bootstrap.min.css">*/
/* 	<link rel="stylesheet" href="{{ base_url() }}public/assets/css/font-awesome.min.css">*/
/* 	<link rel="stylesheet" href="{{ base_url() }}public/assets/css/app.css">*/
/* 	<link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/5.2.1/sweetalert2.min.css">*/
/* 	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">	*/
/* 	{% block head %}*/
/* 		<link rel="stylesheet" href="{{ base_url() }}public/assets/css/simple-sidebar.css">*/
/* 		<style>*/
/* 			body {*/
/* 				background-color: #ecf0f5*/
/* 			}*/
/* 		</style>*/
/* 	{% endblock %}*/
/* </head>*/
/* <body>*/
/* 	<div id="wrapper">*/
/* 	{% block sidebar %}*/
/* 		<div id="sidebar-wrapper">*/
/*             <ul class="sidebar-nav">*/
/*                 <li class="sidebar-brand">*/
/*                        <img src="{{ base_url() }}public/assets/img/logo-tt-white-2.svg" class="img-responsive">*/
/* */
/*                 </li>*/
/* 				<li class="sidebar-username">*/
/* 					Santiago*/
/* 				</li>*/
/*                 <li>*/
/*                     <a href="{{ base_url() }}dashboard/admin/home">*/
/* 	                    Dashboard*/
/* 	                </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ base_url() }}dashboard/admin/supplier">*/
/*                         Supplier Team*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ base_url() }}dashboard/admin/catalog">*/
/* 	                    Catálogo*/
/* 	                </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ base_url() }}dashboard/admin/catalog">*/
/*                         Mensajes*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ base_url() }}dashboard/admin/catalog">*/
/*                         Mi cuenta*/
/*                     </a>*/
/*                 </li>*/
/*             </ul>*/
/*         </div>*/
/* 	{% endblock %}*/
/* 	<div id="bodyWrapper">	*/
/* 		{% block navbar %}*/
/* 			<nav class="navbar navbar-dashboard navbar-static-top">*/
/* 				<div class="container-fluid">*/
/* 					<ul class="nav navbar-nav navbar-left">*/
/* 						<li>*/
/* 							<a href="#" id="menu-toggle">*/
/* 								<i class="fa fa-bars"></i>*/
/* 							</a>*/
/* 						</li>*/
/* 					</ul>*/
/* 				</div>*/
/* 			</nav>*/
/* 		{% endblock %}		*/
/* 		{% block body %}{% endblock %}*/
/* 	</div>*/
/* 	</div>*/
/* 	<script type="text/javascript" src="{{ base_url() }}public/assets/js/jquery.min.js"></script>*/
/* 	<script src="{{ base_url() }}public/assets/js/pace.min.js"></script>	*/
/* 	<script type="text/javascript">*/
/* */
/* 		$(document).ready(function(){*/
/* */
/* 		   var retraso=2000, setTimeoutConst;*/
/* */
/* 		   $('#menu-toggle').hover(function(){*/
/* 		   	*/
/* 				setTimeoutConst = setTimeout(function(){*/
/* */
/* 				$("#wrapper").toggleClass("toggled");*/
/* */
/* 				}, retraso);*/
/* */
/* 		   },function(){*/
/* */
/* 				clearTimeout(setTimeoutConst );*/
/* 		   })*/
/* 		   $("#menu-toggle").click(function(e) {*/
/* 	       		e.preventDefault();*/
/* 	        	$("#wrapper").toggleClass("toggled");*/
/* 	    	});*/
/* 		})*/
/* */
/* 	</script>*/
/* 	<script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/5.2.1/sweetalert2.min.js"></script>*/
/* 	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	*/
/* 	<script type="text/javascript" src="{{ base_url() }}public/assets/js/bootstrap.min.js"></script>	*/
/* 	{% block script %}{% endblock %}*/
/* </body>*/
/* </html>*/
