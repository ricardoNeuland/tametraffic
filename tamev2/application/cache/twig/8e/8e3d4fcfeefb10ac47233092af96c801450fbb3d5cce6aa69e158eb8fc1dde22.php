<?php

/* dashboard/admin/subcategory-add.html */
class __TwigTemplate_49c57f6552d285fd4c45f4e99af6dc150ff7b0c01fc5ed31977ac6878fb9edb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/subcategory-add.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-12 text-center\">
            <h1 class=\"title-dashboard\">Agregar Subcategoría</h1>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <form action=\"";
        // line 20
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/add_subcategory\" method=\"post\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">
                        <div class=\"form-group\">
                            <label for=\"name\">Nombre</label>
                            <input id=\"name\" name=\"name\" type=\"text\" class=\"form-control\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"slug\">Filtro</label>
                            <input id=\"slug\" name=\"slug\" type=\"text\" class=\"form-control\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"category\">Categoría</label>
                            <select name=\"category\" id=\"category\" class=\"form-control\">
                                <option value=\"0\">Seleccionar una opción</option>
                                ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 34
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "                            </select>
                        </div>
                        <div class=\"form-group\">
                            <button class=\"btn btn-info\">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                ";
        // line 47
        if (((isset($context["subcategories_count"]) ? $context["subcategories_count"] : null) > 0)) {
            // line 48
            echo "                <table class=\"table table-hover\">
                    <tr>
                        <th>Nombre</th>
                        <th>Filtro</th>
                        <th>Categoría</th>
                        <th>Acciones</th>
                    </tr>
                    ";
            // line 55
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["subcategories"]) ? $context["subcategories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["subcategory"]) {
                // line 56
                echo "                    <tr class=\"btn-tr\">
                        <td id=\"subcat-name-";
                // line 57
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sname", array()), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sname", array()), "html", null, true);
                echo "</td>
                        <td id=\"subcat-slug-";
                // line 58
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sslug", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sslug", array()), "html", null, true);
                echo "</td>
                        <td id=\"subcat-catid-";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" data-value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "cid", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "cname", array()), "html", null, true);
                echo "</td>
                        <td>
                            <button class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#edit\" data-id=\"";
                // line 61
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\">
                                Editar
                            </button>
                            <button class=\"btn btn-danger\" data-toggle=\"modal\" data-id=\"";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "sid", array()), "html", null, true);
                echo "\" data-target=\"#delete\">
                                Eliminar
                            </button>
                        </td>
                    </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                </table>
                ";
        } else {
            // line 72
            echo "                <p class=\"h3 margin-bottom--20 text-muted text-center\">No hay subcategorias registradas</p>
                ";
        }
        // line 74
        echo "            </div>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"edit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Modal title</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"";
        // line 86
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/update_subcategory\" method=\"post\" accept-charset=\"utf-8\">
                    <div class=\"form-group\">
                        <label for=\"editCatName\">Nombre</label>
                        <input name=\"editCatName\" type=\"text\" id=\"editCatName\" class=\"form-control\">
                    </div>
                    <div class=\"form-group\">
                        <label for=\"editCatSlug\">Slug</label>
                        <input name=\"editCatSlug\" type=\"text\" id=\"editCatSlug\" class=\"form-control\">
                    </div>
                    <div class=\"form-group\">
                        <label for=\"editCategory\">Categoría</label>
                        <select name=\"editCategory\" id=\"editCategory\" class=\"form-control\">
                            <option value=\"0\">Seleccionar una opción</option>
                            ";
        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 100
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "                        </select>
                    </div>
                    <div class=\"form-group\">
                        <input type=\"hidden\" name=\"id\" id=\"editCatId\">
                        <button type=\"submit\" class=\"btn btn-success\">Guardar</button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"delete\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalDelete\">
    <div class=\"modal-dialog modal-sm\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <h3 class=\"modal-title\">Confrimación</h3>
                <p>Confirma que deseas eliminar la subcategoría <span id=\"theCatName\"></span></p>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                <a id=\"deleteUrl\" href=\"\" class=\"btn btn-danger\">Confirmar</a>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 131
    public function block_script($context, array $blocks = array())
    {
        // line 132
        echo "<script>

    var normalize = (function() {
        var from = \"ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç\",
                to   = \"AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc\",
                mapping = {};

        for(var i = 0, j = from.length; i < j; i++ )
            mapping[ from.charAt( i ) ] = to.charAt( i );

        return function( str ) {
            var ret = [];
            for( var i = 0, j = str.length; i < j; i++ ) {
                var c = str.charAt( i );
                if( mapping.hasOwnProperty( str.charAt( i ) ) )
                    ret.push( mapping[ c ] );
                else
                    ret.push( c );
            }
            return ret.join( '' );
        }

    })();

    \$(function(){
        \$(\"#name\").on(\"keyup\", function() {
            var text = \$(this).val();

            var cleanSpace = text.replace(\" \", \"\");
            cleanSpace = cleanSpace.replace(\"/\", \"\");

            \$(\"#slug\").val(normalize(cleanSpace.toLowerCase()));
        });

        \$('#edit').on('show.bs.modal', function (event) {

            var modal = \$(this)
            var button = \$(event.relatedTarget)

            var id = button.data('id')
            var name = \$(\"#subcat-name-\" + id).data('value');
            var slug = \$('#subcat-slug-' + id).data('value');
            var catid = \$('#subcat-catid-' + id).data('value');

            modal.find('.modal-title').text('Editar ' + name)
            modal.find('.modal-body #editCatName').val(name)
            modal.find('.modal-body #editCatSlug').val(slug)
            modal.find('.modal-body #editCategory').val(catid)
            modal.find('.modal-body #editCatId').val(id)
        })

        \$('#delete').on('show.bs.modal', function (event) {

            var modal = \$(this)
            var button = \$(event.relatedTarget)

            var id = button.data('id')
            var name = \$(\"#subcat-name-\" + id).data('value');

            modal.find('#theCatName').html(name)
            modal.find('.modal-footer #deleteUrl').attr('href', '";
        // line 192
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/category/delete_subcategory/' + id)
        })
    });


</script>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/subcategory-add.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 192,  251 => 132,  248 => 131,  216 => 102,  205 => 100,  201 => 99,  185 => 86,  171 => 74,  167 => 72,  163 => 70,  151 => 64,  145 => 61,  136 => 59,  128 => 58,  120 => 57,  117 => 56,  113 => 55,  104 => 48,  102 => 47,  89 => 36,  78 => 34,  74 => 33,  58 => 20,  48 => 12,  45 => 11,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-12 text-center">*/
/*             <h1 class="title-dashboard">Agregar Subcategoría</h1>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <form action="{{ base_url() }}dashboard/admin/category/add_subcategory" method="post" accept-charset="utf-8" enctype="multipart/form-data">*/
/*                         <div class="form-group">*/
/*                             <label for="name">Nombre</label>*/
/*                             <input id="name" name="name" type="text" class="form-control">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="slug">Filtro</label>*/
/*                             <input id="slug" name="slug" type="text" class="form-control">*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="category">Categoría</label>*/
/*                             <select name="category" id="category" class="form-control">*/
/*                                 <option value="0">Seleccionar una opción</option>*/
/*                                 {% for category in categories %}*/
/*                                 <option value="{{ category.idCategory }}">{{ category.name }}</option>*/
/*                                 {% endfor %}*/
/*                             </select>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <button class="btn btn-info">Agregar</button>*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 {% if subcategories_count > 0 %}*/
/*                 <table class="table table-hover">*/
/*                     <tr>*/
/*                         <th>Nombre</th>*/
/*                         <th>Filtro</th>*/
/*                         <th>Categoría</th>*/
/*                         <th>Acciones</th>*/
/*                     </tr>*/
/*                     {% for subcategory in subcategories %}*/
/*                     <tr class="btn-tr">*/
/*                         <td id="subcat-name-{{ subcategory.sid }}" data-value="{{ subcategory.sname }}" >{{ subcategory.sname }}</td>*/
/*                         <td id="subcat-slug-{{ subcategory.sid }}" data-value="{{ subcategory.sslug }}">{{ subcategory.sslug }}</td>*/
/*                         <td id="subcat-catid-{{ subcategory.sid }}" data-value="{{ subcategory.cid }}">{{ subcategory.cname }}</td>*/
/*                         <td>*/
/*                             <button class="btn btn-info" data-toggle="modal" data-target="#edit" data-id="{{ subcategory.sid }}">*/
/*                                 Editar*/
/*                             </button>*/
/*                             <button class="btn btn-danger" data-toggle="modal" data-id="{{ subcategory.sid }}" data-target="#delete">*/
/*                                 Eliminar*/
/*                             </button>*/
/*                         </td>*/
/*                     </tr>*/
/*                     {% endfor %}*/
/*                 </table>*/
/*                 {% else %}*/
/*                 <p class="h3 margin-bottom--20 text-muted text-center">No hay subcategorias registradas</p>*/
/*                 {% endif %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*     <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*                 <h4 class="modal-title" id="myModalLabel">Modal title</h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*                 <form action="{{ base_url() }}dashboard/admin/category/update_subcategory" method="post" accept-charset="utf-8">*/
/*                     <div class="form-group">*/
/*                         <label for="editCatName">Nombre</label>*/
/*                         <input name="editCatName" type="text" id="editCatName" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label for="editCatSlug">Slug</label>*/
/*                         <input name="editCatSlug" type="text" id="editCatSlug" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label for="editCategory">Categoría</label>*/
/*                         <select name="editCategory" id="editCategory" class="form-control">*/
/*                             <option value="0">Seleccionar una opción</option>*/
/*                             {% for category in categories %}*/
/*                             <option value="{{ category.idCategory }}">{{ category.name }}</option>*/
/*                             {% endfor %}*/
/*                         </select>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <input type="hidden" name="id" id="editCatId">*/
/*                         <button type="submit" class="btn btn-success">Guardar</button>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="modalDelete">*/
/*     <div class="modal-dialog modal-sm" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-body">*/
/*                 <h3 class="modal-title">Confrimación</h3>*/
/*                 <p>Confirma que deseas eliminar la subcategoría <span id="theCatName"></span></p>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>*/
/*                 <a id="deleteUrl" href="" class="btn btn-danger">Confirmar</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/* */
/*     var normalize = (function() {*/
/*         var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",*/
/*                 to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",*/
/*                 mapping = {};*/
/* */
/*         for(var i = 0, j = from.length; i < j; i++ )*/
/*             mapping[ from.charAt( i ) ] = to.charAt( i );*/
/* */
/*         return function( str ) {*/
/*             var ret = [];*/
/*             for( var i = 0, j = str.length; i < j; i++ ) {*/
/*                 var c = str.charAt( i );*/
/*                 if( mapping.hasOwnProperty( str.charAt( i ) ) )*/
/*                     ret.push( mapping[ c ] );*/
/*                 else*/
/*                     ret.push( c );*/
/*             }*/
/*             return ret.join( '' );*/
/*         }*/
/* */
/*     })();*/
/* */
/*     $(function(){*/
/*         $("#name").on("keyup", function() {*/
/*             var text = $(this).val();*/
/* */
/*             var cleanSpace = text.replace(" ", "");*/
/*             cleanSpace = cleanSpace.replace("/", "");*/
/* */
/*             $("#slug").val(normalize(cleanSpace.toLowerCase()));*/
/*         });*/
/* */
/*         $('#edit').on('show.bs.modal', function (event) {*/
/* */
/*             var modal = $(this)*/
/*             var button = $(event.relatedTarget)*/
/* */
/*             var id = button.data('id')*/
/*             var name = $("#subcat-name-" + id).data('value');*/
/*             var slug = $('#subcat-slug-' + id).data('value');*/
/*             var catid = $('#subcat-catid-' + id).data('value');*/
/* */
/*             modal.find('.modal-title').text('Editar ' + name)*/
/*             modal.find('.modal-body #editCatName').val(name)*/
/*             modal.find('.modal-body #editCatSlug').val(slug)*/
/*             modal.find('.modal-body #editCategory').val(catid)*/
/*             modal.find('.modal-body #editCatId').val(id)*/
/*         })*/
/* */
/*         $('#delete').on('show.bs.modal', function (event) {*/
/* */
/*             var modal = $(this)*/
/*             var button = $(event.relatedTarget)*/
/* */
/*             var id = button.data('id')*/
/*             var name = $("#subcat-name-" + id).data('value');*/
/* */
/*             modal.find('#theCatName').html(name)*/
/*             modal.find('.modal-footer #deleteUrl').attr('href', '{{ base_url() }}dashboard/admin/category/delete_subcategory/' + id)*/
/*         })*/
/*     });*/
/* */
/* */
/* </script>*/
/* {% endblock %}*/
