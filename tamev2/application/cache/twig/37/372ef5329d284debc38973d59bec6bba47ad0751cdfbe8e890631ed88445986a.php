<?php

/* admin/index.html */
class __TwigTemplate_deeab8c3085e9548f3fe729db448b682914889922ca8fd8248bc83df7b6ece39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "admin/index.html", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t<div class=\"container-fluid bg-dark\" style=\"height: 150px\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-12 text-center margin-top--50\">
\t\t\t\t<img src=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/logo-tt.svg\" height=\"50\">
\t\t\t\t<p class=\"color-white font-weight--300\">Areá de administración</p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4\">
\t\t\t\t<h3 class=\"margin-bottom--20\">Iniciar Sesión</h3>
\t\t\t\t<form action=\"";
        // line 15
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "/action/admin/login\" method=\"\" accept-charset=\"utf-8\">
\t\t\t\t\t<label for=\"user\">Usuario</label>
\t\t\t\t\t<input id=\"user\" type=\"text\" name=\"user\" class=\"form-control margin-bottom--10\">
\t\t\t\t\t<label for=\"pass\">Contraseña</label>
\t\t\t\t\t<input id=\"pass\" type=\"password\" name=\"pass\" class=\"form-control margin-bottom--20\">
\t\t\t\t\t<button class=\"btn btn-default btn-block\">Ingresar</button>
\t\t\t\t</form>
\t\t\t\t<a href=\"#\" class=\"btn btn-link btn-block margin-top--20\">Recuperar contraseña</a>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "admin/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  36 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block body%}*/
/* 	<div class="container-fluid bg-dark" style="height: 150px">*/
/* 		<div class="row">*/
/* 			<div class="col-sm-12 text-center margin-top--50">*/
/* 				<img src="{{ base_url() }}public/assets/img/logo-tt.svg" height="50">*/
/* 				<p class="color-white font-weight--300">Areá de administración</p>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<div class="container">*/
/* 		<div class="row">*/
/* 			<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">*/
/* 				<h3 class="margin-bottom--20">Iniciar Sesión</h3>*/
/* 				<form action="{{ base_url() }}/action/admin/login" method="" accept-charset="utf-8">*/
/* 					<label for="user">Usuario</label>*/
/* 					<input id="user" type="text" name="user" class="form-control margin-bottom--10">*/
/* 					<label for="pass">Contraseña</label>*/
/* 					<input id="pass" type="password" name="pass" class="form-control margin-bottom--20">*/
/* 					<button class="btn btn-default btn-block">Ingresar</button>*/
/* 				</form>*/
/* 				<a href="#" class="btn btn-link btn-block margin-top--20">Recuperar contraseña</a>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
