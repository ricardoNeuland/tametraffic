<?php

/* shop/catalogo-full.html */
class __TwigTemplate_b169c0df253c40e0e3bc37418fa5082ae7c9c0cb4ed40b3825a1240150a0b9cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-shop.html", "shop/catalogo-full.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-shop.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "   <div id=\"boxContent\">
    <span id=\"btnSidenav\" onclick=\"openNav()\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></span>

      <!-- Content -->
      <div class=\"container-fluid\">
        <div class=\"row\">
          <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
            <div class=\"row\">
                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 productCert\">
                  <center>
                    <img class=\"iconSteps\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step1.png\" alt=\"step1\">
                    <img class=\"iconSteps\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step2.png\" alt=\"step2\">
                    <img id=\"iconStepTT\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/stepTT.png\" alt=\"stepTT\">
                    <img class=\"iconSteps\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step3.png\" alt=\"step3\">
                    <img class=\"iconSteps\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step4.png\" alt=\"step4\">
                  </center>
                  <p>463 Productos certificados</p>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">
                  <select name=\"\" id=\"selectAgregados\">
                    <option value=\"#\">RECIÉN AGREGADOS</option>
                    <option value=\"#\">HACE UN MES</option>
                    <option value=\"#\">HACE UNA SEMANA</option>
                    <option value=\"#\">HACE UN DÍA</option>
                  </select>
                </div>
                <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">
                  <nav aria-label=\"Page navigation\" id=\"navPagination\">
                    <ul class=\"pagination\">
                      <li>
                        <a href=\"#\" aria-label=\"Previous\">
                          <span aria-hidden=\"true\"><</span>
                        </a>
                      </li>
                      <li><a href=\"#\">1</a></li>
                      <li><a href=\"#\">2</a></li>
                      <li><a href=\"#\">3</a></li>
                      <li><a href=\"#\">4</a></li>
                      <li>
                        <a href=\"#\" aria-label=\"Next\">
                          <span aria-hidden=\"true\">></span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                  <div class=\"form-group inputPagina\">
                    <label for=\"\">PÁGINA</label>
                    <input type=\"text\" value=\"1\">
                  </div>
                  <div class=\"form-group inputPagina\">
                    <label for=\"\">VER</label>
                    <input type=\"text\" value=\"48\">
                    <input type=\"text\" value=\"96\">
                  </div>
                </div>
            </div>
            

            <div class=\"row topCarrousel\">
              <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
              \t<p class=\"titleGroups\">Tecnología Hip</p>
                <div id=\"TecnologiaHip\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t        <!-- indicators -->
                  <!-- <ol class=\"carousel-indicators\">
                    <li data-target=\"#TecnologiaHip\" data-slide-to=\"0\" class=\"active\"></li>
                    <li data-target=\"#TecnologiaHip\" data-slide-to=\"1\"></li>
                  </ol> -->
\t\t\t\t          <!-- carrousel -->
                  <div class=\"carousel-inner\" role=\"listbox\">
                    <div class=\"item active\">
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 79
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselTwo/slide1.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Anki Kozmo</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 94
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 111
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 116
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Anki Kozmo</p>
                          <p>Tecnología I Niños</p>
                          <p>\$ 43.80 USD I 100 pz min.</p>
                          <p>China Muestra Express Personalizable</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 128
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselTwo/slide2.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>XYZ Printing da Vinci Mini 3D Printer</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 143
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 148
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 165
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>XYZ Printing da Vinci Mini 3D Printer</p>
                          <p>Impresión I Tecnología</p>
                          <p>\$ 218.24 EUR I 50 pz min.</p>
                          <p>Holanda Inventario</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 177
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselTwo/slide3.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Bocina Mass Fidelity Core</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 192
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 197
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 209
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 214
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Bocina Mass Fidelity Core</p>
                          <p>Audio I Electronicos</p>
                          <p>\$ 725.72 MXN I 50 pz min.</p>
                          <p>China Personalizable</p>
                        </div>
                      </div>
                    </div>

                  </div>
\t\t\t\t          <!-- controls -->
                  <!-- <a class=\"left carousel-control\" href=\"#TecnologiaHip\" role=\"button\" data-slide=\"prev\">
                    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                  </a>
                  <a class=\"right carousel-control\" href=\"#TecnologiaHip\" role=\"button\" data-slide=\"next\">
                    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span>
                  </a> -->
                </div>
              </div>
            </div>

            <div class=\"row topCarrousel\">
              <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
              \t<p class=\"titleGroups\">Decor</p>
                <div id=\"Decor\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t        <!-- indicators -->
                  <!-- <ol class=\"carousel-indicators\">
                    <li data-target=\"#Decor\" data-slide-to=\"0\" class=\"active\"></li>
                    <li data-target=\"#Decor\" data-slide-to=\"1\"></li>
                  </ol> -->
\t\t\t\t          <!-- carrousel -->
                  <div class=\"carousel-inner\" role=\"listbox\">
                    <div class=\"item active\">
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 256
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide1.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 271
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 276
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 288
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 293
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Banca Orgánica Madera</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 4,190.00 MXN I 50 pz min.</p>
                          <p>México Personalizable</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 305
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide2.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 320
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 325
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 337
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 342
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Mesa Geometrica de Acero</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 2,764.00 MXN I 10 pz min.</p>
                          <p>México Inventario</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 354
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide3.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 369
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 374
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 386
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 391
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Silla armada en 2 piezas madera</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 3,899.00 MXN I 80 pz min.</p>
                          <p>México</p>
                        </div>
                      </div>
                    </div>

                  </div>
\t\t\t\t          <!-- controls -->
                  <!-- <a class=\"left carousel-control\" href=\"#Decor\" role=\"button\" data-slide=\"prev\">
                    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                  </a>
                  <a class=\"right carousel-control\" href=\"#Decor\" role=\"button\" data-slide=\"next\">
                    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span> -->
                  </a>
                </div>
              </div>
            </div>

            <div class=\"row topCarrousel\">
              <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                <p class=\"titleGroups\">Decor</p>
                <div id=\"Decor\" class=\"carousel slide\" data-ride=\"carousel\">
                <!-- indicators -->
                  <!-- <ol class=\"carousel-indicators\">
                    <li data-target=\"#Decor\" data-slide-to=\"0\" class=\"active\"></li>
                    <li data-target=\"#Decor\" data-slide-to=\"1\"></li>
                  </ol> -->
                  <!-- carrousel -->
                  <div class=\"carousel-inner\" role=\"listbox\">
                    <div class=\"item active\">
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 433
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide1.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 448
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 453
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 465
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 470
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Banca Orgánica Madera</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 4,190.00 MXN I 50 pz min.</p>
                          <p>México Personalizable</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 482
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide2.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 497
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 502
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 514
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 519
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Mesa Geometrica de Acero</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 2,764.00 MXN I 10 pz min.</p>
                          <p>México Inventario</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 531
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide3.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 546
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 551
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 563
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 568
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Silla armada en 2 piezas madera</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 3,899.00 MXN I 80 pz min.</p>
                          <p>México</p>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- controls -->
                  <!-- <a class=\"left carousel-control\" href=\"#Decor\" role=\"button\" data-slide=\"prev\">
                    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                  </a>
                  <a class=\"right carousel-control\" href=\"#Decor\" role=\"button\" data-slide=\"next\">
                    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span>
                  </a> -->
                </div>
              </div>
            </div>


            <br><br><br>
            <div class=\"row\">
                <div class=\"hidden-xs col-sm-6 col-md-6 col-lg-6\">
                </div>
                <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">
                  <nav aria-label=\"Page navigation\" id=\"navPagination\">
                    <ul class=\"pagination\">
                      <li>
                        <a href=\"#\" aria-label=\"Previous\">
                          <span aria-hidden=\"true\"><</span>
                        </a>
                      </li>
                      <li><a href=\"#\">1</a></li>
                      <li><a href=\"#\">2</a></li>
                      <li><a href=\"#\">3</a></li>
                      <li><a href=\"#\">4</a></li>
                      <li>
                        <a href=\"#\" aria-label=\"Next\">
                          <span aria-hidden=\"true\">></span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                  <div class=\"form-group inputPagina\">
                    <label for=\"\">PÁGINA</label>
                    <input type=\"text\" value=\"1\">
                  </div>
                  <div class=\"form-group inputPagina\">
                    <label for=\"\">VER</label>
                    <input type=\"text\" value=\"48\">
                    <input type=\"text\" value=\"96\">
                  </div>
                </div>
            </div>


          </div>
        </div>
      </div>
      <!-- End contend --> 

      <!-- Pre-Footer -->
      <div class=\"row anchoRow\">
        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
          <center>
            <div id=\"top-footer\">
              <p>
              Selecciona los productos que quieras para solicitar un pedido <br>Tame Traffic no es una tienda en linea
              </p>
              <a href=\"#\">¿Como se compra en Tame Traffic?</a><br>
              <a href=\"#\">¿Como selecciono productos?</a><br>
              <a href=\"#\">¿Como se hace un pedido?</a>
            </div>
          </center>
        </div>
      </div>

    </div>
    <!-- End box content -->
";
    }

    public function getTemplateName()
    {
        return "shop/catalogo-full.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  745 => 568,  737 => 563,  722 => 551,  714 => 546,  696 => 531,  681 => 519,  673 => 514,  658 => 502,  650 => 497,  632 => 482,  617 => 470,  609 => 465,  594 => 453,  586 => 448,  568 => 433,  523 => 391,  515 => 386,  500 => 374,  492 => 369,  474 => 354,  459 => 342,  451 => 337,  436 => 325,  428 => 320,  410 => 305,  395 => 293,  387 => 288,  372 => 276,  364 => 271,  346 => 256,  301 => 214,  293 => 209,  278 => 197,  270 => 192,  252 => 177,  237 => 165,  229 => 160,  214 => 148,  206 => 143,  188 => 128,  173 => 116,  165 => 111,  150 => 99,  142 => 94,  124 => 79,  59 => 17,  55 => 16,  51 => 15,  47 => 14,  43 => 13,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-shop.html' %}*/
/* {% block content %}*/
/*    <div id="boxContent">*/
/*     <span id="btnSidenav" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>*/
/* */
/*       <!-- Content -->*/
/*       <div class="container-fluid">*/
/*         <div class="row">*/
/*           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*             <div class="row">*/
/*                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 productCert">*/
/*                   <center>*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step1.png" alt="step1">*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step2.png" alt="step2">*/
/*                     <img id="iconStepTT" src="{{ base_url() }}public/assets/img/stepTT.png" alt="stepTT">*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step3.png" alt="step3">*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step4.png" alt="step4">*/
/*                   </center>*/
/*                   <p>463 Productos certificados</p>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="row">*/
/*                 <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">*/
/*                   <select name="" id="selectAgregados">*/
/*                     <option value="#">RECIÉN AGREGADOS</option>*/
/*                     <option value="#">HACE UN MES</option>*/
/*                     <option value="#">HACE UNA SEMANA</option>*/
/*                     <option value="#">HACE UN DÍA</option>*/
/*                   </select>*/
/*                 </div>*/
/*                 <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">*/
/*                   <nav aria-label="Page navigation" id="navPagination">*/
/*                     <ul class="pagination">*/
/*                       <li>*/
/*                         <a href="#" aria-label="Previous">*/
/*                           <span aria-hidden="true"><</span>*/
/*                         </a>*/
/*                       </li>*/
/*                       <li><a href="#">1</a></li>*/
/*                       <li><a href="#">2</a></li>*/
/*                       <li><a href="#">3</a></li>*/
/*                       <li><a href="#">4</a></li>*/
/*                       <li>*/
/*                         <a href="#" aria-label="Next">*/
/*                           <span aria-hidden="true">></span>*/
/*                         </a>*/
/*                       </li>*/
/*                     </ul>*/
/*                   </nav>*/
/*                   <div class="form-group inputPagina">*/
/*                     <label for="">PÁGINA</label>*/
/*                     <input type="text" value="1">*/
/*                   </div>*/
/*                   <div class="form-group inputPagina">*/
/*                     <label for="">VER</label>*/
/*                     <input type="text" value="48">*/
/*                     <input type="text" value="96">*/
/*                   </div>*/
/*                 </div>*/
/*             </div>*/
/*             */
/* */
/*             <div class="row topCarrousel">*/
/*               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*               	<p class="titleGroups">Tecnología Hip</p>*/
/*                 <div id="TecnologiaHip" class="carousel slide" data-ride="carousel">*/
/* 				        <!-- indicators -->*/
/*                   <!-- <ol class="carousel-indicators">*/
/*                     <li data-target="#TecnologiaHip" data-slide-to="0" class="active"></li>*/
/*                     <li data-target="#TecnologiaHip" data-slide-to="1"></li>*/
/*                   </ol> -->*/
/* 				          <!-- carrousel -->*/
/*                   <div class="carousel-inner" role="listbox">*/
/*                     <div class="item active">*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselTwo/slide1.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Anki Kozmo</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Anki Kozmo</p>*/
/*                           <p>Tecnología I Niños</p>*/
/*                           <p>$ 43.80 USD I 100 pz min.</p>*/
/*                           <p>China Muestra Express Personalizable</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselTwo/slide2.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>XYZ Printing da Vinci Mini 3D Printer</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>XYZ Printing da Vinci Mini 3D Printer</p>*/
/*                           <p>Impresión I Tecnología</p>*/
/*                           <p>$ 218.24 EUR I 50 pz min.</p>*/
/*                           <p>Holanda Inventario</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselTwo/slide3.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Bocina Mass Fidelity Core</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Bocina Mass Fidelity Core</p>*/
/*                           <p>Audio I Electronicos</p>*/
/*                           <p>$ 725.72 MXN I 50 pz min.</p>*/
/*                           <p>China Personalizable</p>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/* */
/*                   </div>*/
/* 				          <!-- controls -->*/
/*                   <!-- <a class="left carousel-control" href="#TecnologiaHip" role="button" data-slide="prev">*/
/*                     <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Previous</span>*/
/*                   </a>*/
/*                   <a class="right carousel-control" href="#TecnologiaHip" role="button" data-slide="next">*/
/*                     <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Next</span>*/
/*                   </a> -->*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/* */
/*             <div class="row topCarrousel">*/
/*               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*               	<p class="titleGroups">Decor</p>*/
/*                 <div id="Decor" class="carousel slide" data-ride="carousel">*/
/* 				        <!-- indicators -->*/
/*                   <!-- <ol class="carousel-indicators">*/
/*                     <li data-target="#Decor" data-slide-to="0" class="active"></li>*/
/*                     <li data-target="#Decor" data-slide-to="1"></li>*/
/*                   </ol> -->*/
/* 				          <!-- carrousel -->*/
/*                   <div class="carousel-inner" role="listbox">*/
/*                     <div class="item active">*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide1.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Banca Orgánica Madera</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 4,190.00 MXN I 50 pz min.</p>*/
/*                           <p>México Personalizable</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide2.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Mesa Geometrica de Acero</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 2,764.00 MXN I 10 pz min.</p>*/
/*                           <p>México Inventario</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide3.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Silla armada en 2 piezas madera</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 3,899.00 MXN I 80 pz min.</p>*/
/*                           <p>México</p>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/* */
/*                   </div>*/
/* 				          <!-- controls -->*/
/*                   <!-- <a class="left carousel-control" href="#Decor" role="button" data-slide="prev">*/
/*                     <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Previous</span>*/
/*                   </a>*/
/*                   <a class="right carousel-control" href="#Decor" role="button" data-slide="next">*/
/*                     <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Next</span> -->*/
/*                   </a>*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/* */
/*             <div class="row topCarrousel">*/
/*               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                 <p class="titleGroups">Decor</p>*/
/*                 <div id="Decor" class="carousel slide" data-ride="carousel">*/
/*                 <!-- indicators -->*/
/*                   <!-- <ol class="carousel-indicators">*/
/*                     <li data-target="#Decor" data-slide-to="0" class="active"></li>*/
/*                     <li data-target="#Decor" data-slide-to="1"></li>*/
/*                   </ol> -->*/
/*                   <!-- carrousel -->*/
/*                   <div class="carousel-inner" role="listbox">*/
/*                     <div class="item active">*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide1.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Banca Orgánica Madera</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 4,190.00 MXN I 50 pz min.</p>*/
/*                           <p>México Personalizable</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide2.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Mesa Geometrica de Acero</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 2,764.00 MXN I 10 pz min.</p>*/
/*                           <p>México Inventario</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide3.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Silla armada en 2 piezas madera</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 3,899.00 MXN I 80 pz min.</p>*/
/*                           <p>México</p>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/* */
/*                   </div>*/
/*                   <!-- controls -->*/
/*                   <!-- <a class="left carousel-control" href="#Decor" role="button" data-slide="prev">*/
/*                     <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Previous</span>*/
/*                   </a>*/
/*                   <a class="right carousel-control" href="#Decor" role="button" data-slide="next">*/
/*                     <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Next</span>*/
/*                   </a> -->*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/* */
/* */
/*             <br><br><br>*/
/*             <div class="row">*/
/*                 <div class="hidden-xs col-sm-6 col-md-6 col-lg-6">*/
/*                 </div>*/
/*                 <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">*/
/*                   <nav aria-label="Page navigation" id="navPagination">*/
/*                     <ul class="pagination">*/
/*                       <li>*/
/*                         <a href="#" aria-label="Previous">*/
/*                           <span aria-hidden="true"><</span>*/
/*                         </a>*/
/*                       </li>*/
/*                       <li><a href="#">1</a></li>*/
/*                       <li><a href="#">2</a></li>*/
/*                       <li><a href="#">3</a></li>*/
/*                       <li><a href="#">4</a></li>*/
/*                       <li>*/
/*                         <a href="#" aria-label="Next">*/
/*                           <span aria-hidden="true">></span>*/
/*                         </a>*/
/*                       </li>*/
/*                     </ul>*/
/*                   </nav>*/
/*                   <div class="form-group inputPagina">*/
/*                     <label for="">PÁGINA</label>*/
/*                     <input type="text" value="1">*/
/*                   </div>*/
/*                   <div class="form-group inputPagina">*/
/*                     <label for="">VER</label>*/
/*                     <input type="text" value="48">*/
/*                     <input type="text" value="96">*/
/*                   </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/* */
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <!-- End contend --> */
/* */
/*       <!-- Pre-Footer -->*/
/*       <div class="row anchoRow">*/
/*         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*           <center>*/
/*             <div id="top-footer">*/
/*               <p>*/
/*               Selecciona los productos que quieras para solicitar un pedido <br>Tame Traffic no es una tienda en linea*/
/*               </p>*/
/*               <a href="#">¿Como se compra en Tame Traffic?</a><br>*/
/*               <a href="#">¿Como selecciono productos?</a><br>*/
/*               <a href="#">¿Como se hace un pedido?</a>*/
/*             </div>*/
/*           </center>*/
/*         </div>*/
/*       </div>*/
/* */
/*     </div>*/
/*     <!-- End box content -->*/
/* {% endblock %}*/
