<?php

/* dashboard/admin/supplier-products.html */
class __TwigTemplate_0174a3d4ae9a3ecdc7216197e9b4c8a7bfa95f6d3f7c0d9c85c378246d7847ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/supplier-products.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-12 text-center margin-bottom--30\">
            <h1 class=\"title-dashboard\">
                Proveedor
            </h1>
            <h3>
                <h3 class=\"text-center\">
                    ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "number_products", array()), "html", null, true);
        echo "
                    ";
        // line 21
        if (($this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "number_products", array()) > 1)) {
            // line 22
            echo "                        Productos
                    ";
        } else {
            // line 24
            echo "                        Producto
                    ";
        }
        // line 26
        echo "                </h3>
            </h3>
        </div>
        <div class=\"col-sm-6 text-right\">

        </div>
        <div class=\"col-sm-12\">
            <h1 class=\"margin-clear\">
                ";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "name", array()), "html", null, true);
        echo "
            </h1>
            <p class=\"text-muted\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "description", array()), "html", null, true);
        echo "</p>
                <a class=\"btn btn-primary pull-left arrow-toggle\" role=\"button\" data-toggle=\"collapse\" href=\"#supplierInfo\" aria-expanded=\"false\" aria-controls=\"supplierInfo\">
                    más información
                </a>\t
            <br><br>   
            <div class=\"collapse\" id=\"supplierInfo\">
                    <b>Razón Social: </b> ";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "legal_name", array()), "html", null, true);
        echo "
                    <br>
                    <b>Supplier ID: </b> ";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idcustome", array()), "html", null, true);
        echo "
                    <br>
                    <b>Fecha de Admisión: </b> ";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "registered", array()), "html", null, true);
        echo "
                    <br>
                    <b>Categoría /Giro: </b> ";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "category", array()), "html", null, true);
        echo "
                    <br>
                    <b>Dirección Oficina: </b> ";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_address", array()), "html", null, true);
        echo "
                    <br>
                    <b>Dirección Bodega: </b> ";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_warehouse", array()), "html", null, true);
        echo "
                
                <h3> Contacto</h3>
                <p>
                    <b>Persona Asignada: </b> ";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee", array()), "html", null, true);
        echo "
                    <br>
                    <b>Teléfono Oficina: </b> ";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_phone", array()), "html", null, true);
        echo "
                    <br>
                    <b>Teléfono Móvil: </b> ";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_mobile", array()), "html", null, true);
        echo "
                    <br>
                    <b>Email: </b> <a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_email", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_email", array()), "html", null, true);
        echo "</a>
                </p>
                <p>
                    <b>Persona Alto Mando: </b> ";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager", array()), "html", null, true);
        echo "
                    <br>
                    <b>Teléfono Oficina: </b> ";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_phone", array()), "html", null, true);
        echo "
                    <br>
                    <b>Teléfono Móvil: </b> ";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_mobile", array()), "html", null, true);
        echo "
                    <br>
                    <b>Email: </b> <a href=\"mailto:";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_email", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_email", array()), "html", null, true);
        echo "</a>
                </p>
                <button class=\"btn btn-warning\" data-toggle=\"modal\" data-target=\"#editDelete\">
                    Modificar proveedor
                </button>
                <button class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#modalDelete\">
                    Eliminar proveedor
                </button>
            </div>
            <hr>
        </div>
            <div class=\"col-sm-12 text-right\">
                <div class=\"row margin-bottom--30\">
                    <div class=\"col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4\">
                        <form method=\"GET\" action=\"";
        // line 85
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/products/";
        echo twig_escape_filter($this->env, (isset($context["idSupplier"]) ? $context["idSupplier"] : null), "html", null, true);
        echo "\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"q\" class=\"form-control\" placeholder=\"Buscar todo\">
                                <span class=\"input-group-btn\">
                                    <button class=\"btn btn-default\" type=\"submit\">
                                        <i class=\"fa fa-search\"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                        <select name=\"category\" id=\"category\" class=\"form-control\">
                            <option value=\"\">Categoría</option>
                            ";
        // line 101
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 102
            echo "                                <option data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "slug", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "                        </select>
                    </div>                  
                    <!-- <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                        <select name=\"\" id=\"\" class=\"form-control\">
                            <option value=\"\">Categoría</option>
                            <option value=\"\">...</option>
                        </select>
                    </div> -->
                    <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                        <select name=\"\" id=\"subcategory\" class=\"form-control\">
                            <option value=\"\">Subcategoría</option>
                            <option value=\"\">...</option>
                        </select>
                    </div>
                    <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                        <select name=\"\" id=\"\" class=\"form-control\">
                            <option value=\"\">Sin definir</option>
                        </select>
                    </div>
                    <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
                        <select name=\"\" id=\"\" class=\"form-control\">
                            <option value=\"\">Sin definir</option>
                        </select>
                    </div>
                </div>
            </div>
        <div class=\"col-sm-12 margin-bottom--20\">
            <button class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#addProduct\">Agregar producto</button>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"row\">
                <div class=\"col-md-4 col-md-offset-4 text-center\">
                    <div>
                        ";
        // line 137
        if (array_key_exists("totalQ", $context)) {
            // line 138
            echo "                            ";
            echo twig_escape_filter($this->env, (isset($context["totalQ"]) ? $context["totalQ"] : null), "html", null, true);
            echo " 
                        ";
        }
        // line 139
        echo "  
                    </div>                     
                </div>
            </div>
            <div class=\"row\">
                ";
        // line 144
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 145
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "dashboard/admin/catalog/product/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "idProduct", array()), "html", null, true);
            echo "\">
                <div class=\"col-sm-12 col-lg-4\">
                    <div class=\"row\">
                        <!--
                        <div class=\"col-sm-6\" style=\"background: url(";
            // line 149
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo ".jpg) no-repeat center; background-size: cover\"></div>
                        -->
                        <div class=\"col-sm-12 text-center\">
                            <div class=\"panel panel-default\">
                                <div class=\"panel-heading\" style=\"position: relative;background: url(";
            // line 153
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo ".jpg) no-repeat center; background-size: cover; height: 200px\">
                                    <div style=\"position: absolute; width: 100%; height: 100%; top: 0; left: 0; background: rgba(0, 0, 0, 0.4)\">

                                    </div>
                                    <div style=\"position: relative\">
                                        <p class=\"h3 text-uppercase color-white\">";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</p>
                                        <p class=\"color-white\">";
            // line 159
            echo twig_escape_filter($this->env, character_limiter($this->getAttribute($context["product"], "description", array()), 100));
            echo "...</p>
                                    </div>
                                </div>
                                <div class=\"panel-body panel-product\">
                                    <p class=\"h2 margin-clear\">";
            // line 163
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "score", array()), "html", null, true);
            echo "</p>
                                    <hr>
                                    <div class=\"row margin-bottom--20\">
                                        <div class=\"col-sm-12 col-md-6\">
                                            <b>ITEM</b><br>
                                            ";
            // line 168
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo "
                                        </div>
                                        <div class=\"col-sm-12 col-md-6\">
                                            <b>SUPPLIER</b><br>
                                           ";
            // line 172
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idcustome", array()), "html", null, true);
            echo "
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-sm-12 col-md-6\">
                                            <i class=\"fa fa-thumbs-up fa-lg\"></i> 42
                                        </div>
                                        <div class=\"col-sm-12 col-md-6\">
                                            <i class=\"fa fa-truck fa-lg\"></i> 8
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </a>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 190
        echo "            </div>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"editDelete\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalDelete\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <br>
                <h3> Editar Proveedor</h3>
                <br>
                <form id=\"editSupplier\" method=\"POST\" >
                <input type=\"hidden\" name=\"secretKey\" value=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idSupplier", array()), "html", null, true);
        echo "\">
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Proveedor: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-name\" value=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "name", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Descripción: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-description\" value=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "description", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Razón Social: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-razon\" value=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "legal_name", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Supplier ID: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"\" value=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idcustome", array()), "html", null, true);
        echo "\" disabled>
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Fecha de Admisión: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"\" value=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "registered", array()), "html", null, true);
        echo "\" disabled>
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Categoría /Giro: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-giro\" value=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "category", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Dirección Oficina: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-oficina\" value=\"";
        // line 242
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_address", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Dirección Bodega: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-bodega\" value=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_warehouse", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <br>
                <h3> Contacto</h3>
                <br>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Persona Asignada: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-asignadaname\" value=\"";
        // line 257
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Teléfono Oficina: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-asignadaoficina\" value=\"";
        // line 263
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_phone", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Teléfono Móvil: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-asignadamovil\" value=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_mobile", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Email: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-asignadaemail\" value=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_email", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <br>
                <br>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Persona Alto Mando: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-altoname\" value=\"";
        // line 283
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Teléfono Oficina: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-altooficina\" value=\"";
        // line 289
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_phone", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Teléfono Móvil: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-altomovil\" value=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_mobile", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                <div class=\"form-group row\">
                    <label class=\"col-md-2 col-form-label\">Email: </label>
                    <div class=\"col-md-10\">
                        <input type=\"text\" class=\"form-control\" name=\"p-altoemail\" value=\"";
        // line 301
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_email", array()), "html", null, true);
        echo "\">
                    </div>
                </div>
                
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                <button type=\"submit\" class=\"btn btn-danger\">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"modalDelete\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalDelete\">
    <div class=\"modal-dialog modal-sm\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <h3 class=\"modal-title\">Confirmación</h3>
                <p>Se eliminara el proveedor: <br> <b>";
        // line 319
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "name", array()), "html", null, true);
        echo "</b></p>
                <p class=\"text-danger small\">*Se borraran sus productos asociados</p>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                <a href=\"";
        // line 324
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/delete/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idSupplier", array()), "html", null, true);
        echo "\" class=\"btn btn-danger\">Confirmar</a>
            </div>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"addProduct\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Agregar nuevo producto</h4>
            </div>
            <div class=\"modal-body\">
                <form id=\"formAddProduct\" action=\"\" enctype=\"multipart/form-data\" class=\"row\">

                    <div class=\"form-group col-sm-6\">
                        <label for=\"pname\">Nombre</label>
                        <input id=\"pname\" name=\"pname\" type=\"text\" class=\"form-control\" >
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pscore\">Calificación</label>
                        <input id=\"pscore\" name=\"pscore\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pheader\">Header</label>
                        <textarea id=\"pheader\" rows=\"3\" name=\"pheader\" type=\"text\" class=\"form-control\"></textarea>
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdescription\">Descripción</label>
                        <textarea id=\"pdescription\" rows=\"3\" name=\"pdescription\" type=\"text\" class=\"form-control\"></textarea>
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pitem\">Item</label>
                        <input id=\"pitem\" name=\"pitem\" type=\"text\" class=\"form-control\" required>
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcategory\">Categoría</label>
                        <!--<input id=\"pcategory\" name=\"pcategory\" type=\"text\" class=\"form-control\">-->

                        <select name=\"category\" id=\"categoryAdd\" class=\"form-control\" required>
                            <option value=\"\" disabled selected>Categoría</option>
                            ";
        // line 365
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 366
            echo "                                <option data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "slug", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 368
        echo "                        </select>


                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"psubcat\">Subcategoría</label>
                        <!--<input id=\"psubcat\" name=\"psubcat\" type=\"text\" class=\"form-control\">-->

                        <select name=\"\" id=\"subcategoryAdd\" class=\"form-control\" required>
                            <option value=\"\" disabled selected>Subcategoría</option>
                            <option value=\"\">...</option>
                        </select>
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pquality\">Calidad</label>
                        <input id=\"pquality\" name=\"pquality\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"porigin\">Origen</label>
                        <input id=\"porigin\" name=\"porigin\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcolor\">Colores</label>
                        <input id=\"pcolor\" name=\"pcolor\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppresentation\">Presentación</label>
                        <input id=\"ppresentation\" name=\"ppresentation\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcharacteristics\">Caracteristicas</label>
                        <input id=\"pcharacteristics\" name=\"pcharacteristics\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pspecifications\">Especificaciones</label>
                        <input id=\"pspecifications\" name=\"pspecifications\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdimensions\">Dimensiones</label>
                        <input id=\"pdimensions\" name=\"pdimensions\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pweight\">Peso</label>
                        <input id=\"pweight\" name=\"pweight\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pminimumOrder\">Minimo de Pedido</label>
                        <input id=\"pminimumOrder\" name=\"pminimumOrder\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdateExpiry\">Fecha de Caducidad</label>
                        <input id=\"pdateExpiry\" name=\"pdateExpiry\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pinvNumPieces\">Inventario No. de Piezas</label>
                        <input id=\"pinvNumPieces\" name=\"pinvNumPieces\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pboutiqueMaxOrder\">Máximo de Pedido</label>
                        <input id=\"pboutiqueMaxOrder\" name=\"pboutiqueMaxOrder\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceWithoutTax\">Precio sin impuesto</label>
                        <input id=\"ppriceWithoutTax\" name=\"ppriceWithoutTax\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceCurrency\">Moneda</label>
                        <input id=\"ppriceCurrency\" name=\"ppriceCurrency\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceSuggested\">Precio Sugerido</label>
                        <input id=\"ppriceSuggested\" name=\"ppriceSuggested\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceTT\">Precio TT</label>
                        <input id=\"ppriceTT\" name=\"ppriceTT\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingNumPieces\">Empaque - No. de Piezas</label>
                        <input id=\"ppackingNumPieces\" name=\"ppackingNumPieces\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingSpecifications\">Empaque - Especifiaciones</label>
                        <input id=\"ppackingSpecifications\" name=\"ppackingSpecifications\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingHeight\">Empaque - Altura</label>
                        <input id=\"ppackingHeight\" name=\"ppackingHeight\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingWidth\">Empaque - Ancho</label>
                        <input id=\"ppackingWidth\" name=\"ppackingWidth\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingLarge\">Empaque - Largo</label>
                        <input id=\"ppackingLarge\" name=\"ppackingLarge\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingWeight\">Empaque - Peso</label>
                        <input id=\"ppackingWeight\" name=\"ppackingWeight\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"plogTimeProduction\">Logistica - Tiempo de producción</label>
                        <input id=\"plogTimeProduction\" name=\"pLogTimeProduction\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"plogFob\">Logistica - FOB</label>
                        <input id=\"plogFob\" name=\"pLogFob\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"plogFobDeliveryTime\">Logistica - FOB Tiempo de entrega</label>
                        <input id=\"plogFobDeliveryTime\" name=\"pLogFobDeliveryTime\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"psample\">Muestra</label>
                        <input id=\"psample\" name=\"psample\" type=\"text\" class=\"form-control\">
                    </div>
                   <!-- <div class=\"form-group col-sm-12\">
                        <div class=\"row\">
                            <div class=\"form-group col-sm-6\">
                                <h3><b>Galería</b></h3>
                                <label for=\"insertImages\"><b>Agregar imágenes</b></label>
                            
                                <style></style>
                                <div class=\"fileUpload btn btn-blue\">
                                    <span>SELECCIONAR ARCHIVO</span>  
                                 
                                    <input name=\"insertImages\" type=\"file\" class=\"handle-new upload\" accept=\"image/jpeg\" style=\"padding-left: 0;\" id=\"getsIm\">                                 
                                </div>                              
                                <button type=\"button\" class=\"btn btn-primary save-img\">AGREGAR</button>
                            </div>
                        </div> 
                    </div>-->
                    <div class=\"row contain-images\">
                        <div class=\"col-lg-12\">
                            <div class=\"row\">
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group col-sm-12\">
                        <input id=\"pid\" type=\"hidden\" name=\"pid\">
                        <input id=\"supplierid\" type=\"hidden\" name=\"supplierid\" value=\"";
        // line 509
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idSupplier", array()), "html", null, true);
        echo "\">
                        <input id=\"userid\" type=\"hidden\" name=\"supplier_id\" value=\"1\">
                        
                    </div>
                
            </div>
            <div class=\"modal-footer\">
                <button type=\"submit\" class=\"btn btn-success\">Guardar</button>
                <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 524
    public function block_script($context, array $blocks = array())
    {
        // line 525
        echo "<script>
    \$(function() {

        \$('.save-img').on('click',function(){                                                                          
            console.log('has agregado una imagen nueva');
            var fil = document.getElementById('getsIm');

            addImg(fil);

                function addImg(file){
                    if (file.files && file.files[0]) {
                        if(file.files[0].type == \"image/jpeg\")
                        {
                            var reader = new FileReader();
                            
                            reader.onload = function (e) {
                            var container = \$('.contain-images>div>div');
                            
                            var div1 = document.createElement('div');
                                div1.setAttribute('class','form-group col-sm-9');
                          
                            var img = document.createElement('img');
                                img.setAttribute('width','250px');
                                img.setAttribute('class','preview');
                                img.setAttribute('src',e.target.result);
                                
                                console.log(\"*****\");
                                jQuery(div1).append(img);
                                jQuery(container).append(div1); 
                                console.log(\"*****\");                    

                            }
                        }
                        else{
                            swal(
                              '',
                              'Imagen inválida, extensíon no permitida',
                              'error'
                            )                   
                            
                        }

                        reader.readAsDataURL(file.files[0]);
                    }
                    else{
                        swal(
                          '',
                          'Por favor selecciona un archivo válido.',
                          'error'
                        )               
                        
                    }           
                }
        });


        \$('#myCarousel').carousel({
            interval: 5000
        });

        //Handles the carousel thumbnails
        \$('[id^=carousel-selector-]').click(function () {
            var id_selector = \$(this).attr(\"id\");
            try {
                var id = /-(\\d+)\$/.exec(id_selector)[1];
                console.log(id_selector, id);
                jQuery('#myCarousel').carousel(parseInt(id));
            } catch (e) {
                console.log('Regex failed!', e);
            }
        });
        // When the carousel slides, auto update the text
        \$('#myCarousel').on('slid.bs.carousel', function (e) {
            var id = \$('.item.active').data('slide-number');
            \$('#carousel-text').html(\$('#slide-content-'+id).html());
        });
        
        \$('#formAddProduct').on('submit', function (e) {
        e.preventDefault();

        var btn = \$(this).find('button');

        var id = \$('#pid').val();
        var row = \$('#rowid').val();
        var pName = \$(\"#pname\").val();
        var pScore = \$(\"#pscore\").val();
        var pHeader = \$(\"#pheader\").val();
        var pDescription = \$(\"#pdescription\").val();
        var pItem = \$('#pitem').val();
        var pCategory = \$('#categoryAdd option:selected').val();
        var pSubcategory = \$('#subcategoryAdd option:selected').val();
        var pQuality = \$('#pquality').val();
        var pOrigin = \$('#porigin').val();
        var pColors = \$('#pcolor').val();
        var pPresentation = \$('#ppresentation').val();
        var pCharacteristics = \$('#pcharacteristics').val();
        var pEspecifications = \$('#pspecifications').val();
        var pDimensions = \$('#pdimensions').val();
        var pWeight = \$('#pweight').val();
        var pMinimumOrder = \$('#pminimumOrder').val();
        var pDateExpiry = \$('#pdateExpiry').val();
        var pInvNumPieces = \$('#pinvNumPieces').val();
        var pBoutiqueMaxOrder = \$('#pboutiqueMaxOrder').val();
        var pPriceWithoutTax = \$('#ppriceWithoutTax').val();
        var pPriceCurrency = \$('#ppriceCurrency').val();
        var pPriceSuggested = \$('#ppriceSuggested').val();
        var pPriceTT = \$('#ppriceTT').val();
        var pPackingNumPieces = \$('#ppackingNumPieces').val();
        var pPackingEspecifications = \$('#ppackingSpecifications').val();
        var pPackingHeight = \$('#ppackingHeight').val();
        var pPackingWidth = \$('#ppackingWidth').val();
        var pPackingLarge = \$('#ppackingLarge').val();
        var pPackingWeight = \$('#ppackingWeight').val();
        var pPackingLogProductionTime = \$('#plogTimeProduction').val();
        var pLogFob = \$('#plogFob').val();
        var pLogFobDeliveryTime = \$('#plogFobDeliveryTime').val();
        var pSample = \$('#psample').val();
        var sId = \$('#supplierid').val();
        var uId = \$('#userid').val();

         console.log(id)

        \$.ajax({
            url: '";
        // line 648
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/add',
            type: 'post',
            dataType: 'html',
            cache: false,
            data: {
                pId : id,
                pName : pName,
                pScore : pScore,
                pHeader : pHeader,
                pDescription : pDescription,
                pItem : pItem,
                pCategory : pCategory,
                pSubcategory : pSubcategory,
                pQuality : pQuality,
                pOrigin : pOrigin,
                pColors : pColors,
                pPresentation : pPresentation,
                pCharacteristics : pCharacteristics,
                pSpecifications : pEspecifications,
                pDimensions : pDimensions,
                pWeight : pWeight,
                pMinimumOrder : pMinimumOrder,
                pDateExpiry : pDateExpiry,
                pInvNumPieces : pInvNumPieces,
                pBoutiqueMaxOrder : pBoutiqueMaxOrder,
                pPriceWithoutTax : pPriceWithoutTax,
                pPriceCurrency : pPriceCurrency,
                pPriceSuggested : pPriceSuggested,
                pPriceTT : pPriceTT,
                pPackingNumPieces : pPackingNumPieces,
                pPackingSpecifications : pPackingEspecifications,
                pPackingHeight : pPackingHeight,
                pPackingWidth : pPackingWidth,
                pPackingLarge : pPackingLarge,
                pPackingWeight : pPackingWeight,
                pLogProductionTime : pPackingLogProductionTime,
                pLogFob : pLogFob,
                pLogFobDeliveryTime : pLogFobDeliveryTime,
                pSample : pSample,
                supplierid : sId,
                supplier_id: uId
            },
            beforeSend: function() {
                btn.prop('disabled', true)
            },
            success: function(response) {
                console.log(response);
                if (response == true) {

                    swal({
                      title: 'Producto agregado!',
                      text: 'Un nuevo producto ha sigo agregado a este proveedor.',
                      timer: 2000
                    }).then(
                      function () {
                        location.reload();
                      },
                      // handling the promise rejection
                      function (dismiss) {
                        if (dismiss === 'timer') {
                          location.reload();
                        }
                      }
                    )
                } else {

                    swal({
                      title: 'Fallo!',
                      text: 'Proporciona los campos requeridos para subir un producto.',
                      timer: 2000
                    }).then(
                      function () {
                        location.reload();
                      },
                      // handling the promise rejection
                      function (dismiss) {
                        if (dismiss === 'timer') {
                          location.reload();
                        }
                      }
                    )
                    
                }
            },
            error: function(xhr, textStatus) {
                alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');
                btn.prop('disabled', false);
            }
        });
        });

        //Event for edit supplier and search

        var category = \"";
        // line 741
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
        echo "\";
        var subcategory = \"";
        // line 742
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\";
        \$(\"#category\").val(category);
        \$(\"#subcategory\").val(subcategory);

        if (category.length > 0) {
                getSubcategories();
                console.log('done');
        }
        else{
            console.log(\" fail\");
        }


        \$(\"#category\").on(\"change\", function() {

            var category = \$(\"#category option:selected\");
            location.href = \"";
        // line 758
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/products/";
        echo twig_escape_filter($this->env, (isset($context["idSupplier"]) ? $context["idSupplier"] : null), "html", null, true);
        echo "?category=\" + category.val();
        });
        \$(\"#subcategory\").on(\"change\", function() {

            var category = \$(\"#category option:selected\").val();
            var subcategory = \$(\"#subcategory option:selected\");

            location.href = \"";
        // line 765
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/products/";
        echo twig_escape_filter($this->env, (isset($context["idSupplier"]) ? $context["idSupplier"] : null), "html", null, true);
        echo "?category=\" + category + \"&subcategory=\" + subcategory.val();

        });

        \$(\"#categoryAdd\").on(\"change\", function() {
            getSubcategories(true);
        });

        function getSubcategories( other = false ) {

            var categoria = other == true?\$(\"#categoryAdd option:selected\"):\$(\"#category option:selected\");
            console.log(categoria);

            \$.get(\"/tamev2/dashboard/admin/supplier/subcategory/\" + categoria.data(\"id\"), function(data) {
                var options = '<option value=\"\">Subcategoría</option>';
                
                \$.each( data, function( key, value ) {
                    options += '<option value=\"' + value.sslug + '\"> ' + value.sname + '</option>';
                });

                if(other){
                    \$(\"#subcategoryAdd\").html(options)
                    \$(\"#subcategoryAdd\").val(\"";
        // line 787
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\");
                }else{
                    \$(\"#subcategory\").html(options)
                    \$(\"#subcategory\").val(\"";
        // line 790
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\");
                }
            });
        }

        \$('#editSupplier').on('submit',function(e){
            e.preventDefault();
            \$form = \$(this);
            \$.ajax({
                url: '";
        // line 799
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/products/editSupplier',
                type: 'post',
                dataType: 'html',
                cache: false,
                data: \$form.serialize(),
                beforeSend: function() {
                    console.log(\"Antes de enviar desabilito el boton\");
                },
                success: function(response) {
                    console.log(response);
                        swal({
                          title: 'Tu información se ha actualizado con éxito!',
                          text: 'Tu proveedor esta actualizado.',
                          timer: 2000
                        }).then(
                          function () {
                            location.reload();
                          },
                          // handling the promise rejection
                          function (dismiss) {
                            if (dismiss === 'timer') {
                              location.reload();
                            }
                          }
                        )
                },
                error: function(xhr, textStatus) {
                    console.log(xhr);
                    alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');
                }
            });
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/supplier-products.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1079 => 799,  1067 => 790,  1061 => 787,  1034 => 765,  1022 => 758,  1003 => 742,  999 => 741,  903 => 648,  778 => 525,  775 => 524,  756 => 509,  613 => 368,  600 => 366,  596 => 365,  550 => 324,  542 => 319,  521 => 301,  512 => 295,  503 => 289,  494 => 283,  483 => 275,  474 => 269,  465 => 263,  456 => 257,  444 => 248,  435 => 242,  426 => 236,  417 => 230,  408 => 224,  399 => 218,  390 => 212,  381 => 206,  374 => 202,  360 => 190,  336 => 172,  329 => 168,  321 => 163,  314 => 159,  310 => 158,  298 => 153,  287 => 149,  277 => 145,  273 => 144,  266 => 139,  260 => 138,  258 => 137,  223 => 104,  210 => 102,  206 => 101,  185 => 85,  166 => 71,  161 => 69,  156 => 67,  151 => 65,  143 => 62,  138 => 60,  133 => 58,  128 => 56,  121 => 52,  116 => 50,  111 => 48,  106 => 46,  101 => 44,  96 => 42,  87 => 36,  82 => 34,  72 => 26,  68 => 24,  64 => 22,  62 => 21,  58 => 20,  48 => 12,  45 => 11,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-12 text-center margin-bottom--30">*/
/*             <h1 class="title-dashboard">*/
/*                 Proveedor*/
/*             </h1>*/
/*             <h3>*/
/*                 <h3 class="text-center">*/
/*                     {{ supplier.number_products }}*/
/*                     {% if supplier.number_products > 1 %}*/
/*                         Productos*/
/*                     {% else %}*/
/*                         Producto*/
/*                     {% endif %}*/
/*                 </h3>*/
/*             </h3>*/
/*         </div>*/
/*         <div class="col-sm-6 text-right">*/
/* */
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <h1 class="margin-clear">*/
/*                 {{ supplier.name }}*/
/*             </h1>*/
/*             <p class="text-muted">{{ supplier.description }}</p>*/
/*                 <a class="btn btn-primary pull-left arrow-toggle" role="button" data-toggle="collapse" href="#supplierInfo" aria-expanded="false" aria-controls="supplierInfo">*/
/*                     más información*/
/*                 </a>	*/
/*             <br><br>   */
/*             <div class="collapse" id="supplierInfo">*/
/*                     <b>Razón Social: </b> {{ supplier.legal_name }}*/
/*                     <br>*/
/*                     <b>Supplier ID: </b> {{ supplier.idcustome }}*/
/*                     <br>*/
/*                     <b>Fecha de Admisión: </b> {{ supplier.registered }}*/
/*                     <br>*/
/*                     <b>Categoría /Giro: </b> {{ supplier.category }}*/
/*                     <br>*/
/*                     <b>Dirección Oficina: </b> {{ supplier.office_address }}*/
/*                     <br>*/
/*                     <b>Dirección Bodega: </b> {{ supplier.office_warehouse }}*/
/*                 */
/*                 <h3> Contacto</h3>*/
/*                 <p>*/
/*                     <b>Persona Asignada: </b> {{ supplier.designee }}*/
/*                     <br>*/
/*                     <b>Teléfono Oficina: </b> {{ supplier.designee_phone }}*/
/*                     <br>*/
/*                     <b>Teléfono Móvil: </b> {{ supplier.designee_mobile }}*/
/*                     <br>*/
/*                     <b>Email: </b> <a href="{{ supplier.designee_email }}">{{ supplier.designee_email }}</a>*/
/*                 </p>*/
/*                 <p>*/
/*                     <b>Persona Alto Mando: </b> {{ supplier.manager }}*/
/*                     <br>*/
/*                     <b>Teléfono Oficina: </b> {{ supplier.manager_phone }}*/
/*                     <br>*/
/*                     <b>Teléfono Móvil: </b> {{ supplier.manager_mobile }}*/
/*                     <br>*/
/*                     <b>Email: </b> <a href="mailto:{{ supplier.manager_email }}">{{ supplier.manager_email }}</a>*/
/*                 </p>*/
/*                 <button class="btn btn-warning" data-toggle="modal" data-target="#editDelete">*/
/*                     Modificar proveedor*/
/*                 </button>*/
/*                 <button class="btn btn-danger" data-toggle="modal" data-target="#modalDelete">*/
/*                     Eliminar proveedor*/
/*                 </button>*/
/*             </div>*/
/*             <hr>*/
/*         </div>*/
/*             <div class="col-sm-12 text-right">*/
/*                 <div class="row margin-bottom--30">*/
/*                     <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">*/
/*                         <form method="GET" action="{{ base_url() }}dashboard/admin/supplier/products/{{ idSupplier }}">*/
/*                             <div class="input-group">*/
/*                                 <input type="text" name="q" class="form-control" placeholder="Buscar todo">*/
/*                                 <span class="input-group-btn">*/
/*                                     <button class="btn btn-default" type="submit">*/
/*                                         <i class="fa fa-search"></i>*/
/*                                     </button>*/
/*                                 </span>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                         <select name="category" id="category" class="form-control">*/
/*                             <option value="">Categoría</option>*/
/*                             {% for category in categories %}*/
/*                                 <option data-id="{{ category.idCategory }}" value="{{ category.slug }}">{{ category.name }}</option>*/
/*                             {% endfor %}*/
/*                         </select>*/
/*                     </div>                  */
/*                     <!-- <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                         <select name="" id="" class="form-control">*/
/*                             <option value="">Categoría</option>*/
/*                             <option value="">...</option>*/
/*                         </select>*/
/*                     </div> -->*/
/*                     <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                         <select name="" id="subcategory" class="form-control">*/
/*                             <option value="">Subcategoría</option>*/
/*                             <option value="">...</option>*/
/*                         </select>*/
/*                     </div>*/
/*                     <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                         <select name="" id="" class="form-control">*/
/*                             <option value="">Sin definir</option>*/
/*                         </select>*/
/*                     </div>*/
/*                     <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/*                         <select name="" id="" class="form-control">*/
/*                             <option value="">Sin definir</option>*/
/*                         </select>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         <div class="col-sm-12 margin-bottom--20">*/
/*             <button class="btn btn-success" data-toggle="modal" data-target="#addProduct">Agregar producto</button>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="row">*/
/*                 <div class="col-md-4 col-md-offset-4 text-center">*/
/*                     <div>*/
/*                         {% if totalQ is defined %}*/
/*                             {{ totalQ }} */
/*                         {% endif %}  */
/*                     </div>                     */
/*                 </div>*/
/*             </div>*/
/*             <div class="row">*/
/*                 {% for product in products %}*/
/*                 <a href="{{ base_url() }}dashboard/admin/catalog/product/{{ product.idProduct }}">*/
/*                 <div class="col-sm-12 col-lg-4">*/
/*                     <div class="row">*/
/*                         <!--*/
/*                         <div class="col-sm-6" style="background: url({{ base_url() }}public/uploads/supplier/catalog/{{ product.directory }}/{{ product.item }}.jpg) no-repeat center; background-size: cover"></div>*/
/*                         -->*/
/*                         <div class="col-sm-12 text-center">*/
/*                             <div class="panel panel-default">*/
/*                                 <div class="panel-heading" style="position: relative;background: url({{ base_url() }}public/uploads/supplier/catalog/{{ product.directory }}/{{ product.item }}.jpg) no-repeat center; background-size: cover; height: 200px">*/
/*                                     <div style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; background: rgba(0, 0, 0, 0.4)">*/
/* */
/*                                     </div>*/
/*                                     <div style="position: relative">*/
/*                                         <p class="h3 text-uppercase color-white">{{ product.name }}</p>*/
/*                                         <p class="color-white">{{ character_limiter(product.description, 100)|e }}...</p>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="panel-body panel-product">*/
/*                                     <p class="h2 margin-clear">{{ product.score }}</p>*/
/*                                     <hr>*/
/*                                     <div class="row margin-bottom--20">*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <b>ITEM</b><br>*/
/*                                             {{ product.item }}*/
/*                                         </div>*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <b>SUPPLIER</b><br>*/
/*                                            {{ supplier.idcustome }}*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="row">*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <i class="fa fa-thumbs-up fa-lg"></i> 42*/
/*                                         </div>*/
/*                                         <div class="col-sm-12 col-md-6">*/
/*                                             <i class="fa fa-truck fa-lg"></i> 8*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 </a>*/
/*                 {% endfor %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="editDelete" tabindex="-1" role="dialog" aria-labelledby="modalDelete">*/
/*     <div class="modal-dialog modal-lg" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-body">*/
/*                 <br>*/
/*                 <h3> Editar Proveedor</h3>*/
/*                 <br>*/
/*                 <form id="editSupplier" method="POST" >*/
/*                 <input type="hidden" name="secretKey" value="{{ supplier.idSupplier  }}">*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Proveedor: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-name" value="{{ supplier.name }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Descripción: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-description" value="{{ supplier.description }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Razón Social: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-razon" value="{{ supplier.legal_name }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Supplier ID: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="" value="{{ supplier.idcustome }}" disabled>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Fecha de Admisión: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="" value="{{ supplier.registered }}" disabled>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Categoría /Giro: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-giro" value="{{ supplier.category }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Dirección Oficina: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-oficina" value="{{ supplier.office_address }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Dirección Bodega: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-bodega" value="{{ supplier.office_warehouse }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <br>*/
/*                 <h3> Contacto</h3>*/
/*                 <br>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Persona Asignada: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-asignadaname" value="{{ supplier.designee }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Teléfono Oficina: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-asignadaoficina" value="{{ supplier.designee_phone }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Teléfono Móvil: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-asignadamovil" value="{{ supplier.designee_mobile }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Email: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-asignadaemail" value="{{ supplier.designee_email }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <br>*/
/*                 <br>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Persona Alto Mando: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-altoname" value="{{ supplier.manager }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Teléfono Oficina: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-altooficina" value="{{ supplier.manager_phone }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Teléfono Móvil: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-altomovil" value="{{ supplier.manager_mobile }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group row">*/
/*                     <label class="col-md-2 col-form-label">Email: </label>*/
/*                     <div class="col-md-10">*/
/*                         <input type="text" class="form-control" name="p-altoemail" value="{{ supplier.manager_email }}">*/
/*                     </div>*/
/*                 </div>*/
/*                 */
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>*/
/*                 <button type="submit" class="btn btn-danger">Guardar</button>*/
/*             </div>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDelete">*/
/*     <div class="modal-dialog modal-sm" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-body">*/
/*                 <h3 class="modal-title">Confirmación</h3>*/
/*                 <p>Se eliminara el proveedor: <br> <b>{{ supplier.name }}</b></p>*/
/*                 <p class="text-danger small">*Se borraran sus productos asociados</p>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>*/
/*                 <a href="{{ base_url() }}dashboard/admin/supplier/delete/{{ supplier.idSupplier }}" class="btn btn-danger">Confirmar</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*     <div class="modal-dialog modal-lg" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*                 <h4 class="modal-title" id="myModalLabel">Agregar nuevo producto</h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*                 <form id="formAddProduct" action="" enctype="multipart/form-data" class="row">*/
/* */
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pname">Nombre</label>*/
/*                         <input id="pname" name="pname" type="text" class="form-control" >*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pscore">Calificación</label>*/
/*                         <input id="pscore" name="pscore" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pheader">Header</label>*/
/*                         <textarea id="pheader" rows="3" name="pheader" type="text" class="form-control"></textarea>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdescription">Descripción</label>*/
/*                         <textarea id="pdescription" rows="3" name="pdescription" type="text" class="form-control"></textarea>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pitem">Item</label>*/
/*                         <input id="pitem" name="pitem" type="text" class="form-control" required>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcategory">Categoría</label>*/
/*                         <!--<input id="pcategory" name="pcategory" type="text" class="form-control">-->*/
/* */
/*                         <select name="category" id="categoryAdd" class="form-control" required>*/
/*                             <option value="" disabled selected>Categoría</option>*/
/*                             {% for category in categories %}*/
/*                                 <option data-id="{{ category.idCategory }}" value="{{ category.slug }}">{{ category.name }}</option>*/
/*                             {% endfor %}*/
/*                         </select>*/
/* */
/* */
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="psubcat">Subcategoría</label>*/
/*                         <!--<input id="psubcat" name="psubcat" type="text" class="form-control">-->*/
/* */
/*                         <select name="" id="subcategoryAdd" class="form-control" required>*/
/*                             <option value="" disabled selected>Subcategoría</option>*/
/*                             <option value="">...</option>*/
/*                         </select>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pquality">Calidad</label>*/
/*                         <input id="pquality" name="pquality" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="porigin">Origen</label>*/
/*                         <input id="porigin" name="porigin" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcolor">Colores</label>*/
/*                         <input id="pcolor" name="pcolor" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppresentation">Presentación</label>*/
/*                         <input id="ppresentation" name="ppresentation" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcharacteristics">Caracteristicas</label>*/
/*                         <input id="pcharacteristics" name="pcharacteristics" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pspecifications">Especificaciones</label>*/
/*                         <input id="pspecifications" name="pspecifications" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdimensions">Dimensiones</label>*/
/*                         <input id="pdimensions" name="pdimensions" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pweight">Peso</label>*/
/*                         <input id="pweight" name="pweight" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pminimumOrder">Minimo de Pedido</label>*/
/*                         <input id="pminimumOrder" name="pminimumOrder" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdateExpiry">Fecha de Caducidad</label>*/
/*                         <input id="pdateExpiry" name="pdateExpiry" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pinvNumPieces">Inventario No. de Piezas</label>*/
/*                         <input id="pinvNumPieces" name="pinvNumPieces" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pboutiqueMaxOrder">Máximo de Pedido</label>*/
/*                         <input id="pboutiqueMaxOrder" name="pboutiqueMaxOrder" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceWithoutTax">Precio sin impuesto</label>*/
/*                         <input id="ppriceWithoutTax" name="ppriceWithoutTax" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceCurrency">Moneda</label>*/
/*                         <input id="ppriceCurrency" name="ppriceCurrency" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceSuggested">Precio Sugerido</label>*/
/*                         <input id="ppriceSuggested" name="ppriceSuggested" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceTT">Precio TT</label>*/
/*                         <input id="ppriceTT" name="ppriceTT" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingNumPieces">Empaque - No. de Piezas</label>*/
/*                         <input id="ppackingNumPieces" name="ppackingNumPieces" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingSpecifications">Empaque - Especifiaciones</label>*/
/*                         <input id="ppackingSpecifications" name="ppackingSpecifications" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingHeight">Empaque - Altura</label>*/
/*                         <input id="ppackingHeight" name="ppackingHeight" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingWidth">Empaque - Ancho</label>*/
/*                         <input id="ppackingWidth" name="ppackingWidth" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingLarge">Empaque - Largo</label>*/
/*                         <input id="ppackingLarge" name="ppackingLarge" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingWeight">Empaque - Peso</label>*/
/*                         <input id="ppackingWeight" name="ppackingWeight" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="plogTimeProduction">Logistica - Tiempo de producción</label>*/
/*                         <input id="plogTimeProduction" name="pLogTimeProduction" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="plogFob">Logistica - FOB</label>*/
/*                         <input id="plogFob" name="pLogFob" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="plogFobDeliveryTime">Logistica - FOB Tiempo de entrega</label>*/
/*                         <input id="plogFobDeliveryTime" name="pLogFobDeliveryTime" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="psample">Muestra</label>*/
/*                         <input id="psample" name="psample" type="text" class="form-control">*/
/*                     </div>*/
/*                    <!-- <div class="form-group col-sm-12">*/
/*                         <div class="row">*/
/*                             <div class="form-group col-sm-6">*/
/*                                 <h3><b>Galería</b></h3>*/
/*                                 <label for="insertImages"><b>Agregar imágenes</b></label>*/
/*                             */
/*                                 <style></style>*/
/*                                 <div class="fileUpload btn btn-blue">*/
/*                                     <span>SELECCIONAR ARCHIVO</span>  */
/*                                  */
/*                                     <input name="insertImages" type="file" class="handle-new upload" accept="image/jpeg" style="padding-left: 0;" id="getsIm">                                 */
/*                                 </div>                              */
/*                                 <button type="button" class="btn btn-primary save-img">AGREGAR</button>*/
/*                             </div>*/
/*                         </div> */
/*                     </div>-->*/
/*                     <div class="row contain-images">*/
/*                         <div class="col-lg-12">*/
/*                             <div class="row">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-12">*/
/*                         <input id="pid" type="hidden" name="pid">*/
/*                         <input id="supplierid" type="hidden" name="supplierid" value="{{ supplier.idSupplier }}">*/
/*                         <input id="userid" type="hidden" name="supplier_id" value="1">*/
/*                         */
/*                     </div>*/
/*                 */
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="submit" class="btn btn-success">Guardar</button>*/
/*                 <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>*/
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/*     $(function() {*/
/* */
/*         $('.save-img').on('click',function(){                                                                          */
/*             console.log('has agregado una imagen nueva');*/
/*             var fil = document.getElementById('getsIm');*/
/* */
/*             addImg(fil);*/
/* */
/*                 function addImg(file){*/
/*                     if (file.files && file.files[0]) {*/
/*                         if(file.files[0].type == "image/jpeg")*/
/*                         {*/
/*                             var reader = new FileReader();*/
/*                             */
/*                             reader.onload = function (e) {*/
/*                             var container = $('.contain-images>div>div');*/
/*                             */
/*                             var div1 = document.createElement('div');*/
/*                                 div1.setAttribute('class','form-group col-sm-9');*/
/*                           */
/*                             var img = document.createElement('img');*/
/*                                 img.setAttribute('width','250px');*/
/*                                 img.setAttribute('class','preview');*/
/*                                 img.setAttribute('src',e.target.result);*/
/*                                 */
/*                                 console.log("*****");*/
/*                                 jQuery(div1).append(img);*/
/*                                 jQuery(container).append(div1); */
/*                                 console.log("*****");                    */
/* */
/*                             }*/
/*                         }*/
/*                         else{*/
/*                             swal(*/
/*                               '',*/
/*                               'Imagen inválida, extensíon no permitida',*/
/*                               'error'*/
/*                             )                   */
/*                             */
/*                         }*/
/* */
/*                         reader.readAsDataURL(file.files[0]);*/
/*                     }*/
/*                     else{*/
/*                         swal(*/
/*                           '',*/
/*                           'Por favor selecciona un archivo válido.',*/
/*                           'error'*/
/*                         )               */
/*                         */
/*                     }           */
/*                 }*/
/*         });*/
/* */
/* */
/*         $('#myCarousel').carousel({*/
/*             interval: 5000*/
/*         });*/
/* */
/*         //Handles the carousel thumbnails*/
/*         $('[id^=carousel-selector-]').click(function () {*/
/*             var id_selector = $(this).attr("id");*/
/*             try {*/
/*                 var id = /-(\d+)$/.exec(id_selector)[1];*/
/*                 console.log(id_selector, id);*/
/*                 jQuery('#myCarousel').carousel(parseInt(id));*/
/*             } catch (e) {*/
/*                 console.log('Regex failed!', e);*/
/*             }*/
/*         });*/
/*         // When the carousel slides, auto update the text*/
/*         $('#myCarousel').on('slid.bs.carousel', function (e) {*/
/*             var id = $('.item.active').data('slide-number');*/
/*             $('#carousel-text').html($('#slide-content-'+id).html());*/
/*         });*/
/*         */
/*         $('#formAddProduct').on('submit', function (e) {*/
/*         e.preventDefault();*/
/* */
/*         var btn = $(this).find('button');*/
/* */
/*         var id = $('#pid').val();*/
/*         var row = $('#rowid').val();*/
/*         var pName = $("#pname").val();*/
/*         var pScore = $("#pscore").val();*/
/*         var pHeader = $("#pheader").val();*/
/*         var pDescription = $("#pdescription").val();*/
/*         var pItem = $('#pitem').val();*/
/*         var pCategory = $('#categoryAdd option:selected').val();*/
/*         var pSubcategory = $('#subcategoryAdd option:selected').val();*/
/*         var pQuality = $('#pquality').val();*/
/*         var pOrigin = $('#porigin').val();*/
/*         var pColors = $('#pcolor').val();*/
/*         var pPresentation = $('#ppresentation').val();*/
/*         var pCharacteristics = $('#pcharacteristics').val();*/
/*         var pEspecifications = $('#pspecifications').val();*/
/*         var pDimensions = $('#pdimensions').val();*/
/*         var pWeight = $('#pweight').val();*/
/*         var pMinimumOrder = $('#pminimumOrder').val();*/
/*         var pDateExpiry = $('#pdateExpiry').val();*/
/*         var pInvNumPieces = $('#pinvNumPieces').val();*/
/*         var pBoutiqueMaxOrder = $('#pboutiqueMaxOrder').val();*/
/*         var pPriceWithoutTax = $('#ppriceWithoutTax').val();*/
/*         var pPriceCurrency = $('#ppriceCurrency').val();*/
/*         var pPriceSuggested = $('#ppriceSuggested').val();*/
/*         var pPriceTT = $('#ppriceTT').val();*/
/*         var pPackingNumPieces = $('#ppackingNumPieces').val();*/
/*         var pPackingEspecifications = $('#ppackingSpecifications').val();*/
/*         var pPackingHeight = $('#ppackingHeight').val();*/
/*         var pPackingWidth = $('#ppackingWidth').val();*/
/*         var pPackingLarge = $('#ppackingLarge').val();*/
/*         var pPackingWeight = $('#ppackingWeight').val();*/
/*         var pPackingLogProductionTime = $('#plogTimeProduction').val();*/
/*         var pLogFob = $('#plogFob').val();*/
/*         var pLogFobDeliveryTime = $('#plogFobDeliveryTime').val();*/
/*         var pSample = $('#psample').val();*/
/*         var sId = $('#supplierid').val();*/
/*         var uId = $('#userid').val();*/
/* */
/*          console.log(id)*/
/* */
/*         $.ajax({*/
/*             url: '{{ base_url() }}dashboard/admin/product/action/add',*/
/*             type: 'post',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             data: {*/
/*                 pId : id,*/
/*                 pName : pName,*/
/*                 pScore : pScore,*/
/*                 pHeader : pHeader,*/
/*                 pDescription : pDescription,*/
/*                 pItem : pItem,*/
/*                 pCategory : pCategory,*/
/*                 pSubcategory : pSubcategory,*/
/*                 pQuality : pQuality,*/
/*                 pOrigin : pOrigin,*/
/*                 pColors : pColors,*/
/*                 pPresentation : pPresentation,*/
/*                 pCharacteristics : pCharacteristics,*/
/*                 pSpecifications : pEspecifications,*/
/*                 pDimensions : pDimensions,*/
/*                 pWeight : pWeight,*/
/*                 pMinimumOrder : pMinimumOrder,*/
/*                 pDateExpiry : pDateExpiry,*/
/*                 pInvNumPieces : pInvNumPieces,*/
/*                 pBoutiqueMaxOrder : pBoutiqueMaxOrder,*/
/*                 pPriceWithoutTax : pPriceWithoutTax,*/
/*                 pPriceCurrency : pPriceCurrency,*/
/*                 pPriceSuggested : pPriceSuggested,*/
/*                 pPriceTT : pPriceTT,*/
/*                 pPackingNumPieces : pPackingNumPieces,*/
/*                 pPackingSpecifications : pPackingEspecifications,*/
/*                 pPackingHeight : pPackingHeight,*/
/*                 pPackingWidth : pPackingWidth,*/
/*                 pPackingLarge : pPackingLarge,*/
/*                 pPackingWeight : pPackingWeight,*/
/*                 pLogProductionTime : pPackingLogProductionTime,*/
/*                 pLogFob : pLogFob,*/
/*                 pLogFobDeliveryTime : pLogFobDeliveryTime,*/
/*                 pSample : pSample,*/
/*                 supplierid : sId,*/
/*                 supplier_id: uId*/
/*             },*/
/*             beforeSend: function() {*/
/*                 btn.prop('disabled', true)*/
/*             },*/
/*             success: function(response) {*/
/*                 console.log(response);*/
/*                 if (response == true) {*/
/* */
/*                     swal({*/
/*                       title: 'Producto agregado!',*/
/*                       text: 'Un nuevo producto ha sigo agregado a este proveedor.',*/
/*                       timer: 2000*/
/*                     }).then(*/
/*                       function () {*/
/*                         location.reload();*/
/*                       },*/
/*                       // handling the promise rejection*/
/*                       function (dismiss) {*/
/*                         if (dismiss === 'timer') {*/
/*                           location.reload();*/
/*                         }*/
/*                       }*/
/*                     )*/
/*                 } else {*/
/* */
/*                     swal({*/
/*                       title: 'Fallo!',*/
/*                       text: 'Proporciona los campos requeridos para subir un producto.',*/
/*                       timer: 2000*/
/*                     }).then(*/
/*                       function () {*/
/*                         location.reload();*/
/*                       },*/
/*                       // handling the promise rejection*/
/*                       function (dismiss) {*/
/*                         if (dismiss === 'timer') {*/
/*                           location.reload();*/
/*                         }*/
/*                       }*/
/*                     )*/
/*                     */
/*                 }*/
/*             },*/
/*             error: function(xhr, textStatus) {*/
/*                 alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');*/
/*                 btn.prop('disabled', false);*/
/*             }*/
/*         });*/
/*         });*/
/* */
/*         //Event for edit supplier and search*/
/* */
/*         var category = "{{ category }}";*/
/*         var subcategory = "{{ subcategory }}";*/
/*         $("#category").val(category);*/
/*         $("#subcategory").val(subcategory);*/
/* */
/*         if (category.length > 0) {*/
/*                 getSubcategories();*/
/*                 console.log('done');*/
/*         }*/
/*         else{*/
/*             console.log(" fail");*/
/*         }*/
/* */
/* */
/*         $("#category").on("change", function() {*/
/* */
/*             var category = $("#category option:selected");*/
/*             location.href = "{{ base_url() }}dashboard/admin/supplier/products/{{ idSupplier }}?category=" + category.val();*/
/*         });*/
/*         $("#subcategory").on("change", function() {*/
/* */
/*             var category = $("#category option:selected").val();*/
/*             var subcategory = $("#subcategory option:selected");*/
/* */
/*             location.href = "{{ base_url() }}dashboard/admin/supplier/products/{{ idSupplier }}?category=" + category + "&subcategory=" + subcategory.val();*/
/* */
/*         });*/
/* */
/*         $("#categoryAdd").on("change", function() {*/
/*             getSubcategories(true);*/
/*         });*/
/* */
/*         function getSubcategories( other = false ) {*/
/* */
/*             var categoria = other == true?$("#categoryAdd option:selected"):$("#category option:selected");*/
/*             console.log(categoria);*/
/* */
/*             $.get("/tamev2/dashboard/admin/supplier/subcategory/" + categoria.data("id"), function(data) {*/
/*                 var options = '<option value="">Subcategoría</option>';*/
/*                 */
/*                 $.each( data, function( key, value ) {*/
/*                     options += '<option value="' + value.sslug + '"> ' + value.sname + '</option>';*/
/*                 });*/
/* */
/*                 if(other){*/
/*                     $("#subcategoryAdd").html(options)*/
/*                     $("#subcategoryAdd").val("{{ subcategory }}");*/
/*                 }else{*/
/*                     $("#subcategory").html(options)*/
/*                     $("#subcategory").val("{{ subcategory }}");*/
/*                 }*/
/*             });*/
/*         }*/
/* */
/*         $('#editSupplier').on('submit',function(e){*/
/*             e.preventDefault();*/
/*             $form = $(this);*/
/*             $.ajax({*/
/*                 url: '{{ base_url() }}dashboard/admin/supplier/products/editSupplier',*/
/*                 type: 'post',*/
/*                 dataType: 'html',*/
/*                 cache: false,*/
/*                 data: $form.serialize(),*/
/*                 beforeSend: function() {*/
/*                     console.log("Antes de enviar desabilito el boton");*/
/*                 },*/
/*                 success: function(response) {*/
/*                     console.log(response);*/
/*                         swal({*/
/*                           title: 'Tu información se ha actualizado con éxito!',*/
/*                           text: 'Tu proveedor esta actualizado.',*/
/*                           timer: 2000*/
/*                         }).then(*/
/*                           function () {*/
/*                             location.reload();*/
/*                           },*/
/*                           // handling the promise rejection*/
/*                           function (dismiss) {*/
/*                             if (dismiss === 'timer') {*/
/*                               location.reload();*/
/*                             }*/
/*                           }*/
/*                         )*/
/*                 },*/
/*                 error: function(xhr, textStatus) {*/
/*                     console.log(xhr);*/
/*                     alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');*/
/*                 }*/
/*             });*/
/*         });*/
/*     });*/
/* </script>*/
/* {% endblock %}*/
