<?php

/* common/sidebar/sidebar-shop.html */
class __TwigTemplate_c686d99a5a8ba811180949cee981cf296f1c65f9d97b85d333c0e3ec8a6440c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <div id=\"boxMenu\" class=\"sidenav\">
      <a href=\"javascript:void(0)\" class=\"closebtn\" onclick=\"closeNav()\">&times;</a>
      <a href=\"#\">About</a>
      <a href=\"#\">Services</a>
      <a href=\"#\">Clients</a>
      <a href=\"#\">Contact</a>
    </div>";
    }

    public function getTemplateName()
    {
        return "common/sidebar/sidebar-shop.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/*     <div id="boxMenu" class="sidenav">*/
/*       <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>*/
/*       <a href="#">About</a>*/
/*       <a href="#">Services</a>*/
/*       <a href="#">Clients</a>*/
/*       <a href="#">Contact</a>*/
/*     </div>*/
