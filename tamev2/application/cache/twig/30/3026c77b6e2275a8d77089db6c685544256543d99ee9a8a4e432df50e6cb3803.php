<?php

/* dashboard/admin/catalog-product.html */
class __TwigTemplate_e5202f0674f34a6fac3a72d58d410c28497fece2b707c68c6e070246d7bd473a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/catalog-product.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }
</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"container-fluid containerNoPrint\">
    <div class=\"row\">
        <div class=\"col-sm-12 col-lg-5 col-lg-offset-1 margin-bottom--20\" id=\"imagenesPrint\">
            <div class=\"row\">
                <!--<div class=\"col-sm-12 margin-bottom--50\">
                    <img src=\"";
        // line 17
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/uploads/supplier/catalog/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo ".jpg\" class=\"img-responsive img-center\">
                </div>-->
\t\t\t\t
                ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["gallery"]) ? $context["gallery"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["img"]) {
            // line 21
            echo "                <div class=\"col-sm-12 margin-bottom--50\">
                     <img src=\"";
            // line 22
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["img"], "name", array()), "html", null, true);
            echo "\" class=\"img-responsive\">
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['img'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "            </div>
        </div>
        <div class=\"col-sm-12 col-lg-5\" style=\"position: relative; \" id=\"descripcionPrint\">
            <div class=\"row\" style=\"position: fixed; width: 30%;\">
                <div class=\"col-sm-12 margin-bottom--20\" style=\"max-height: 500px;overflow-y: auto\" id=\"desPrint\">
                  <div class=\"col-lg-8\">
                    <p class=\"text-muted\">";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "c_name", array()), "html", null, true);
        echo "</p>
                    <h2 class=\"margin-clear margin-bottom--10\">";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "</h2>
                    <h4 class=\"margin-clear margin-bottom--10 text-muted\">\$";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "</h4>
                    <p class=\"lead margin-clear margin-bottom--10\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "header", array()), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-lg-4 text-right\">
                    <p class=\"h1\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "score", array()), "html", null, true);
        echo "</p>
                </div>                
                <div class=\"col-sm-12\">
                    <p>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "html", null, true);
        echo "</p>
                    <hr>
                    <p>
                        <b>Cantidad minima de pedido: </b> ";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "minimumOrder", array()), "html", null, true);
        echo "<br>
                        <b>Item: </b> ";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo "<br>
                        <b>Supplier ID: </b> ";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_custome", array()), "html", null, true);
        echo "<br>
                        <b>Supplier Name: </b> <a href=\"";
        // line 46
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/products/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_id", array()), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_name", array()), "html", null, true);
        echo " </a><br>
                        <b>Calidad: </b> ";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quality", array()), "html", null, true);
        echo "<br>
                        <b>Origen: </b> ";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "origin2", array()), "html", null, true);
        echo "<br>
                        <b>País: </b> ";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "origin", array()), "html", null, true);
        echo "<br>
                        <b>Presentación: </b> ";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "presentation", array()), "html", null, true);
        echo "
                    </p>
                    <div class=\"row text-center\" style=\"display:flex; padding: 15px;\">
                        <p>COLORES</p>
                        ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["colors"]) ? $context["colors"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
            // line 55
            echo "                            <div class=\"col-sm-1\" style=\"display:flex; flex: 1;\">
                                <div class=\"panel\" style=\"flex: 1; height: 20px; background: ";
            // line 56
            echo twig_escape_filter($this->env, $context["color"], "html", null, true);
            echo "\"></div>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                    </div>
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapseCharacteristics\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        CARACTERISTICAS <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapseCharacteristics\">
                        ";
        // line 64
        if (twig_test_empty($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()))) {
            // line 65
            echo "                            <span class=\"text-muted\">No hay una descripción</span>
                        ";
        } else {
            // line 67
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()), "html", null, true);
            echo "
                        ";
        }
        // line 69
        echo "                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapseEspecifications\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        ESPECIFICACIONES <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapseEspecifications\">
                        ";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "especifications", array()), "html", null, true);
        echo "<br><br>
                        <b>Dimensiones: </b> ";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "dimensions", array()), "html", null, true);
        echo "<br>
                        <b>Peso: </b> ";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "weight", array()), "html", null, true);
        echo "<br><br>
                        
                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapsePacking\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        EMPAQUE <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapsePacking\">
                        <b>No. piezas: </b> ";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_numPieces", array()), "html", null, true);
        echo "<br>
                        <b>Especificaciones: </b> ";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_especifications", array()), "html", null, true);
        echo "<br>
                        <b>Alto: </b> ";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_height", array()), "html", null, true);
        echo "cm<br>
                        <b>Ancho: </b> ";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_width", array()), "html", null, true);
        echo "cm<br>
                        <b>Largo: </b> ";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_large", array()), "html", null, true);
        echo "cm<br>
                        <b>Peso:</b> ";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_weight", array()), "html", null, true);
        echo "kg
                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapseLogistic\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        LOGISTICA <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapseLogistic\">
                        <b>Tiempo de producción: </b> ";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_productionTime", array()), "html", null, true);
        echo "<br>
                        <b>FOB: </b> ";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fob", array()), "html", null, true);
        echo "<br>
                        <b>Tiempo de entrega FOB: </b> ";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fobDeliveryTime", array()), "html", null, true);
        echo "<br>
                        <b>Muestra fisica: </b> ";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sample", array()), "html", null, true);
        echo "<br>
                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                    <a class=\"btn btn-link btn-block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapsePrice\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                        PRECIO <i class=\"fa fa-angle-down fa-lg\"></i>
                    </a>
                    <p class=\"collapse\" id=\"collapsePrice\">
                        <b>Precio s/IVA: </b> \$";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo "<br>
                        <b>Moneda: </b> ";
        // line 108
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "<br>
                        <b>Precio sugerido: </b> \$";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_suggested", array()), "html", null, true);
        echo "<br>
                        <b>Precio TT: </b> \$";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo "<br>
                        ";
        // line 111
        $context["utilidad"] = ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()) - $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()));
        // line 112
        echo "                        <b>Utilidad: </b> \$";
        echo twig_escape_filter($this->env, (isset($context["utilidad"]) ? $context["utilidad"] : null), "html", null, true);
        echo "
                    </p>
                    <hr class=\"margin-clear margin-bottom--10\">
                </div>
                
                  
              </div>
                <div class=\"col-sm-12 margin-bottom--10\">
                    <button class=\"btn btn-block\" data-toggle=\"modal\" data-target=\"#editProduct\">
                        EDITAR
                    </button>
                </div>
                <div class=\"col-sm-12 margin-bottom--10\">                                                            
                    <a href=\"";
        // line 125
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/tcpdf/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "idProduct", array()), "html", null, true);
        echo "\" target=\"_blank\" class=\"btn btn-block\">IMPRIMIR</a>
                    <!-- <button class=\"btn btn-block\" onclick=\"imprimir()\">
                        IMPRIMIR
                    </button> -->
                </div>
                <div class=\"col-sm-12\">
                    <button data-id=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "idProduct", array()), "html", null, true);
        echo "\" class=\"btn btn-danger btn-block\" data-toggle=\"modal\" data-target=\"#modalDelete\">
                        ELIMINAR
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=\"modal fade containerNoPrint\" id=\"editProduct\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" data-main=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
        echo "\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header head-edit\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\"><b>Editar producto</b></h4>
            </div>
            <div class=\"modal-body\">
                <form id=\"formEditProduct\" action=\"\" class=\"row\" enctype=\"multipart/form-data\">
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcategory\">Categoría</label>
                        <input id=\"pcategory\" name=\"pcategory\" type=\"text\" class=\"form-control\" value=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "categoryA", array()), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pscore\">Calificación</label>
                        <input id=\"pscore\" name=\"pscore\" type=\"text\" class=\"form-control\" value=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "score", array()), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"psubcat\">Subcategoría</label>
                        <input id=\"psubcat\" name=\"psubcat\" type=\"text\" class=\"form-control\" value=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "subcategoryA", array()), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pminimumOrder\">Cantidad mínima de pedido</label>
                        <input id=\"pminimumOrder\" name=\"pminimumOrder\" type=\"text\" class=\"form-control\" value=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "minimumOrder", array()), "html", null, true);
        echo "\">
                    </div>\t\t\t\t\t
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pname\">Nombre</label>
                        <input id=\"pname\" name=\"pname\" type=\"text\" class=\"form-control\" value=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pboutiqueMaxOrder\">Cantidad máximo de pedido</label>
                        <input id=\"pboutiqueMaxOrder\" name=\"pboutiqueMaxOrder\" type=\"text\" class=\"form-control\" value=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "boutique_maximumOrder", array()), "html", null, true);
        echo "\">
                    </div>\t\t\t\t\t
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceTT\">Precio TT</label>
                        <input id=\"ppriceTT\" name=\"ppriceTT\" type=\"text\" class=\"form-control\" value=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo "\">
                    </div>\t
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pinvNumPieces\">Inventario No. de piezas</label>
                        <input id=\"pinvNumPieces\" name=\"pinvNumPieces\" type=\"text\" class=\"form-control\" value=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "inventory_numPieces", array()), "html", null, true);
        echo "\">
                    </div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"pheader\">Header</label>
\t\t\t\t\t\t\t\t<textarea id=\"pheader\" rows=\"3\" name=\"pheader\" type=\"text\" class=\"form-control\">";
        // line 184
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "header", array()), "html", null, true);
        echo "</textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"pdateExpiry\">Fecha de caducidad</label>
\t\t\t\t\t\t\t\t<input id=\"pdateExpiry\" name=\"pdateExpiry\" type=\"text\" class=\"form-control\" value=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "dataExpiry", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t\t\t\t
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdescription\">Descripción</label>
                        <textarea id=\"pdescription\" rows=\"3\" name=\"pdescription\" type=\"text\" class=\"form-control\">";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "html", null, true);
        echo "</textarea>
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pitem\">Item</label>
                        <input id=\"pitem\" name=\"pitem\" type=\"text\" class=\"form-control\" value=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo "\">
                    </div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<h3><b>Características</b></h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"pquality\">Calidad</label>
\t\t\t\t\t\t\t\t<input id=\"pquality\" name=\"pquality\" type=\"text\" class=\"form-control\" value=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quality", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t</div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcharacteristics\">Caracteristicas</label>
                        <input id=\"pcharacteristics\" name=\"pcharacteristics\" type=\"text\" class=\"form-control\" value=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()), "html", null, true);
        echo "\">
                    </div>\t\t\t\t\t
                    <div class=\"form-group col-sm-6\">
                        <label for=\"porigin\">Origen</label>
                        <input id=\"porigin\" name=\"porigin\" type=\"text\" class=\"form-control\" value=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "origin", array()), "html", null, true);
        echo "\">
                    </div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<h3><b>Especificaciones</b></h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"ppresentation\">Presentación</label>
\t\t\t\t\t\t\t\t<input id=\"ppresentation\" name=\"ppresentation\" type=\"text\" class=\"form-control\" value=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "presentation", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div> \t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdimensions\">Dimensiones</label>
                        <input id=\"pdimensions\" name=\"pdimensions\" type=\"text\" class=\"form-control\" value=\"";
        // line 232
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "dimensions", array()), "html", null, true);
        echo "\">
                    </div>\t\t\t\t\t
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcolor\">Colores</label>
                        <input id=\"pcolor\" name=\"pcolor\" type=\"text\" class=\"form-control\" value=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "colors", array()), "html", null, true);
        echo "\">
                    </div>                   
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pweight\">Peso</label>
                        <input id=\"pweight\" name=\"pweight\" type=\"text\" class=\"form-control\" value=\"";
        // line 240
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "weight", array()), "html", null, true);
        echo "\">
                    </div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<div class=\"form-group col-sm-12\">
\t\t\t\t\t\t\t\t<h3><b>Empaque</b></h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"ppackingNumPieces\">Empaque - No. de Piezas</label>
\t\t\t\t\t\t\t\t<input id=\"ppackingNumPieces\" name=\"ppackingNumPieces\" type=\"text\" class=\"form-control\" value=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_numPieces", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"ppackingWidth\">Empaque - Ancho</label>
\t\t\t\t\t\t\t\t<input id=\"ppackingWidth\" name=\"ppackingWidth\" type=\"text\" class=\"form-control\" value=\"";
        // line 253
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_width", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"ppackingSpecifications\">Empaque - Especifiaciones</label>
\t\t\t\t\t\t\t\t<input id=\"ppackingSpecifications\" name=\"ppackingSpecifications\" type=\"text\" class=\"form-control\" value=\"";
        // line 257
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_especifications", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"ppackingLarge\">Empaque - Largo</label>
\t\t\t\t\t\t\t\t<input id=\"ppackingLarge\" name=\"ppackingLarge\" type=\"text\" class=\"form-control\" value=\"";
        // line 261
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_large", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"ppackingHeight\">Empaque - Altura</label>
\t\t\t\t\t\t\t\t<input id=\"ppackingHeight\" name=\"ppackingHeight\" type=\"text\" class=\"form-control\" value=\"";
        // line 265
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_height", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"ppackingWeight\">Empaque - Peso</label>
\t\t\t\t\t\t\t\t<input id=\"ppackingWeight\" name=\"ppackingWeight\" type=\"text\" class=\"form-control\" value=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_weight", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<div class=\"form-group col-sm-12\">
\t\t\t\t\t\t\t\t<h3><b>Logística</b></h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"plogTimeProduction\">Logistica - Tiempo de producción</label>
\t\t\t\t\t\t\t\t<input id=\"plogTimeProduction\" name=\"pLogTimeProduction\" type=\"text\" class=\"form-control\" value=\"";
        // line 280
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_productionTime", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"plogFobDeliveryTime\">Logistica - FOB Tiempo de entrega</label>
\t\t\t\t\t\t\t\t<input id=\"plogFobDeliveryTime\" name=\"pLogFobDeliveryTime\" type=\"text\" class=\"form-control\" value=\"";
        // line 284
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fobDeliveryTime", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"plogFob\">Logistica - FOB</label>
\t\t\t\t\t\t\t\t<input id=\"plogFob\" name=\"pLogFob\" type=\"text\" class=\"form-control\" value=\"";
        // line 288
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fob", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<label for=\"psample\">Muestra</label>
\t\t\t\t\t\t\t\t<input id=\"psample\" name=\"psample\" type=\"text\" class=\"form-control\" value=\"";
        // line 292
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sample", array()), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<div class=\"form-group col-sm-12\">
\t\t\t\t\t\t\t\t<h3><b>Precio</b></h3>
\t\t\t\t\t\t\t</div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceWithoutTax\">Precio sin impuesto</label>
                        <input id=\"ppriceWithoutTax\" name=\"ppriceWithoutTax\" type=\"text\" class=\"form-control\" value=\"";
        // line 303
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceSuggested\">Precio Sugerido</label>
                        <input id=\"ppriceSuggested\" name=\"ppriceSuggested\" type=\"text\" class=\"form-control\" value=\"";
        // line 307
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_suggested", array()), "html", null, true);
        echo "\">
                    </div>\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>



\t\t\t\t\t<div class=\"form-group col-sm-12\">
\t\t\t\t\t&nbsp;
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t<h3><b>Galería</b></h3>
\t\t\t\t\t\t\t\t<label for=\"insertImages\"><b>Agregar imágenes</b></label>
\t\t\t\t\t\t\t\t<!----test button--->
\t\t\t\t\t\t\t\t<style>
\t\t\t\t\t\t\t\t\t.fileUpload {
\t\t\t\t\t\t\t\t\t\tposition: relative;
\t\t\t\t\t\t\t\t\t\toverflow: hidden;
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t.fileUpload input.upload {
\t\t\t\t\t\t\t\t\t\tposition: absolute;
\t\t\t\t\t\t\t\t\t\ttop: 0;
\t\t\t\t\t\t\t\t\t\tright: 0;
\t\t\t\t\t\t\t\t\t\tmargin: 0;
\t\t\t\t\t\t\t\t\t\tpadding: 0;
\t\t\t\t\t\t\t\t\t\tfont-size: 20px;
\t\t\t\t\t\t\t\t\t\tcursor: pointer;
\t\t\t\t\t\t\t\t\t\topacity: 0;
\t\t\t\t\t\t\t\t\t\tfilter: alpha(opacity=0);
\t\t\t\t\t\t\t\t\t}\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</style>
\t\t\t\t\t\t\t\t<div class=\"fileUpload btn btn-blue\">
\t\t\t\t\t\t\t\t\t<span>SELECCIONAR ARCHIVO</span>  
\t\t\t\t\t\t\t\t\t<!--<input class=\"upload\" type=\"file\">-->
\t\t\t\t\t\t\t\t\t<input name=\"insertImages\" type=\"file\" class=\"handle-new upload\" data-origen=\"";
        // line 341
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
        echo "\" data-prefijo='";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo "' accept=\"image/jpeg\" style=\"padding-left: 0;\" id=\"getsIm\"/>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!----test end--->
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary save-img\" >GUARDAR</button>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-6 col-lg-offset-3\">
\t\t\t\t\t\t\t<span id=\"progress-message\" class=\"text-center\"></span>
\t\t\t\t\t\t\t<progress class=\"upload-img tool\" id=\"progress\" value=\"0\" max=\"100\" data-toggle=\"tooltip\" title=\"0%\"></progress>\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                    <div class=\"form-group col-sm-12\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<label for=\"preview\"><p>Preview</p></label>\t\t\t\t\t\t
\t\t\t\t\t\t<div>
\t\t\t\t\t\t<ul id=\"sortable\" >
\t\t\t\t\t\t";
        // line 357
        if ((twig_length_filter($this->env, (isset($context["getImages"]) ? $context["getImages"] : null)) == 0)) {
            // line 358
            echo "\t\t\t\t\t\t\t<li></li>
\t\t\t\t\t\t";
        } else {
            // line 360
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["getImages"]) ? $context["getImages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["imagenes"]) {
                // line 361
                echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<img width=\"250px\" class=\"preview\" src=\"http://yankuserver.com/tamev2/public/uploads/supplier/catalog/";
                // line 364
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $context["imagenes"], "html", null, true);
                echo "\" alt=\"\"/>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<p>";
                // line 369
                echo twig_escape_filter($this->env, $context["imagenes"], "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"fileUpload btn btn-primary btn-sustitute\">
\t\t\t\t\t\t\t\t\t\t<span>Sustituir imagen</span>  
\t\t\t\t\t\t\t\t\t\t<!--<input class=\"upload\" type=\"file\">-->
\t\t\t\t\t\t\t\t\t\t<input name=\"changeImages\" data-last=\"";
                // line 374
                echo twig_escape_filter($this->env, $context["imagenes"], "html", null, true);
                echo "\" data-url=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
                echo "\" class=\"handle-upload btn upload\" type=\"file\" accept=\"image/jpeg\"  />\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger del-img\" data-origin=\"";
                // line 378
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
                echo "\" data-delete=\"";
                echo twig_escape_filter($this->env, $context["imagenes"], "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\tEliminar imagen
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t\t\t\t<!--<input name=\"changeImages\" data-last=\"";
                // line 384
                echo twig_escape_filter($this->env, $context["imagenes"], "html", null, true);
                echo "\" data-url=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
                echo "\" class=\"handle-upload btn\" type=\"file\" accept=\"image/jpeg\"  />
\t\t\t\t\t\t\t\t\t<p class=\"message-file\"></p>
\t\t\t\t\t\t\t\t\t<img class=\"new-preview\" width=\"250px\" src=\"#\" alt=\"tu imagen \" />-->
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div style=\"clear:both;\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagenes'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 393
            echo "\t\t\t\t\t\t";
        }
        // line 394
        echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
                    </div>\t\t\t\t\t
\t\t\t\t\t<br>
                    <div class=\"form-group col-sm-12 text-center\">
                        <input id=\"pid\" type=\"hidden\" name=\"pid\" value=\"";
        // line 399
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "idProduct", array()), "html", null, true);
        echo "\">
                        <button type=\"submit\" class=\"btn btn-success\">GUARDAR Y PUBLICAR CAMBIOS</button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class=\"modal fade containerNoPrint\" id=\"modalDelete\" tabindex=\"-1\" aria-labelledby=\"modalDelete\">
    <div class=\"modal-dialog modal-sm\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <h3 class=\"modal-title\">Confirmación</h3>
                <p>Se eliminara el producto: <br> <b>";
        // line 415
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "</b></p>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                <a href=\"";
        // line 419
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/delete/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "idProduct", array()), "html", null, true);
        echo "\" class=\"btn btn-danger\">Confirmar</a>
            </div>
        </div>
    </div>
</div>


<div id=\"containerPrint\">
    <div id=\"imagenesPrint\">
        <div>
            <div>
                <img src=\"";
        // line 430
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/uploads/supplier/catalog/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo ".jpg\">
            </div>
            
            ";
        // line 433
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["gallery"]) ? $context["gallery"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["img"]) {
            // line 434
            echo "            <div>
                 <img src=\"";
            // line 435
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["img"], "name", array()), "html", null, true);
            echo "\">
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['img'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 438
        echo "        </div>
    </div>
    <div id=\"descripcionPrint\">
        
        <div>
            <p>";
        // line 443
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "c_name", array()), "html", null, true);
        echo "</p>
            <h2>";
        // line 444
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "</h2>
            <h4>\$";
        // line 445
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "</h4>
            <p>";
        // line 446
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "header", array()), "html", null, true);
        echo "</p>
        </div>
        <div>
            <p>";
        // line 449
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "score", array()), "html", null, true);
        echo "</p>
        </div>                
        <div>
            <p>";
        // line 452
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "html", null, true);
        echo "</p>
            <p>
                <b>Cantidad minima de pedido: </b> ";
        // line 454
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "minimumOrder", array()), "html", null, true);
        echo "<br>
                <b>Item: </b> ";
        // line 455
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "item", array()), "html", null, true);
        echo "<br>
                <b>Supplier ID: </b> ";
        // line 456
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_custome", array()), "html", null, true);
        echo "<br>
                <b>Supplier Name: </b> <a href=\"";
        // line 457
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/products/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_id", array()), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "s_name", array()), "html", null, true);
        echo " </a><br>
                <b>Calidad: </b> ";
        // line 458
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quality", array()), "html", null, true);
        echo "<br>
                <b>Origen: </b> ";
        // line 459
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "origin", array()), "html", null, true);
        echo "<br>
                <b>Presentación: </b> ";
        // line 460
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "presentation", array()), "html", null, true);
        echo "
            </p>
            <div>
                <p>COLORES</p>
                ";
        // line 464
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["colors"]) ? $context["colors"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
            // line 465
            echo "                    <div>
                        <div></div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 469
        echo "            </div>
            <a>
                CARACTERISTICAS 
            </a>
            <p>
                ";
        // line 474
        if (twig_test_empty($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()))) {
            // line 475
            echo "                    <span>No hay una descripción</span>
                ";
        } else {
            // line 477
            echo "                    ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "characteristics", array()), "html", null, true);
            echo "
                ";
        }
        // line 479
        echo "            </p>
            <a>
                ESPECIFICACIONES 
            </a>
            <p>
                ";
        // line 484
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "especifications", array()), "html", null, true);
        echo "<br>
                <b>Dimensiones: </b> ";
        // line 485
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "dimensions", array()), "html", null, true);
        echo "<br>
                <b>Peso: </b> ";
        // line 486
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "weight", array()), "html", null, true);
        echo "<br>
            </p>
            <a>
                EMPAQUE
            </a>
            <p>
                <b>No. piezas: </b> ";
        // line 492
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_numPieces", array()), "html", null, true);
        echo "<br>
                <b>Especificaciones: </b> ";
        // line 493
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_especifications", array()), "html", null, true);
        echo "<br>
                <b>Alto: </b> ";
        // line 494
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_height", array()), "html", null, true);
        echo "cm<br>
                <b>Ancho: </b> ";
        // line 495
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_width", array()), "html", null, true);
        echo "cm<br>
                <b>Largo: </b> ";
        // line 496
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_large", array()), "html", null, true);
        echo "cm<br>
                <b>Peso:</b> ";
        // line 497
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "packing_weight", array()), "html", null, true);
        echo "kg
            </p>
            <a>
                LOGISTICA
            </a>
            <p>
                <b>Tiempo de producción: </b> ";
        // line 503
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_productionTime", array()), "html", null, true);
        echo "<br>
                <b>FOB: </b> ";
        // line 504
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fob", array()), "html", null, true);
        echo "<br>
                <b>Tiempo de entrega FOB: </b> ";
        // line 505
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "logistics_fobDeliveryTime", array()), "html", null, true);
        echo "<br>
                <b>Muestra fisica: </b> ";
        // line 506
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sample", array()), "html", null, true);
        echo "<br>
            </p>
            <a>
                PRECIO
            </a>
            <p>
                <b>Precio s/IVA: </b> \$";
        // line 512
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()), "html", null, true);
        echo "<br>
                <b>Moneda: </b> ";
        // line 513
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_currency", array()), "html", null, true);
        echo "<br>
                <b>Precio sugerido: </b> \$";
        // line 514
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_suggested", array()), "html", null, true);
        echo "<br>
                <b>Precio TT: </b> \$";
        // line 515
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()), "html", null, true);
        echo "<br>
                ";
        // line 516
        $context["utilidad"] = ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_tt", array()) - $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price_withoutTax", array()));
        // line 517
        echo "                <b>Utilidad: </b> \$";
        echo twig_escape_filter($this->env, (isset($context["utilidad"]) ? $context["utilidad"] : null), "html", null, true);
        echo "
            </p>
        </div>
    </div>
</div>
";
    }

    // line 523
    public function block_script($context, array $blocks = array())
    {
        // line 524
        echo "<script>
    /*  OCTAVIO */

    function imprimir() {

        if(\$(\"#wrapper\").toggleClass(\"toggled\")){
            console.log(\"se colapso...\");
            window.print();
        }
        //window.print();
    }

    \$(function() {
\t\t\t\$( \"#sortable\" ).sortable();
\t\t\t

\t
\t
        \$('#myCarousel').carousel({
            interval: 5000
        });
\t\t\$(\"body\").on('click','.modal-body .del-img',function(){
\t\t\tvar here = \$(this);\t
\t\t\tvar origin = \$(this).data('origin');
\t\t\tvar imagen = \$(this).data('delete');
\t\t\tvar ruta = '";
        // line 549
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/del_img';\t\t
\t\t\tswal({
\t\t\t  title: 'Estas seguro que deseas eliminar esta imagen',
\t\t\t  text: \"\",
\t\t\t  type: 'warning',
\t\t\t  showCancelButton: true,
\t\t\t  confirmButtonColor: '#3085d6',
\t\t\t  cancelButtonColor: '#d33',
\t\t\t  confirmButtonText: 'Eliminar!'
\t\t\t}).then(function() {
\t\t\t\t\t
\t\t\t\t\tconsole.log(origin);
\t\t\t\t\tconsole.log(imagen);
\t\t\t\t\tconsole.log(ruta);
\t\t\t\t\t
\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\turl: ruta,
\t\t\t\t\t\t\ttype: \"POST\",
\t\t\t\t\t\t\tdata: { origen: origin, imagen: imagen },
\t\t\t\t\t\t\tsuccess: function(datos)
\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\tif(datos==1){
\t\t\t\t\t\t\t\tvar img     = here.parent().parent().prev().remove();
\t\t\t\t\t\t\t\tvar actions = here.parent().parent().remove();
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t  swal(
\t\t\t\t\t\t\t\t\t'Eliminado!',
\t\t\t\t\t\t\t\t\t'Tu archivo se ha borrado correctamente.',
\t\t\t\t\t\t\t\t\t'success'\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t  )
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\telse{
\t\t\t\t\t\t\t\t\t  swal(
\t\t\t\t\t\t\t\t\t\t'Lo sentimos!',
\t\t\t\t\t\t\t\t\t\t'Intenta de nuevo.',
\t\t\t\t\t\t\t\t\t\t'success'\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t  )\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});\t\t\t\t
\t\t\t\t
\t\t\t  
\t\t\t})\t\t
\t\t\t/*var origin = \$(this).data('origin');
\t\t\tvar imagen = \$(this).data('delete');
\t\t\tvar ruta = '";
        // line 594
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/del_img';
\t\t\tif (confirm('Estas seguro que deseas eliminar esta imagen')){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: ruta,
\t\t\t\t\ttype: \"POST\",
\t\t\t\t\tdata: { origen: origin, imagen: imagen },
\t\t\t\t\tsuccess: function(datos)
\t\t\t\t\t{
\t\t\t\t\t\tconsole.log(datos);
\t\t\t\t\t\tif(datos==1){
\t\t\t\t\t\t\talert(\"Tu imagen se ha borrado correctamente.\");
\t\t\t\t\t\t\tlocation.reload();\t
\t\t\t\t\t\t}
\t\t\t\t\t\telse{
\t\t\t\t\t\t\talert(\"Tu imagen no se borro correctamente intenta de nuevo\");
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t});\t\t\t\t
\t\t\t}else{
\t\t\t   
\t\t\t}\t*/\t\t
\t\t
\t\t});
\t\t
\t\t//Hide progress
\t\t\$(\".upload-img\").hide();
\t\t\$(\".upload\").change(function(){
\t\t\t\$('#progress-message').text(\"Guarda la imagen pulsando el boton 'GUARDAR'\");
\t\t});
\t\t
\t\t//Handles the new upload file
\t\t\$('.save-img').on('click',function(){
\t\t\t\$('#progress-message').empty();
\t\t\tvar progressBar = document.getElementById(\"progress\");
\t\t\tvar newfile = document.getElementById('getsIm');
\t\t\tvar prefijo = \$('#getsIm').data('prefijo');
\t\t\tvar origen = \$('#getsIm').data('origen');
\t\t\t
\t\t\tconsole.log(newfile);
\t\t\tconsole.log(prefijo);
\t\t\tconsole.log(origen);
\t\t\t
\t\t\t// time life progress upload
\t\t\tfunction progress(e){

\t\t\t\tif(e.lengthComputable){
\t\t\t\t\t
\t\t\t\t\t\$(\".upload-img\").show();
\t\t\t\t\tprogressBar.max = e.total;
\t\t\t\t\tprogressBar.value = e.loaded;
\t\t\t\t\t
\t\t\t\t\tvar current = e.loaded / e.total; 
\t\t\t\t\tvar Percentage = current * 100;
\t\t\t\t\t
\t\t\t\t\tconsole.log(Percentage);
\t\t\t\t\t
\t\t\t\t\tvar percentReal = parseInt(Percentage) + ' %';
\t\t\t\t\t\$(\"#progress\").attr('data-original-title', percentReal);\t\t\t\t
\t\t\t\t\t\$('.tool').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show');
\t\t\t\t\t
\t\t\t\t\tif(Percentage >= 100)
\t\t\t\t\t{
\t\t\t\t\t\t\$(\"#progress\").attr('data-original-title', percentReal);
\t\t\t\t\t\t\$('.tool').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show');
\t\t\t\t\t\t\$('#progress-message').text('Se completo el proceso');
\t\t\t\t\t\t//alert('Se ha agregado la imagen satisfactoriamente.');
\t\t\t\t\t\t//location.reload();\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\telse{
\t\t\t\t\t\t\$('#progress-message').text('Cargando ...');
\t\t\t\t\t}
\t\t\t\t}  
\t\t\t }\t\t\t
\t\t\tinsertar(newfile);
\t\t\tfunction insertar(file){
\t\t\t\tif (file.files && file.files[0]) {
\t\t\t\t\tif(file.files[0].type == \"image/jpeg\")
\t\t\t\t\t{
\t\t\t\t\t\tvar reader = new FileReader();
\t\t\t\t\t\treader.onload = function (e) {
\t\t\t\t\t\t\t//Peticion ajax
\t\t\t\t\t\t\t\tvar formData = new FormData();
\t\t\t\t\t\t\t\t\tformData.append('imagen', newfile.files[0]);
\t\t\t\t\t\t\t\t\tformData.append('prefijo',prefijo);
\t\t\t\t\t\t\t\t\tformData.append('origen',origen);
\t\t\t\t\t\t\t\tvar ruta = '";
        // line 679
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/new_upload_img';
\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\turl: ruta,
\t\t\t\t\t\t\t\t\t\ttype: \"POST\",
\t\t\t\t\t\t\t\t\t\tdata: formData,
\t\t\t\t\t\t\t\t\t\txhr: function() {
\t\t\t\t\t\t\t\t\t\t\t\tvar myXhr = \$.ajaxSettings.xhr();
\t\t\t\t\t\t\t\t\t\t\t\tif(myXhr.upload){
\t\t\t\t\t\t\t\t\t\t\t\t\tmyXhr.upload.addEventListener('progress',progress, false);
\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\treturn myXhr;
\t\t\t\t\t\t\t\t\t\t},\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\tcontentType: false,
\t\t\t\t\t\t\t\t\t\tprocessData: false,
\t\t\t\t\t\t\t\t\t\tsuccess: function(datos)
\t\t\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\tvar result = JSON.parse(datos);
\t\t\t\t\t\t\t\t\t\t\tif(result.status==1)
\t\t\t\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\$('#progress-message').empty();
\t\t\t\t\t\t\t\t\t\t\t\t\$('#progress-message').text('Tu imagen se ha guardado exitosamente.');
\t\t\t\t\t\t\t\t\t\t\t\t\tswal(
\t\t\t\t\t\t\t\t\t\t\t\t\t  '',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'Se ha agregado la imagen satisfactoriamente',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'success'
\t\t\t\t\t\t\t\t\t\t\t\t\t)
\t\t\t\t\t\t\t\t\t\t\t\tvar name = result.name;
\t\t\t\t\t\t\t\t\t\t\t\t\taddImg(newfile,name);
\t\t\t\t\t\t\t\t\t\t\t\t//location.reload();\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t}else
\t\t\t\t\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\t\t\t\tswal(
\t\t\t\t\t\t\t\t\t\t\t\t\t  '',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'No se pudo completar la operacion, contacte al administrador.',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'error'
\t\t\t\t\t\t\t\t\t\t\t\t\t)
\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\terror: function(data){
\t\t\t\t\t\t\t\t\t\t\tswal(
\t\t\t\t\t\t\t\t\t\t\t  'Intente de nuevo',
\t\t\t\t\t\t\t\t\t\t\t  'Ocurrio un problema grave, por favor consulte al administrador.',
\t\t\t\t\t\t\t\t\t\t\t  'error'
\t\t\t\t\t\t\t\t\t\t\t)
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t}\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t});\t\t\t\t\t\t\t
\t\t\t\t\t\t\t//Fin de la peticion
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\telse{
\t\t\t\t\t
\t\t\t\t\t\tswal(
\t\t\t\t\t\t  'Intente de nuevo',
\t\t\t\t\t\t  'Imagen inválida, extensíon no permitida.',
\t\t\t\t\t\t  'error'
\t\t\t\t\t\t)\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t}

\t\t\t\t\treader.readAsDataURL(file.files[0]);
\t\t\t\t}
\t\t\t\telse{

\t\t\t\t\tswal(
\t\t\t\t\t  'Intente de nuevo',
\t\t\t\t\t  'Por favor seleccione una imagen válida.',
\t\t\t\t\t  'error'
\t\t\t\t\t)\t\t\t\t\t
\t\t\t\t}\t\t\t
\t\t\t}\t\t\t
\t\t});
\t\t//function add img recent upload
\t\tfunction addImg(file,name){
\t\t\tif (file.files && file.files[0]) {
\t\t\t\tif(file.files[0].type == \"image/jpeg\")
\t\t\t\t{
\t\t\t\t\tvar reader = new FileReader();
\t\t\t\t\t
\t\t\t\t\treader.onload = function (e) {
\t\t\t\t\tvar container = \$('#sortable');
\t\t\t\t\tvar containerli = container.find('li:nth-child(1)');
\t\t\t\t\t
\t\t\t\t\tvar li = document.createElement('li');
\t\t\t\t\t\tdiv1 = document.createElement('div');
\t\t\t\t\t\tdiv1.setAttribute('class','form-group col-sm-6');
\t\t\t\t\tvar div11 = document.createElement('div');
\t\t\t\t\tvar img = document.createElement('img');
\t\t\t\t\t\timg.setAttribute('width','250px');
\t\t\t\t\t\timg.setAttribute('class','preview');
\t\t\t\t\t\timg.setAttribute('src',e.target.result);
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tjQuery(div11).append(img);
\t\t\t\t\t\tjQuery(div1).append(div11);
\t\t\t\t\t\t
\t\t\t\t\t\tdiv2 = document.createElement('div');
\t\t\t\t\t\tdiv2.setAttribute('class','form-group col-sm-6');
\t\t\t\t\t\t// Nombre de imagen
\t\t\t\t\t\tdiv22 = document.createElement('div');
\t\t\t\t\t\tdiv22p = document.createElement('p');
\t\t\t\t\t\tjQuery(div22p).append(name+'.jpg');
\t\t\t\t\t\tjQuery(div22).append(div22p);
\t\t\t\t\t\tjQuery(div2).append(div22);
\t\t\t\t\t\t// Boton sustituir imagen
\t\t\t\t\t\tvar div3 = document.createElement('div');
\t\t\t\t\t\t\tdiv3.setAttribute('class','fileUpload btn btn-primary btn-sustitute');
\t\t\t\t\t\tvar div3span = document.createElement('span');
\t\t\t\t\t\t\tjQuery(div3span).append('Sustituir imagen');
\t\t\t\t\t\tvar div3button = document.createElement('input');
\t\t\t\t\t\tvar datamain = \$('body .modal').data('main');
\t\t\t\t\t\t\tdiv3button.setAttribute('name','changeImages');
\t\t\t\t\t\t\tdiv3button.setAttribute('data-last',name+'.jpg');
\t\t\t\t\t\t\tdiv3button.setAttribute('data-url',datamain);
\t\t\t\t\t\t\tdiv3button.setAttribute('class','handle-upload btn upload');
\t\t\t\t\t\t\tdiv3button.setAttribute('type','file');
\t\t\t\t\t\t\tdiv3button.setAttribute('accept','image/jpeg');
\t\t\t\t\t\tjQuery(div3).append(div3span);
\t\t\t\t\t\tjQuery(div3).append(div3button);
\t\t\t\t\t\tjQuery(div2).append(div3);
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t// Boton eliminar imagen
\t\t\t\t\t\tdiv4 = document.createElement('div');
\t\t\t\t\t\tdiv4button = document.createElement('button');
\t\t\t\t\t\tdiv4button.setAttribute('type','button');
\t\t\t\t\t\tdiv4button.setAttribute('class','btn btn-danger del-img');
\t\t\t\t\t\tdiv4button.setAttribute('data-origin',datamain);
\t\t\t\t\t\tdiv4button.setAttribute('data-delete',name+'.jpg');
\t\t\t\t\t\tjQuery(div4button).append('Eliminar imagen');
\t\t\t\t\t\tjQuery(div4).append(div4button);
\t\t\t\t\t\tjQuery(div2).append(div4);
\t\t\t\t\t\t
\t\t\t\t\t\tvar divClear = document.createElement('div');
\t\t\t\t\t\t\tdivClear.setAttribute('style','clear:both');
\t\t\t\t\t\t
\t\t\t\t\t\tjQuery(li).append(div1);
\t\t\t\t\t\tjQuery(li).append(div2);
\t\t\t\t\t\tjQuery(li).append(divClear);
\t\t\t\t\t\tjQuery(containerli).append(li);\t\t\t\t\t\t

\t\t\t\t\t}
\t\t\t\t}
\t\t\t\telse{
\t\t\t\t\tswal(
\t\t\t\t\t  '',
\t\t\t\t\t  'Imagen inválida, extensíon no permitida',
\t\t\t\t\t  'error'
\t\t\t\t\t)\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t}

\t\t\t\treader.readAsDataURL(file.files[0]);
\t\t\t}
\t\t\telse{
\t\t\t\tswal(
\t\t\t\t  '',
\t\t\t\t  'Por favor selecciona un archivo válido.',
\t\t\t\t  'error'
\t\t\t\t)\t\t\t\t
\t\t\t\t
\t\t\t}\t\t\t
\t\t}\t\t
\t\t//Handles the change upload file preview
\t\t\$(\"body\").on('change','.modal .handle-upload',function(){
\t\t\t
\t\t\tvar newfile = \$(this);
\t\t\tvar dis = this;
\t\t\tvar indice = newfile.index();
\t\t\tvar ultimo = newfile.data(\"last\");
\t\t\tvar url = newfile.data(\"url\");
\t\t\t
\t\t\tswal({
\t\t\t  title: 'Desea cambiarlo definitivamente?',
\t\t\t  text: \"Este cambio no es reversible!\",
\t\t\t  type: 'warning',
\t\t\t  showCancelButton: true,
\t\t\t  confirmButtonColor: '#3085d6',
\t\t\t  cancelButtonColor: '#d33',
\t\t\t  confirmButtonText: 'Cambiar'
\t\t\t}).then(function() {
\t\t\t  checar(dis);
\t\t\t  swal(
\t\t\t\t'',
\t\t\t\t'Tu imagen se ha cambiado satisfactoriamente.',
\t\t\t\t'success'
\t\t\t  )
\t\t\t}, function(dismiss) {
\t\t\t  if (dismiss === 'cancel') {
\t\t\t\tnewfile.replaceWith(newfile.clone(true));
\t\t\t\t//newfile.attr({ value: '' });
\t\t\t  }
\t\t\t})
\t\t\tfunction checar(file,mensaje){
\t\t\t\tif (file.files && file.files[0]) {
\t\t\t\t\tif(file.files[0].type == \"image/jpeg\")
\t\t\t\t\t{
\t\t\t\t\t\tvar reader = new FileReader();
\t\t\t\t\t\t
\t\t\t\t\t\treader.onload = function (e) {
\t\t\t\t\t\t\tchange = newfile.parent().parent().prev().find(\".preview\");
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tchange.attr('src', e.target.result);
\t\t\t\t\t\t\t//Peticion ajax
\t\t\t\t\t\t\t\tvar x= \$(\".handle-upload\");
\t\t\t\t\t\t\t\t\txy = newfile;
\t\t\t\t\t\t\t\tvar formData = new FormData(\$(\"#formEditProduct\")[0]);
\t\t\t\t\t\t\t\t\tformData.append(\"ultimo\", ultimo);
\t\t\t\t\t\t\t\t\tformData.append(\"url\",url);
\t\t\t\t\t\t\t\t\tformData.append(\"imagen\", file.files[0]);
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tvar ruta = '";
        // line 891
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/upload_img';
\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\turl: ruta,
\t\t\t\t\t\t\t\t\t\ttype: \"POST\",
\t\t\t\t\t\t\t\t\t\tdata: formData,
\t\t\t\t\t\t\t\t\t\tcontentType: false,
\t\t\t\t\t\t\t\t\t\tprocessData: false,
\t\t\t\t\t\t\t\t\t\tsuccess: function(datos)
\t\t\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\t\tswitch(datos){
\t\t\t\t\t\t\t\t\t\t\t\tcase \"0\":
\t\t\t\t\t\t\t\t\t\t\t\t\tswal(
\t\t\t\t\t\t\t\t\t\t\t\t\t  '',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'Por favor verifica que tu imagen sea válida o intenta nuevamente',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'error'
\t\t\t\t\t\t\t\t\t\t\t\t\t)\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\tbreak;
\t\t\t\t\t\t\t\t\t\t\t\tcase \"1\":
\t\t\t\t\t\t\t\t\t\t\t\t\tswal(
\t\t\t\t\t\t\t\t\t\t\t\t\t  '',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'Ha ocurrido un error, intenta nuevamente.',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'error'
\t\t\t\t\t\t\t\t\t\t\t\t\t)\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\tbreak;
\t\t\t\t\t\t\t\t\t\t\t\tcase \"2\":
\t\t\t\t\t\t\t\t\t\t\t\t\tswal(
\t\t\t\t\t\t\t\t\t\t\t\t\t  '',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'Ha ocurrido un error, intenta nuevamente.',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'error'
\t\t\t\t\t\t\t\t\t\t\t\t\t)\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\tbreak;
\t\t\t\t\t\t\t\t\t\t\t\tcase \"3\":
\t\t\t\t\t\t\t\t\t\t\t\t\tswal(
\t\t\t\t\t\t\t\t\t\t\t\t\t  '',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'Se ha cambiado esta imagen satisfactoriamente',
\t\t\t\t\t\t\t\t\t\t\t\t\t  'success'
\t\t\t\t\t\t\t\t\t\t\t\t\t)\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\tbreak;\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t});\t\t\t\t\t\t\t
\t\t\t\t\t\t\t//Fin de la peticion
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\telse{
\t\t\t\t\t\tswal(
\t\t\t\t\t\t  '',
\t\t\t\t\t\t  'Imagen inválida, extensíon no permitida',
\t\t\t\t\t\t  'error'
\t\t\t\t\t\t)\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t}

\t\t\t\t\treader.readAsDataURL(file.files[0]);
\t\t\t\t}
\t\t\t\telse{
\t\t\t\t\tswal(
\t\t\t\t\t  '',
\t\t\t\t\t  'Lo sentimos en este momento no podemos procesar tu imagen intenta de nuevo.',
\t\t\t\t\t  'error'
\t\t\t\t\t)\t\t\t\t
\t\t\t\t\t
\t\t\t\t}\t\t\t
\t\t\t}
\t\t});\t\t
        //Handles the carousel thumbnails
        \$('[id^=carousel-selector-]').click(function () {
            var id_selector = \$(this).attr(\"id\");
            try {
                var id = /-(\\d+)\$/.exec(id_selector)[1];
                console.log(id_selector, id);
                jQuery('#myCarousel').carousel(parseInt(id));
            } catch (e) {
                console.log('Regex failed!', e);
            }
        });
        // When the carousel slides, auto update the text
        \$('#myCarousel').on('slid.bs.carousel', function (e) {
            var id = \$('.item.active').data('slide-number');
            \$('#carousel-text').html(\$('#slide-content-'+id).html());
        });
        
        \$('#formEditProduct').on('submit', function (e) {
        e.preventDefault();

\t\t/*var formData = new FormData(\$(\"#formEditProduct\")[0]);
\t\t\tconsole.log(formData);
\t\tvar ruta = '";
        // line 978
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/upload_img';
\t\t\t\$.ajax({
\t\t\t\turl: ruta,
\t\t\t\ttype: \"POST\",
\t\t\t\tdata: formData,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tsuccess: function(datos)
\t\t\t\t{
\t\t\t\t\tconsole.log(datos);
\t\t\t\t}
\t\t\t});
\t\t\t
\t\t\te.preventDefault();*/
        var btn = \$(this).find('button');

        var id = \$('#pid').val();
        var row = \$('#rowid').val();
        var pName = \$(\"#pname\").val();
        var pScore = \$(\"#pscore\").val();
        var pHeader = \$(\"#pheader\").val();
        var pDescription = \$(\"#pdescription\").val();
        var pItem = \$('#pitem').val();
        var pCategory = \$('#pcategory').val();
        var pSubcategory = \$('#psubcat').val();
        var pQuality = \$('#pquality').val();
        var pOrigin = \$('#porigin').val();
        var pColors = \$('#pcolor').val();
        var pPresentation = \$('#ppresentation').val();
        var pCharacteristics = \$('#pcharacteristics').val();
        var pEspecifications = \$('#pspecifications').val();
        var pDimensions = \$('#pdimensions').val();
        var pWeight = \$('#pweight').val();
        var pMinimumOrder = \$('#pminimumOrder').val();
        var pDateExpiry = \$('#pdateExpiry').val();
        var pInvNumPieces = \$('#pinvNumPieces').val();
        var pBoutiqueMaxOrder = \$('#pboutiqueMaxOrder').val();
        var pPriceWithoutTax = \$('#ppriceWithoutTax').val();
        var pPriceCurrency = \$('#ppriceCurrency').val();
        var pPriceSuggested = \$('#ppriceSuggested').val();
        var pPriceTT = \$('#ppriceTT').val();
        var pPackingNumPieces = \$('#ppackingNumPieces').val();
        var pPackingEspecifications = \$('#ppackingSpecifications').val();
        var pPackingHeight = \$('#ppackingHeight').val();
        var pPackingWidth = \$('#ppackingWidth').val();
        var pPackingLarge = \$('#ppackingLarge').val();
        var pPackingWeight = \$('#ppackingWeight').val();
        var pPackingLogProductionTime = \$('#plogTimeProduction').val();
        var pLogFob = \$('#plogFob').val();
        var pLogFobDeliveryTime = \$('#plogFobDeliveryTime').val();
        var pSample = \$('#psample').val();

         console.log(id)

        \$.ajax({
            url: '";
        // line 1033
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/edit',
            type: 'post',
            dataType: 'html',
            cache: false,
            data: {
                pId : id,
                pName : pName,
                pScore : pScore,
                pHeader : pHeader,
                pDescription : pDescription,
                pItem : pItem,
                pCategory : pCategory,
                pSubcategory : pSubcategory,
                pQuality : pQuality,
                pOrigin : pOrigin,
                pColors : pColors,
                pPresentation : pPresentation,
                pCharacteristics : pCharacteristics,
                pSpecifications : pEspecifications,
                pDimensions : pDimensions,
                pWeight : pWeight,
                pMinimumOrder : pMinimumOrder,
                pDateExpiry : pDateExpiry,
                pInvNumPieces : pInvNumPieces,
                pBoutiqueMaxOrder : pBoutiqueMaxOrder,
                pPriceWithoutTax : pPriceWithoutTax,
                pPriceCurrency : pPriceCurrency,
                pPriceSuggested : pPriceSuggested,
                pPriceTT : pPriceTT,
                pPackingNumPieces : pPackingNumPieces,
                pPackingSpecifications : pPackingEspecifications,
                pPackingHeight : pPackingHeight,
                pPackingWidth : pPackingWidth,
                pPackingLarge : pPackingLarge,
                pPackingWeight : pPackingWeight,
                pLogProductionTime : pPackingLogProductionTime,
                pLogFob : pLogFob,
                pLogFobDeliveryTime : pLogFobDeliveryTime,
                pSample : pSample
            },
            beforeSend: function() {
                btn.prop('disabled', true)
            },
            success: function(response) {

                if (response == '1') {

                    alert('Editado correctamente');
                    location.reload();
                    
                    btn.prop('disabled', false);
                } else {
                    
                }
            },
            error: function(xhr, textStatus) {
                alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');
                btn.prop('disabled', false);
            }
        });
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/catalog-product.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1544 => 1033,  1486 => 978,  1396 => 891,  1181 => 679,  1093 => 594,  1045 => 549,  1018 => 524,  1015 => 523,  1004 => 517,  1002 => 516,  998 => 515,  994 => 514,  990 => 513,  986 => 512,  977 => 506,  973 => 505,  969 => 504,  965 => 503,  956 => 497,  952 => 496,  948 => 495,  944 => 494,  940 => 493,  936 => 492,  927 => 486,  923 => 485,  919 => 484,  912 => 479,  906 => 477,  902 => 475,  900 => 474,  893 => 469,  884 => 465,  880 => 464,  873 => 460,  869 => 459,  865 => 458,  857 => 457,  853 => 456,  849 => 455,  845 => 454,  840 => 452,  834 => 449,  828 => 446,  822 => 445,  818 => 444,  814 => 443,  807 => 438,  794 => 435,  791 => 434,  787 => 433,  777 => 430,  761 => 419,  754 => 415,  735 => 399,  728 => 394,  725 => 393,  708 => 384,  697 => 378,  688 => 374,  680 => 369,  670 => 364,  665 => 361,  660 => 360,  656 => 358,  654 => 357,  633 => 341,  596 => 307,  589 => 303,  575 => 292,  568 => 288,  561 => 284,  554 => 280,  540 => 269,  533 => 265,  526 => 261,  519 => 257,  512 => 253,  505 => 249,  493 => 240,  486 => 236,  479 => 232,  470 => 226,  458 => 217,  451 => 213,  442 => 207,  430 => 198,  423 => 194,  414 => 188,  407 => 184,  398 => 178,  391 => 174,  384 => 170,  377 => 166,  370 => 162,  363 => 158,  356 => 154,  349 => 150,  335 => 139,  324 => 131,  313 => 125,  296 => 112,  294 => 111,  290 => 110,  286 => 109,  282 => 108,  278 => 107,  268 => 100,  264 => 99,  260 => 98,  256 => 97,  246 => 90,  242 => 89,  238 => 88,  234 => 87,  230 => 86,  226 => 85,  215 => 77,  211 => 76,  207 => 75,  199 => 69,  193 => 67,  189 => 65,  187 => 64,  180 => 59,  171 => 56,  168 => 55,  164 => 54,  157 => 50,  153 => 49,  149 => 48,  145 => 47,  137 => 46,  133 => 45,  129 => 44,  125 => 43,  119 => 40,  113 => 37,  107 => 34,  101 => 33,  97 => 32,  93 => 31,  85 => 25,  72 => 22,  69 => 21,  65 => 20,  55 => 17,  48 => 12,  45 => 11,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div class="container-fluid containerNoPrint">*/
/*     <div class="row">*/
/*         <div class="col-sm-12 col-lg-5 col-lg-offset-1 margin-bottom--20" id="imagenesPrint">*/
/*             <div class="row">*/
/*                 <!--<div class="col-sm-12 margin-bottom--50">*/
/*                     <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ product.s_directory }}/{{ product.item }}.jpg" class="img-responsive img-center">*/
/*                 </div>-->*/
/* 				*/
/*                 {% for img in gallery %}*/
/*                 <div class="col-sm-12 margin-bottom--50">*/
/*                      <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ product.s_directory }}/{{ img.name }}" class="img-responsive">*/
/*                 </div>*/
/*                 {% endfor %}*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-sm-12 col-lg-5" style="position: relative; " id="descripcionPrint">*/
/*             <div class="row" style="position: fixed; width: 30%;">*/
/*                 <div class="col-sm-12 margin-bottom--20" style="max-height: 500px;overflow-y: auto" id="desPrint">*/
/*                   <div class="col-lg-8">*/
/*                     <p class="text-muted">{{ product.c_name }}</p>*/
/*                     <h2 class="margin-clear margin-bottom--10">{{ product.name }}</h2>*/
/*                     <h4 class="margin-clear margin-bottom--10 text-muted">${{ product.price_tt }} {{ product.price_currency }}</h4>*/
/*                     <p class="lead margin-clear margin-bottom--10">{{ product.header }}</p>*/
/*                 </div>*/
/*                 <div class="col-lg-4 text-right">*/
/*                     <p class="h1">{{ product.score }}</p>*/
/*                 </div>                */
/*                 <div class="col-sm-12">*/
/*                     <p>{{ product.description }}</p>*/
/*                     <hr>*/
/*                     <p>*/
/*                         <b>Cantidad minima de pedido: </b> {{ product.minimumOrder }}<br>*/
/*                         <b>Item: </b> {{ product.item }}<br>*/
/*                         <b>Supplier ID: </b> {{ product.s_custome }}<br>*/
/*                         <b>Supplier Name: </b> <a href="{{ base_url() }}dashboard/admin/supplier/products/{{ product.s_id }}"> {{ product.s_name }} </a><br>*/
/*                         <b>Calidad: </b> {{ product.quality }}<br>*/
/*                         <b>Origen: </b> {{ product.origin2 }}<br>*/
/*                         <b>País: </b> {{ product.origin }}<br>*/
/*                         <b>Presentación: </b> {{ product.presentation }}*/
/*                     </p>*/
/*                     <div class="row text-center" style="display:flex; padding: 15px;">*/
/*                         <p>COLORES</p>*/
/*                         {% for color in colors %}*/
/*                             <div class="col-sm-1" style="display:flex; flex: 1;">*/
/*                                 <div class="panel" style="flex: 1; height: 20px; background: {{ color }}"></div>*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                     </div>*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapseCharacteristics" aria-expanded="false" aria-controls="collapseExample">*/
/*                         CARACTERISTICAS <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapseCharacteristics">*/
/*                         {% if product.characteristics is empty %}*/
/*                             <span class="text-muted">No hay una descripción</span>*/
/*                         {% else %}*/
/*                             {{ product.characteristics }}*/
/*                         {% endif %}*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapseEspecifications" aria-expanded="false" aria-controls="collapseExample">*/
/*                         ESPECIFICACIONES <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapseEspecifications">*/
/*                         {{ product.especifications }}<br><br>*/
/*                         <b>Dimensiones: </b> {{ product.dimensions }}<br>*/
/*                         <b>Peso: </b> {{ product.weight }}<br><br>*/
/*                         */
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapsePacking" aria-expanded="false" aria-controls="collapseExample">*/
/*                         EMPAQUE <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapsePacking">*/
/*                         <b>No. piezas: </b> {{ product.packing_numPieces }}<br>*/
/*                         <b>Especificaciones: </b> {{ product.packing_especifications }}<br>*/
/*                         <b>Alto: </b> {{ product.packing_height }}cm<br>*/
/*                         <b>Ancho: </b> {{ product.packing_width }}cm<br>*/
/*                         <b>Largo: </b> {{ product.packing_large }}cm<br>*/
/*                         <b>Peso:</b> {{ product.packing_weight }}kg*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapseLogistic" aria-expanded="false" aria-controls="collapseExample">*/
/*                         LOGISTICA <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapseLogistic">*/
/*                         <b>Tiempo de producción: </b> {{ product.logistics_productionTime }}<br>*/
/*                         <b>FOB: </b> {{ product.logistics_fob }}<br>*/
/*                         <b>Tiempo de entrega FOB: </b> {{ product.logistics_fobDeliveryTime }}<br>*/
/*                         <b>Muestra fisica: </b> {{ product.sample }}<br>*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                     <a class="btn btn-link btn-block" role="button" data-toggle="collapse" href="#collapsePrice" aria-expanded="false" aria-controls="collapseExample">*/
/*                         PRECIO <i class="fa fa-angle-down fa-lg"></i>*/
/*                     </a>*/
/*                     <p class="collapse" id="collapsePrice">*/
/*                         <b>Precio s/IVA: </b> ${{ product.price_withoutTax}}<br>*/
/*                         <b>Moneda: </b> {{ product.price_currency }}<br>*/
/*                         <b>Precio sugerido: </b> ${{ product.price_suggested }}<br>*/
/*                         <b>Precio TT: </b> ${{ product.price_tt }}<br>*/
/*                         {% set utilidad = product.price_tt - product.price_withoutTax %}*/
/*                         <b>Utilidad: </b> ${{ utilidad }}*/
/*                     </p>*/
/*                     <hr class="margin-clear margin-bottom--10">*/
/*                 </div>*/
/*                 */
/*                   */
/*               </div>*/
/*                 <div class="col-sm-12 margin-bottom--10">*/
/*                     <button class="btn btn-block" data-toggle="modal" data-target="#editProduct">*/
/*                         EDITAR*/
/*                     </button>*/
/*                 </div>*/
/*                 <div class="col-sm-12 margin-bottom--10">                                                            */
/*                     <a href="{{ base_url() }}dashboard/admin/product/tcpdf/{{ product.idProduct }}" target="_blank" class="btn btn-block">IMPRIMIR</a>*/
/*                     <!-- <button class="btn btn-block" onclick="imprimir()">*/
/*                         IMPRIMIR*/
/*                     </button> -->*/
/*                 </div>*/
/*                 <div class="col-sm-12">*/
/*                     <button data-id="{{ product.idProduct }}" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modalDelete">*/
/*                         ELIMINAR*/
/*                     </button>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade containerNoPrint" id="editProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-main="{{product.s_directory}}">*/
/*     <div class="modal-dialog modal-lg" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header head-edit">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*                 <h4 class="modal-title" id="myModalLabel"><b>Editar producto</b></h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*                 <form id="formEditProduct" action="" class="row" enctype="multipart/form-data">*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcategory">Categoría</label>*/
/*                         <input id="pcategory" name="pcategory" type="text" class="form-control" value="{{ product.categoryA }}">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pscore">Calificación</label>*/
/*                         <input id="pscore" name="pscore" type="text" class="form-control" value="{{ product.score }}">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="psubcat">Subcategoría</label>*/
/*                         <input id="psubcat" name="psubcat" type="text" class="form-control" value="{{ product.subcategoryA }}">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pminimumOrder">Cantidad mínima de pedido</label>*/
/*                         <input id="pminimumOrder" name="pminimumOrder" type="text" class="form-control" value="{{ product.minimumOrder }}">*/
/*                     </div>					*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pname">Nombre</label>*/
/*                         <input id="pname" name="pname" type="text" class="form-control" value="{{ product.name }}">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pboutiqueMaxOrder">Cantidad máximo de pedido</label>*/
/*                         <input id="pboutiqueMaxOrder" name="pboutiqueMaxOrder" type="text" class="form-control" value="{{ product.boutique_maximumOrder }}">*/
/*                     </div>					*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceTT">Precio TT</label>*/
/*                         <input id="ppriceTT" name="ppriceTT" type="text" class="form-control" value="{{ product.price_tt }}">*/
/*                     </div>	*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pinvNumPieces">Inventario No. de piezas</label>*/
/*                         <input id="pinvNumPieces" name="pinvNumPieces" type="text" class="form-control" value="{{ product.inventory_numPieces }}">*/
/*                     </div>*/
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="pheader">Header</label>*/
/* 								<textarea id="pheader" rows="3" name="pheader" type="text" class="form-control">{{ product.header }}</textarea>*/
/* 							</div>*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="pdateExpiry">Fecha de caducidad</label>*/
/* 								<input id="pdateExpiry" name="pdateExpiry" type="text" class="form-control" value="{{ product.dataExpiry }}">*/
/* 							</div>						*/
/* 						</div>*/
/* 					</div>					*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdescription">Descripción</label>*/
/*                         <textarea id="pdescription" rows="3" name="pdescription" type="text" class="form-control">{{ product.description }}</textarea>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pitem">Item</label>*/
/*                         <input id="pitem" name="pitem" type="text" class="form-control" value="{{ product.item }}">*/
/*                     </div>*/
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<div class="form-group col-sm-6">*/
/* 								<h3><b>Características</b></h3>*/
/* 							</div>*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="pquality">Calidad</label>*/
/* 								<input id="pquality" name="pquality" type="text" class="form-control" value="{{ product.quality }}">*/
/* 							</div>						*/
/* 						</div>					*/
/* 					</div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcharacteristics">Caracteristicas</label>*/
/*                         <input id="pcharacteristics" name="pcharacteristics" type="text" class="form-control" value="{{ product.characteristics }}">*/
/*                     </div>					*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="porigin">Origen</label>*/
/*                         <input id="porigin" name="porigin" type="text" class="form-control" value="{{ product.origin }}">*/
/*                     </div>*/
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<div class="form-group col-sm-6">*/
/* 								<h3><b>Especificaciones</b></h3>*/
/* 							</div>*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="ppresentation">Presentación</label>*/
/* 								<input id="ppresentation" name="ppresentation" type="text" class="form-control" value="{{ product.presentation }}">*/
/* 							</div> 							*/
/* 						</div>*/
/* 					</div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdimensions">Dimensiones</label>*/
/*                         <input id="pdimensions" name="pdimensions" type="text" class="form-control" value="{{ product.dimensions }}">*/
/*                     </div>					*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcolor">Colores</label>*/
/*                         <input id="pcolor" name="pcolor" type="text" class="form-control" value="{{ product.colors }}">*/
/*                     </div>                   */
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pweight">Peso</label>*/
/*                         <input id="pweight" name="pweight" type="text" class="form-control" value="{{ product.weight }}">*/
/*                     </div>*/
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<div class="form-group col-sm-12">*/
/* 								<h3><b>Empaque</b></h3>*/
/* 							</div>*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="ppackingNumPieces">Empaque - No. de Piezas</label>*/
/* 								<input id="ppackingNumPieces" name="ppackingNumPieces" type="text" class="form-control" value="{{ product.packing_numPieces }}">*/
/* 							</div>							*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="ppackingWidth">Empaque - Ancho</label>*/
/* 								<input id="ppackingWidth" name="ppackingWidth" type="text" class="form-control" value="{{ product.packing_width }}">*/
/* 							</div>							*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="ppackingSpecifications">Empaque - Especifiaciones</label>*/
/* 								<input id="ppackingSpecifications" name="ppackingSpecifications" type="text" class="form-control" value="{{ product.packing_especifications }}">*/
/* 							</div>								*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="ppackingLarge">Empaque - Largo</label>*/
/* 								<input id="ppackingLarge" name="ppackingLarge" type="text" class="form-control" value="{{ product.packing_large }}">*/
/* 							</div>							*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="ppackingHeight">Empaque - Altura</label>*/
/* 								<input id="ppackingHeight" name="ppackingHeight" type="text" class="form-control" value="{{ product.packing_height }}">*/
/* 							</div>							*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="ppackingWeight">Empaque - Peso</label>*/
/* 								<input id="ppackingWeight" name="ppackingWeight" type="text" class="form-control" value="{{ product.packing_weight }}">*/
/* 							</div>												*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<div class="form-group col-sm-12">*/
/* 								<h3><b>Logística</b></h3>*/
/* 							</div>*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="plogTimeProduction">Logistica - Tiempo de producción</label>*/
/* 								<input id="plogTimeProduction" name="pLogTimeProduction" type="text" class="form-control" value="{{ product.logistics_productionTime }}">*/
/* 							</div>	*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="plogFobDeliveryTime">Logistica - FOB Tiempo de entrega</label>*/
/* 								<input id="plogFobDeliveryTime" name="pLogFobDeliveryTime" type="text" class="form-control" value="{{ product.logistics_fobDeliveryTime }}">*/
/* 							</div>*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="plogFob">Logistica - FOB</label>*/
/* 								<input id="plogFob" name="pLogFob" type="text" class="form-control" value="{{ product.logistics_fob }}">*/
/* 							</div>*/
/* 							<div class="form-group col-sm-6">*/
/* 								<label for="psample">Muestra</label>*/
/* 								<input id="psample" name="psample" type="text" class="form-control" value="{{ product.sample }}">*/
/* 							</div>					*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<div class="form-group col-sm-12">*/
/* 								<h3><b>Precio</b></h3>*/
/* 							</div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceWithoutTax">Precio sin impuesto</label>*/
/*                         <input id="ppriceWithoutTax" name="ppriceWithoutTax" type="text" class="form-control" value="{{ product.price_withoutTax }}">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceSuggested">Precio Sugerido</label>*/
/*                         <input id="ppriceSuggested" name="ppriceSuggested" type="text" class="form-control" value="{{ product.price_suggested }}">*/
/*                     </div>					*/
/* 						</div>*/
/* 					</div>*/
/* */
/* */
/* */
/* 					<div class="form-group col-sm-12">*/
/* 					&nbsp;*/
/* 					</div>*/
/* 					<div class="form-group col-sm-6">*/
/* 								<h3><b>Galería</b></h3>*/
/* 								<label for="insertImages"><b>Agregar imágenes</b></label>*/
/* 								<!----test button--->*/
/* 								<style>*/
/* 									.fileUpload {*/
/* 										position: relative;*/
/* 										overflow: hidden;*/
/* 									}*/
/* 									.fileUpload input.upload {*/
/* 										position: absolute;*/
/* 										top: 0;*/
/* 										right: 0;*/
/* 										margin: 0;*/
/* 										padding: 0;*/
/* 										font-size: 20px;*/
/* 										cursor: pointer;*/
/* 										opacity: 0;*/
/* 										filter: alpha(opacity=0);*/
/* 									}								*/
/* 								</style>*/
/* 								<div class="fileUpload btn btn-blue">*/
/* 									<span>SELECCIONAR ARCHIVO</span>  */
/* 									<!--<input class="upload" type="file">-->*/
/* 									<input name="insertImages" type="file" class="handle-new upload" data-origen="{{product.s_directory}}" data-prefijo='{{ product.item }}' accept="image/jpeg" style="padding-left: 0;" id="getsIm"/>									*/
/* 								</div>								*/
/* 								<!----test end--->*/
/* 								<button type="button" class="btn btn-primary save-img" >GUARDAR</button>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col-lg-6 col-lg-offset-3">*/
/* 							<span id="progress-message" class="text-center"></span>*/
/* 							<progress class="upload-img tool" id="progress" value="0" max="100" data-toggle="tooltip" title="0%"></progress>													*/
/* 						</div>*/
/* 					</div>*/
/*                     <div class="form-group col-sm-12">*/
/* 						<div class="col-lg-12">*/
/* 							<label for="preview"><p>Preview</p></label>						*/
/* 						<div>*/
/* 						<ul id="sortable" >*/
/* 						{% if getImages|length ==0  %}*/
/* 							<li></li>*/
/* 						{% else %}*/
/*                         {% for imagenes in getImages %}*/
/* 							<li>*/
/* 								<div class="form-group col-sm-6">*/
/* 									<div>*/
/* 										<img width="250px" class="preview" src="http://yankuserver.com/tamev2/public/uploads/supplier/catalog/{{product.s_directory}}/{{imagenes}}" alt=""/>*/
/* 									</div>*/
/* 								</div>							*/
/* 								<div class="form-group col-sm-6">*/
/* 									<div>*/
/* 										<p>{{imagenes}}</p>*/
/* 									</div>*/
/* 									<div class="fileUpload btn btn-primary btn-sustitute">*/
/* 										<span>Sustituir imagen</span>  */
/* 										<!--<input class="upload" type="file">-->*/
/* 										<input name="changeImages" data-last="{{imagenes}}" data-url="{{product.s_directory}}" class="handle-upload btn upload" type="file" accept="image/jpeg"  />									*/
/* 									</div>										*/
/* */
/* 									<div>*/
/* 										<button type="button" class="btn btn-danger del-img" data-origin="{{product.s_directory}}" data-delete="{{imagenes}}">*/
/* 										Eliminar imagen*/
/* 										</button>*/
/* 									</div>*/
/* 									*/
/* 									<br><br>*/
/* 									<!--<input name="changeImages" data-last="{{imagenes}}" data-url="{{product.s_directory}}" class="handle-upload btn" type="file" accept="image/jpeg"  />*/
/* 									<p class="message-file"></p>*/
/* 									<img class="new-preview" width="250px" src="#" alt="tu imagen " />-->*/
/* 								</div>						*/
/* 							*/
/* 								<div style="clear:both;">*/
/* 								</div>*/
/* 							</li>*/
/*                         {% endfor %}*/
/* 						{% endif %}*/
/* 						</ul>*/
/* 						</div>							*/
/*                     </div>					*/
/* 					<br>*/
/*                     <div class="form-group col-sm-12 text-center">*/
/*                         <input id="pid" type="hidden" name="pid" value="{{ product.idProduct }}">*/
/*                         <button type="submit" class="btn btn-success">GUARDAR Y PUBLICAR CAMBIOS</button>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade containerNoPrint" id="modalDelete" tabindex="-1" aria-labelledby="modalDelete">*/
/*     <div class="modal-dialog modal-sm" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-body">*/
/*                 <h3 class="modal-title">Confirmación</h3>*/
/*                 <p>Se eliminara el producto: <br> <b>{{ product.name }}</b></p>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>*/
/*                 <a href="{{ base_url() }}dashboard/admin/product/delete/{{ product.idProduct }}" class="btn btn-danger">Confirmar</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* */
/* <div id="containerPrint">*/
/*     <div id="imagenesPrint">*/
/*         <div>*/
/*             <div>*/
/*                 <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ product.s_directory }}/{{ product.item }}.jpg">*/
/*             </div>*/
/*             */
/*             {% for img in gallery %}*/
/*             <div>*/
/*                  <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ product.s_directory }}/{{ img.name }}">*/
/*             </div>*/
/*             {% endfor %}*/
/*         </div>*/
/*     </div>*/
/*     <div id="descripcionPrint">*/
/*         */
/*         <div>*/
/*             <p>{{ product.c_name }}</p>*/
/*             <h2>{{ product.name }}</h2>*/
/*             <h4>${{ product.price_tt }} {{ product.price_currency }}</h4>*/
/*             <p>{{ product.header }}</p>*/
/*         </div>*/
/*         <div>*/
/*             <p>{{ product.score }}</p>*/
/*         </div>                */
/*         <div>*/
/*             <p>{{ product.description }}</p>*/
/*             <p>*/
/*                 <b>Cantidad minima de pedido: </b> {{ product.minimumOrder }}<br>*/
/*                 <b>Item: </b> {{ product.item }}<br>*/
/*                 <b>Supplier ID: </b> {{ product.s_custome }}<br>*/
/*                 <b>Supplier Name: </b> <a href="{{ base_url() }}dashboard/admin/supplier/products/{{ product.s_id }}"> {{ product.s_name }} </a><br>*/
/*                 <b>Calidad: </b> {{ product.quality }}<br>*/
/*                 <b>Origen: </b> {{ product.origin }}<br>*/
/*                 <b>Presentación: </b> {{ product.presentation }}*/
/*             </p>*/
/*             <div>*/
/*                 <p>COLORES</p>*/
/*                 {% for color in colors %}*/
/*                     <div>*/
/*                         <div></div>*/
/*                     </div>*/
/*                 {% endfor %}*/
/*             </div>*/
/*             <a>*/
/*                 CARACTERISTICAS */
/*             </a>*/
/*             <p>*/
/*                 {% if product.characteristics is empty %}*/
/*                     <span>No hay una descripción</span>*/
/*                 {% else %}*/
/*                     {{ product.characteristics }}*/
/*                 {% endif %}*/
/*             </p>*/
/*             <a>*/
/*                 ESPECIFICACIONES */
/*             </a>*/
/*             <p>*/
/*                 {{ product.especifications }}<br>*/
/*                 <b>Dimensiones: </b> {{ product.dimensions }}<br>*/
/*                 <b>Peso: </b> {{ product.weight }}<br>*/
/*             </p>*/
/*             <a>*/
/*                 EMPAQUE*/
/*             </a>*/
/*             <p>*/
/*                 <b>No. piezas: </b> {{ product.packing_numPieces }}<br>*/
/*                 <b>Especificaciones: </b> {{ product.packing_especifications }}<br>*/
/*                 <b>Alto: </b> {{ product.packing_height }}cm<br>*/
/*                 <b>Ancho: </b> {{ product.packing_width }}cm<br>*/
/*                 <b>Largo: </b> {{ product.packing_large }}cm<br>*/
/*                 <b>Peso:</b> {{ product.packing_weight }}kg*/
/*             </p>*/
/*             <a>*/
/*                 LOGISTICA*/
/*             </a>*/
/*             <p>*/
/*                 <b>Tiempo de producción: </b> {{ product.logistics_productionTime }}<br>*/
/*                 <b>FOB: </b> {{ product.logistics_fob }}<br>*/
/*                 <b>Tiempo de entrega FOB: </b> {{ product.logistics_fobDeliveryTime }}<br>*/
/*                 <b>Muestra fisica: </b> {{ product.sample }}<br>*/
/*             </p>*/
/*             <a>*/
/*                 PRECIO*/
/*             </a>*/
/*             <p>*/
/*                 <b>Precio s/IVA: </b> ${{ product.price_withoutTax}}<br>*/
/*                 <b>Moneda: </b> {{ product.price_currency }}<br>*/
/*                 <b>Precio sugerido: </b> ${{ product.price_suggested }}<br>*/
/*                 <b>Precio TT: </b> ${{ product.price_tt }}<br>*/
/*                 {% set utilidad = product.price_tt - product.price_withoutTax %}*/
/*                 <b>Utilidad: </b> ${{ utilidad }}*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/*     /*  OCTAVIO *//* */
/* */
/*     function imprimir() {*/
/* */
/*         if($("#wrapper").toggleClass("toggled")){*/
/*             console.log("se colapso...");*/
/*             window.print();*/
/*         }*/
/*         //window.print();*/
/*     }*/
/* */
/*     $(function() {*/
/* 			$( "#sortable" ).sortable();*/
/* 			*/
/* */
/* 	*/
/* 	*/
/*         $('#myCarousel').carousel({*/
/*             interval: 5000*/
/*         });*/
/* 		$("body").on('click','.modal-body .del-img',function(){*/
/* 			var here = $(this);	*/
/* 			var origin = $(this).data('origin');*/
/* 			var imagen = $(this).data('delete');*/
/* 			var ruta = '{{ base_url() }}dashboard/admin/product/action/del_img';		*/
/* 			swal({*/
/* 			  title: 'Estas seguro que deseas eliminar esta imagen',*/
/* 			  text: "",*/
/* 			  type: 'warning',*/
/* 			  showCancelButton: true,*/
/* 			  confirmButtonColor: '#3085d6',*/
/* 			  cancelButtonColor: '#d33',*/
/* 			  confirmButtonText: 'Eliminar!'*/
/* 			}).then(function() {*/
/* 					*/
/* 					console.log(origin);*/
/* 					console.log(imagen);*/
/* 					console.log(ruta);*/
/* 					*/
/* 						$.ajax({*/
/* 							url: ruta,*/
/* 							type: "POST",*/
/* 							data: { origen: origin, imagen: imagen },*/
/* 							success: function(datos)*/
/* 							{*/
/* 								if(datos==1){*/
/* 								var img     = here.parent().parent().prev().remove();*/
/* 								var actions = here.parent().parent().remove();*/
/* 								*/
/* 								  swal(*/
/* 									'Eliminado!',*/
/* 									'Tu archivo se ha borrado correctamente.',*/
/* 									'success'									*/
/* 									  )*/
/* 								}*/
/* 								else{*/
/* 									  swal(*/
/* 										'Lo sentimos!',*/
/* 										'Intenta de nuevo.',*/
/* 										'success'									*/
/* 										  )									*/
/* 								}*/
/* 							}*/
/* 						});				*/
/* 				*/
/* 			  */
/* 			})		*/
/* 			/*var origin = $(this).data('origin');*/
/* 			var imagen = $(this).data('delete');*/
/* 			var ruta = '{{ base_url() }}dashboard/admin/product/action/del_img';*/
/* 			if (confirm('Estas seguro que deseas eliminar esta imagen')){*/
/* 				$.ajax({*/
/* 					url: ruta,*/
/* 					type: "POST",*/
/* 					data: { origen: origin, imagen: imagen },*/
/* 					success: function(datos)*/
/* 					{*/
/* 						console.log(datos);*/
/* 						if(datos==1){*/
/* 							alert("Tu imagen se ha borrado correctamente.");*/
/* 							location.reload();	*/
/* 						}*/
/* 						else{*/
/* 							alert("Tu imagen no se borro correctamente intenta de nuevo");*/
/* 						}*/
/* 					}*/
/* 				});				*/
/* 			}else{*/
/* 			   */
/* 			}	*//* 		*/
/* 		*/
/* 		});*/
/* 		*/
/* 		//Hide progress*/
/* 		$(".upload-img").hide();*/
/* 		$(".upload").change(function(){*/
/* 			$('#progress-message').text("Guarda la imagen pulsando el boton 'GUARDAR'");*/
/* 		});*/
/* 		*/
/* 		//Handles the new upload file*/
/* 		$('.save-img').on('click',function(){*/
/* 			$('#progress-message').empty();*/
/* 			var progressBar = document.getElementById("progress");*/
/* 			var newfile = document.getElementById('getsIm');*/
/* 			var prefijo = $('#getsIm').data('prefijo');*/
/* 			var origen = $('#getsIm').data('origen');*/
/* 			*/
/* 			console.log(newfile);*/
/* 			console.log(prefijo);*/
/* 			console.log(origen);*/
/* 			*/
/* 			// time life progress upload*/
/* 			function progress(e){*/
/* */
/* 				if(e.lengthComputable){*/
/* 					*/
/* 					$(".upload-img").show();*/
/* 					progressBar.max = e.total;*/
/* 					progressBar.value = e.loaded;*/
/* 					*/
/* 					var current = e.loaded / e.total; */
/* 					var Percentage = current * 100;*/
/* 					*/
/* 					console.log(Percentage);*/
/* 					*/
/* 					var percentReal = parseInt(Percentage) + ' %';*/
/* 					$("#progress").attr('data-original-title', percentReal);				*/
/* 					$('.tool').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show');*/
/* 					*/
/* 					if(Percentage >= 100)*/
/* 					{*/
/* 						$("#progress").attr('data-original-title', percentReal);*/
/* 						$('.tool').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show');*/
/* 						$('#progress-message').text('Se completo el proceso');*/
/* 						//alert('Se ha agregado la imagen satisfactoriamente.');*/
/* 						//location.reload();						*/
/* 					}*/
/* 					else{*/
/* 						$('#progress-message').text('Cargando ...');*/
/* 					}*/
/* 				}  */
/* 			 }			*/
/* 			insertar(newfile);*/
/* 			function insertar(file){*/
/* 				if (file.files && file.files[0]) {*/
/* 					if(file.files[0].type == "image/jpeg")*/
/* 					{*/
/* 						var reader = new FileReader();*/
/* 						reader.onload = function (e) {*/
/* 							//Peticion ajax*/
/* 								var formData = new FormData();*/
/* 									formData.append('imagen', newfile.files[0]);*/
/* 									formData.append('prefijo',prefijo);*/
/* 									formData.append('origen',origen);*/
/* 								var ruta = '{{ base_url() }}dashboard/admin/product/action/new_upload_img';*/
/* 									$.ajax({*/
/* 										url: ruta,*/
/* 										type: "POST",*/
/* 										data: formData,*/
/* 										xhr: function() {*/
/* 												var myXhr = $.ajaxSettings.xhr();*/
/* 												if(myXhr.upload){*/
/* 													myXhr.upload.addEventListener('progress',progress, false);*/
/* 												}*/
/* 												return myXhr;*/
/* 										},										*/
/* 										contentType: false,*/
/* 										processData: false,*/
/* 										success: function(datos)*/
/* 										{*/
/* 										*/
/* 											var result = JSON.parse(datos);*/
/* 											if(result.status==1)*/
/* 											{*/
/* 													*/
/* 												$('#progress-message').empty();*/
/* 												$('#progress-message').text('Tu imagen se ha guardado exitosamente.');*/
/* 													swal(*/
/* 													  '',*/
/* 													  'Se ha agregado la imagen satisfactoriamente',*/
/* 													  'success'*/
/* 													)*/
/* 												var name = result.name;*/
/* 													addImg(newfile,name);*/
/* 												//location.reload();											*/
/* 											}else*/
/* 												{*/
/* 													swal(*/
/* 													  '',*/
/* 													  'No se pudo completar la operacion, contacte al administrador.',*/
/* 													  'error'*/
/* 													)*/
/* 												}*/
/* 										},*/
/* 										error: function(data){*/
/* 											swal(*/
/* 											  'Intente de nuevo',*/
/* 											  'Ocurrio un problema grave, por favor consulte al administrador.',*/
/* 											  'error'*/
/* 											)*/
/* 										*/
/* 										}										*/
/* 									});							*/
/* 							//Fin de la peticion*/
/* 						}*/
/* 					}*/
/* 					else{*/
/* 					*/
/* 						swal(*/
/* 						  'Intente de nuevo',*/
/* 						  'Imagen inválida, extensíon no permitida.',*/
/* 						  'error'*/
/* 						)					*/
/* 					*/
/* 					}*/
/* */
/* 					reader.readAsDataURL(file.files[0]);*/
/* 				}*/
/* 				else{*/
/* */
/* 					swal(*/
/* 					  'Intente de nuevo',*/
/* 					  'Por favor seleccione una imagen válida.',*/
/* 					  'error'*/
/* 					)					*/
/* 				}			*/
/* 			}			*/
/* 		});*/
/* 		//function add img recent upload*/
/* 		function addImg(file,name){*/
/* 			if (file.files && file.files[0]) {*/
/* 				if(file.files[0].type == "image/jpeg")*/
/* 				{*/
/* 					var reader = new FileReader();*/
/* 					*/
/* 					reader.onload = function (e) {*/
/* 					var container = $('#sortable');*/
/* 					var containerli = container.find('li:nth-child(1)');*/
/* 					*/
/* 					var li = document.createElement('li');*/
/* 						div1 = document.createElement('div');*/
/* 						div1.setAttribute('class','form-group col-sm-6');*/
/* 					var div11 = document.createElement('div');*/
/* 					var img = document.createElement('img');*/
/* 						img.setAttribute('width','250px');*/
/* 						img.setAttribute('class','preview');*/
/* 						img.setAttribute('src',e.target.result);*/
/* 						*/
/* 						*/
/* 						jQuery(div11).append(img);*/
/* 						jQuery(div1).append(div11);*/
/* 						*/
/* 						div2 = document.createElement('div');*/
/* 						div2.setAttribute('class','form-group col-sm-6');*/
/* 						// Nombre de imagen*/
/* 						div22 = document.createElement('div');*/
/* 						div22p = document.createElement('p');*/
/* 						jQuery(div22p).append(name+'.jpg');*/
/* 						jQuery(div22).append(div22p);*/
/* 						jQuery(div2).append(div22);*/
/* 						// Boton sustituir imagen*/
/* 						var div3 = document.createElement('div');*/
/* 							div3.setAttribute('class','fileUpload btn btn-primary btn-sustitute');*/
/* 						var div3span = document.createElement('span');*/
/* 							jQuery(div3span).append('Sustituir imagen');*/
/* 						var div3button = document.createElement('input');*/
/* 						var datamain = $('body .modal').data('main');*/
/* 							div3button.setAttribute('name','changeImages');*/
/* 							div3button.setAttribute('data-last',name+'.jpg');*/
/* 							div3button.setAttribute('data-url',datamain);*/
/* 							div3button.setAttribute('class','handle-upload btn upload');*/
/* 							div3button.setAttribute('type','file');*/
/* 							div3button.setAttribute('accept','image/jpeg');*/
/* 						jQuery(div3).append(div3span);*/
/* 						jQuery(div3).append(div3button);*/
/* 						jQuery(div2).append(div3);*/
/* 																					*/
/* 						// Boton eliminar imagen*/
/* 						div4 = document.createElement('div');*/
/* 						div4button = document.createElement('button');*/
/* 						div4button.setAttribute('type','button');*/
/* 						div4button.setAttribute('class','btn btn-danger del-img');*/
/* 						div4button.setAttribute('data-origin',datamain);*/
/* 						div4button.setAttribute('data-delete',name+'.jpg');*/
/* 						jQuery(div4button).append('Eliminar imagen');*/
/* 						jQuery(div4).append(div4button);*/
/* 						jQuery(div2).append(div4);*/
/* 						*/
/* 						var divClear = document.createElement('div');*/
/* 							divClear.setAttribute('style','clear:both');*/
/* 						*/
/* 						jQuery(li).append(div1);*/
/* 						jQuery(li).append(div2);*/
/* 						jQuery(li).append(divClear);*/
/* 						jQuery(containerli).append(li);						*/
/* */
/* 					}*/
/* 				}*/
/* 				else{*/
/* 					swal(*/
/* 					  '',*/
/* 					  'Imagen inválida, extensíon no permitida',*/
/* 					  'error'*/
/* 					)					*/
/* 					*/
/* 				}*/
/* */
/* 				reader.readAsDataURL(file.files[0]);*/
/* 			}*/
/* 			else{*/
/* 				swal(*/
/* 				  '',*/
/* 				  'Por favor selecciona un archivo válido.',*/
/* 				  'error'*/
/* 				)				*/
/* 				*/
/* 			}			*/
/* 		}		*/
/* 		//Handles the change upload file preview*/
/* 		$("body").on('change','.modal .handle-upload',function(){*/
/* 			*/
/* 			var newfile = $(this);*/
/* 			var dis = this;*/
/* 			var indice = newfile.index();*/
/* 			var ultimo = newfile.data("last");*/
/* 			var url = newfile.data("url");*/
/* 			*/
/* 			swal({*/
/* 			  title: 'Desea cambiarlo definitivamente?',*/
/* 			  text: "Este cambio no es reversible!",*/
/* 			  type: 'warning',*/
/* 			  showCancelButton: true,*/
/* 			  confirmButtonColor: '#3085d6',*/
/* 			  cancelButtonColor: '#d33',*/
/* 			  confirmButtonText: 'Cambiar'*/
/* 			}).then(function() {*/
/* 			  checar(dis);*/
/* 			  swal(*/
/* 				'',*/
/* 				'Tu imagen se ha cambiado satisfactoriamente.',*/
/* 				'success'*/
/* 			  )*/
/* 			}, function(dismiss) {*/
/* 			  if (dismiss === 'cancel') {*/
/* 				newfile.replaceWith(newfile.clone(true));*/
/* 				//newfile.attr({ value: '' });*/
/* 			  }*/
/* 			})*/
/* 			function checar(file,mensaje){*/
/* 				if (file.files && file.files[0]) {*/
/* 					if(file.files[0].type == "image/jpeg")*/
/* 					{*/
/* 						var reader = new FileReader();*/
/* 						*/
/* 						reader.onload = function (e) {*/
/* 							change = newfile.parent().parent().prev().find(".preview");*/
/* 							*/
/* 							change.attr('src', e.target.result);*/
/* 							//Peticion ajax*/
/* 								var x= $(".handle-upload");*/
/* 									xy = newfile;*/
/* 								var formData = new FormData($("#formEditProduct")[0]);*/
/* 									formData.append("ultimo", ultimo);*/
/* 									formData.append("url",url);*/
/* 									formData.append("imagen", file.files[0]);*/
/* 									*/
/* 								var ruta = '{{ base_url() }}dashboard/admin/product/action/upload_img';*/
/* 									$.ajax({*/
/* 										url: ruta,*/
/* 										type: "POST",*/
/* 										data: formData,*/
/* 										contentType: false,*/
/* 										processData: false,*/
/* 										success: function(datos)*/
/* 										{*/
/* 											switch(datos){*/
/* 												case "0":*/
/* 													swal(*/
/* 													  '',*/
/* 													  'Por favor verifica que tu imagen sea válida o intenta nuevamente',*/
/* 													  'error'*/
/* 													)																									*/
/* 												break;*/
/* 												case "1":*/
/* 													swal(*/
/* 													  '',*/
/* 													  'Ha ocurrido un error, intenta nuevamente.',*/
/* 													  'error'*/
/* 													)																									*/
/* 												break;*/
/* 												case "2":*/
/* 													swal(*/
/* 													  '',*/
/* 													  'Ha ocurrido un error, intenta nuevamente.',*/
/* 													  'error'*/
/* 													)												*/
/* 												break;*/
/* 												case "3":*/
/* 													swal(*/
/* 													  '',*/
/* 													  'Se ha cambiado esta imagen satisfactoriamente',*/
/* 													  'success'*/
/* 													)																							*/
/* 												break;												*/
/* 											}*/
/* 										}*/
/* 									});							*/
/* 							//Fin de la peticion*/
/* 						}*/
/* 					}*/
/* 					else{*/
/* 						swal(*/
/* 						  '',*/
/* 						  'Imagen inválida, extensíon no permitida',*/
/* 						  'error'*/
/* 						)					*/
/* 						*/
/* 					}*/
/* */
/* 					reader.readAsDataURL(file.files[0]);*/
/* 				}*/
/* 				else{*/
/* 					swal(*/
/* 					  '',*/
/* 					  'Lo sentimos en este momento no podemos procesar tu imagen intenta de nuevo.',*/
/* 					  'error'*/
/* 					)				*/
/* 					*/
/* 				}			*/
/* 			}*/
/* 		});		*/
/*         //Handles the carousel thumbnails*/
/*         $('[id^=carousel-selector-]').click(function () {*/
/*             var id_selector = $(this).attr("id");*/
/*             try {*/
/*                 var id = /-(\d+)$/.exec(id_selector)[1];*/
/*                 console.log(id_selector, id);*/
/*                 jQuery('#myCarousel').carousel(parseInt(id));*/
/*             } catch (e) {*/
/*                 console.log('Regex failed!', e);*/
/*             }*/
/*         });*/
/*         // When the carousel slides, auto update the text*/
/*         $('#myCarousel').on('slid.bs.carousel', function (e) {*/
/*             var id = $('.item.active').data('slide-number');*/
/*             $('#carousel-text').html($('#slide-content-'+id).html());*/
/*         });*/
/*         */
/*         $('#formEditProduct').on('submit', function (e) {*/
/*         e.preventDefault();*/
/* */
/* 		/*var formData = new FormData($("#formEditProduct")[0]);*/
/* 			console.log(formData);*/
/* 		var ruta = '{{ base_url() }}dashboard/admin/product/action/upload_img';*/
/* 			$.ajax({*/
/* 				url: ruta,*/
/* 				type: "POST",*/
/* 				data: formData,*/
/* 				contentType: false,*/
/* 				processData: false,*/
/* 				success: function(datos)*/
/* 				{*/
/* 					console.log(datos);*/
/* 				}*/
/* 			});*/
/* 			*/
/* 			e.preventDefault();*//* */
/*         var btn = $(this).find('button');*/
/* */
/*         var id = $('#pid').val();*/
/*         var row = $('#rowid').val();*/
/*         var pName = $("#pname").val();*/
/*         var pScore = $("#pscore").val();*/
/*         var pHeader = $("#pheader").val();*/
/*         var pDescription = $("#pdescription").val();*/
/*         var pItem = $('#pitem').val();*/
/*         var pCategory = $('#pcategory').val();*/
/*         var pSubcategory = $('#psubcat').val();*/
/*         var pQuality = $('#pquality').val();*/
/*         var pOrigin = $('#porigin').val();*/
/*         var pColors = $('#pcolor').val();*/
/*         var pPresentation = $('#ppresentation').val();*/
/*         var pCharacteristics = $('#pcharacteristics').val();*/
/*         var pEspecifications = $('#pspecifications').val();*/
/*         var pDimensions = $('#pdimensions').val();*/
/*         var pWeight = $('#pweight').val();*/
/*         var pMinimumOrder = $('#pminimumOrder').val();*/
/*         var pDateExpiry = $('#pdateExpiry').val();*/
/*         var pInvNumPieces = $('#pinvNumPieces').val();*/
/*         var pBoutiqueMaxOrder = $('#pboutiqueMaxOrder').val();*/
/*         var pPriceWithoutTax = $('#ppriceWithoutTax').val();*/
/*         var pPriceCurrency = $('#ppriceCurrency').val();*/
/*         var pPriceSuggested = $('#ppriceSuggested').val();*/
/*         var pPriceTT = $('#ppriceTT').val();*/
/*         var pPackingNumPieces = $('#ppackingNumPieces').val();*/
/*         var pPackingEspecifications = $('#ppackingSpecifications').val();*/
/*         var pPackingHeight = $('#ppackingHeight').val();*/
/*         var pPackingWidth = $('#ppackingWidth').val();*/
/*         var pPackingLarge = $('#ppackingLarge').val();*/
/*         var pPackingWeight = $('#ppackingWeight').val();*/
/*         var pPackingLogProductionTime = $('#plogTimeProduction').val();*/
/*         var pLogFob = $('#plogFob').val();*/
/*         var pLogFobDeliveryTime = $('#plogFobDeliveryTime').val();*/
/*         var pSample = $('#psample').val();*/
/* */
/*          console.log(id)*/
/* */
/*         $.ajax({*/
/*             url: '{{ base_url() }}dashboard/admin/product/action/edit',*/
/*             type: 'post',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             data: {*/
/*                 pId : id,*/
/*                 pName : pName,*/
/*                 pScore : pScore,*/
/*                 pHeader : pHeader,*/
/*                 pDescription : pDescription,*/
/*                 pItem : pItem,*/
/*                 pCategory : pCategory,*/
/*                 pSubcategory : pSubcategory,*/
/*                 pQuality : pQuality,*/
/*                 pOrigin : pOrigin,*/
/*                 pColors : pColors,*/
/*                 pPresentation : pPresentation,*/
/*                 pCharacteristics : pCharacteristics,*/
/*                 pSpecifications : pEspecifications,*/
/*                 pDimensions : pDimensions,*/
/*                 pWeight : pWeight,*/
/*                 pMinimumOrder : pMinimumOrder,*/
/*                 pDateExpiry : pDateExpiry,*/
/*                 pInvNumPieces : pInvNumPieces,*/
/*                 pBoutiqueMaxOrder : pBoutiqueMaxOrder,*/
/*                 pPriceWithoutTax : pPriceWithoutTax,*/
/*                 pPriceCurrency : pPriceCurrency,*/
/*                 pPriceSuggested : pPriceSuggested,*/
/*                 pPriceTT : pPriceTT,*/
/*                 pPackingNumPieces : pPackingNumPieces,*/
/*                 pPackingSpecifications : pPackingEspecifications,*/
/*                 pPackingHeight : pPackingHeight,*/
/*                 pPackingWidth : pPackingWidth,*/
/*                 pPackingLarge : pPackingLarge,*/
/*                 pPackingWeight : pPackingWeight,*/
/*                 pLogProductionTime : pPackingLogProductionTime,*/
/*                 pLogFob : pLogFob,*/
/*                 pLogFobDeliveryTime : pLogFobDeliveryTime,*/
/*                 pSample : pSample*/
/*             },*/
/*             beforeSend: function() {*/
/*                 btn.prop('disabled', true)*/
/*             },*/
/*             success: function(response) {*/
/* */
/*                 if (response == '1') {*/
/* */
/*                     alert('Editado correctamente');*/
/*                     location.reload();*/
/*                     */
/*                     btn.prop('disabled', false);*/
/*                 } else {*/
/*                     */
/*                 }*/
/*             },*/
/*             error: function(xhr, textStatus) {*/
/*                 alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');*/
/*                 btn.prop('disabled', false);*/
/*             }*/
/*         });*/
/*         });*/
/*     });*/
/* </script>*/
/* {% endblock %}*/
