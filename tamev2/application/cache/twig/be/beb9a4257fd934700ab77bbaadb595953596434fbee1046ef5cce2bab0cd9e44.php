<?php

/* dashboard/admin/supplier.html */
class __TwigTemplate_305b7abc7190cf46b01061b82894ce049533850c9daa693baa2373bd790a2c4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/supplier.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        echo "\t";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
\t<style>
\t#bodyWrapper {
\t\theight: 100%;
\t\tbackground-color: #fff;
\t}
\t</style>
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "\t<div  class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-12 margin-bottom--30\">
\t\t\t\t<h1 class=\"title-dashboard text-center\">
\t\t\t\t\tSUPPLIER TEAM
\t\t\t\t</h1>
\t\t\t\t<h3 class=\"text-center\">
\t\t\t\t\t";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["suppliers_count"]) ? $context["suppliers_count"] : null), "html", null, true);
        echo "
\t\t\t\t\t";
        // line 20
        if (((isset($context["suppliers_count"]) ? $context["suppliers_count"] : null) > 1)) {
            // line 21
            echo "\t\t\t\t\t\tProveedores
\t\t\t\t\t";
        } else {
            // line 23
            echo "\t\t\t\t\t\tProveedor
\t\t\t\t\t";
        }
        // line 25
        echo "\t\t\t\t</h3>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 text-right\">
\t\t\t\t<div class=\"row margin-bottom--30\">
\t\t\t\t\t<div class=\"col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4\">
\t\t\t\t\t\t<form method=\"GET\" action=\"";
        // line 30
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/search?\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"q\" class=\"form-control\" placeholder=\"Buscar todo\">
\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-6 col-md-3 margin-bottom--30\">
\t\t\t\t\t    <select name=\"category\" id=\"category\" class=\"form-control\">
\t\t\t\t\t        <option value=\"\">Categoría</option>
\t\t\t\t\t        ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 47
            echo "\t\t\t\t\t            <option data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "idCategory", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "slug", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</option>
\t\t\t\t\t        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "\t\t\t\t\t    </select>
\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t<!-- <div class=\"col-sm-6 col-md-3 margin-bottom--30\">
\t\t\t\t\t\t<select name=\"\" id=\"\" class=\"form-control\">
\t\t\t\t\t\t\t<option value=\"\">Categoría</option>
\t\t\t\t\t\t\t<option value=\"\">...</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div> -->
\t\t\t\t\t<div class=\"col-sm-6 col-md-3 margin-bottom--30\">
\t\t\t\t\t\t<select name=\"\" id=\"subcategory\" class=\"form-control\">
\t\t\t\t\t\t\t<option value=\"\">Subcategoría</option>
\t\t\t\t\t\t\t<option value=\"\">...</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-md-3 margin-bottom--30\">
\t\t\t\t\t\t<select name=\"\" id=\"\" class=\"form-control\">
\t\t\t\t\t\t\t<option value=\"\">Tipo</option>
\t\t\t\t\t\t\t<option value=\"\">Catálogo</option>
\t\t\t\t\t\t\t<option value=\"\">Desarrollo de Producto</option>
\t\t\t\t\t\t\t<option value=\"\">Smart Line Product</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-md-3 margin-bottom--30\">
\t\t\t\t\t\t<select name=\"\" id=\"\" class=\"form-control\">
\t\t\t\t\t\t\t<option value=\"\">Otro</option>
\t\t\t\t\t\t\t<option value=\"\">Valor</option>
\t\t\t\t\t\t\t<option value=\"\">Popularidad</option>
\t\t\t\t\t\t\t<option value=\"\">Fecha de admisión</option>
\t\t\t\t\t\t\t<option value=\"\">Más Productos</option>
\t\t\t\t\t\t\t<option value=\"\">Menos Productos</option>
\t\t\t\t\t\t\t<option value=\"\">Menos Valor</option>
\t\t\t\t\t\t\t<option value=\"\">Menos Popular</option>
\t\t\t\t\t\t\t<option value=\"\">A-Z</option>
\t\t\t\t\t\t\t<option value=\"\">Z-A</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12\">
\t\t\t\t<p class=\"text-left\">
\t\t\t\t\t<a href=\"";
        // line 89
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/add\" class=\"btn btn-success\">
\t\t\t\t\t\t<i class=\"fa fa-plus-circle margin-right--5\"></i>
\t\t\t\t\t\tAgregar
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t";
        // line 95
        if (((isset($context["suppliers_count"]) ? $context["suppliers_count"] : null) > 0)) {
            // line 96
            echo "\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Nombre del Proveedor</th>
\t\t\t\t\t\t\t<th>ID</th>
\t\t\t\t\t\t\t<th>Número de Productos</th>
\t\t\t\t\t\t\t<th>Fecha de Admisión</th>
\t\t\t\t\t\t\t<th>Calificación</th>
\t\t\t\t\t\t\t<th>Acciones</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            // line 105
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["suppliers"]) ? $context["suppliers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["supplier"]) {
                // line 106
                echo "\t\t\t\t\t\t\t<tr class=\"btn-tr\" data-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["supplier"], "idSupplier", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t<td>";
                // line 107
                echo twig_escape_filter($this->env, $this->getAttribute($context["supplier"], "name", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 108
                echo twig_escape_filter($this->env, $this->getAttribute($context["supplier"], "idcustome", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 109
                echo twig_escape_filter($this->env, $this->getAttribute($context["supplier"], "number_products", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 110
                echo twig_escape_filter($this->env, $this->getAttribute($context["supplier"], "registered", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 111
                echo twig_escape_filter($this->env, ($this->getAttribute($context["supplier"], "total_score", array()) / $this->getAttribute($context["supplier"], "number_products", array())), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 113
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "dashboard/admin/supplier/products/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["supplier"], "idSupplier", array()), "html", null, true);
                echo "\" class=\"btn btn-info btn-sm\">
\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-info-circle margin-right--5\"></span>
\t\t\t\t\t\t\t\t\t\tVer info
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['supplier'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 120
            echo "\t\t\t\t\t</table>
\t\t\t\t\t";
        } else {
            // line 122
            echo "\t\t\t\t\t<p class=\"h3 margin-bottom--20 text-muted text-center\">No hay proveedores registrados</p>
\t\t\t\t\t";
        }
        // line 124
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    // line 129
    public function block_script($context, array $blocks = array())
    {
        // line 130
        echo "<script>
\t
\tvar category = \"";
        // line 132
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
        echo "\";
\tconsole.log(category);
\tvar subcategory = \"";
        // line 134
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\";
\tvar quality = \"";
        // line 135
        echo twig_escape_filter($this->env, (isset($context["quality"]) ? $context["quality"] : null), "html", null, true);
        echo "\";
\t
\t\$(\"#category\").val(category);
\t\$(\"#subcategory\").val(subcategory);
\t\$(\"#quality\").val(quality);
\t
\tvar subcategoryOptions = \$(\"#subcategory option\").length;
\t
\tif (category.length > 0) {
\t\t\tgetSubcategories();
\t\t\tconsole.log('done');
\t}
\telse{
\t\tconsole.log(\" fail\");
\t}
\t
    \$(\"#category\").on(\"change\", function() {

\t\tvar category = \$(\"#category option:selected\");
\t\tlocation.href = \"";
        // line 154
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/search?category=\" + category.val();
    });
    
    function getSubcategories() {
\t    var category = \$(\"#category option:selected\");

        \$.get(\"/tamev2/dashboard/admin/supplier/subcategory/\" + category.data(\"id\"), function(data) {
\t\t\tconsole.log(data);
            var options = '<option value=\"\">Subcategoría</option>';
\t\t\t
            \$.each( data, function( key, value ) {
                options += '<option value=\"' + value.sslug + '\"> ' + value.sname + '</option>';
            });

            \$(\"#subcategory\").html(options)
            \$(\"#subcategory\").val(\"";
        // line 169
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\");
        });
    }
    
    \$(\"#subcategory\").on(\"change\", function() {

\t\tvar category = \$(\"#category option:selected\").val();
\t\tvar subcategory = \$(\"#subcategory option:selected\");
\t\tvar quality= \"";
        // line 177
        echo twig_escape_filter($this->env, (isset($context["quality"]) ? $context["quality"] : null), "html", null, true);
        echo "\";

\t\tif (quality.length > 0) {
\t\t\tlocation.href = \"";
        // line 180
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/search?category=\" + category + \"&subcategory=\" + subcategory.val() + \"&quality=\" + quality;
\t\t} else {
\t\t\tlocation.href = \"";
        // line 182
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/search?category=\" + category + \"&subcategory=\" + subcategory.val();
\t\t}
    });
    
    /*\$(\"#quality\").on(\"change\", function() {

\t\tvar category = \"";
        // line 188
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
        echo "\";
\t\tvar subcategory = \"";
        // line 189
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : null), "html", null, true);
        echo "\";
\t\tvar quality= \$(\"#quality option:selected\");

\t\tif (category.length > 0) {
\t\t\tif (subcategory.length > 0) {
\t    \t\tlocation.href = \"";
        // line 194
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&subcategory=\" + subcategory + \"&quality=\" + quality.val(); 
\t    \t} else {
        \t\tlocation.href = \"";
        // line 196
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?category=\" + category + \"&quality=\" + quality.val();
        \t}
        } else {
\t        location.href = \"";
        // line 199
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/search?quality=\" + quality.val();
        }
    });*/
    
</script>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/supplier.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  350 => 199,  344 => 196,  339 => 194,  331 => 189,  327 => 188,  318 => 182,  313 => 180,  307 => 177,  296 => 169,  278 => 154,  256 => 135,  252 => 134,  247 => 132,  243 => 130,  240 => 129,  232 => 124,  228 => 122,  224 => 120,  209 => 113,  204 => 111,  200 => 110,  196 => 109,  192 => 108,  188 => 107,  183 => 106,  179 => 105,  168 => 96,  166 => 95,  157 => 89,  115 => 49,  102 => 47,  98 => 46,  79 => 30,  72 => 25,  68 => 23,  64 => 21,  62 => 20,  58 => 19,  49 => 12,  46 => 11,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* 	{{ parent() }}*/
/* 	<style>*/
/* 	#bodyWrapper {*/
/* 		height: 100%;*/
/* 		background-color: #fff;*/
/* 	}*/
/* 	</style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* 	<div  class="container-fluid">*/
/* 		<div class="row">*/
/* 			<div class="col-sm-12 margin-bottom--30">*/
/* 				<h1 class="title-dashboard text-center">*/
/* 					SUPPLIER TEAM*/
/* 				</h1>*/
/* 				<h3 class="text-center">*/
/* 					{{ suppliers_count }}*/
/* 					{% if suppliers_count > 1 %}*/
/* 						Proveedores*/
/* 					{% else %}*/
/* 						Proveedor*/
/* 					{% endif %}*/
/* 				</h3>*/
/* 			</div>*/
/* 			<div class="col-sm-12 text-right">*/
/* 				<div class="row margin-bottom--30">*/
/* 					<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">*/
/* 						<form method="GET" action="{{ base_url() }}dashboard/admin/supplier/search?">*/
/* 							<div class="input-group">*/
/* 								<input type="text" name="q" class="form-control" placeholder="Buscar todo">*/
/* 								<span class="input-group-btn">*/
/* 									<button class="btn btn-default" type="submit">*/
/* 										<i class="fa fa-search"></i>*/
/* 									</button>*/
/* 								</span>*/
/* 							</div>*/
/* 						</form>*/
/* 					</div>*/
/* 				</div>*/
/* 				<div class="row">*/
/* 					<div class="col-sm-6 col-md-3 margin-bottom--30">*/
/* 					    <select name="category" id="category" class="form-control">*/
/* 					        <option value="">Categoría</option>*/
/* 					        {% for category in categories %}*/
/* 					            <option data-id="{{ category.idCategory }}" value="{{ category.slug }}">{{ category.name }}</option>*/
/* 					        {% endfor %}*/
/* 					    </select>*/
/* 					</div>					*/
/* 					<!-- <div class="col-sm-6 col-md-3 margin-bottom--30">*/
/* 						<select name="" id="" class="form-control">*/
/* 							<option value="">Categoría</option>*/
/* 							<option value="">...</option>*/
/* 						</select>*/
/* 					</div> -->*/
/* 					<div class="col-sm-6 col-md-3 margin-bottom--30">*/
/* 						<select name="" id="subcategory" class="form-control">*/
/* 							<option value="">Subcategoría</option>*/
/* 							<option value="">...</option>*/
/* 						</select>*/
/* 					</div>*/
/* 					<div class="col-sm-6 col-md-3 margin-bottom--30">*/
/* 						<select name="" id="" class="form-control">*/
/* 							<option value="">Tipo</option>*/
/* 							<option value="">Catálogo</option>*/
/* 							<option value="">Desarrollo de Producto</option>*/
/* 							<option value="">Smart Line Product</option>*/
/* 						</select>*/
/* 					</div>*/
/* 					<div class="col-sm-6 col-md-3 margin-bottom--30">*/
/* 						<select name="" id="" class="form-control">*/
/* 							<option value="">Otro</option>*/
/* 							<option value="">Valor</option>*/
/* 							<option value="">Popularidad</option>*/
/* 							<option value="">Fecha de admisión</option>*/
/* 							<option value="">Más Productos</option>*/
/* 							<option value="">Menos Productos</option>*/
/* 							<option value="">Menos Valor</option>*/
/* 							<option value="">Menos Popular</option>*/
/* 							<option value="">A-Z</option>*/
/* 							<option value="">Z-A</option>*/
/* 						</select>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="col-sm-12">*/
/* 				<p class="text-left">*/
/* 					<a href="{{ base_url() }}dashboard/admin/supplier/add" class="btn btn-success">*/
/* 						<i class="fa fa-plus-circle margin-right--5"></i>*/
/* 						Agregar*/
/* 					</a>*/
/* 				</p>*/
/* 				<div class="panel panel-default">*/
/* 					{% if suppliers_count > 0 %}*/
/* 					<table class="table table-hover">*/
/* 						<tr>*/
/* 							<th>Nombre del Proveedor</th>*/
/* 							<th>ID</th>*/
/* 							<th>Número de Productos</th>*/
/* 							<th>Fecha de Admisión</th>*/
/* 							<th>Calificación</th>*/
/* 							<th>Acciones</th>*/
/* 						</tr>*/
/* 						{% for supplier in suppliers %}*/
/* 							<tr class="btn-tr" data-id="{{ supplier.idSupplier }}">*/
/* 								<td>{{ supplier.name }}</td>*/
/* 								<td>{{ supplier.idcustome }}</td>*/
/* 								<td>{{ supplier.number_products }}</td>*/
/* 								<td>{{ supplier.registered }}</td>*/
/* 								<td>{{ supplier.total_score / supplier.number_products }}</td>*/
/* 								<td>*/
/* 									<a href="{{ base_url() }}dashboard/admin/supplier/products/{{ supplier.idSupplier }}" class="btn btn-info btn-sm">*/
/* 										<span class="fa fa-info-circle margin-right--5"></span>*/
/* 										Ver info*/
/* 									</a>*/
/* 								</td>*/
/* 							</tr>*/
/* 						{% endfor %}*/
/* 					</table>*/
/* 					{% else %}*/
/* 					<p class="h3 margin-bottom--20 text-muted text-center">No hay proveedores registrados</p>*/
/* 					{% endif %}*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/* 	*/
/* 	var category = "{{ category }}";*/
/* 	console.log(category);*/
/* 	var subcategory = "{{ subcategory }}";*/
/* 	var quality = "{{ quality }}";*/
/* 	*/
/* 	$("#category").val(category);*/
/* 	$("#subcategory").val(subcategory);*/
/* 	$("#quality").val(quality);*/
/* 	*/
/* 	var subcategoryOptions = $("#subcategory option").length;*/
/* 	*/
/* 	if (category.length > 0) {*/
/* 			getSubcategories();*/
/* 			console.log('done');*/
/* 	}*/
/* 	else{*/
/* 		console.log(" fail");*/
/* 	}*/
/* 	*/
/*     $("#category").on("change", function() {*/
/* */
/* 		var category = $("#category option:selected");*/
/* 		location.href = "{{ base_url() }}dashboard/admin/supplier/search?category=" + category.val();*/
/*     });*/
/*     */
/*     function getSubcategories() {*/
/* 	    var category = $("#category option:selected");*/
/* */
/*         $.get("/tamev2/dashboard/admin/supplier/subcategory/" + category.data("id"), function(data) {*/
/* 			console.log(data);*/
/*             var options = '<option value="">Subcategoría</option>';*/
/* 			*/
/*             $.each( data, function( key, value ) {*/
/*                 options += '<option value="' + value.sslug + '"> ' + value.sname + '</option>';*/
/*             });*/
/* */
/*             $("#subcategory").html(options)*/
/*             $("#subcategory").val("{{ subcategory }}");*/
/*         });*/
/*     }*/
/*     */
/*     $("#subcategory").on("change", function() {*/
/* */
/* 		var category = $("#category option:selected").val();*/
/* 		var subcategory = $("#subcategory option:selected");*/
/* 		var quality= "{{ quality }}";*/
/* */
/* 		if (quality.length > 0) {*/
/* 			location.href = "{{ base_url() }}dashboard/admin/supplier/search?category=" + category + "&subcategory=" + subcategory.val() + "&quality=" + quality;*/
/* 		} else {*/
/* 			location.href = "{{ base_url() }}dashboard/admin/supplier/search?category=" + category + "&subcategory=" + subcategory.val();*/
/* 		}*/
/*     });*/
/*     */
/*     /*$("#quality").on("change", function() {*/
/* */
/* 		var category = "{{ category }}";*/
/* 		var subcategory = "{{ subcategory }}";*/
/* 		var quality= $("#quality option:selected");*/
/* */
/* 		if (category.length > 0) {*/
/* 			if (subcategory.length > 0) {*/
/* 	    		location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&subcategory=" + subcategory + "&quality=" + quality.val(); */
/* 	    	} else {*/
/*         		location.href = "{{ base_url() }}dashboard/admin/catalog/search?category=" + category + "&quality=" + quality.val();*/
/*         	}*/
/*         } else {*/
/* 	        location.href = "{{ base_url() }}dashboard/admin/catalog/search?quality=" + quality.val();*/
/*         }*/
/*     });*//* */
/*     */
/* </script>*/
/* {% endblock %}*/
