<?php

/* shop/comparacion-producto.html */
class __TwigTemplate_37bf853214f26ae0526143da8d6364490955ce5668a478d0eec67f8c802d8a06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-shop.html", "shop/comparacion-producto.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-shop.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
   <div id=\"boxContent\">

      <!-- Content -->
      <div class=\"container\">
          <div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">
            <!-- Indicators -->
            <ol class=\"carousel-indicators\">
              <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" class=\"active\"></li>
              <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\"></li>
              <li data-target=\"#carousel-example-generic\" data-slide-to=\"2\"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class=\"carousel-inner\" role=\"listbox\">
              <div class=\"item active\">            
                <div class=\"row sortable\">
                  
                  <div class=\"col-xs-12 col-sm-6 col-md-4 col-lg-4 columnaUno\">
                    <div class=\"boxBorder boxColFirst\">
                      <div class=\"circuloComparar\"></div>
                      <span class=\"cerrarComparar\">x</span>
                      <center>
                        <img class=\"comparaPic\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/comparaPic1.jpg\" alt=\"comparaPic1\">
                      </center>
                    </div>
                    <br>
                    <div class=\"boxBorder\">
                      <center>
                       <div class=\"boxFavoritos\">
                          <ul>
                            <li>
                              <center>¡Regístrate para guardar este producto!</center>
                            </li>
                            <li>
                              <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                              Favoritos
                            </li>
                            <hr>
                            <li>
                              <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                              Nueva lista
                            </li>
                            <hr>
                            <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                          </ul>
                       </div>
                      </center>
                      <div class=\"row\">
                        <div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3 icono-like-navRight\">
                          <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\" width=\"45px\">
                        </div>
                        <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 icono-comparar-navRight\">
                          <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\" width=\"30px\">
                          <p>Comparar</p>
                        </div>
                        <div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3 pdf-navRight\">
                         <p>PDF</p>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 textProductNR\">
                          <p>Impresión | Tecnología</p>
                          <h5>XYZ Printing da Vinci Mini 3D Printer</h5>
                          <p>
                            COSTO: \$ 218.24 EUR <br>
                            CANTIDAD MINIMA DE PIEZAS: 50
                          </p>
                          <p>Holanda Inventario</p>
                          <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol\">
                            <center><button class=\"btnProductNR btnPersonalizar\">PERSONALIZAR</button></center>
                          </div>
                          <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol\">
                            <center>
                              <button class=\"btnProductNR\"><img src=\"";
        // line 78
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconDiamantBlack.png\" alt=\"iconDiamantBlack\" id=\"diamantbtn\">MUESTRA</button>
                            </center>
                          </div>
                          <center><button id=\"btnPedido\">COMENZAR PEDIDO</button></center>
                          
                          <p class=\"titlesSlides\">
                            Términos y Condiciones <br>
                            <span>Tu pedido pasara por un proceso de autorización, no hay compra automática en esta plataforma.</span> <br>
                          </p>
                          <div class=\"panel_tc\">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.
                            </p>
                          </div>
                          <p class=\"slide_tc\">Leer más</p>


                          <p class=\"titlesSlides\">
                            Exelente opción para estudiantes y principiantes <br>
                            <span>
                              Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles. <br><br>
                              Los filamentos da Vinci Mini se fabrican con plásticos PLA derivados del almidón de maíz, haciéndolos biodegradables y no tóxicos. Los filamentos de XYZprintin.
                            </span> <br>
                          </p>
                          <div class=\"panel_eo\">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.
                            </p>
                          </div>
                          <p class=\"slide_eo\">Leer más</p>          
                        </div>
                      </div>

                      <hr>
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            ITEM:
                            <span>
                              THKLD
                            </span>
                          </p>
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            CALIDAD:
                            <span>
                              Premium
                            </span>
                          </p>
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            PRESENTACIÓN:
                            <span>
                              Caja
                            </span>
                          </p>
                        </div>
                      </div>

                      <center>
                        <div class=\"circle_color\" style=\"background-color: black\"></div>
                        <div class=\"circle_color\" style=\"background-color: red\"></div>
                        <div class=\"circle_color\" style=\"background-color: yellow\"></div>
                        <div class=\"circle_color\" style=\"background-color: green\"></div>
                        <div class=\"circle_color\" style=\"background-color: blue\"></div>
                        <div class=\"circle_color\" style=\"background-color: purple\"></div>
                        <div class=\"circle_color\" style=\"background-color: pink\"></div>
                        <div class=\"circle_color\" style=\"background-color: orange\"></div>
                      </center>

                      <div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">
                            <h4 class=\"panel-title\">
                              <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                                VENTAJA COMPETITIVA
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                            <div class=\"panel-body\">
                              <ul>
                                <li>Compacta y Productiva</li>
                                <li>Practicidad de impresión en cualquier parte de la casa.</li>
                                <li>Eco-friendly</li>
                                <li>Impresión family safe</li>
                                <li>Indicador de status LED</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                                CARACTERISTICAS
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseTwo\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">
                            <div class=\"panel-body\">
                              <ul>
                                <li>XYZ Sotware</li>
                                <li>Cama de impresión de aluminio</li>
                                <li>Compatible para platucos PLA</li>
                                <li>Garantía DHP</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingThree\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                                ESPECIFICACIONES
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseThree\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingThree\">
                            <div class=\"panel-body\">
                              <p class=\"titlesSlides\" style=\"margin-top: 0\">

                                <span>
                                FFF (Fused Filament Fabrication)
                                </span>
                                Product Dimension
                                <span>
                                  (WxDxH) <br>
                                  15.35 x 13.19 x 14.17 inch (390 x 335 x 360 mm)
                                </span>

                                <br>

                                Package Dimension
                                <span>
                                  21.65x15.35x18.50 inch (550x 390 x 470 mm)
                                </span>

                                <br>

                                Gross Weight
                                <span>
                                  24.25lbs (10 kg)
                                </span>

                                <br>

                                Maximum Build Volume
                                <span>
                                  (WxDxH) <br>
                                  5.9 x 5.9 x 5.9 inch (15x15x15cm)
                                </span>

                                <br>

                                Resolution
                                <span>
                                  Fine 0.1 mm (100 microns)
                                  Standard 0.2 mm (200 microns)
                                  Speed 0.3 mm (300 microns)
                                  Ultra Fast 0.4 mm (400 microns)
                                </span>

                                <br>

                                Print Head
                                <span>
                                  Single Nozzle
                                </span>

                                <br>

                                Nozzle Diameter
                                <span>
                                  0.4 mm
                                </span>

                                <br>

                                Filament Diameter
                                <span>
                                  1.75 mm
                                </span>

                                <br>

                                Filament Material
                                <span>
                                  PLA
                                </span>

                                <br>

                                Connectivity
                                <span>
                                  USB 2.0, WiFi (802.11 b/g/n)
                                </span>

                                <br>

                                Software
                                <span>
                                  XYZware
                                  File Types
                                  .stl , XYZ Format (.3w), 3mf
                                  OS Support
                                  Windows 7 and above (for PC)
                                  Mac OSX 10.8 and above (for Mac)
                                  Note: Stardard VGA driver on operating system, or a graphics card doesn't support OpenGL 2.1,
                                </span>

                                <br>

                                Hardware
                                <span>
                                  X86 32/64-bit compatible PCs with 4GB+ DRAM
                                </span>

                                <br>

                                Hardware Requirements (for PC/Mac)
                                <span>
                                  X86 64-bit compatible Macs with 4GB+ DRAM
                                </span>

                              </p>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingFour\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseFour\">
                                EMPAQUE
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseFour\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingFour\">
                            <div class=\"panel-body\">
                              <p class=\"titlesSlides\" style=\"margin-top: 0\">
                                Caja maestra:
                                <span>
                                  6 piezas
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 SR_last\">

                          <input type=\"hidden\" name=\"rating\" id=\"rating\" />
                          <ul onMouseOut=\"resetRating();\">
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                          </ul>
                          
                          <br>

                          <a href=\"#\"><i class=\"fa fa-facebook faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-twitter faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-instagram faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-youtube faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-google-plus faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-pinterest faRedes\" aria-hidden=\"true\"></i></a>

                          <br>

                          <div>
                            <p><a href=\"#\">¿Tienes dudas respecto al producto?</a></p>
                            <p><a href=\"#\">AUTORIZACIÓN</a> | <a href=\"#\">ENTREGA</a> | <a href=\"#\">DEVOLUCIONES</a></p>
                          </div>
                          
                        </div>
                      </div>
                      
                    </div> <!-- boxBorder -->
                    
                  </div> <!-- div columnaUno -->
                  <div class=\"col-xs-12 col-sm-6 col-md-4 col-lg-4 columnaDos\">
                    <div class=\"boxBorder boxColFirst\">
                      <div class=\"circuloComparar\"></div>
                      <span class=\"cerrarComparar\">x</span>
                      <center>
                        <img class=\"comparaPic\" src=\"";
        // line 375
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/comparaPic2.jpg\" alt=\"comparaPic2\">
                      </center>
                    </div>
                    <br>
                    <div class=\"boxBorder\">
                      <center>
                       <div class=\"boxFavoritos\">
                          <ul>
                            <li>
                              <center>¡Regístrate para guardar este producto!</center>
                            </li>
                            <li>
                              <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 387
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                              Favoritos
                            </li>
                            <hr>
                            <li>
                              <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 392
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                              Nueva lista
                            </li>
                            <hr>
                            <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                          </ul>
                       </div>
                      </center>
                      <div class=\"row\">
                        <div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3 icono-like-navRight\">
                          <img src=\"";
        // line 402
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\" width=\"45px\">
                        </div>
                        <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 icono-comparar-navRight\">
                          <img src=\"";
        // line 405
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\" width=\"30px\">
                          <p>Comparar</p>
                        </div>
                        <div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3 pdf-navRight\">
                         <p>PDF</p>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 textProductNR\">
                          <p>Impresión | Tecnología</p>
                          <h5>XYZ Printing da Vinci Mini 3D Printer</h5>
                          <p>
                            COSTO: \$ 218.24 EUR <br>
                            CANTIDAD MINIMA DE PIEZAS: 50
                          </p>
                          <p>Holanda Inventario</p>
                          <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol\">
                            <center><button class=\"btnProductNR btnPersonalizar\">PERSONALIZAR</button></center>
                          </div>
                          <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol\">
                            <center>
                              <button class=\"btnProductNR\"><img src=\"";
        // line 427
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconDiamantBlack.png\" alt=\"iconDiamantBlack\" id=\"diamantbtn\">MUESTRA</button>
                            </center>
                          </div>
                          <center><button id=\"btnPedido\">COMENZAR PEDIDO</button></center>
                          
                          <p class=\"titlesSlides\">
                            Términos y Condiciones <br>
                            <span>Tu pedido pasara por un proceso de autorización, no hay compra automática en esta plataforma.</span> <br>
                          </p>
                          <div class=\"panel_tc\">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.
                            </p>
                          </div>
                          <p class=\"slide_tc\">Leer más</p>


                          <p class=\"titlesSlides\">
                            Exelente opción para estudiantes y principiantes <br>
                            <span>
                              Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles. <br><br>
                              Los filamentos da Vinci Mini se fabrican con plásticos PLA derivados del almidón de maíz, haciéndolos biodegradables y no tóxicos. Los filamentos de XYZprintin.
                            </span> <br>
                          </p>
                          <div class=\"panel_eo\">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.
                            </p>
                          </div>
                          <p class=\"slide_eo\">Leer más</p>          
                        </div>
                      </div>

                      <hr>
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            ITEM:
                            <span>
                              THKLD
                            </span>
                          </p>
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            CALIDAD:
                            <span>
                              Premium
                            </span>
                          </p>
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            PRESENTACIÓN:
                            <span>
                              Caja
                            </span>
                          </p>
                        </div>
                      </div>

                      <center>
                        <div class=\"circle_color\" style=\"background-color: black\"></div>
                        <div class=\"circle_color\" style=\"background-color: red\"></div>
                        <div class=\"circle_color\" style=\"background-color: yellow\"></div>
                        <div class=\"circle_color\" style=\"background-color: green\"></div>
                        <div class=\"circle_color\" style=\"background-color: blue\"></div>
                        <div class=\"circle_color\" style=\"background-color: purple\"></div>
                        <div class=\"circle_color\" style=\"background-color: pink\"></div>
                        <div class=\"circle_color\" style=\"background-color: orange\"></div>
                      </center>

                      <div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">
                            <h4 class=\"panel-title\">
                              <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                                VENTAJA COMPETITIVA
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                            <div class=\"panel-body\">
                              <ul>
                                <li>Compacta y Productiva</li>
                                <li>Practicidad de impresión en cualquier parte de la casa.</li>
                                <li>Eco-friendly</li>
                                <li>Impresión family safe</li>
                                <li>Indicador de status LED</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                                CARACTERISTICAS
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseTwo\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">
                            <div class=\"panel-body\">
                              <ul>
                                <li>XYZ Sotware</li>
                                <li>Cama de impresión de aluminio</li>
                                <li>Compatible para platucos PLA</li>
                                <li>Garantía DHP</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingThree\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                                ESPECIFICACIONES
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseThree\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingThree\">
                            <div class=\"panel-body\">
                              <p class=\"titlesSlides\" style=\"margin-top: 0\">

                                <span>
                                FFF (Fused Filament Fabrication)
                                </span>
                                Product Dimension
                                <span>
                                  (WxDxH) <br>
                                  15.35 x 13.19 x 14.17 inch (390 x 335 x 360 mm)
                                </span>

                                <br>

                                Package Dimension
                                <span>
                                  21.65x15.35x18.50 inch (550x 390 x 470 mm)
                                </span>

                                <br>

                                Gross Weight
                                <span>
                                  24.25lbs (10 kg)
                                </span>

                                <br>

                                Maximum Build Volume
                                <span>
                                  (WxDxH) <br>
                                  5.9 x 5.9 x 5.9 inch (15x15x15cm)
                                </span>

                                <br>

                                Resolution
                                <span>
                                  Fine 0.1 mm (100 microns)
                                  Standard 0.2 mm (200 microns)
                                  Speed 0.3 mm (300 microns)
                                  Ultra Fast 0.4 mm (400 microns)
                                </span>

                                <br>

                                Print Head
                                <span>
                                  Single Nozzle
                                </span>

                                <br>

                                Nozzle Diameter
                                <span>
                                  0.4 mm
                                </span>

                                <br>

                                Filament Diameter
                                <span>
                                  1.75 mm
                                </span>

                                <br>

                                Filament Material
                                <span>
                                  PLA
                                </span>

                                <br>

                                Connectivity
                                <span>
                                  USB 2.0, WiFi (802.11 b/g/n)
                                </span>

                                <br>

                                Software
                                <span>
                                  XYZware
                                  File Types
                                  .stl , XYZ Format (.3w), 3mf
                                  OS Support
                                  Windows 7 and above (for PC)
                                  Mac OSX 10.8 and above (for Mac)
                                  Note: Stardard VGA driver on operating system, or a graphics card doesn't support OpenGL 2.1,
                                </span>

                                <br>

                                Hardware
                                <span>
                                  X86 32/64-bit compatible PCs with 4GB+ DRAM
                                </span>

                                <br>

                                Hardware Requirements (for PC/Mac)
                                <span>
                                  X86 64-bit compatible Macs with 4GB+ DRAM
                                </span>

                              </p>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingFour\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseFour\">
                                EMPAQUE
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseFour\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingFour\">
                            <div class=\"panel-body\">
                              <p class=\"titlesSlides\" style=\"margin-top: 0\">
                                Caja maestra:
                                <span>
                                  6 piezas
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 SR_last\">

                          <input type=\"hidden\" name=\"rating\" id=\"rating\" />
                          <ul onMouseOut=\"resetRating();\">
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                          </ul>
                          
                          <br>

                          <a href=\"#\"><i class=\"fa fa-facebook faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-twitter faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-instagram faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-youtube faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-google-plus faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-pinterest faRedes\" aria-hidden=\"true\"></i></a>

                          <br>

                          <div>
                            <p><a href=\"#\">¿Tienes dudas respecto al producto?</a></p>
                            <p><a href=\"#\">AUTORIZACIÓN</a> | <a href=\"#\">ENTREGA</a> | <a href=\"#\">DEVOLUCIONES</a></p>
                          </div>
                          
                        </div>
                      </div>
                      
                    </div> <!-- boxBorder -->
                    
                  </div> <!-- div columnaDos -->
                  <div class=\"col-xs-12 col-sm-6 col-md-4 col-lg-4 columnaTres\">
                    <div class=\"boxBorder boxColFirst\">
                      <div class=\"circuloComparar\"></div>
                      <span class=\"cerrarComparar\">x</span>
                      <center>
                        <img class=\"comparaPic\" src=\"";
        // line 724
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/comparaPic3.jpg\" alt=\"comparaPic3\">
                      </center>
                    </div>
                    <br>
                    <div class=\"boxBorder\">
                      <center>
                       <div class=\"boxFavoritos\">
                          <ul>
                            <li>
                              <center>¡Regístrate para guardar este producto!</center>
                            </li>
                            <li>
                              <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 736
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                              Favoritos
                            </li>
                            <hr>
                            <li>
                              <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 741
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                              Nueva lista
                            </li>
                            <hr>
                            <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                          </ul>
                       </div>
                      </center>
                      <div class=\"row\">
                        <div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3 icono-like-navRight\">
                          <img src=\"";
        // line 751
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\" width=\"45px\">
                        </div>
                        <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 icono-comparar-navRight\">
                          <img src=\"";
        // line 754
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\" width=\"30px\">
                          <p>Comparar</p>
                        </div>
                        <div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3 pdf-navRight\">
                         <p>PDF</p>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 textProductNR\">
                          <p>Impresión | Tecnología</p>
                          <h5>XYZ Printing da Vinci Mini 3D Printer</h5>
                          <p>
                            COSTO: \$ 218.24 EUR <br>
                            CANTIDAD MINIMA DE PIEZAS: 50
                          </p>
                          <p>Holanda Inventario</p>
                          <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol\">
                            <center><button class=\"btnProductNR btnPersonalizar\">PERSONALIZAR</button></center>
                          </div>
                          <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol\">
                            <center>
                              <button class=\"btnProductNR\"><img src=\"";
        // line 776
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconDiamantBlack.png\" alt=\"iconDiamantBlack\" id=\"diamantbtn\">MUESTRA</button>
                            </center>
                          </div>
                          <center><button id=\"btnPedido\">COMENZAR PEDIDO</button></center>
                          
                          <p class=\"titlesSlides\">
                            Términos y Condiciones <br>
                            <span>Tu pedido pasara por un proceso de autorización, no hay compra automática en esta plataforma.</span> <br>
                          </p>
                          <div class=\"panel_tc\">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.
                            </p>
                          </div>
                          <p class=\"slide_tc\">Leer más</p>


                          <p class=\"titlesSlides\">
                            Exelente opción para estudiantes y principiantes <br>
                            <span>
                              Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles. <br><br>
                              Los filamentos da Vinci Mini se fabrican con plásticos PLA derivados del almidón de maíz, haciéndolos biodegradables y no tóxicos. Los filamentos de XYZprintin.
                            </span> <br>
                          </p>
                          <div class=\"panel_eo\">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.
                            </p>
                          </div>
                          <p class=\"slide_eo\">Leer más</p>          
                        </div>
                      </div>

                      <hr>
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            ITEM:
                            <span>
                              THKLD
                            </span>
                          </p>
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            CALIDAD:
                            <span>
                              Premium
                            </span>
                          </p>
                          <p class=\"titlesSlides\" style=\"margin-top: 0\">
                            PRESENTACIÓN:
                            <span>
                              Caja
                            </span>
                          </p>
                        </div>
                      </div>

                      <center>
                        <div class=\"circle_color\" style=\"background-color: black\"></div>
                        <div class=\"circle_color\" style=\"background-color: red\"></div>
                        <div class=\"circle_color\" style=\"background-color: yellow\"></div>
                        <div class=\"circle_color\" style=\"background-color: green\"></div>
                        <div class=\"circle_color\" style=\"background-color: blue\"></div>
                        <div class=\"circle_color\" style=\"background-color: purple\"></div>
                        <div class=\"circle_color\" style=\"background-color: pink\"></div>
                        <div class=\"circle_color\" style=\"background-color: orange\"></div>
                      </center>

                      <div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">
                            <h4 class=\"panel-title\">
                              <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                                VENTAJA COMPETITIVA
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                            <div class=\"panel-body\">
                              <ul>
                                <li>Compacta y Productiva</li>
                                <li>Practicidad de impresión en cualquier parte de la casa.</li>
                                <li>Eco-friendly</li>
                                <li>Impresión family safe</li>
                                <li>Indicador de status LED</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                                CARACTERISTICAS
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseTwo\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">
                            <div class=\"panel-body\">
                              <ul>
                                <li>XYZ Sotware</li>
                                <li>Cama de impresión de aluminio</li>
                                <li>Compatible para platucos PLA</li>
                                <li>Garantía DHP</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingThree\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                                ESPECIFICACIONES
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseThree\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingThree\">
                            <div class=\"panel-body\">
                              <p class=\"titlesSlides\" style=\"margin-top: 0\">

                                <span>
                                FFF (Fused Filament Fabrication)
                                </span>
                                Product Dimension
                                <span>
                                  (WxDxH) <br>
                                  15.35 x 13.19 x 14.17 inch (390 x 335 x 360 mm)
                                </span>

                                <br>

                                Package Dimension
                                <span>
                                  21.65x15.35x18.50 inch (550x 390 x 470 mm)
                                </span>

                                <br>

                                Gross Weight
                                <span>
                                  24.25lbs (10 kg)
                                </span>

                                <br>

                                Maximum Build Volume
                                <span>
                                  (WxDxH) <br>
                                  5.9 x 5.9 x 5.9 inch (15x15x15cm)
                                </span>

                                <br>

                                Resolution
                                <span>
                                  Fine 0.1 mm (100 microns)
                                  Standard 0.2 mm (200 microns)
                                  Speed 0.3 mm (300 microns)
                                  Ultra Fast 0.4 mm (400 microns)
                                </span>

                                <br>

                                Print Head
                                <span>
                                  Single Nozzle
                                </span>

                                <br>

                                Nozzle Diameter
                                <span>
                                  0.4 mm
                                </span>

                                <br>

                                Filament Diameter
                                <span>
                                  1.75 mm
                                </span>

                                <br>

                                Filament Material
                                <span>
                                  PLA
                                </span>

                                <br>

                                Connectivity
                                <span>
                                  USB 2.0, WiFi (802.11 b/g/n)
                                </span>

                                <br>

                                Software
                                <span>
                                  XYZware
                                  File Types
                                  .stl , XYZ Format (.3w), 3mf
                                  OS Support
                                  Windows 7 and above (for PC)
                                  Mac OSX 10.8 and above (for Mac)
                                  Note: Stardard VGA driver on operating system, or a graphics card doesn't support OpenGL 2.1,
                                </span>

                                <br>

                                Hardware
                                <span>
                                  X86 32/64-bit compatible PCs with 4GB+ DRAM
                                </span>

                                <br>

                                Hardware Requirements (for PC/Mac)
                                <span>
                                  X86 64-bit compatible Macs with 4GB+ DRAM
                                </span>

                              </p>
                            </div>
                          </div>
                        </div>
                        <div class=\"panel panel-default\">
                          <div class=\"panel-heading\" role=\"tab\" id=\"headingFour\">
                            <h4 class=\"panel-title\">
                              <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseFour\">
                                EMPAQUE
                              </a>
                            </h4>
                          </div>
                          <div id=\"collapseFour\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingFour\">
                            <div class=\"panel-body\">
                              <p class=\"titlesSlides\" style=\"margin-top: 0\">
                                Caja maestra:
                                <span>
                                  6 piezas
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 SR_last\">

                          <input type=\"hidden\" name=\"rating\" id=\"rating\" />
                          <ul onMouseOut=\"resetRating();\">
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                            <li onmouseover=\"highlightStar(this);\" onmouseout=\"removeHighlight();\" onClick=\"addRating(this);\" class=\"starsClass\">
                              <i class=\"fa fa-star-o faStars\" aria-hidden=\"true\"></i>
                            </li>
                          </ul>
                          
                          <br>

                          <a href=\"#\"><i class=\"fa fa-facebook faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-twitter faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-instagram faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-youtube faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-google-plus faRedes\" aria-hidden=\"true\"></i></a>
                          <a href=\"#\"><i class=\"fa fa-pinterest faRedes\" aria-hidden=\"true\"></i></a>

                          <br>

                          <div>
                            <p><a href=\"#\">¿Tienes dudas respecto al producto?</a></p>
                            <p><a href=\"#\">AUTORIZACIÓN</a> | <a href=\"#\">ENTREGA</a> | <a href=\"#\">DEVOLUCIONES</a></p>
                          </div>
                          
                        </div>
                      </div>
                      
                    </div> <!-- boxBorder -->
                    
                  </div> <!-- div columnaTres -->

                </div> <!-- row -->

              </div> <!-- divItem Active -->
            </div>

            <!-- Controls -->
            <a class=\"left carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"prev\">
              <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
              <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"right carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"next\">
              <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
              <span class=\"sr-only\">Next</span>
            </a>
        </div>
      </div> <!-- container -->

      <!-- End contend --> 


      <!-- Pre-Footer -->
      <div class=\"row anchoRow\">
        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
          <center>
            <div id=\"top-footer\">
              <p>
              Selecciona los productos que quieras para solicitar un pedido <br>Tame Traffic no es una tienda en linea
              </p>
              <a href=\"#\">¿Como se compra en Tame Traffic?</a><br>
              <a href=\"#\">¿Como selecciono productos?</a><br>
              <a href=\"#\">¿Como se hace un pedido?</a>
            </div>
          </center>
        </div>
      </div>

    </div>
    <!-- End box content -->

";
    }

    public function getTemplateName()
    {
        return "shop/comparacion-producto.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  857 => 776,  832 => 754,  826 => 751,  813 => 741,  805 => 736,  790 => 724,  490 => 427,  465 => 405,  459 => 402,  446 => 392,  438 => 387,  423 => 375,  123 => 78,  98 => 56,  92 => 53,  79 => 43,  71 => 38,  56 => 26,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-shop.html' %}*/
/* {% block content %}*/
/* */
/*    <div id="boxContent">*/
/* */
/*       <!-- Content -->*/
/*       <div class="container">*/
/*           <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">*/
/*             <!-- Indicators -->*/
/*             <ol class="carousel-indicators">*/
/*               <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>*/
/*               <li data-target="#carousel-example-generic" data-slide-to="1"></li>*/
/*               <li data-target="#carousel-example-generic" data-slide-to="2"></li>*/
/*             </ol>*/
/* */
/*             <!-- Wrapper for slides -->*/
/*             <div class="carousel-inner" role="listbox">*/
/*               <div class="item active">            */
/*                 <div class="row sortable">*/
/*                   */
/*                   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 columnaUno">*/
/*                     <div class="boxBorder boxColFirst">*/
/*                       <div class="circuloComparar"></div>*/
/*                       <span class="cerrarComparar">x</span>*/
/*                       <center>*/
/*                         <img class="comparaPic" src="{{ base_url() }}public/assets/img/comparaPic1.jpg" alt="comparaPic1">*/
/*                       </center>*/
/*                     </div>*/
/*                     <br>*/
/*                     <div class="boxBorder">*/
/*                       <center>*/
/*                        <div class="boxFavoritos">*/
/*                           <ul>*/
/*                             <li>*/
/*                               <center>¡Regístrate para guardar este producto!</center>*/
/*                             </li>*/
/*                             <li>*/
/*                               <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                               Favoritos*/
/*                             </li>*/
/*                             <hr>*/
/*                             <li>*/
/*                               <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                               Nueva lista*/
/*                             </li>*/
/*                             <hr>*/
/*                             <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                           </ul>*/
/*                        </div>*/
/*                       </center>*/
/*                       <div class="row">*/
/*                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 icono-like-navRight">*/
/*                           <img src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png" width="45px">*/
/*                         </div>*/
/*                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 icono-comparar-navRight">*/
/*                           <img src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png" width="30px">*/
/*                           <p>Comparar</p>*/
/*                         </div>*/
/*                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pdf-navRight">*/
/*                          <p>PDF</p>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 textProductNR">*/
/*                           <p>Impresión | Tecnología</p>*/
/*                           <h5>XYZ Printing da Vinci Mini 3D Printer</h5>*/
/*                           <p>*/
/*                             COSTO: $ 218.24 EUR <br>*/
/*                             CANTIDAD MINIMA DE PIEZAS: 50*/
/*                           </p>*/
/*                           <p>Holanda Inventario</p>*/
/*                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol">*/
/*                             <center><button class="btnProductNR btnPersonalizar">PERSONALIZAR</button></center>*/
/*                           </div>*/
/*                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol">*/
/*                             <center>*/
/*                               <button class="btnProductNR"><img src="{{ base_url() }}public/assets/img/iconDiamantBlack.png" alt="iconDiamantBlack" id="diamantbtn">MUESTRA</button>*/
/*                             </center>*/
/*                           </div>*/
/*                           <center><button id="btnPedido">COMENZAR PEDIDO</button></center>*/
/*                           */
/*                           <p class="titlesSlides">*/
/*                             Términos y Condiciones <br>*/
/*                             <span>Tu pedido pasara por un proceso de autorización, no hay compra automática en esta plataforma.</span> <br>*/
/*                           </p>*/
/*                           <div class="panel_tc">*/
/*                             <p>*/
/*                               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.*/
/*                             </p>*/
/*                           </div>*/
/*                           <p class="slide_tc">Leer más</p>*/
/* */
/* */
/*                           <p class="titlesSlides">*/
/*                             Exelente opción para estudiantes y principiantes <br>*/
/*                             <span>*/
/*                               Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles. <br><br>*/
/*                               Los filamentos da Vinci Mini se fabrican con plásticos PLA derivados del almidón de maíz, haciéndolos biodegradables y no tóxicos. Los filamentos de XYZprintin.*/
/*                             </span> <br>*/
/*                           </p>*/
/*                           <div class="panel_eo">*/
/*                             <p>*/
/*                               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.*/
/*                             </p>*/
/*                           </div>*/
/*                           <p class="slide_eo">Leer más</p>          */
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <hr>*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             ITEM:*/
/*                             <span>*/
/*                               THKLD*/
/*                             </span>*/
/*                           </p>*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             CALIDAD:*/
/*                             <span>*/
/*                               Premium*/
/*                             </span>*/
/*                           </p>*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             PRESENTACIÓN:*/
/*                             <span>*/
/*                               Caja*/
/*                             </span>*/
/*                           </p>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <center>*/
/*                         <div class="circle_color" style="background-color: black"></div>*/
/*                         <div class="circle_color" style="background-color: red"></div>*/
/*                         <div class="circle_color" style="background-color: yellow"></div>*/
/*                         <div class="circle_color" style="background-color: green"></div>*/
/*                         <div class="circle_color" style="background-color: blue"></div>*/
/*                         <div class="circle_color" style="background-color: purple"></div>*/
/*                         <div class="circle_color" style="background-color: pink"></div>*/
/*                         <div class="circle_color" style="background-color: orange"></div>*/
/*                       </center>*/
/* */
/*                       <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingOne">*/
/*                             <h4 class="panel-title">*/
/*                               <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">*/
/*                                 VENTAJA COMPETITIVA*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">*/
/*                             <div class="panel-body">*/
/*                               <ul>*/
/*                                 <li>Compacta y Productiva</li>*/
/*                                 <li>Practicidad de impresión en cualquier parte de la casa.</li>*/
/*                                 <li>Eco-friendly</li>*/
/*                                 <li>Impresión family safe</li>*/
/*                                 <li>Indicador de status LED</li>*/
/*                               </ul>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingTwo">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseTwo">*/
/*                                 CARACTERISTICAS*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">*/
/*                             <div class="panel-body">*/
/*                               <ul>*/
/*                                 <li>XYZ Sotware</li>*/
/*                                 <li>Cama de impresión de aluminio</li>*/
/*                                 <li>Compatible para platucos PLA</li>*/
/*                                 <li>Garantía DHP</li>*/
/*                               </ul>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingThree">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseThree">*/
/*                                 ESPECIFICACIONES*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">*/
/*                             <div class="panel-body">*/
/*                               <p class="titlesSlides" style="margin-top: 0">*/
/* */
/*                                 <span>*/
/*                                 FFF (Fused Filament Fabrication)*/
/*                                 </span>*/
/*                                 Product Dimension*/
/*                                 <span>*/
/*                                   (WxDxH) <br>*/
/*                                   15.35 x 13.19 x 14.17 inch (390 x 335 x 360 mm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Package Dimension*/
/*                                 <span>*/
/*                                   21.65x15.35x18.50 inch (550x 390 x 470 mm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Gross Weight*/
/*                                 <span>*/
/*                                   24.25lbs (10 kg)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Maximum Build Volume*/
/*                                 <span>*/
/*                                   (WxDxH) <br>*/
/*                                   5.9 x 5.9 x 5.9 inch (15x15x15cm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Resolution*/
/*                                 <span>*/
/*                                   Fine 0.1 mm (100 microns)*/
/*                                   Standard 0.2 mm (200 microns)*/
/*                                   Speed 0.3 mm (300 microns)*/
/*                                   Ultra Fast 0.4 mm (400 microns)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Print Head*/
/*                                 <span>*/
/*                                   Single Nozzle*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Nozzle Diameter*/
/*                                 <span>*/
/*                                   0.4 mm*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Filament Diameter*/
/*                                 <span>*/
/*                                   1.75 mm*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Filament Material*/
/*                                 <span>*/
/*                                   PLA*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Connectivity*/
/*                                 <span>*/
/*                                   USB 2.0, WiFi (802.11 b/g/n)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Software*/
/*                                 <span>*/
/*                                   XYZware*/
/*                                   File Types*/
/*                                   .stl , XYZ Format (.3w), 3mf*/
/*                                   OS Support*/
/*                                   Windows 7 and above (for PC)*/
/*                                   Mac OSX 10.8 and above (for Mac)*/
/*                                   Note: Stardard VGA driver on operating system, or a graphics card doesn't support OpenGL 2.1,*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Hardware*/
/*                                 <span>*/
/*                                   X86 32/64-bit compatible PCs with 4GB+ DRAM*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Hardware Requirements (for PC/Mac)*/
/*                                 <span>*/
/*                                   X86 64-bit compatible Macs with 4GB+ DRAM*/
/*                                 </span>*/
/* */
/*                               </p>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingFour">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseFour">*/
/*                                 EMPAQUE*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">*/
/*                             <div class="panel-body">*/
/*                               <p class="titlesSlides" style="margin-top: 0">*/
/*                                 Caja maestra:*/
/*                                 <span>*/
/*                                   6 piezas*/
/*                                 </span>*/
/*                               </p>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 SR_last">*/
/* */
/*                           <input type="hidden" name="rating" id="rating" />*/
/*                           <ul onMouseOut="resetRating();">*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                           </ul>*/
/*                           */
/*                           <br>*/
/* */
/*                           <a href="#"><i class="fa fa-facebook faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-twitter faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-instagram faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-youtube faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-google-plus faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-pinterest faRedes" aria-hidden="true"></i></a>*/
/* */
/*                           <br>*/
/* */
/*                           <div>*/
/*                             <p><a href="#">¿Tienes dudas respecto al producto?</a></p>*/
/*                             <p><a href="#">AUTORIZACIÓN</a> | <a href="#">ENTREGA</a> | <a href="#">DEVOLUCIONES</a></p>*/
/*                           </div>*/
/*                           */
/*                         </div>*/
/*                       </div>*/
/*                       */
/*                     </div> <!-- boxBorder -->*/
/*                     */
/*                   </div> <!-- div columnaUno -->*/
/*                   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 columnaDos">*/
/*                     <div class="boxBorder boxColFirst">*/
/*                       <div class="circuloComparar"></div>*/
/*                       <span class="cerrarComparar">x</span>*/
/*                       <center>*/
/*                         <img class="comparaPic" src="{{ base_url() }}public/assets/img/comparaPic2.jpg" alt="comparaPic2">*/
/*                       </center>*/
/*                     </div>*/
/*                     <br>*/
/*                     <div class="boxBorder">*/
/*                       <center>*/
/*                        <div class="boxFavoritos">*/
/*                           <ul>*/
/*                             <li>*/
/*                               <center>¡Regístrate para guardar este producto!</center>*/
/*                             </li>*/
/*                             <li>*/
/*                               <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                               Favoritos*/
/*                             </li>*/
/*                             <hr>*/
/*                             <li>*/
/*                               <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                               Nueva lista*/
/*                             </li>*/
/*                             <hr>*/
/*                             <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                           </ul>*/
/*                        </div>*/
/*                       </center>*/
/*                       <div class="row">*/
/*                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 icono-like-navRight">*/
/*                           <img src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png" width="45px">*/
/*                         </div>*/
/*                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 icono-comparar-navRight">*/
/*                           <img src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png" width="30px">*/
/*                           <p>Comparar</p>*/
/*                         </div>*/
/*                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pdf-navRight">*/
/*                          <p>PDF</p>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 textProductNR">*/
/*                           <p>Impresión | Tecnología</p>*/
/*                           <h5>XYZ Printing da Vinci Mini 3D Printer</h5>*/
/*                           <p>*/
/*                             COSTO: $ 218.24 EUR <br>*/
/*                             CANTIDAD MINIMA DE PIEZAS: 50*/
/*                           </p>*/
/*                           <p>Holanda Inventario</p>*/
/*                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol">*/
/*                             <center><button class="btnProductNR btnPersonalizar">PERSONALIZAR</button></center>*/
/*                           </div>*/
/*                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol">*/
/*                             <center>*/
/*                               <button class="btnProductNR"><img src="{{ base_url() }}public/assets/img/iconDiamantBlack.png" alt="iconDiamantBlack" id="diamantbtn">MUESTRA</button>*/
/*                             </center>*/
/*                           </div>*/
/*                           <center><button id="btnPedido">COMENZAR PEDIDO</button></center>*/
/*                           */
/*                           <p class="titlesSlides">*/
/*                             Términos y Condiciones <br>*/
/*                             <span>Tu pedido pasara por un proceso de autorización, no hay compra automática en esta plataforma.</span> <br>*/
/*                           </p>*/
/*                           <div class="panel_tc">*/
/*                             <p>*/
/*                               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.*/
/*                             </p>*/
/*                           </div>*/
/*                           <p class="slide_tc">Leer más</p>*/
/* */
/* */
/*                           <p class="titlesSlides">*/
/*                             Exelente opción para estudiantes y principiantes <br>*/
/*                             <span>*/
/*                               Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles. <br><br>*/
/*                               Los filamentos da Vinci Mini se fabrican con plásticos PLA derivados del almidón de maíz, haciéndolos biodegradables y no tóxicos. Los filamentos de XYZprintin.*/
/*                             </span> <br>*/
/*                           </p>*/
/*                           <div class="panel_eo">*/
/*                             <p>*/
/*                               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.*/
/*                             </p>*/
/*                           </div>*/
/*                           <p class="slide_eo">Leer más</p>          */
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <hr>*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             ITEM:*/
/*                             <span>*/
/*                               THKLD*/
/*                             </span>*/
/*                           </p>*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             CALIDAD:*/
/*                             <span>*/
/*                               Premium*/
/*                             </span>*/
/*                           </p>*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             PRESENTACIÓN:*/
/*                             <span>*/
/*                               Caja*/
/*                             </span>*/
/*                           </p>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <center>*/
/*                         <div class="circle_color" style="background-color: black"></div>*/
/*                         <div class="circle_color" style="background-color: red"></div>*/
/*                         <div class="circle_color" style="background-color: yellow"></div>*/
/*                         <div class="circle_color" style="background-color: green"></div>*/
/*                         <div class="circle_color" style="background-color: blue"></div>*/
/*                         <div class="circle_color" style="background-color: purple"></div>*/
/*                         <div class="circle_color" style="background-color: pink"></div>*/
/*                         <div class="circle_color" style="background-color: orange"></div>*/
/*                       </center>*/
/* */
/*                       <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingOne">*/
/*                             <h4 class="panel-title">*/
/*                               <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">*/
/*                                 VENTAJA COMPETITIVA*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">*/
/*                             <div class="panel-body">*/
/*                               <ul>*/
/*                                 <li>Compacta y Productiva</li>*/
/*                                 <li>Practicidad de impresión en cualquier parte de la casa.</li>*/
/*                                 <li>Eco-friendly</li>*/
/*                                 <li>Impresión family safe</li>*/
/*                                 <li>Indicador de status LED</li>*/
/*                               </ul>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingTwo">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseTwo">*/
/*                                 CARACTERISTICAS*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">*/
/*                             <div class="panel-body">*/
/*                               <ul>*/
/*                                 <li>XYZ Sotware</li>*/
/*                                 <li>Cama de impresión de aluminio</li>*/
/*                                 <li>Compatible para platucos PLA</li>*/
/*                                 <li>Garantía DHP</li>*/
/*                               </ul>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingThree">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseThree">*/
/*                                 ESPECIFICACIONES*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">*/
/*                             <div class="panel-body">*/
/*                               <p class="titlesSlides" style="margin-top: 0">*/
/* */
/*                                 <span>*/
/*                                 FFF (Fused Filament Fabrication)*/
/*                                 </span>*/
/*                                 Product Dimension*/
/*                                 <span>*/
/*                                   (WxDxH) <br>*/
/*                                   15.35 x 13.19 x 14.17 inch (390 x 335 x 360 mm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Package Dimension*/
/*                                 <span>*/
/*                                   21.65x15.35x18.50 inch (550x 390 x 470 mm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Gross Weight*/
/*                                 <span>*/
/*                                   24.25lbs (10 kg)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Maximum Build Volume*/
/*                                 <span>*/
/*                                   (WxDxH) <br>*/
/*                                   5.9 x 5.9 x 5.9 inch (15x15x15cm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Resolution*/
/*                                 <span>*/
/*                                   Fine 0.1 mm (100 microns)*/
/*                                   Standard 0.2 mm (200 microns)*/
/*                                   Speed 0.3 mm (300 microns)*/
/*                                   Ultra Fast 0.4 mm (400 microns)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Print Head*/
/*                                 <span>*/
/*                                   Single Nozzle*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Nozzle Diameter*/
/*                                 <span>*/
/*                                   0.4 mm*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Filament Diameter*/
/*                                 <span>*/
/*                                   1.75 mm*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Filament Material*/
/*                                 <span>*/
/*                                   PLA*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Connectivity*/
/*                                 <span>*/
/*                                   USB 2.0, WiFi (802.11 b/g/n)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Software*/
/*                                 <span>*/
/*                                   XYZware*/
/*                                   File Types*/
/*                                   .stl , XYZ Format (.3w), 3mf*/
/*                                   OS Support*/
/*                                   Windows 7 and above (for PC)*/
/*                                   Mac OSX 10.8 and above (for Mac)*/
/*                                   Note: Stardard VGA driver on operating system, or a graphics card doesn't support OpenGL 2.1,*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Hardware*/
/*                                 <span>*/
/*                                   X86 32/64-bit compatible PCs with 4GB+ DRAM*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Hardware Requirements (for PC/Mac)*/
/*                                 <span>*/
/*                                   X86 64-bit compatible Macs with 4GB+ DRAM*/
/*                                 </span>*/
/* */
/*                               </p>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingFour">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseFour">*/
/*                                 EMPAQUE*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">*/
/*                             <div class="panel-body">*/
/*                               <p class="titlesSlides" style="margin-top: 0">*/
/*                                 Caja maestra:*/
/*                                 <span>*/
/*                                   6 piezas*/
/*                                 </span>*/
/*                               </p>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 SR_last">*/
/* */
/*                           <input type="hidden" name="rating" id="rating" />*/
/*                           <ul onMouseOut="resetRating();">*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                           </ul>*/
/*                           */
/*                           <br>*/
/* */
/*                           <a href="#"><i class="fa fa-facebook faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-twitter faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-instagram faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-youtube faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-google-plus faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-pinterest faRedes" aria-hidden="true"></i></a>*/
/* */
/*                           <br>*/
/* */
/*                           <div>*/
/*                             <p><a href="#">¿Tienes dudas respecto al producto?</a></p>*/
/*                             <p><a href="#">AUTORIZACIÓN</a> | <a href="#">ENTREGA</a> | <a href="#">DEVOLUCIONES</a></p>*/
/*                           </div>*/
/*                           */
/*                         </div>*/
/*                       </div>*/
/*                       */
/*                     </div> <!-- boxBorder -->*/
/*                     */
/*                   </div> <!-- div columnaDos -->*/
/*                   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 columnaTres">*/
/*                     <div class="boxBorder boxColFirst">*/
/*                       <div class="circuloComparar"></div>*/
/*                       <span class="cerrarComparar">x</span>*/
/*                       <center>*/
/*                         <img class="comparaPic" src="{{ base_url() }}public/assets/img/comparaPic3.jpg" alt="comparaPic3">*/
/*                       </center>*/
/*                     </div>*/
/*                     <br>*/
/*                     <div class="boxBorder">*/
/*                       <center>*/
/*                        <div class="boxFavoritos">*/
/*                           <ul>*/
/*                             <li>*/
/*                               <center>¡Regístrate para guardar este producto!</center>*/
/*                             </li>*/
/*                             <li>*/
/*                               <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                               Favoritos*/
/*                             </li>*/
/*                             <hr>*/
/*                             <li>*/
/*                               <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                               Nueva lista*/
/*                             </li>*/
/*                             <hr>*/
/*                             <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                           </ul>*/
/*                        </div>*/
/*                       </center>*/
/*                       <div class="row">*/
/*                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 icono-like-navRight">*/
/*                           <img src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png" width="45px">*/
/*                         </div>*/
/*                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 icono-comparar-navRight">*/
/*                           <img src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png" width="30px">*/
/*                           <p>Comparar</p>*/
/*                         </div>*/
/*                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pdf-navRight">*/
/*                          <p>PDF</p>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 textProductNR">*/
/*                           <p>Impresión | Tecnología</p>*/
/*                           <h5>XYZ Printing da Vinci Mini 3D Printer</h5>*/
/*                           <p>*/
/*                             COSTO: $ 218.24 EUR <br>*/
/*                             CANTIDAD MINIMA DE PIEZAS: 50*/
/*                           </p>*/
/*                           <p>Holanda Inventario</p>*/
/*                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol">*/
/*                             <center><button class="btnProductNR btnPersonalizar">PERSONALIZAR</button></center>*/
/*                           </div>*/
/*                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 anchoCol">*/
/*                             <center>*/
/*                               <button class="btnProductNR"><img src="{{ base_url() }}public/assets/img/iconDiamantBlack.png" alt="iconDiamantBlack" id="diamantbtn">MUESTRA</button>*/
/*                             </center>*/
/*                           </div>*/
/*                           <center><button id="btnPedido">COMENZAR PEDIDO</button></center>*/
/*                           */
/*                           <p class="titlesSlides">*/
/*                             Términos y Condiciones <br>*/
/*                             <span>Tu pedido pasara por un proceso de autorización, no hay compra automática en esta plataforma.</span> <br>*/
/*                           </p>*/
/*                           <div class="panel_tc">*/
/*                             <p>*/
/*                               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.*/
/*                             </p>*/
/*                           </div>*/
/*                           <p class="slide_tc">Leer más</p>*/
/* */
/* */
/*                           <p class="titlesSlides">*/
/*                             Exelente opción para estudiantes y principiantes <br>*/
/*                             <span>*/
/*                               Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles. <br><br>*/
/*                               Los filamentos da Vinci Mini se fabrican con plásticos PLA derivados del almidón de maíz, haciéndolos biodegradables y no tóxicos. Los filamentos de XYZprintin.*/
/*                             </span> <br>*/
/*                           </p>*/
/*                           <div class="panel_eo">*/
/*                             <p>*/
/*                               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit voluptas recusandae consequuntur officiis neque voluptates omnis, alias ea dignissimos totam iusto enim, magnam voluptatibus quas iure incidunt aliquid cumque.*/
/*                             </p>*/
/*                           </div>*/
/*                           <p class="slide_eo">Leer más</p>          */
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <hr>*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             ITEM:*/
/*                             <span>*/
/*                               THKLD*/
/*                             </span>*/
/*                           </p>*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             CALIDAD:*/
/*                             <span>*/
/*                               Premium*/
/*                             </span>*/
/*                           </p>*/
/*                           <p class="titlesSlides" style="margin-top: 0">*/
/*                             PRESENTACIÓN:*/
/*                             <span>*/
/*                               Caja*/
/*                             </span>*/
/*                           </p>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <center>*/
/*                         <div class="circle_color" style="background-color: black"></div>*/
/*                         <div class="circle_color" style="background-color: red"></div>*/
/*                         <div class="circle_color" style="background-color: yellow"></div>*/
/*                         <div class="circle_color" style="background-color: green"></div>*/
/*                         <div class="circle_color" style="background-color: blue"></div>*/
/*                         <div class="circle_color" style="background-color: purple"></div>*/
/*                         <div class="circle_color" style="background-color: pink"></div>*/
/*                         <div class="circle_color" style="background-color: orange"></div>*/
/*                       </center>*/
/* */
/*                       <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingOne">*/
/*                             <h4 class="panel-title">*/
/*                               <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">*/
/*                                 VENTAJA COMPETITIVA*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">*/
/*                             <div class="panel-body">*/
/*                               <ul>*/
/*                                 <li>Compacta y Productiva</li>*/
/*                                 <li>Practicidad de impresión en cualquier parte de la casa.</li>*/
/*                                 <li>Eco-friendly</li>*/
/*                                 <li>Impresión family safe</li>*/
/*                                 <li>Indicador de status LED</li>*/
/*                               </ul>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingTwo">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseTwo">*/
/*                                 CARACTERISTICAS*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">*/
/*                             <div class="panel-body">*/
/*                               <ul>*/
/*                                 <li>XYZ Sotware</li>*/
/*                                 <li>Cama de impresión de aluminio</li>*/
/*                                 <li>Compatible para platucos PLA</li>*/
/*                                 <li>Garantía DHP</li>*/
/*                               </ul>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingThree">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseThree">*/
/*                                 ESPECIFICACIONES*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">*/
/*                             <div class="panel-body">*/
/*                               <p class="titlesSlides" style="margin-top: 0">*/
/* */
/*                                 <span>*/
/*                                 FFF (Fused Filament Fabrication)*/
/*                                 </span>*/
/*                                 Product Dimension*/
/*                                 <span>*/
/*                                   (WxDxH) <br>*/
/*                                   15.35 x 13.19 x 14.17 inch (390 x 335 x 360 mm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Package Dimension*/
/*                                 <span>*/
/*                                   21.65x15.35x18.50 inch (550x 390 x 470 mm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Gross Weight*/
/*                                 <span>*/
/*                                   24.25lbs (10 kg)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Maximum Build Volume*/
/*                                 <span>*/
/*                                   (WxDxH) <br>*/
/*                                   5.9 x 5.9 x 5.9 inch (15x15x15cm)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Resolution*/
/*                                 <span>*/
/*                                   Fine 0.1 mm (100 microns)*/
/*                                   Standard 0.2 mm (200 microns)*/
/*                                   Speed 0.3 mm (300 microns)*/
/*                                   Ultra Fast 0.4 mm (400 microns)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Print Head*/
/*                                 <span>*/
/*                                   Single Nozzle*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Nozzle Diameter*/
/*                                 <span>*/
/*                                   0.4 mm*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Filament Diameter*/
/*                                 <span>*/
/*                                   1.75 mm*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Filament Material*/
/*                                 <span>*/
/*                                   PLA*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Connectivity*/
/*                                 <span>*/
/*                                   USB 2.0, WiFi (802.11 b/g/n)*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Software*/
/*                                 <span>*/
/*                                   XYZware*/
/*                                   File Types*/
/*                                   .stl , XYZ Format (.3w), 3mf*/
/*                                   OS Support*/
/*                                   Windows 7 and above (for PC)*/
/*                                   Mac OSX 10.8 and above (for Mac)*/
/*                                   Note: Stardard VGA driver on operating system, or a graphics card doesn't support OpenGL 2.1,*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Hardware*/
/*                                 <span>*/
/*                                   X86 32/64-bit compatible PCs with 4GB+ DRAM*/
/*                                 </span>*/
/* */
/*                                 <br>*/
/* */
/*                                 Hardware Requirements (for PC/Mac)*/
/*                                 <span>*/
/*                                   X86 64-bit compatible Macs with 4GB+ DRAM*/
/*                                 </span>*/
/* */
/*                               </p>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                         <div class="panel panel-default">*/
/*                           <div class="panel-heading" role="tab" id="headingFour">*/
/*                             <h4 class="panel-title">*/
/*                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseFour">*/
/*                                 EMPAQUE*/
/*                               </a>*/
/*                             </h4>*/
/*                           </div>*/
/*                           <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">*/
/*                             <div class="panel-body">*/
/*                               <p class="titlesSlides" style="margin-top: 0">*/
/*                                 Caja maestra:*/
/*                                 <span>*/
/*                                   6 piezas*/
/*                                 </span>*/
/*                               </p>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                       </div>*/
/* */
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 SR_last">*/
/* */
/*                           <input type="hidden" name="rating" id="rating" />*/
/*                           <ul onMouseOut="resetRating();">*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                             <li onmouseover="highlightStar(this);" onmouseout="removeHighlight();" onClick="addRating(this);" class="starsClass">*/
/*                               <i class="fa fa-star-o faStars" aria-hidden="true"></i>*/
/*                             </li>*/
/*                           </ul>*/
/*                           */
/*                           <br>*/
/* */
/*                           <a href="#"><i class="fa fa-facebook faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-twitter faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-instagram faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-youtube faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-google-plus faRedes" aria-hidden="true"></i></a>*/
/*                           <a href="#"><i class="fa fa-pinterest faRedes" aria-hidden="true"></i></a>*/
/* */
/*                           <br>*/
/* */
/*                           <div>*/
/*                             <p><a href="#">¿Tienes dudas respecto al producto?</a></p>*/
/*                             <p><a href="#">AUTORIZACIÓN</a> | <a href="#">ENTREGA</a> | <a href="#">DEVOLUCIONES</a></p>*/
/*                           </div>*/
/*                           */
/*                         </div>*/
/*                       </div>*/
/*                       */
/*                     </div> <!-- boxBorder -->*/
/*                     */
/*                   </div> <!-- div columnaTres -->*/
/* */
/*                 </div> <!-- row -->*/
/* */
/*               </div> <!-- divItem Active -->*/
/*             </div>*/
/* */
/*             <!-- Controls -->*/
/*             <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">*/
/*               <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*               <span class="sr-only">Previous</span>*/
/*             </a>*/
/*             <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">*/
/*               <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*               <span class="sr-only">Next</span>*/
/*             </a>*/
/*         </div>*/
/*       </div> <!-- container -->*/
/* */
/*       <!-- End contend --> */
/* */
/* */
/*       <!-- Pre-Footer -->*/
/*       <div class="row anchoRow">*/
/*         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*           <center>*/
/*             <div id="top-footer">*/
/*               <p>*/
/*               Selecciona los productos que quieras para solicitar un pedido <br>Tame Traffic no es una tienda en linea*/
/*               </p>*/
/*               <a href="#">¿Como se compra en Tame Traffic?</a><br>*/
/*               <a href="#">¿Como selecciono productos?</a><br>*/
/*               <a href="#">¿Como se hace un pedido?</a>*/
/*             </div>*/
/*           </center>*/
/*         </div>*/
/*       </div>*/
/* */
/*     </div>*/
/*     <!-- End box content -->*/
/* */
/* {% endblock %}*/
