<?php

/* dashboard/admin/supplier-preview-test.html */
class __TwigTemplate_c4357bc989bebe69fbea8a15a2580484b41e6cf8dcdb12d1b0caac5a66456bba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/supplier-preview-test.html", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<style>
    #bodyWrapper {
        height: 100%;
        background-color: #fff;
    }

    .table-responsive
    {
        overflow-x: auto;
    }
</style>
";
    }

    // line 16
    public function block_body($context, array $blocks = array())
    {
        // line 17
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-6\">
            <h1 class=\"title-dashboard text-uppercase\">Vista Previa</h1>
        </div>
        <div class=\"col-sm-6 text-right\">
            <a href=\"";
        // line 23
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/add\" class=\"btn btn-sm btn-success\">
                <i class=\"fa fa-plus-circle fa-right--10\"></i>
                Agregar otro Supplier
            </a>
            <a href=\"";
        // line 27
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier\" class=\"btn btn-sm btn-info\">
                <i class=\"fa fa-list fa-right--10\"></i>
                Suppliers
            </a>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <h3 class=\"page-header margin-clear margin-bottom--20\">Supplier</h3>
                    <div class=\"row\">
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sName\">Nombre</label>
                                <input id=\"sName\" type=\"text\" class=\"form-control\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "name", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sLegalName\">Razón Social</label>
                                <input id=\"sLegalName\" type=\"text\" class=\"form-control\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "legal_name", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sCustomeId\">ID</label>
                                <input id=\"sCustomeId\" type=\"text\" class=\"form-control\" value=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idcustome", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sCreatedAt\">Fecha de admisión</label>
                                <input id=\"sCreatedAt\" type=\"text\" class=\"form-control\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "m/d/Y"), "html", null, true);
        echo "\" disabled>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sCategory\">Categoría</label>
                                <input id=\"sCategory\" type=\"text\" class=\"form-control\" value=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "category", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sDescription\">Descripción</label>
                                <input id=\"sDescription\" type=\"text\" class=\"form-control\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "description", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sOfficeAddress\">Dirección Oficinas</label>
                                <input id=\"sOfficeAddress\" type=\"text\" class=\"form-control\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_address", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sWarehouseAddress\">Dirección Bodega</label>
                                <input id=\"sWarehouseAddress\" type=\"text\" class=\"form-control\" value=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "office_warehouse", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sDesignee\">Persona Asignada</label>
                                <input id=\"sDesignee\" type=\"text\" class=\"form-control\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sDesigneePhone\">P.A. Tel. OficIna</label>
                                <input id=\"sDesigneePhone\" type=\"text\" class=\"form-control\" value=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_phone", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sDesigneeMobile\">P.A. Tel. Móvil</label>
                                <input id=\"sDesigneeMobile\" type=\"text\" class=\"form-control\" value=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_mobile", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sDesigneeEmail\">P.A. Email</label>
                                <input id=\"sDesigneeEmail\" type=\"text\" class=\"form-control\" value=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "designee_email", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sManager\">Persona Alto Mando</label>
                                <input id=\"sManager\" type=\"text\" class=\"form-control\" value=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sManagerPhone\">P.A.M. Tel. Oficina</label>
                                <input id=\"sManagerPhone\" type=\"text\" class=\"form-control\" value=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_phone", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sManagerMobile\">P.A.M. Tel. Móvil</label>
                                <input id=\"sManagerMobile\" type=\"text\" class=\"form-control\" value=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_mobile", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-3\">
                            <div class=\"form-group\">
                                <label for=\"sManagerEmail\">P.A.M. Email</label>
                                <input id=\"sManagerEmail\" type=\"text\" class=\"form-control\" value=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "manager_email", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-sm-12\">
                            <input type=\"hidden\" id=\"sId\" value=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "idSupplier", array()), "html", null, true);
        echo "\">
                            <button id=\"editSupplier\" class=\"btn btn-primary\">Editar Supplier</button>
                        </div>
                    </div>
                    <h3 class=\"page-header margin-clear margin-bottom--10 margin-top--20\">Productos</h3>
                </div>
                <table class=\"table\">
                    <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th>Nombre</th>
                        <th>Advertencias</th>
                        <th>Acción</th>
                    </tr>
                    ";
        // line 154
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 155
            echo "
                    <tr id=\"product-";
            // line 156
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" class=\"row-product\">
                        <td>
                            ";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "
                            <form id=\"form-product-";
            // line 159
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\">
                              ";
            // line 160
            if (file_exists((((("public/uploads/supplier/catalog/" . $this->getAttribute($context["product"], "directory", array())) . "/") . $this->getAttribute($context["product"], "item", array())) . ".jpg"))) {
                // line 161
                echo "                                <input id=\"input-image-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "\" type=\"hidden\"  class=\"form-control\" value=\"true\">
                              ";
            } else {
                // line 163
                echo "                                <input id=\"input-image-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "\" type=\"hidden\"  class=\"form-control\" value=\"false\">
                              ";
            }
            // line 165
            echo "                                <input id=\"input-id-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "idProduct", array()), "html", null, true);
            echo "\">
                                <input id=\"input-name-";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "\">
                                <input id=\"input-score-";
            // line 167
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "score", array()), "html", null, true);
            echo "\">
                                <input id=\"input-header-";
            // line 168
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "header", array()), "html", null, true);
            echo "\">
                                <input id=\"input-description-";
            // line 169
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "description", array()), "html", null, true);
            echo "\">
                                <input id=\"input-item-";
            // line 170
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo "\">
                                <input id=\"input-category-";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "categoryA", array()), "html", null, true);
            echo "\">
                                <input id=\"input-subcategory-";
            // line 172
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "subcategoryA", array()), "html", null, true);
            echo "\">
                                <input id=\"input-quality-";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "quality", array()), "html", null, true);
            echo "\">
                                <input id=\"input-origin-";
            // line 174
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "origin", array()), "html", null, true);
            echo "\">
                                <input id=\"input-colors-";
            // line 175
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "colors", array()), "html", null, true);
            echo "\">
                                <input id=\"input-presentation-";
            // line 176
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "presentation", array()), "html", null, true);
            echo "\">
                                <input id=\"input-characteristics-";
            // line 177
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "characteristics", array()), "html", null, true);
            echo "\">
                                <input id=\"input-especifications-";
            // line 178
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "especifications", array()), "html", null, true);
            echo "\">
                                <input id=\"input-dimensions-";
            // line 179
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "dimensions", array()), "html", null, true);
            echo "\">
                                <input id=\"input-weight-";
            // line 180
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "weight", array()), "html", null, true);
            echo "\">
                                <input id=\"input-minimumOrder-";
            // line 181
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "minimumOrder", array()), "html", null, true);
            echo "\">
                                <input id=\"input-dateExpiry-";
            // line 182
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "dataExpiry", array()), "html", null, true);
            echo "\">
                                <input id=\"input-inventory_numPieces-";
            // line 183
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "inventory_numPieces", array()), "html", null, true);
            echo "\">
                                <input id=\"input-boutique_maximumOrder-";
            // line 184
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "boutique_maximumOrder", array()), "html", null, true);
            echo "\">
                                <input id=\"input-price_withoutTax-";
            // line 185
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price_withoutTax", array()), "html", null, true);
            echo "\">
                                <input id=\"input-price_currency-";
            // line 186
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price_currency", array()), "html", null, true);
            echo "\">
                                <input id=\"input-price_suggested-";
            // line 187
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price_suggested", array()), "html", null, true);
            echo "\">
                                <input id=\"input-price_tt-";
            // line 188
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price_tt", array()), "html", null, true);
            echo "\">
                                <input id=\"input-packing_numPieces-";
            // line 189
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "packing_numPieces", array()), "html", null, true);
            echo "\">
                                <input id=\"input-packing_especifications-";
            // line 190
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "packing_especifications", array()), "html", null, true);
            echo "\">
                                <input id=\"input-packing_height-";
            // line 191
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "packing_height", array()), "html", null, true);
            echo "\">
                                <input id=\"input-packing_width-";
            // line 192
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "packing_width", array()), "html", null, true);
            echo "\">
                                <input id=\"input-packing_large-";
            // line 193
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "packing_large", array()), "html", null, true);
            echo "\">
                                <input id=\"input-packing_weight-";
            // line 194
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "packing_weight", array()), "html", null, true);
            echo "\">
                                <input id=\"input-logistics_productionTime-";
            // line 195
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "logistics_productionTime", array()), "html", null, true);
            echo "\">
                                <input id=\"input-logistics_fob-";
            // line 196
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "logistics_fob", array()), "html", null, true);
            echo "\">
                                <input id=\"input-logistics_fobDeliveryTime-";
            // line 197
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "logistics_fobDeliveryTime", array()), "html", null, true);
            echo "\">
                                <input id=\"input-sample-";
            // line 198
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" type=\"hidden\"  class=\"form-control\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sample", array()), "html", null, true);
            echo "\">
                            </form>
                        </td>
                        <td id=\"product-item-";
            // line 201
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "item", array()), "html", null, true);
            echo "</td>
                        <td id=\"product-name-";
            // line 202
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</td>
                        <td>
                            <ul id=\"errors-";
            // line 204
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\"></ul>
                        </td>
                        <td>
                            <button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#editProduct\" data-id=\"";
            // line 207
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\">Editar</button>
                        </td>
                    </tr>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 211
        echo "                </table>
            </div>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"editProduct\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Editar producto</h4>
            </div>
            <div class=\"modal-body\">
                <form id=\"formEditProduct\" action=\"\" class=\"row\">
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pname\">Nombre</label>
                        <input id=\"pname\" name=\"pname\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pscore\">Calificación</label>
                        <input id=\"pscore\" name=\"pscore\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pheader\">Header</label>
                        <textarea id=\"pheader\" rows=\"3\" name=\"pheader\" type=\"text\" class=\"form-control\"></textarea>
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdescription\">Calificación</label>
                        <textarea id=\"pdescription\" rows=\"3\" name=\"pdescription\" type=\"text\" class=\"form-control\"></textarea>
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pitem\">Item</label>
                        <input id=\"pitem\" name=\"pitem\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcategory\">Categoría</label>
                        <input id=\"pcategory\" name=\"pcategory\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"psubcat\">Subcategoría</label>
                        <input id=\"psubcat\" name=\"psubcat\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pquality\">Calidad</label>
                        <input id=\"pquality\" name=\"pquality\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"porigin\">Origen</label>
                        <input id=\"porigin\" name=\"porigin\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcolor\">Colores</label>
                        <input id=\"pcolor\" name=\"pcolor\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppresentation\">Presentación</label>
                        <input id=\"ppresentation\" name=\"ppresentation\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pcharacteristics\">Caracteristicas</label>
                        <input id=\"pcharacteristics\" name=\"pcharacteristics\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pspecifications\">Especificaciones</label>
                        <input id=\"pspecifications\" name=\"pspecifications\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdimensions\">Dimensiones</label>
                        <input id=\"pdimensions\" name=\"pdimensions\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pweight\">Peso</label>
                        <input id=\"pweight\" name=\"pweight\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pminimumOrder\">Minimo de Pedido</label>
                        <input id=\"pminimumOrder\" name=\"pminimumOrder\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pdateExpiry\">Fecha de Caducidad</label>
                        <input id=\"pdateExpiry\" name=\"pdateExpiry\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pinvNumPieces\">Inventario No. de Piezas</label>
                        <input id=\"pinvNumPieces\" name=\"pinvNumPieces\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"pboutiqueMaxOrder\">Máximo de Pedido</label>
                        <input id=\"pboutiqueMaxOrder\" name=\"pboutiqueMaxOrder\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceWithoutTax\">Precio sin impuesto</label>
                        <input id=\"ppriceWithoutTax\" name=\"ppriceWithoutTax\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceCurrency\">Moneda</label>
                        <input id=\"ppriceCurrency\" name=\"ppriceCurrency\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceSuggested\">Precio Sugerido</label>
                        <input id=\"ppriceSuggested\" name=\"ppriceSuggested\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppriceTT\">Precio TT</label>
                        <input id=\"ppriceTT\" name=\"ppriceTT\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingNumPieces\">Empaque - No. de Piezas</label>
                        <input id=\"ppackingNumPieces\" name=\"ppackingNumPieces\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingSpecifications\">Empaque - Especifiaciones</label>
                        <input id=\"ppackingSpecifications\" name=\"ppackingSpecifications\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingHeight\">Empaque - Altura</label>
                        <input id=\"ppackingHeight\" name=\"ppackingHeight\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingWidth\">Empaque - Ancho</label>
                        <input id=\"ppackingWidth\" name=\"ppackingWidth\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingLarge\">Empaque - Largo</label>
                        <input id=\"ppackingLarge\" name=\"ppackingLarge\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"ppackingWeight\">Empaque - Peso</label>
                        <input id=\"ppackingWeight\" name=\"ppackingWeight\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"plogTimeProduction\">Logistica - Tiempo de producción</label>
                        <input id=\"plogTimeProduction\" name=\"pLogTimeProduction\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"plogFob\">Logistica - FOB</label>
                        <input id=\"plogFob\" name=\"pLogFob\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"plogFobDeliveryTime\">Logistica - FOB Tiempo de entrega</label>
                        <input id=\"plogFobDeliveryTime\" name=\"pLogFobDeliveryTime\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-6\">
                        <label for=\"psample\">Muestra</label>
                        <input id=\"psample\" name=\"psample\" type=\"text\" class=\"form-control\">
                    </div>
                    <div class=\"form-group col-sm-12\">
                        <input id=\"rowid\" type=\"hidden\" name=\"rowid\">
                        <input id=\"pid\" type=\"hidden\" name=\"pid\">
                        <button type=\"submit\" class=\"btn btn-success\">Guardar</button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Cancelar</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 371
    public function block_script($context, array $blocks = array())
    {
        // line 372
        echo "<script>

    function findProductImage(name, callback, callbackSuccess)
    {
      
        \$.ajax({
    url:'";
        // line 378
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/uploads/supplier/catalog/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "directory", array()), "html", null, true);
        echo "/' + name + '.jpg',
    type:'HEAD',
    error: function()
    {
        callback();
    },
    success: function()
    {
       callbackSuccess();
    }
});
    }
  
    function getProducts() {
        var rows = \$('.row-product').length;
        console.log(rows);

        var errors = [];

        for (var i = 1; i <= rows; i++ ) {

            var inputName = \$('#input-name-' + i);
            var inputImage = \$('#input-image-' + i);
            var inputScore = \$('#input-score-' + i);
            var inputHeader = \$('#input-header-' + i);
            var inputDescription = \$('#input-description-' + i);
            var inputItem = \$('#input-item-' + i);
            var inputCategory = \$('#input-category-' + i);
            var inputSubcategory = \$('#input-subcategory-' + i);
            var inputQuality = \$('#input-quality-' + i);
            var inputOrigin = \$('#input-origin-' + i);
            var inputColors = \$('#input-colors-' + i);
            var inputPresentation = \$('#input-presentation-' + i);
            var inputCharacteristics = \$('#input-characteristics-' + i);
            var inputEspecifications = \$('#input-especifications-' + i);
            var inputDimensions = \$('#input-dimensions-' + i);
            var inputWeight = \$('#input-weight-' + i);
            var inputMinimumOrder = \$('#input-minimumOrder-' + i);
            var inputDateExpiry = \$('#input-dateExpiry-' + i);
            var inputInvNumPieces = \$('#input-inventory_numPieces-' + i);
            var inputBoutiqueMaxOrder = \$('#input-boutique_maximumOrder-' + i);
            var inputPriceWithoutTax = \$('#input-price_withoutTax-' + i);
            var inputPriceCurrency = \$('#input-price_currency-' + i);
            var inputPriceSuggested = \$('#input-price_suggested-' + i);
            var inputPriceTT = \$('#input-price_tt-' + i);
            var inputPackingNumPieces = \$('#input-packing_numPieces-' + i);
            var inputPackingEspecifications = \$('#input-packing_especifications-' + i);
            var inputPackingHeight = \$('#input-packing_height-' + i);
            var inputPackingWidth = \$('#input-packing_width-' + i);
            var inputPackingLarge = \$('#input-packing_large-' + i);
            var inputPackingWeight = \$('#input-packing_weight-' + i);
            var inputPackingLogProductionTime = \$('#input-logistics_productionTime-' + i);
            var inputLogFob = \$('#input-logistics_fob-' + i);
            var inputLogFobDeliveryTime = \$('#input-logistics_fobDeliveryTime-' + i);
            var inputSample = \$('#input-sample-' + i);

            
          
            if (inputName.val() == '') {
                errors.push('El nombre esta vacio');
            }

            if (inputScore.val() == '') {
                errors.push('La calificación esta vacia');
            }
       
          
          if (inputImage.val() == 'false') {
            errors.push('No tiene imagen de cabecera');
          }
                

            /*if (inputHeader.val() == '') {
             errors.push('El header esta vacio');
             }*/

            if (inputDescription.val() == '') {
                errors.push('La descripción esta vacia');
            }

            if (inputItem.val() == '') {
                errors.push('El item esta vacio');
            }

            if (inputCategory.val() == '') {
                errors.push('La categoría esta vacia');
            }

            if (inputSubcategory.val() == '') {
                errors.push('La subcategoría esta vacia');
            }

            if (inputQuality.val() == '') {
                errors.push('La calidad esta vacia');
            }

            if (inputOrigin.val() == '') {
                errors.push('El origen esta vacio');
            }

            if (inputColors.val() == '') {
                errors.push('El color esta vacio');
            }

            if (inputPresentation.val() == '') {
                errors.push('La presentación esta vacia');
            }

            if (inputCharacteristics.val() == '') {
                errors.push('La caracteristica esta vacio');
            }

            if (inputEspecifications.val() == '') {
                errors.push('La especificación esta vacia');
            }

            /*if (inputDimensions.val() == '') {
             errors.push('La dimensión  esta vacia');
             }*/

            /*if (inputWeight.val() == '') {
             errors.push('El peso esta vacio');
             }*/

            /*if (inputDateExpiry.val() == '') {
             errors.push('La fecha de caducidad esta vacia');
             }*/

            if (inputMinimumOrder.val() == '') {
                errors.push('La orden minima esta vacia');
            }

            if (inputInvNumPieces.val() == '') {
                errors.push('Inventario No. de piezas esta vacio');
            }

            if (inputBoutiqueMaxOrder.val() == '') {
                errors.push('El maximo de orden esta vacio');
            }

            if (inputPriceWithoutTax.val() == '') {
                errors.push('El precio sin impuesto  esta vacio');
            }

            if (inputPriceCurrency.val() == '') {
                errors.push('El tipo de moneda esta vacio');
            }

            if (inputPriceSuggested.val() == '') {
                errors.push('El precio sugerido esta vacio');
            }

            if (inputPriceTT.val() == '') {
                errors.push('El precio TT esta vacio');
            }

            if (inputPackingNumPieces.val() == '') {
                errors.push('El número de piezas de empaquetado esta vacio');
            }

            if (inputPackingEspecifications.val() == '') {
                errors.push('Las especificaciones de empaquetado esta vacio');
            }

            /*if (inputPackingHeight.val() == '') {
             errors.push('El alto del empaque esta vacio');
             }

             if (inputPackingWidth.val() == '') {
             errors.push('El ancho del empaque esta vacio');
             }

             if (inputPackingLarge.val() == '') {
             errors.push('El largo del empaque esta vacio');
             }

             if (inputPackingWeight.val() == '') {
             errors.push('El peso del empaque esta vacio');
             }*/

            if (inputPackingLogProductionTime.val() == '') {
                errors.push('El tiempo de producción esta vacio');
            }

            if (inputLogFob.val() == '') {
                errors.push('El Fob esta vacio');
            }

            if (inputLogFobDeliveryTime.val() == '') {
                errors.push('El tiempo de entrega del FOB  esta vacio');
            }

            if (inputSample.val() == '') {
                errors.push('La muestra esta vacia');
            }

            /*\$('#form-product-' + i).children('input').each(function () {
             console.log(this.value);
             });*/

            var errorList = '';

            if (errors.length > 0) {
                \$(\"#product-\" + i).addClass('bg-warning');
            } else {
                \$(\"#product-\" + i).removeClass('bg-warning');
                \$(\"#product-\" + i).addClass('bg-success');
                errorList += '<li>Cumple con los requisitos minimos</li>';
            }



            \$.each(errors, function (k, v) {
                errorList += '<li>' + v + '</li>';
            });

            \$('#errors-' + i).html(errorList);

            errors = [];

        }
    }

    getProducts()

    \$('#formEditProduct').on('submit', function (e) {
        e.preventDefault();

        var btn = \$(this).find('button');

        var id = \$('#pid').val();
        var row = \$('#rowid').val();
        var pName = \$(\"#pname\").val();
        var pScore = \$(\"#pscore\").val();
        var pHeader = \$(\"#pheader\").val();
        var pDescription = \$(\"#pdescription\").val();
        var pItem = \$('#pitem').val();
        var pCategory = \$('#pcategory').val();
        var pSubcategory = \$('#psubcat').val();
        var pQuality = \$('#pquality').val();
        var pOrigin = \$('#porigin').val();
        var pColors = \$('#pcolor').val();
        var pPresentation = \$('#ppresentation').val();
        var pCharacteristics = \$('#pcharacteristics').val();
        var pEspecifications = \$('#pspecifications').val();
        var pDimensions = \$('#pdimensions').val();
        var pWeight = \$('#pweight').val();
        var pMinimumOrder = \$('#pminimumOrder').val();
        var pDateExpiry = \$('#pdateExpiry').val();
        var pInvNumPieces = \$('#pinvNumPieces').val();
        var pBoutiqueMaxOrder = \$('#pboutiqueMaxOrder').val();
        var pPriceWithoutTax = \$('#ppriceWithoutTax').val();
        var pPriceCurrency = \$('#ppriceCurrency').val();
        var pPriceSuggested = \$('#ppriceSuggested').val();
        var pPriceTT = \$('#ppriceTT').val();
        var pPackingNumPieces = \$('#ppackingNumPieces').val();
        var pPackingEspecifications = \$('#ppackingSpecifications').val();
        var pPackingHeight = \$('#ppackingHeight').val();
        var pPackingWidth = \$('#ppackingWidth').val();
        var pPackingLarge = \$('#ppackingLarge').val();
        var pPackingWeight = \$('#ppackingWeight').val();
        var pPackingLogProductionTime = \$('#plogTimeProduction').val();
        var pLogFob = \$('#plogFob').val();
        var pLogFobDeliveryTime = \$('#plogFobDeliveryTime').val();
        var pSample = \$('#psample').val();

         console.log(id)

        \$.ajax({
            url: '";
        // line 647
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/product/action/edit',
            type: 'post',
            dataType: 'html',
            cache: false,
            data: {
                pId : id,
                pName : pName,
                pScore : pScore,
                pHeader : pHeader,
                pDescription : pDescription,
                pItem : pItem,
                pCategory : pCategory,
                pSubcategory : pSubcategory,
                pQuality : pQuality,
                pOrigin : pOrigin,
                pColors : pColors,
                pPresentation : pPresentation,
                pCharacteristics : pCharacteristics,
                pSpecifications : pEspecifications,
                pDimensions : pDimensions,
                pWeight : pWeight,
                pMinimumOrder : pMinimumOrder,
                pDateExpiry : pDateExpiry,
                pInvNumPieces : pInvNumPieces,
                pBoutiqueMaxOrder : pBoutiqueMaxOrder,
                pPriceWithoutTax : pPriceWithoutTax,
                pPriceCurrency : pPriceCurrency,
                pPriceSuggested : pPriceSuggested,
                pPriceTT : pPriceTT,
                pPackingNumPieces : pPackingNumPieces,
                pPackingSpecifications : pPackingEspecifications,
                pPackingHeight : pPackingHeight,
                pPackingWidth : pPackingWidth,
                pPackingLarge : pPackingLarge,
                pPackingWeight : pPackingWeight,
                pLogProductionTime : pPackingLogProductionTime,
                pLogFob : pLogFob,
                pLogFobDeliveryTime : pLogFobDeliveryTime,
                pSample : pSample
            },
            beforeSend: function() {
                btn.prop('disabled', true)
            },
            success: function(response) {

                if (response == '1') {

                    \$(\"#input-id-\" + row).val(id);
                    \$(\"#input-name-\" + row).val(pName);
                    \$(\"#input-score-\" + row).val(pScore);
                    \$(\"#input-header-\" + row).val(pHeader);
                    \$(\"#input-description-\" + row).val(pDescription);
                    \$('#input-item-' + row).val(pItem);
                    \$('#input-category-' + row).val(pCategory);
                    \$('#input-subcategory-' + row).val(pSubcategory);
                    \$('#input-quality-' + row).val(pQuality);
                    \$('#input-origin-' + row).val(pOrigin);
                    \$('#input-colors-' + row).val(pColors);
                    \$('#input-presentation-' + row).val(pPresentation);
                    \$('#input-characteristics-' + row).val(pCharacteristics);
                    \$('#input-especifications-' + row).val(pEspecifications);
                    \$('#input-dimensions-' + row).val(pDimensions);
                    \$('#input-weight-' + row).val(pWeight);
                    \$('#input-minimumOrder-' + row).val(pMinimumOrder);
                    \$('#input-dateExpiry-' + row).val(pDateExpiry);
                    \$('#input-inventory_numPieces-' + row).val(pInvNumPieces);
                    \$('#input-boutique_maximumOrder-' + row).val(pBoutiqueMaxOrder);
                    \$('#input-price_withoutTax-' + row).val(pPriceWithoutTax);
                    \$('#input-price_currency-' + row).val(pPriceCurrency);
                    \$('#input-price_suggested-' + row).val(pPriceSuggested);
                    \$('#input-price_tt-' + row).val(pPriceTT);
                    \$('#input-packing_numPieces-' + row).val(pPackingNumPieces);
                    \$('#input-packing_especifications-' + row).val(pPackingEspecifications);
                    \$('#input-packing_height-' + row).val(pPackingHeight);
                    \$('#input-packing_width-' + row).val(pPackingWidth);
                    \$('#input-packing_large-' + row).val(pPackingLarge);
                    \$('#input-packing_weight-' + row).val(pPackingWeight);
                    \$('#input-logistics_productionTime-' + row).val(pPackingLogProductionTime);
                    \$('#input-logistics_fob-' + row).val(pLogFob);
                    \$('#input-logistics_fobDeliveryTime-' + row).val(pLogFobDeliveryTime);
                    \$('#input-sample-' + row).val(pSample);

                    getProducts();

                    \$('#product-name-' + row).html(pName);
                    \$('#product-item-' + row).html(pItem);

                    \$('#editProduct').modal('hide');
                    btn.prop('disabled', false);
                } else {
                    alert(response);
                    btn.prop('disabled', false);
                }
            },
            error: function(xhr, textStatus) {
                alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');
                btn.prop('disabled', false);
            }
        })

        /*\$(\"#input-name-\" + id).val(pName);
        \$(\"#input-score-\" + id).val(pScore);
        \$(\"#input-header-\" + id).val(pHeader);
        \$(\"#input-description-\" + id).val(pDescription);
        \$('#input-item-' + id).val(pItem);
        \$('#input-category-' + id).val(pCategory);
        \$('#input-subcategory-' + id).val(pSubcategory);
        \$('#input-quality-' + id).val(pQuality);
        \$('#input-origin-' + id).val(pOrigin);
        \$('#input-colors-' + id).val(pColors);
        \$('#input-presentation-' + id).val(pPresentation);
        \$('#input-characteristics-' + id).val(pCharacteristics);
        \$('#input-especifications-' + id).val(pEspecifications);
        \$('#input-dimensions-' + id).val(pDimensions);
        \$('#input-weight-' + id).val(pWeight);
        \$('#input-minimumOrder-' + id).val(pMinimumOrder);
        \$('#input-dateExpiry-' + id).val(pDateExpiry);
        \$('#input-inventory_numPieces-' + id).val(pInvNumPieces);
        \$('#input-boutique_maximumOrder-' + id).val(pBoutiqueMaxOrder);
        \$('#input-price_withoutTax-' + id).val(pPriceWithoutTax);
        \$('#input-price_currency-' + id).val(pPriceCurrency);
        \$('#input-price_suggested-' + id).val(pPriceSuggested);
        \$('#input-price_tt-' + id).val(pPriceTT);
        \$('#input-packing_numPieces-' + id).val(pPackingNumPieces);
        \$('#input-packing_especifications-' + id).val(pPackingEspecifications);
        \$('#input-packing_height-' + id).val(pPackingHeight);
        \$('#input-packing_width-' + id).val(pPackingWidth);
        \$('#input-packing_large-' + id).val(pPackingLarge);
        \$('#input-packing_weight-' + id).val(pPackingWeight);
        \$('#input-logistics_productionTime-' + id).val(pPackingLogProductionTime);
        \$('#input-logistics_fob-' + id).val(pLogFob);
        \$('#input-logistics_fobDeliveryTime-' + id).val(pLogFobDeliveryTime);
        \$('#input-sample-' + id).val(pSample);

        getProducts();

        \$('#product-name-' + id).html(pName);
        \$('#product-item-' + id).html(pItem);

        \$('#editProduct').modal('hide');*/

    })

    \$('#editProduct').on('hide.bs.modal', function (event) {

    });

    \$('#editProduct').on('show.bs.modal', function (event) {

        var modal = \$(this)
        var button = \$(event.relatedTarget)
        var id = button.data('id')

        var inputId = \$(\"#input-id-\" + id).val()
        var inputName = \$(\"#input-name-\" + id).val()
        var inputScore = \$(\"#input-score-\" + id).val()
        var inputHeader = \$(\"#input-header-\" + id).val()
        var inputDescription = \$(\"#input-description-\" + id).val()
        var inputItem = \$('#input-item-' + id).val();
        var inputCategory = \$('#input-category-' + id).val();
        var inputSubcategory = \$('#input-subcategory-' + id).val();
        var inputQuality = \$('#input-quality-' + id).val();
        var inputOrigin = \$('#input-origin-' + id).val();
        var inputColors = \$('#input-colors-' + id).val();
        var inputPresentation = \$('#input-presentation-' + id).val();
        var inputCharacteristics = \$('#input-characteristics-' + id).val();
        var inputEspecifications = \$('#input-especifications-' + id).val();
        var inputDimensions = \$('#input-dimensions-' + id).val();
        var inputWeight = \$('#input-weight-' + id).val();
        var inputMinimumOrder = \$('#input-minimumOrder-' + id).val();
        var inputDateExpiry = \$('#input-dateExpiry-' + id).val();
        var inputInvNumPieces = \$('#input-inventory_numPieces-' + id).val();
        var inputBoutiqueMaxOrder = \$('#input-boutique_maximumOrder-' + id).val();
        var inputPriceWithoutTax = \$('#input-price_withoutTax-' + id).val();
        var inputPriceCurrency = \$('#input-price_currency-' + id).val();
        var inputPriceSuggested = \$('#input-price_suggested-' + id).val();
        var inputPriceTT = \$('#input-price_tt-' + id).val();
        var inputPackingNumPieces = \$('#input-packing_numPieces-' + id).val();
        var inputPackingEspecifications = \$('#input-packing_especifications-' + id).val();
        var inputPackingHeight = \$('#input-packing_height-' + id).val();
        var inputPackingWidth = \$('#input-packing_width-' + id).val();
        var inputPackingLarge = \$('#input-packing_large-' + id).val();
        var inputPackingWeight = \$('#input-packing_weight-' + id).val();
        var inputPackingLogProductionTime = \$('#input-logistics_productionTime-' + id).val();
        var inputLogFob = \$('#input-logistics_fob-' + id).val();
        var inputLogFobDeliveryTime = \$('#input-logistics_fobDeliveryTime-' + id).val();
        var inputSample = \$('#input-sample-' + id).val();

        modal.find('.modal-title').text('Editar ' + inputName)
        modal.find('.modal-body #rowid').val(id)
        modal.find('.modal-body #pid').val(inputId)
        modal.find('.modal-body #pname').val(inputName)
        modal.find('.modal-body #pscore').val(inputScore)
        modal.find('.modal-body #pheader').val(inputHeader)
        modal.find('.modal-body #pdescription').val(inputDescription)
        modal.find('.modal-body #pitem').val(inputItem)
        modal.find('.modal-body #pcategory').val(inputCategory)
        modal.find('.modal-body #psubcat').val(inputSubcategory)
        modal.find('.modal-body #pquality').val(inputQuality)
        modal.find('.modal-body #pcolor').val(inputColors)
        modal.find('.modal-body #porigin').val(inputOrigin)
        modal.find('.modal-body #ppresentation').val(inputPresentation)
        modal.find('.modal-body #pcharacteristics').val(inputCharacteristics)
        modal.find('.modal-body #pspecifications').val(inputEspecifications)
        modal.find('.modal-body #pdimensions').val(inputDimensions)
        modal.find('.modal-body #pweight').val(inputWeight)
        modal.find('.modal-body #pminimumOrder').val(inputMinimumOrder)
        modal.find('.modal-body #pdateExpiry').val(inputDateExpiry)
        modal.find('.modal-body #pinvNumPieces').val(inputInvNumPieces)
        modal.find('.modal-body #pboutiqueMaxOrder').val(inputBoutiqueMaxOrder)
        modal.find('.modal-body #ppriceWithoutTax').val(inputPriceWithoutTax)
        modal.find('.modal-body #ppriceCurrency').val(inputPriceCurrency)
        modal.find('.modal-body #ppriceSuggested').val(inputPriceSuggested)
        modal.find('.modal-body #ppriceTT').val(inputPriceTT)
        modal.find('.modal-body #ppackingNumPieces').val(inputPackingNumPieces)
        modal.find('.modal-body #ppackingSpecifications').val(inputPackingEspecifications)
        modal.find('.modal-body #ppackingHeight').val(inputPackingHeight)
        modal.find('.modal-body #ppackingWidth').val(inputPackingWidth)
        modal.find('.modal-body #ppackingLarge').val(inputPackingLarge)
        modal.find('.modal-body #ppackingWeight').val(inputPackingWeight)
        modal.find('.modal-body #plogTimeProduction').val(inputPackingLogProductionTime)
        modal.find('.modal-body #plogFob').val(inputLogFob)
        modal.find('.modal-body #plogFobDeliveryTime').val(inputLogFobDeliveryTime)
        modal.find('.modal-body #psample').val(inputSample)
    })


    \$('#editSupplier').on('click', function(){

        var btn = \$(this);

        var id = \$(\"#sId\").val();
        var name = \$('#sName').val();
        var legalName = \$('#sLegalName').val();
        var customeId = \$('#sCustomeId').val();
        var category = \$('#sCategory').val();
        var description = \$('#sDescription').val();
        var officeAddress = \$('#sOfficeAddress').val();
        var warehouseAddress = \$('#sWarehouseAddress').val();
        var designee = \$('#sDesignee').val();
        var designeePhone = \$('#sDesigneePhone').val();
        var designeeMobile = \$('#sDesigneeMobile').val();
        var designeeEmail = \$('#sDesigneeEmail').val();
        var manager = \$('#sManager').val();
        var managerPhone = \$('#sManagerPhone').val();
        var managerMobile = \$('#sManagerMobile').val();
        var managerEmail = \$('#sManagerEmail').val();

        \$.ajax({
            url: '";
        // line 896
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/supplier/action/edit',
            type: 'post',
            dataType: 'html',
            cache: false,
            data: {
                sId : id,
                sName : name,
                sLegalName : legalName,
                sCustomeId : customeId,
                sCategory : category,
                sDescription : description,
                sOfficeAddress : officeAddress,
                sWarehouseAddress : warehouseAddress,
                sDesignee : designee,
                sDesigneePhone : designeePhone,
                sDesigneeMobile : designeeMobile,
                sDesigneeEmail : designeeEmail,
                sManager : manager,
                sManagerPhone : managerPhone,
                sManagerMobile : managerMobile,
                sManagerEmail : managerEmail
            },
            beforeSend: function() {
                btn.prop('disabled', true)
            },
            success: function(response) {

                if (response == '1') {
                    location.reload();
                } else {
                    alert(response);
                    btn.prop('disabled', false);
                }
            },
            error: function(xhr, textStatus) {
                alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');
                btn.prop('disabled', false);
            }
        })
    })

</script>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/supplier-preview-test.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1240 => 896,  988 => 647,  714 => 378,  706 => 372,  703 => 371,  540 => 211,  522 => 207,  516 => 204,  509 => 202,  503 => 201,  495 => 198,  489 => 197,  483 => 196,  477 => 195,  471 => 194,  465 => 193,  459 => 192,  453 => 191,  447 => 190,  441 => 189,  435 => 188,  429 => 187,  423 => 186,  417 => 185,  411 => 184,  405 => 183,  399 => 182,  393 => 181,  387 => 180,  381 => 179,  375 => 178,  369 => 177,  363 => 176,  357 => 175,  351 => 174,  345 => 173,  339 => 172,  333 => 171,  327 => 170,  321 => 169,  315 => 168,  309 => 167,  303 => 166,  296 => 165,  290 => 163,  284 => 161,  282 => 160,  278 => 159,  274 => 158,  269 => 156,  266 => 155,  249 => 154,  232 => 140,  225 => 136,  216 => 130,  207 => 124,  198 => 118,  187 => 110,  178 => 104,  169 => 98,  160 => 92,  149 => 84,  140 => 78,  131 => 72,  122 => 66,  111 => 58,  102 => 52,  93 => 46,  84 => 40,  68 => 27,  61 => 23,  53 => 17,  50 => 16,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block head %}*/
/* {{ parent() }}*/
/* <style>*/
/*     #bodyWrapper {*/
/*         height: 100%;*/
/*         background-color: #fff;*/
/*     }*/
/* */
/*     .table-responsive*/
/*     {*/
/*         overflow-x: auto;*/
/*     }*/
/* </style>*/
/* {% endblock %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-6">*/
/*             <h1 class="title-dashboard text-uppercase">Vista Previa</h1>*/
/*         </div>*/
/*         <div class="col-sm-6 text-right">*/
/*             <a href="{{ base_url() }}dashboard/admin/supplier/add" class="btn btn-sm btn-success">*/
/*                 <i class="fa fa-plus-circle fa-right--10"></i>*/
/*                 Agregar otro Supplier*/
/*             </a>*/
/*             <a href="{{ base_url() }}dashboard/admin/supplier" class="btn btn-sm btn-info">*/
/*                 <i class="fa fa-list fa-right--10"></i>*/
/*                 Suppliers*/
/*             </a>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <h3 class="page-header margin-clear margin-bottom--20">Supplier</h3>*/
/*                     <div class="row">*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sName">Nombre</label>*/
/*                                 <input id="sName" type="text" class="form-control" value="{{ supplier.name }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sLegalName">Razón Social</label>*/
/*                                 <input id="sLegalName" type="text" class="form-control" value="{{ supplier.legal_name }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sCustomeId">ID</label>*/
/*                                 <input id="sCustomeId" type="text" class="form-control" value="{{ supplier.idcustome }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sCreatedAt">Fecha de admisión</label>*/
/*                                 <input id="sCreatedAt" type="text" class="form-control" value="{{ 'now'|date('m/d/Y') }}" disabled>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sCategory">Categoría</label>*/
/*                                 <input id="sCategory" type="text" class="form-control" value="{{ supplier.category }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sDescription">Descripción</label>*/
/*                                 <input id="sDescription" type="text" class="form-control" value="{{ supplier.description }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sOfficeAddress">Dirección Oficinas</label>*/
/*                                 <input id="sOfficeAddress" type="text" class="form-control" value="{{ supplier.office_address }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sWarehouseAddress">Dirección Bodega</label>*/
/*                                 <input id="sWarehouseAddress" type="text" class="form-control" value="{{ supplier.office_warehouse }}">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sDesignee">Persona Asignada</label>*/
/*                                 <input id="sDesignee" type="text" class="form-control" value="{{ supplier.designee }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sDesigneePhone">P.A. Tel. OficIna</label>*/
/*                                 <input id="sDesigneePhone" type="text" class="form-control" value="{{ supplier.designee_phone }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sDesigneeMobile">P.A. Tel. Móvil</label>*/
/*                                 <input id="sDesigneeMobile" type="text" class="form-control" value="{{ supplier.designee_mobile }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sDesigneeEmail">P.A. Email</label>*/
/*                                 <input id="sDesigneeEmail" type="text" class="form-control" value="{{ supplier.designee_email }}">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sManager">Persona Alto Mando</label>*/
/*                                 <input id="sManager" type="text" class="form-control" value="{{ supplier.manager }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sManagerPhone">P.A.M. Tel. Oficina</label>*/
/*                                 <input id="sManagerPhone" type="text" class="form-control" value="{{ supplier.manager_phone }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sManagerMobile">P.A.M. Tel. Móvil</label>*/
/*                                 <input id="sManagerMobile" type="text" class="form-control" value="{{ supplier.manager_mobile }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-3">*/
/*                             <div class="form-group">*/
/*                                 <label for="sManagerEmail">P.A.M. Email</label>*/
/*                                 <input id="sManagerEmail" type="text" class="form-control" value="{{ supplier.manager_email }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-12">*/
/*                             <input type="hidden" id="sId" value="{{ supplier.idSupplier }}">*/
/*                             <button id="editSupplier" class="btn btn-primary">Editar Supplier</button>*/
/*                         </div>*/
/*                     </div>*/
/*                     <h3 class="page-header margin-clear margin-bottom--10 margin-top--20">Productos</h3>*/
/*                 </div>*/
/*                 <table class="table">*/
/*                     <tr>*/
/*                         <th>#</th>*/
/*                         <th>Item</th>*/
/*                         <th>Nombre</th>*/
/*                         <th>Advertencias</th>*/
/*                         <th>Acción</th>*/
/*                     </tr>*/
/*                     {% for product in products %}*/
/* */
/*                     <tr id="product-{{ loop.index }}" class="row-product">*/
/*                         <td>*/
/*                             {{ loop.index }}*/
/*                             <form id="form-product-{{ loop.index }}">*/
/*                               {% if file_exists('public/uploads/supplier/catalog/' ~ product.directory ~ '/' ~ product.item ~ '.jpg') %}*/
/*                                 <input id="input-image-{{ loop.index }}" type="hidden"  class="form-control" value="true">*/
/*                               {% else %}*/
/*                                 <input id="input-image-{{ loop.index }}" type="hidden"  class="form-control" value="false">*/
/*                               {% endif %}*/
/*                                 <input id="input-id-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.idProduct }}">*/
/*                                 <input id="input-name-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.name }}">*/
/*                                 <input id="input-score-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.score }}">*/
/*                                 <input id="input-header-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.header }}">*/
/*                                 <input id="input-description-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.description }}">*/
/*                                 <input id="input-item-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.item }}">*/
/*                                 <input id="input-category-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.categoryA }}">*/
/*                                 <input id="input-subcategory-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.subcategoryA }}">*/
/*                                 <input id="input-quality-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.quality }}">*/
/*                                 <input id="input-origin-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.origin }}">*/
/*                                 <input id="input-colors-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.colors }}">*/
/*                                 <input id="input-presentation-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.presentation }}">*/
/*                                 <input id="input-characteristics-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.characteristics }}">*/
/*                                 <input id="input-especifications-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.especifications }}">*/
/*                                 <input id="input-dimensions-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.dimensions }}">*/
/*                                 <input id="input-weight-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.weight }}">*/
/*                                 <input id="input-minimumOrder-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.minimumOrder }}">*/
/*                                 <input id="input-dateExpiry-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.dataExpiry }}">*/
/*                                 <input id="input-inventory_numPieces-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.inventory_numPieces }}">*/
/*                                 <input id="input-boutique_maximumOrder-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.boutique_maximumOrder }}">*/
/*                                 <input id="input-price_withoutTax-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.price_withoutTax }}">*/
/*                                 <input id="input-price_currency-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.price_currency }}">*/
/*                                 <input id="input-price_suggested-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.price_suggested }}">*/
/*                                 <input id="input-price_tt-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.price_tt }}">*/
/*                                 <input id="input-packing_numPieces-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.packing_numPieces }}">*/
/*                                 <input id="input-packing_especifications-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.packing_especifications }}">*/
/*                                 <input id="input-packing_height-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.packing_height }}">*/
/*                                 <input id="input-packing_width-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.packing_width }}">*/
/*                                 <input id="input-packing_large-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.packing_large }}">*/
/*                                 <input id="input-packing_weight-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.packing_weight }}">*/
/*                                 <input id="input-logistics_productionTime-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.logistics_productionTime }}">*/
/*                                 <input id="input-logistics_fob-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.logistics_fob }}">*/
/*                                 <input id="input-logistics_fobDeliveryTime-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.logistics_fobDeliveryTime }}">*/
/*                                 <input id="input-sample-{{ loop.index }}" type="hidden"  class="form-control" value="{{ product.sample }}">*/
/*                             </form>*/
/*                         </td>*/
/*                         <td id="product-item-{{ loop.index }}">{{ product.item }}</td>*/
/*                         <td id="product-name-{{ loop.index }}">{{ product.name }}</td>*/
/*                         <td>*/
/*                             <ul id="errors-{{ loop.index }}"></ul>*/
/*                         </td>*/
/*                         <td>*/
/*                             <button class="btn btn-primary" data-toggle="modal" data-target="#editProduct" data-id="{{ loop.index }}">Editar</button>*/
/*                         </td>*/
/*                     </tr>*/
/*                     {% endfor %}*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal fade" id="editProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*     <div class="modal-dialog modal-lg" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*                 <h4 class="modal-title" id="myModalLabel">Editar producto</h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*                 <form id="formEditProduct" action="" class="row">*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pname">Nombre</label>*/
/*                         <input id="pname" name="pname" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pscore">Calificación</label>*/
/*                         <input id="pscore" name="pscore" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pheader">Header</label>*/
/*                         <textarea id="pheader" rows="3" name="pheader" type="text" class="form-control"></textarea>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdescription">Calificación</label>*/
/*                         <textarea id="pdescription" rows="3" name="pdescription" type="text" class="form-control"></textarea>*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pitem">Item</label>*/
/*                         <input id="pitem" name="pitem" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcategory">Categoría</label>*/
/*                         <input id="pcategory" name="pcategory" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="psubcat">Subcategoría</label>*/
/*                         <input id="psubcat" name="psubcat" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pquality">Calidad</label>*/
/*                         <input id="pquality" name="pquality" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="porigin">Origen</label>*/
/*                         <input id="porigin" name="porigin" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcolor">Colores</label>*/
/*                         <input id="pcolor" name="pcolor" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppresentation">Presentación</label>*/
/*                         <input id="ppresentation" name="ppresentation" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pcharacteristics">Caracteristicas</label>*/
/*                         <input id="pcharacteristics" name="pcharacteristics" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pspecifications">Especificaciones</label>*/
/*                         <input id="pspecifications" name="pspecifications" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdimensions">Dimensiones</label>*/
/*                         <input id="pdimensions" name="pdimensions" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pweight">Peso</label>*/
/*                         <input id="pweight" name="pweight" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pminimumOrder">Minimo de Pedido</label>*/
/*                         <input id="pminimumOrder" name="pminimumOrder" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pdateExpiry">Fecha de Caducidad</label>*/
/*                         <input id="pdateExpiry" name="pdateExpiry" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pinvNumPieces">Inventario No. de Piezas</label>*/
/*                         <input id="pinvNumPieces" name="pinvNumPieces" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="pboutiqueMaxOrder">Máximo de Pedido</label>*/
/*                         <input id="pboutiqueMaxOrder" name="pboutiqueMaxOrder" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceWithoutTax">Precio sin impuesto</label>*/
/*                         <input id="ppriceWithoutTax" name="ppriceWithoutTax" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceCurrency">Moneda</label>*/
/*                         <input id="ppriceCurrency" name="ppriceCurrency" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceSuggested">Precio Sugerido</label>*/
/*                         <input id="ppriceSuggested" name="ppriceSuggested" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppriceTT">Precio TT</label>*/
/*                         <input id="ppriceTT" name="ppriceTT" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingNumPieces">Empaque - No. de Piezas</label>*/
/*                         <input id="ppackingNumPieces" name="ppackingNumPieces" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingSpecifications">Empaque - Especifiaciones</label>*/
/*                         <input id="ppackingSpecifications" name="ppackingSpecifications" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingHeight">Empaque - Altura</label>*/
/*                         <input id="ppackingHeight" name="ppackingHeight" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingWidth">Empaque - Ancho</label>*/
/*                         <input id="ppackingWidth" name="ppackingWidth" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingLarge">Empaque - Largo</label>*/
/*                         <input id="ppackingLarge" name="ppackingLarge" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="ppackingWeight">Empaque - Peso</label>*/
/*                         <input id="ppackingWeight" name="ppackingWeight" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="plogTimeProduction">Logistica - Tiempo de producción</label>*/
/*                         <input id="plogTimeProduction" name="pLogTimeProduction" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="plogFob">Logistica - FOB</label>*/
/*                         <input id="plogFob" name="pLogFob" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="plogFobDeliveryTime">Logistica - FOB Tiempo de entrega</label>*/
/*                         <input id="plogFobDeliveryTime" name="pLogFobDeliveryTime" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-6">*/
/*                         <label for="psample">Muestra</label>*/
/*                         <input id="psample" name="psample" type="text" class="form-control">*/
/*                     </div>*/
/*                     <div class="form-group col-sm-12">*/
/*                         <input id="rowid" type="hidden" name="rowid">*/
/*                         <input id="pid" type="hidden" name="pid">*/
/*                         <button type="submit" class="btn btn-success">Guardar</button>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {% block script %}*/
/* <script>*/
/* */
/*     function findProductImage(name, callback, callbackSuccess)*/
/*     {*/
/*       */
/*         $.ajax({*/
/*     url:'{{ base_url() }}public/uploads/supplier/catalog/{{ supplier.directory }}/' + name + '.jpg',*/
/*     type:'HEAD',*/
/*     error: function()*/
/*     {*/
/*         callback();*/
/*     },*/
/*     success: function()*/
/*     {*/
/*        callbackSuccess();*/
/*     }*/
/* });*/
/*     }*/
/*   */
/*     function getProducts() {*/
/*         var rows = $('.row-product').length;*/
/*         console.log(rows);*/
/* */
/*         var errors = [];*/
/* */
/*         for (var i = 1; i <= rows; i++ ) {*/
/* */
/*             var inputName = $('#input-name-' + i);*/
/*             var inputImage = $('#input-image-' + i);*/
/*             var inputScore = $('#input-score-' + i);*/
/*             var inputHeader = $('#input-header-' + i);*/
/*             var inputDescription = $('#input-description-' + i);*/
/*             var inputItem = $('#input-item-' + i);*/
/*             var inputCategory = $('#input-category-' + i);*/
/*             var inputSubcategory = $('#input-subcategory-' + i);*/
/*             var inputQuality = $('#input-quality-' + i);*/
/*             var inputOrigin = $('#input-origin-' + i);*/
/*             var inputColors = $('#input-colors-' + i);*/
/*             var inputPresentation = $('#input-presentation-' + i);*/
/*             var inputCharacteristics = $('#input-characteristics-' + i);*/
/*             var inputEspecifications = $('#input-especifications-' + i);*/
/*             var inputDimensions = $('#input-dimensions-' + i);*/
/*             var inputWeight = $('#input-weight-' + i);*/
/*             var inputMinimumOrder = $('#input-minimumOrder-' + i);*/
/*             var inputDateExpiry = $('#input-dateExpiry-' + i);*/
/*             var inputInvNumPieces = $('#input-inventory_numPieces-' + i);*/
/*             var inputBoutiqueMaxOrder = $('#input-boutique_maximumOrder-' + i);*/
/*             var inputPriceWithoutTax = $('#input-price_withoutTax-' + i);*/
/*             var inputPriceCurrency = $('#input-price_currency-' + i);*/
/*             var inputPriceSuggested = $('#input-price_suggested-' + i);*/
/*             var inputPriceTT = $('#input-price_tt-' + i);*/
/*             var inputPackingNumPieces = $('#input-packing_numPieces-' + i);*/
/*             var inputPackingEspecifications = $('#input-packing_especifications-' + i);*/
/*             var inputPackingHeight = $('#input-packing_height-' + i);*/
/*             var inputPackingWidth = $('#input-packing_width-' + i);*/
/*             var inputPackingLarge = $('#input-packing_large-' + i);*/
/*             var inputPackingWeight = $('#input-packing_weight-' + i);*/
/*             var inputPackingLogProductionTime = $('#input-logistics_productionTime-' + i);*/
/*             var inputLogFob = $('#input-logistics_fob-' + i);*/
/*             var inputLogFobDeliveryTime = $('#input-logistics_fobDeliveryTime-' + i);*/
/*             var inputSample = $('#input-sample-' + i);*/
/* */
/*             */
/*           */
/*             if (inputName.val() == '') {*/
/*                 errors.push('El nombre esta vacio');*/
/*             }*/
/* */
/*             if (inputScore.val() == '') {*/
/*                 errors.push('La calificación esta vacia');*/
/*             }*/
/*        */
/*           */
/*           if (inputImage.val() == 'false') {*/
/*             errors.push('No tiene imagen de cabecera');*/
/*           }*/
/*                 */
/* */
/*             /*if (inputHeader.val() == '') {*/
/*              errors.push('El header esta vacio');*/
/*              }*//* */
/* */
/*             if (inputDescription.val() == '') {*/
/*                 errors.push('La descripción esta vacia');*/
/*             }*/
/* */
/*             if (inputItem.val() == '') {*/
/*                 errors.push('El item esta vacio');*/
/*             }*/
/* */
/*             if (inputCategory.val() == '') {*/
/*                 errors.push('La categoría esta vacia');*/
/*             }*/
/* */
/*             if (inputSubcategory.val() == '') {*/
/*                 errors.push('La subcategoría esta vacia');*/
/*             }*/
/* */
/*             if (inputQuality.val() == '') {*/
/*                 errors.push('La calidad esta vacia');*/
/*             }*/
/* */
/*             if (inputOrigin.val() == '') {*/
/*                 errors.push('El origen esta vacio');*/
/*             }*/
/* */
/*             if (inputColors.val() == '') {*/
/*                 errors.push('El color esta vacio');*/
/*             }*/
/* */
/*             if (inputPresentation.val() == '') {*/
/*                 errors.push('La presentación esta vacia');*/
/*             }*/
/* */
/*             if (inputCharacteristics.val() == '') {*/
/*                 errors.push('La caracteristica esta vacio');*/
/*             }*/
/* */
/*             if (inputEspecifications.val() == '') {*/
/*                 errors.push('La especificación esta vacia');*/
/*             }*/
/* */
/*             /*if (inputDimensions.val() == '') {*/
/*              errors.push('La dimensión  esta vacia');*/
/*              }*//* */
/* */
/*             /*if (inputWeight.val() == '') {*/
/*              errors.push('El peso esta vacio');*/
/*              }*//* */
/* */
/*             /*if (inputDateExpiry.val() == '') {*/
/*              errors.push('La fecha de caducidad esta vacia');*/
/*              }*//* */
/* */
/*             if (inputMinimumOrder.val() == '') {*/
/*                 errors.push('La orden minima esta vacia');*/
/*             }*/
/* */
/*             if (inputInvNumPieces.val() == '') {*/
/*                 errors.push('Inventario No. de piezas esta vacio');*/
/*             }*/
/* */
/*             if (inputBoutiqueMaxOrder.val() == '') {*/
/*                 errors.push('El maximo de orden esta vacio');*/
/*             }*/
/* */
/*             if (inputPriceWithoutTax.val() == '') {*/
/*                 errors.push('El precio sin impuesto  esta vacio');*/
/*             }*/
/* */
/*             if (inputPriceCurrency.val() == '') {*/
/*                 errors.push('El tipo de moneda esta vacio');*/
/*             }*/
/* */
/*             if (inputPriceSuggested.val() == '') {*/
/*                 errors.push('El precio sugerido esta vacio');*/
/*             }*/
/* */
/*             if (inputPriceTT.val() == '') {*/
/*                 errors.push('El precio TT esta vacio');*/
/*             }*/
/* */
/*             if (inputPackingNumPieces.val() == '') {*/
/*                 errors.push('El número de piezas de empaquetado esta vacio');*/
/*             }*/
/* */
/*             if (inputPackingEspecifications.val() == '') {*/
/*                 errors.push('Las especificaciones de empaquetado esta vacio');*/
/*             }*/
/* */
/*             /*if (inputPackingHeight.val() == '') {*/
/*              errors.push('El alto del empaque esta vacio');*/
/*              }*/
/* */
/*              if (inputPackingWidth.val() == '') {*/
/*              errors.push('El ancho del empaque esta vacio');*/
/*              }*/
/* */
/*              if (inputPackingLarge.val() == '') {*/
/*              errors.push('El largo del empaque esta vacio');*/
/*              }*/
/* */
/*              if (inputPackingWeight.val() == '') {*/
/*              errors.push('El peso del empaque esta vacio');*/
/*              }*//* */
/* */
/*             if (inputPackingLogProductionTime.val() == '') {*/
/*                 errors.push('El tiempo de producción esta vacio');*/
/*             }*/
/* */
/*             if (inputLogFob.val() == '') {*/
/*                 errors.push('El Fob esta vacio');*/
/*             }*/
/* */
/*             if (inputLogFobDeliveryTime.val() == '') {*/
/*                 errors.push('El tiempo de entrega del FOB  esta vacio');*/
/*             }*/
/* */
/*             if (inputSample.val() == '') {*/
/*                 errors.push('La muestra esta vacia');*/
/*             }*/
/* */
/*             /*$('#form-product-' + i).children('input').each(function () {*/
/*              console.log(this.value);*/
/*              });*//* */
/* */
/*             var errorList = '';*/
/* */
/*             if (errors.length > 0) {*/
/*                 $("#product-" + i).addClass('bg-warning');*/
/*             } else {*/
/*                 $("#product-" + i).removeClass('bg-warning');*/
/*                 $("#product-" + i).addClass('bg-success');*/
/*                 errorList += '<li>Cumple con los requisitos minimos</li>';*/
/*             }*/
/* */
/* */
/* */
/*             $.each(errors, function (k, v) {*/
/*                 errorList += '<li>' + v + '</li>';*/
/*             });*/
/* */
/*             $('#errors-' + i).html(errorList);*/
/* */
/*             errors = [];*/
/* */
/*         }*/
/*     }*/
/* */
/*     getProducts()*/
/* */
/*     $('#formEditProduct').on('submit', function (e) {*/
/*         e.preventDefault();*/
/* */
/*         var btn = $(this).find('button');*/
/* */
/*         var id = $('#pid').val();*/
/*         var row = $('#rowid').val();*/
/*         var pName = $("#pname").val();*/
/*         var pScore = $("#pscore").val();*/
/*         var pHeader = $("#pheader").val();*/
/*         var pDescription = $("#pdescription").val();*/
/*         var pItem = $('#pitem').val();*/
/*         var pCategory = $('#pcategory').val();*/
/*         var pSubcategory = $('#psubcat').val();*/
/*         var pQuality = $('#pquality').val();*/
/*         var pOrigin = $('#porigin').val();*/
/*         var pColors = $('#pcolor').val();*/
/*         var pPresentation = $('#ppresentation').val();*/
/*         var pCharacteristics = $('#pcharacteristics').val();*/
/*         var pEspecifications = $('#pspecifications').val();*/
/*         var pDimensions = $('#pdimensions').val();*/
/*         var pWeight = $('#pweight').val();*/
/*         var pMinimumOrder = $('#pminimumOrder').val();*/
/*         var pDateExpiry = $('#pdateExpiry').val();*/
/*         var pInvNumPieces = $('#pinvNumPieces').val();*/
/*         var pBoutiqueMaxOrder = $('#pboutiqueMaxOrder').val();*/
/*         var pPriceWithoutTax = $('#ppriceWithoutTax').val();*/
/*         var pPriceCurrency = $('#ppriceCurrency').val();*/
/*         var pPriceSuggested = $('#ppriceSuggested').val();*/
/*         var pPriceTT = $('#ppriceTT').val();*/
/*         var pPackingNumPieces = $('#ppackingNumPieces').val();*/
/*         var pPackingEspecifications = $('#ppackingSpecifications').val();*/
/*         var pPackingHeight = $('#ppackingHeight').val();*/
/*         var pPackingWidth = $('#ppackingWidth').val();*/
/*         var pPackingLarge = $('#ppackingLarge').val();*/
/*         var pPackingWeight = $('#ppackingWeight').val();*/
/*         var pPackingLogProductionTime = $('#plogTimeProduction').val();*/
/*         var pLogFob = $('#plogFob').val();*/
/*         var pLogFobDeliveryTime = $('#plogFobDeliveryTime').val();*/
/*         var pSample = $('#psample').val();*/
/* */
/*          console.log(id)*/
/* */
/*         $.ajax({*/
/*             url: '{{ base_url() }}dashboard/admin/product/action/edit',*/
/*             type: 'post',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             data: {*/
/*                 pId : id,*/
/*                 pName : pName,*/
/*                 pScore : pScore,*/
/*                 pHeader : pHeader,*/
/*                 pDescription : pDescription,*/
/*                 pItem : pItem,*/
/*                 pCategory : pCategory,*/
/*                 pSubcategory : pSubcategory,*/
/*                 pQuality : pQuality,*/
/*                 pOrigin : pOrigin,*/
/*                 pColors : pColors,*/
/*                 pPresentation : pPresentation,*/
/*                 pCharacteristics : pCharacteristics,*/
/*                 pSpecifications : pEspecifications,*/
/*                 pDimensions : pDimensions,*/
/*                 pWeight : pWeight,*/
/*                 pMinimumOrder : pMinimumOrder,*/
/*                 pDateExpiry : pDateExpiry,*/
/*                 pInvNumPieces : pInvNumPieces,*/
/*                 pBoutiqueMaxOrder : pBoutiqueMaxOrder,*/
/*                 pPriceWithoutTax : pPriceWithoutTax,*/
/*                 pPriceCurrency : pPriceCurrency,*/
/*                 pPriceSuggested : pPriceSuggested,*/
/*                 pPriceTT : pPriceTT,*/
/*                 pPackingNumPieces : pPackingNumPieces,*/
/*                 pPackingSpecifications : pPackingEspecifications,*/
/*                 pPackingHeight : pPackingHeight,*/
/*                 pPackingWidth : pPackingWidth,*/
/*                 pPackingLarge : pPackingLarge,*/
/*                 pPackingWeight : pPackingWeight,*/
/*                 pLogProductionTime : pPackingLogProductionTime,*/
/*                 pLogFob : pLogFob,*/
/*                 pLogFobDeliveryTime : pLogFobDeliveryTime,*/
/*                 pSample : pSample*/
/*             },*/
/*             beforeSend: function() {*/
/*                 btn.prop('disabled', true)*/
/*             },*/
/*             success: function(response) {*/
/* */
/*                 if (response == '1') {*/
/* */
/*                     $("#input-id-" + row).val(id);*/
/*                     $("#input-name-" + row).val(pName);*/
/*                     $("#input-score-" + row).val(pScore);*/
/*                     $("#input-header-" + row).val(pHeader);*/
/*                     $("#input-description-" + row).val(pDescription);*/
/*                     $('#input-item-' + row).val(pItem);*/
/*                     $('#input-category-' + row).val(pCategory);*/
/*                     $('#input-subcategory-' + row).val(pSubcategory);*/
/*                     $('#input-quality-' + row).val(pQuality);*/
/*                     $('#input-origin-' + row).val(pOrigin);*/
/*                     $('#input-colors-' + row).val(pColors);*/
/*                     $('#input-presentation-' + row).val(pPresentation);*/
/*                     $('#input-characteristics-' + row).val(pCharacteristics);*/
/*                     $('#input-especifications-' + row).val(pEspecifications);*/
/*                     $('#input-dimensions-' + row).val(pDimensions);*/
/*                     $('#input-weight-' + row).val(pWeight);*/
/*                     $('#input-minimumOrder-' + row).val(pMinimumOrder);*/
/*                     $('#input-dateExpiry-' + row).val(pDateExpiry);*/
/*                     $('#input-inventory_numPieces-' + row).val(pInvNumPieces);*/
/*                     $('#input-boutique_maximumOrder-' + row).val(pBoutiqueMaxOrder);*/
/*                     $('#input-price_withoutTax-' + row).val(pPriceWithoutTax);*/
/*                     $('#input-price_currency-' + row).val(pPriceCurrency);*/
/*                     $('#input-price_suggested-' + row).val(pPriceSuggested);*/
/*                     $('#input-price_tt-' + row).val(pPriceTT);*/
/*                     $('#input-packing_numPieces-' + row).val(pPackingNumPieces);*/
/*                     $('#input-packing_especifications-' + row).val(pPackingEspecifications);*/
/*                     $('#input-packing_height-' + row).val(pPackingHeight);*/
/*                     $('#input-packing_width-' + row).val(pPackingWidth);*/
/*                     $('#input-packing_large-' + row).val(pPackingLarge);*/
/*                     $('#input-packing_weight-' + row).val(pPackingWeight);*/
/*                     $('#input-logistics_productionTime-' + row).val(pPackingLogProductionTime);*/
/*                     $('#input-logistics_fob-' + row).val(pLogFob);*/
/*                     $('#input-logistics_fobDeliveryTime-' + row).val(pLogFobDeliveryTime);*/
/*                     $('#input-sample-' + row).val(pSample);*/
/* */
/*                     getProducts();*/
/* */
/*                     $('#product-name-' + row).html(pName);*/
/*                     $('#product-item-' + row).html(pItem);*/
/* */
/*                     $('#editProduct').modal('hide');*/
/*                     btn.prop('disabled', false);*/
/*                 } else {*/
/*                     alert(response);*/
/*                     btn.prop('disabled', false);*/
/*                 }*/
/*             },*/
/*             error: function(xhr, textStatus) {*/
/*                 alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');*/
/*                 btn.prop('disabled', false);*/
/*             }*/
/*         })*/
/* */
/*         /*$("#input-name-" + id).val(pName);*/
/*         $("#input-score-" + id).val(pScore);*/
/*         $("#input-header-" + id).val(pHeader);*/
/*         $("#input-description-" + id).val(pDescription);*/
/*         $('#input-item-' + id).val(pItem);*/
/*         $('#input-category-' + id).val(pCategory);*/
/*         $('#input-subcategory-' + id).val(pSubcategory);*/
/*         $('#input-quality-' + id).val(pQuality);*/
/*         $('#input-origin-' + id).val(pOrigin);*/
/*         $('#input-colors-' + id).val(pColors);*/
/*         $('#input-presentation-' + id).val(pPresentation);*/
/*         $('#input-characteristics-' + id).val(pCharacteristics);*/
/*         $('#input-especifications-' + id).val(pEspecifications);*/
/*         $('#input-dimensions-' + id).val(pDimensions);*/
/*         $('#input-weight-' + id).val(pWeight);*/
/*         $('#input-minimumOrder-' + id).val(pMinimumOrder);*/
/*         $('#input-dateExpiry-' + id).val(pDateExpiry);*/
/*         $('#input-inventory_numPieces-' + id).val(pInvNumPieces);*/
/*         $('#input-boutique_maximumOrder-' + id).val(pBoutiqueMaxOrder);*/
/*         $('#input-price_withoutTax-' + id).val(pPriceWithoutTax);*/
/*         $('#input-price_currency-' + id).val(pPriceCurrency);*/
/*         $('#input-price_suggested-' + id).val(pPriceSuggested);*/
/*         $('#input-price_tt-' + id).val(pPriceTT);*/
/*         $('#input-packing_numPieces-' + id).val(pPackingNumPieces);*/
/*         $('#input-packing_especifications-' + id).val(pPackingEspecifications);*/
/*         $('#input-packing_height-' + id).val(pPackingHeight);*/
/*         $('#input-packing_width-' + id).val(pPackingWidth);*/
/*         $('#input-packing_large-' + id).val(pPackingLarge);*/
/*         $('#input-packing_weight-' + id).val(pPackingWeight);*/
/*         $('#input-logistics_productionTime-' + id).val(pPackingLogProductionTime);*/
/*         $('#input-logistics_fob-' + id).val(pLogFob);*/
/*         $('#input-logistics_fobDeliveryTime-' + id).val(pLogFobDeliveryTime);*/
/*         $('#input-sample-' + id).val(pSample);*/
/* */
/*         getProducts();*/
/* */
/*         $('#product-name-' + id).html(pName);*/
/*         $('#product-item-' + id).html(pItem);*/
/* */
/*         $('#editProduct').modal('hide');*//* */
/* */
/*     })*/
/* */
/*     $('#editProduct').on('hide.bs.modal', function (event) {*/
/* */
/*     });*/
/* */
/*     $('#editProduct').on('show.bs.modal', function (event) {*/
/* */
/*         var modal = $(this)*/
/*         var button = $(event.relatedTarget)*/
/*         var id = button.data('id')*/
/* */
/*         var inputId = $("#input-id-" + id).val()*/
/*         var inputName = $("#input-name-" + id).val()*/
/*         var inputScore = $("#input-score-" + id).val()*/
/*         var inputHeader = $("#input-header-" + id).val()*/
/*         var inputDescription = $("#input-description-" + id).val()*/
/*         var inputItem = $('#input-item-' + id).val();*/
/*         var inputCategory = $('#input-category-' + id).val();*/
/*         var inputSubcategory = $('#input-subcategory-' + id).val();*/
/*         var inputQuality = $('#input-quality-' + id).val();*/
/*         var inputOrigin = $('#input-origin-' + id).val();*/
/*         var inputColors = $('#input-colors-' + id).val();*/
/*         var inputPresentation = $('#input-presentation-' + id).val();*/
/*         var inputCharacteristics = $('#input-characteristics-' + id).val();*/
/*         var inputEspecifications = $('#input-especifications-' + id).val();*/
/*         var inputDimensions = $('#input-dimensions-' + id).val();*/
/*         var inputWeight = $('#input-weight-' + id).val();*/
/*         var inputMinimumOrder = $('#input-minimumOrder-' + id).val();*/
/*         var inputDateExpiry = $('#input-dateExpiry-' + id).val();*/
/*         var inputInvNumPieces = $('#input-inventory_numPieces-' + id).val();*/
/*         var inputBoutiqueMaxOrder = $('#input-boutique_maximumOrder-' + id).val();*/
/*         var inputPriceWithoutTax = $('#input-price_withoutTax-' + id).val();*/
/*         var inputPriceCurrency = $('#input-price_currency-' + id).val();*/
/*         var inputPriceSuggested = $('#input-price_suggested-' + id).val();*/
/*         var inputPriceTT = $('#input-price_tt-' + id).val();*/
/*         var inputPackingNumPieces = $('#input-packing_numPieces-' + id).val();*/
/*         var inputPackingEspecifications = $('#input-packing_especifications-' + id).val();*/
/*         var inputPackingHeight = $('#input-packing_height-' + id).val();*/
/*         var inputPackingWidth = $('#input-packing_width-' + id).val();*/
/*         var inputPackingLarge = $('#input-packing_large-' + id).val();*/
/*         var inputPackingWeight = $('#input-packing_weight-' + id).val();*/
/*         var inputPackingLogProductionTime = $('#input-logistics_productionTime-' + id).val();*/
/*         var inputLogFob = $('#input-logistics_fob-' + id).val();*/
/*         var inputLogFobDeliveryTime = $('#input-logistics_fobDeliveryTime-' + id).val();*/
/*         var inputSample = $('#input-sample-' + id).val();*/
/* */
/*         modal.find('.modal-title').text('Editar ' + inputName)*/
/*         modal.find('.modal-body #rowid').val(id)*/
/*         modal.find('.modal-body #pid').val(inputId)*/
/*         modal.find('.modal-body #pname').val(inputName)*/
/*         modal.find('.modal-body #pscore').val(inputScore)*/
/*         modal.find('.modal-body #pheader').val(inputHeader)*/
/*         modal.find('.modal-body #pdescription').val(inputDescription)*/
/*         modal.find('.modal-body #pitem').val(inputItem)*/
/*         modal.find('.modal-body #pcategory').val(inputCategory)*/
/*         modal.find('.modal-body #psubcat').val(inputSubcategory)*/
/*         modal.find('.modal-body #pquality').val(inputQuality)*/
/*         modal.find('.modal-body #pcolor').val(inputColors)*/
/*         modal.find('.modal-body #porigin').val(inputOrigin)*/
/*         modal.find('.modal-body #ppresentation').val(inputPresentation)*/
/*         modal.find('.modal-body #pcharacteristics').val(inputCharacteristics)*/
/*         modal.find('.modal-body #pspecifications').val(inputEspecifications)*/
/*         modal.find('.modal-body #pdimensions').val(inputDimensions)*/
/*         modal.find('.modal-body #pweight').val(inputWeight)*/
/*         modal.find('.modal-body #pminimumOrder').val(inputMinimumOrder)*/
/*         modal.find('.modal-body #pdateExpiry').val(inputDateExpiry)*/
/*         modal.find('.modal-body #pinvNumPieces').val(inputInvNumPieces)*/
/*         modal.find('.modal-body #pboutiqueMaxOrder').val(inputBoutiqueMaxOrder)*/
/*         modal.find('.modal-body #ppriceWithoutTax').val(inputPriceWithoutTax)*/
/*         modal.find('.modal-body #ppriceCurrency').val(inputPriceCurrency)*/
/*         modal.find('.modal-body #ppriceSuggested').val(inputPriceSuggested)*/
/*         modal.find('.modal-body #ppriceTT').val(inputPriceTT)*/
/*         modal.find('.modal-body #ppackingNumPieces').val(inputPackingNumPieces)*/
/*         modal.find('.modal-body #ppackingSpecifications').val(inputPackingEspecifications)*/
/*         modal.find('.modal-body #ppackingHeight').val(inputPackingHeight)*/
/*         modal.find('.modal-body #ppackingWidth').val(inputPackingWidth)*/
/*         modal.find('.modal-body #ppackingLarge').val(inputPackingLarge)*/
/*         modal.find('.modal-body #ppackingWeight').val(inputPackingWeight)*/
/*         modal.find('.modal-body #plogTimeProduction').val(inputPackingLogProductionTime)*/
/*         modal.find('.modal-body #plogFob').val(inputLogFob)*/
/*         modal.find('.modal-body #plogFobDeliveryTime').val(inputLogFobDeliveryTime)*/
/*         modal.find('.modal-body #psample').val(inputSample)*/
/*     })*/
/* */
/* */
/*     $('#editSupplier').on('click', function(){*/
/* */
/*         var btn = $(this);*/
/* */
/*         var id = $("#sId").val();*/
/*         var name = $('#sName').val();*/
/*         var legalName = $('#sLegalName').val();*/
/*         var customeId = $('#sCustomeId').val();*/
/*         var category = $('#sCategory').val();*/
/*         var description = $('#sDescription').val();*/
/*         var officeAddress = $('#sOfficeAddress').val();*/
/*         var warehouseAddress = $('#sWarehouseAddress').val();*/
/*         var designee = $('#sDesignee').val();*/
/*         var designeePhone = $('#sDesigneePhone').val();*/
/*         var designeeMobile = $('#sDesigneeMobile').val();*/
/*         var designeeEmail = $('#sDesigneeEmail').val();*/
/*         var manager = $('#sManager').val();*/
/*         var managerPhone = $('#sManagerPhone').val();*/
/*         var managerMobile = $('#sManagerMobile').val();*/
/*         var managerEmail = $('#sManagerEmail').val();*/
/* */
/*         $.ajax({*/
/*             url: '{{ base_url() }}dashboard/admin/supplier/action/edit',*/
/*             type: 'post',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             data: {*/
/*                 sId : id,*/
/*                 sName : name,*/
/*                 sLegalName : legalName,*/
/*                 sCustomeId : customeId,*/
/*                 sCategory : category,*/
/*                 sDescription : description,*/
/*                 sOfficeAddress : officeAddress,*/
/*                 sWarehouseAddress : warehouseAddress,*/
/*                 sDesignee : designee,*/
/*                 sDesigneePhone : designeePhone,*/
/*                 sDesigneeMobile : designeeMobile,*/
/*                 sDesigneeEmail : designeeEmail,*/
/*                 sManager : manager,*/
/*                 sManagerPhone : managerPhone,*/
/*                 sManagerMobile : managerMobile,*/
/*                 sManagerEmail : managerEmail*/
/*             },*/
/*             beforeSend: function() {*/
/*                 btn.prop('disabled', true)*/
/*             },*/
/*             success: function(response) {*/
/* */
/*                 if (response == '1') {*/
/*                     location.reload();*/
/*                 } else {*/
/*                     alert(response);*/
/*                     btn.prop('disabled', false);*/
/*                 }*/
/*             },*/
/*             error: function(xhr, textStatus) {*/
/*                 alert('Error: ' + textStatus + '. Si el error persiste comunicate con el administrador.');*/
/*                 btn.prop('disabled', false);*/
/*             }*/
/*         })*/
/*     })*/
/* */
/* </script>*/
/* {% endblock %}*/
