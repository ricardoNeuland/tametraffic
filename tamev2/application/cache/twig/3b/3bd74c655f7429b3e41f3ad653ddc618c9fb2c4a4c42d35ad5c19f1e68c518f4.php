<?php

/* dashboard/admin/index.html */
class __TwigTemplate_37f3b93f1bb928a9a3f6f101fa910e8a72de824f08a257af0a6b8763c3d11ed0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/index.html", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t <div class=\"container-fluid\">
         <div class=\"row\">
             <div class=\"col-sm-12\">
                 <span class=\"title-office-yellow\">USUARIOS</span>
             </div>
         </div>
     </div>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block body%}*/
/* 	 <div class="container-fluid">*/
/*          <div class="row">*/
/*              <div class="col-sm-12">*/
/*                  <span class="title-office-yellow">USUARIOS</span>*/
/*              </div>*/
/*          </div>*/
/*      </div>*/
/* {% endblock %}*/
