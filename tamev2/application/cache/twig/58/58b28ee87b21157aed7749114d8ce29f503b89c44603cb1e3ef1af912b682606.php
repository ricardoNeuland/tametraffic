<?php

/* shop/catalogo.html */
class __TwigTemplate_2a151b48583053517aec143e723e7136769946fad311fe1f27df93c6e378ea61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-shop.html", "shop/catalogo.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-shop.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "   <div id=\"boxContent\">
    <span id=\"btnSidenav\" onclick=\"openNav()\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></span>

      <!-- Content -->
      <div class=\"container-fluid\">
        <div class=\"row\">
          <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
            <div class=\"row\">
                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 productCert\">
                  <center>
                    <img class=\"iconSteps\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step1.png\" alt=\"step1\">
                    <img class=\"iconSteps\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step2.png\" alt=\"step2\">
                    <img id=\"iconStepTT\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/stepTT.png\" alt=\"stepTT\">
                    <img class=\"iconSteps\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step3.png\" alt=\"step3\">
                    <img class=\"iconSteps\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/step4.png\" alt=\"step4\">
                  </center>
                  <p>1,563 Productos certificados</p>
                </div>
            </div>
          <div class=\"col-sm-6 col-sm-offset-3 text-center\">
            <progress class=\"progress-search text-center\" id=\"progress\" value=\"0\" max=\"100\"></progress>
            <span class=\"label label-success\"></span>
          </div>  
            <main>
            <div class=\"row\">
              <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                <p class=\"titleGroups\">Fashion Trends 2017</p>
                <a href=\"catalogo/catalogoFull\"><p class=\"moreTitleGroups\">Ver más</p></a>
                <div id=\"FashionTrends\" class=\"carousel slide\" data-ride=\"carousel\">
                  <!-- indicators -->
                  <ol class=\"carousel-indicators\">
                    <li data-target=\"#FashionTrends\" data-slide-to=\"0\" class=\"active\"></li>
                    <li data-target=\"#FashionTrends\" data-slide-to=\"1\"></li>
                  </ol>
                  <!-- carrousel -->
                  <div class=\"carousel-inner\" role=\"listbox\">
                    <div class=\"item active\">
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselOne/slide1.png\" alt=\"step1\">
                              <div class=\"overlay\">
                                 <p>Statement mexicano</p>
                                 <p>
                                  Fue en un viaje a Hamburgo que a Ahmed le mostró el artista austriaco Michael Hacker un diseño de chaleco con un pez y la frase “México Is The Shit”. Mientras esto ocurría , Anuar le mandó un mensaje pues había tenido la misma idea de Hacker. Fue así como nació la idea de crear estas chamarras.
                                 </p>
                                 <a href=\"catalogo/catalogoProducto\"><p>Leer más</p></a>
                              </div>
                          </div>
                          <p>Statement mexicano</p>
                          <p>Chamarra MITS</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselOne/slide2.png\" alt=\"step1\">
                              <div class=\"overlay\">
                                 <p>Mad Max lo más chico</p>
                                 <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, aliquid, ducimus. Laboriosam adipisci reprehenderit, temporibus omnis sunt eos natus facere repellendus, recusandae cupiditate sint at asperiores fuga neque quisquam reiciendis.
                                 </p>
                                 <a href=\"catalogo/catalogoProducto\"><p>Leer más</p></a>
                              </div>
                          </div>
                          <p>Mad Max lo más chico</p>
                          <p>Brazalete Acero Boneface</p>
                        </div>
                      </div>
                    </div>
                    <div class=\"item\">
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselOne/slide1.png\" alt=\"step1\">
                              <div class=\"overlay\">
                                 <p>Statement mexicano</p>
                                 <p>
                                  Fue en un viaje a Hamburgo que a Ahmed le mostró el artista austriaco Michael Hacker un diseño de chaleco con un pez y la frase “México Is The Shit”. Mientras esto ocurría , Anuar le mandó un mensaje pues había tenido la misma idea de Hacker. Fue así como nació la idea de crear estas chamarras.
                                 </p>
                                 <a href=\"catalogo/catalogoProducto\"><p>Leer más</p></a>
                              </div>
                          </div>
                          <p>Statement mexicano</p>
                          <p>Chamarra MITS</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 89
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselOne/slide2.png\" alt=\"step1\">
                              <div class=\"overlay\">
                                 <p>Mad Max lo más chico</p>
                                 <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, aliquid, ducimus. Laboriosam adipisci reprehenderit, temporibus omnis sunt eos natus facere repellendus, recusandae cupiditate sint at asperiores fuga neque quisquam reiciendis.
                                 </p>
                                 <a href=\"catalogo/catalogoProducto\"><p>Leer más</p></a>
                              </div>
                          </div>
                          <p>Mad Max lo más chico</p>
                          <p>Brazalete Acero Boneface</p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <!-- controls -->
                  <a class=\"left carousel-control\" href=\"#FashionTrends\" role=\"button\" data-slide=\"prev\">
                    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                  </a>
                  <a class=\"right carousel-control\" href=\"#FashionTrends\" role=\"button\" data-slide=\"next\">
                    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span>
                  </a>
                </div>
              </div>
            </div>

            <div class=\"row topCarrousel\">
              <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                <p class=\"titleGroups\">Tecnología Hip</p>
                <a href=\"catalogo/catalogoFull\"><p class=\"moreTitleGroups\">Ver más</p></a>
                <div id=\"TecnologiaHip\" class=\"carousel slide\" data-ride=\"carousel\">
                <!-- indicators -->
                  <ol class=\"carousel-indicators\">
                    <li data-target=\"#TecnologiaHip\" data-slide-to=\"0\" class=\"active\"></li>
                    <li data-target=\"#TecnologiaHip\" data-slide-to=\"1\"></li>
                  </ol>
                  <!-- carrousel -->
                  <div class=\"carousel-inner\" role=\"listbox\">
                    <div class=\"item active\">
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselTwo/slide1.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Anki Kozmo</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 149
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 154
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogo/catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 166
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 171
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Anki Kozmo</p>
                          <p>Tecnología I Niños</p>
                          <p>\$ 43.80 USD I 100 pz min.</p>
                          <p>China Muestra Express Personalizable</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 183
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselTwo/slide2.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>XYZ Printing da Vinci Mini 3D Printer</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 198
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 203
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogo/catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 215
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 220
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>XYZ Printing da Vinci Mini 3D Printer</p>
                          <p>Impresión I Tecnología</p>
                          <p>\$ 218.24 EUR I 50 pz min.</p>
                          <p>Holanda Inventario</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 232
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselTwo/slide3.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Bocina Mass Fidelity Core</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 247
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 252
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogo/catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 264
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 269
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Bocina Mass Fidelity Core</p>
                          <p>Audio I Electronicos</p>
                          <p>\$ 725.72 MXN I 50 pz min.</p>
                          <p>China Personalizable</p>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- controls -->
                  <a class=\"left carousel-control\" href=\"#TecnologiaHip\" role=\"button\" data-slide=\"prev\">
                    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                  </a>
                  <a class=\"right carousel-control\" href=\"#TecnologiaHip\" role=\"button\" data-slide=\"next\">
                    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span>
                  </a>
                </div>
              </div>
            </div>

            <div class=\"row topCarrousel\">
              <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                <p class=\"titleGroups\">Decor</p>
                <a href=\"catalogo/catalogoFull\"><p class=\"moreTitleGroups\">Ver más</p></a>
                <div id=\"Decor\" class=\"carousel slide\" data-ride=\"carousel\">
                <!-- indicators -->
                  <ol class=\"carousel-indicators\">
                    <li data-target=\"#Decor\" data-slide-to=\"0\" class=\"active\"></li>
                    <li data-target=\"#Decor\" data-slide-to=\"1\"></li>
                  </ol>
                  <!-- carrousel -->
                  <div class=\"carousel-inner\" role=\"listbox\">
                    <div class=\"item active\">
                      <div class=\"row\">
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 312
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide1.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 327
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 332
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogo/catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 344
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 349
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Banca Orgánica Madera</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 4,190.00 MXN I 50 pz min.</p>
                          <p>México Personalizable</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 361
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide2.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 376
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 381
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogo/catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 393
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 398
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Mesa Geometrica de Acero</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 2,764.00 MXN I 10 pz min.</p>
                          <p>México Inventario</p>
                        </div>
                        <div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles\">
                          <div class=\"hovereffect\">
                              <img class=\"imgCarrouselOne\" src=\"";
        // line 410
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/carrouselThree/slide3.png\" alt=\"step1\">
                              <div class=\"overlay overlay-small\">
                                 <p>Banca Orgánica Madera</p>
                                 <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
                                 <p>
                                  Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
                                 </p>
                                 <a>
                                   <center>
                                     <div class=\"boxFavoritos\">
                                        <ul>
                                          <li>
                                            <center>¡Regístrate para guardar este producto!</center>
                                          </li>
                                          <li>
                                            <img class=\"iconFavoritos iconFavo\" src=\"";
        // line 425
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconFavoritas.png\" alt=\"iconFavoritas\">
                                            Favoritos
                                          </li>
                                          <hr>
                                          <li>
                                            <img class=\"iconFavoritos nuevaLista\" src=\"";
        // line 430
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/iconPlus.png\" alt=\"iconPlus\">
                                            Nueva lista
                                          </li>
                                          <hr>
                                          <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
                                        </ul>
                                     </div>
                                   </center>
                                 </a>
                                 <a href=\"catalogo/catalogoProducto\" class=\"read_more\"><p>Leer más</p></a>
                                 <a href=\"#\" class=\"p_comparar\">
                                   <p>
                                      <img class=\"icono-comparar\" src=\"";
        // line 442
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-comparar.png\" alt=\"icono-comparar.png\"> Comparar
                                   </p>
                                 </a>   
                                 <a>
                                   <p>
                                    <img class=\"icono-like\" src=\"";
        // line 447
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "public/assets/img/icono-like.png\" alt=\"icono-like.png\">
                                   </p>
                                 </a>                 
                              </div>
                          </div>
                          <p>Silla armada en 2 piezas madera</p>
                          <p>Muebles I Hogar</p>
                          <p>\$ 3,899.00 MXN I 80 pz min.</p>
                          <p>México</p>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- controls -->
                  <a class=\"left carousel-control\" href=\"#Decor\" role=\"button\" data-slide=\"prev\">
                    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                  </a>
                  <a class=\"right carousel-control\" href=\"#Decor\" role=\"button\" data-slide=\"next\">
                    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span>
                  </a>
                </div>
              </div>
            </div>              
            </main>
<!--             <div class=\"row search-products\">
              <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
                
              </div>
            </div>   -->        
          </div>
        </div>
      </div>
      <!-- End contend --> 

      <!-- Pre-Footer -->
      <div class=\"row anchoRow\">
        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
          <center>
            <div id=\"top-footer\">
              <p>
              Selecciona los productos que quieras para solicitar un pedido <br>Tame Traffic no es una tienda en linea
              </p>
              <a href=\"#\">¿Como se compra en Tame Traffic?</a><br>
              <a href=\"#\">¿Como selecciono productos?</a><br>
              <a href=\"#\">¿Como se hace un pedido?</a>
            </div>
          </center>
        </div>
      </div>

    </div>
    <!-- End box content -->
";
    }

    public function getTemplateName()
    {
        return "shop/catalogo.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  591 => 447,  583 => 442,  568 => 430,  560 => 425,  542 => 410,  527 => 398,  519 => 393,  504 => 381,  496 => 376,  478 => 361,  463 => 349,  455 => 344,  440 => 332,  432 => 327,  414 => 312,  368 => 269,  360 => 264,  345 => 252,  337 => 247,  319 => 232,  304 => 220,  296 => 215,  281 => 203,  273 => 198,  255 => 183,  240 => 171,  232 => 166,  217 => 154,  209 => 149,  191 => 134,  143 => 89,  126 => 75,  105 => 57,  88 => 43,  59 => 17,  55 => 16,  51 => 15,  47 => 14,  43 => 13,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-shop.html' %}*/
/* {% block content %}*/
/*    <div id="boxContent">*/
/*     <span id="btnSidenav" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>*/
/* */
/*       <!-- Content -->*/
/*       <div class="container-fluid">*/
/*         <div class="row">*/
/*           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*             <div class="row">*/
/*                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 productCert">*/
/*                   <center>*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step1.png" alt="step1">*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step2.png" alt="step2">*/
/*                     <img id="iconStepTT" src="{{ base_url() }}public/assets/img/stepTT.png" alt="stepTT">*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step3.png" alt="step3">*/
/*                     <img class="iconSteps" src="{{ base_url() }}public/assets/img/step4.png" alt="step4">*/
/*                   </center>*/
/*                   <p>1,563 Productos certificados</p>*/
/*                 </div>*/
/*             </div>*/
/*           <div class="col-sm-6 col-sm-offset-3 text-center">*/
/*             <progress class="progress-search text-center" id="progress" value="0" max="100"></progress>*/
/*             <span class="label label-success"></span>*/
/*           </div>  */
/*             <main>*/
/*             <div class="row">*/
/*               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                 <p class="titleGroups">Fashion Trends 2017</p>*/
/*                 <a href="catalogo/catalogoFull"><p class="moreTitleGroups">Ver más</p></a>*/
/*                 <div id="FashionTrends" class="carousel slide" data-ride="carousel">*/
/*                   <!-- indicators -->*/
/*                   <ol class="carousel-indicators">*/
/*                     <li data-target="#FashionTrends" data-slide-to="0" class="active"></li>*/
/*                     <li data-target="#FashionTrends" data-slide-to="1"></li>*/
/*                   </ol>*/
/*                   <!-- carrousel -->*/
/*                   <div class="carousel-inner" role="listbox">*/
/*                     <div class="item active">*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselOne/slide1.png" alt="step1">*/
/*                               <div class="overlay">*/
/*                                  <p>Statement mexicano</p>*/
/*                                  <p>*/
/*                                   Fue en un viaje a Hamburgo que a Ahmed le mostró el artista austriaco Michael Hacker un diseño de chaleco con un pez y la frase “México Is The Shit”. Mientras esto ocurría , Anuar le mandó un mensaje pues había tenido la misma idea de Hacker. Fue así como nació la idea de crear estas chamarras.*/
/*                                  </p>*/
/*                                  <a href="catalogo/catalogoProducto"><p>Leer más</p></a>*/
/*                               </div>*/
/*                           </div>*/
/*                           <p>Statement mexicano</p>*/
/*                           <p>Chamarra MITS</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselOne/slide2.png" alt="step1">*/
/*                               <div class="overlay">*/
/*                                  <p>Mad Max lo más chico</p>*/
/*                                  <p>*/
/*                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, aliquid, ducimus. Laboriosam adipisci reprehenderit, temporibus omnis sunt eos natus facere repellendus, recusandae cupiditate sint at asperiores fuga neque quisquam reiciendis.*/
/*                                  </p>*/
/*                                  <a href="catalogo/catalogoProducto"><p>Leer más</p></a>*/
/*                               </div>*/
/*                           </div>*/
/*                           <p>Mad Max lo más chico</p>*/
/*                           <p>Brazalete Acero Boneface</p>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                     <div class="item">*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselOne/slide1.png" alt="step1">*/
/*                               <div class="overlay">*/
/*                                  <p>Statement mexicano</p>*/
/*                                  <p>*/
/*                                   Fue en un viaje a Hamburgo que a Ahmed le mostró el artista austriaco Michael Hacker un diseño de chaleco con un pez y la frase “México Is The Shit”. Mientras esto ocurría , Anuar le mandó un mensaje pues había tenido la misma idea de Hacker. Fue así como nació la idea de crear estas chamarras.*/
/*                                  </p>*/
/*                                  <a href="catalogo/catalogoProducto"><p>Leer más</p></a>*/
/*                               </div>*/
/*                           </div>*/
/*                           <p>Statement mexicano</p>*/
/*                           <p>Chamarra MITS</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselOne/slide2.png" alt="step1">*/
/*                               <div class="overlay">*/
/*                                  <p>Mad Max lo más chico</p>*/
/*                                  <p>*/
/*                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, aliquid, ducimus. Laboriosam adipisci reprehenderit, temporibus omnis sunt eos natus facere repellendus, recusandae cupiditate sint at asperiores fuga neque quisquam reiciendis.*/
/*                                  </p>*/
/*                                  <a href="catalogo/catalogoProducto"><p>Leer más</p></a>*/
/*                               </div>*/
/*                           </div>*/
/*                           <p>Mad Max lo más chico</p>*/
/*                           <p>Brazalete Acero Boneface</p>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                     */
/*                   </div>*/
/*                   <!-- controls -->*/
/*                   <a class="left carousel-control" href="#FashionTrends" role="button" data-slide="prev">*/
/*                     <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Previous</span>*/
/*                   </a>*/
/*                   <a class="right carousel-control" href="#FashionTrends" role="button" data-slide="next">*/
/*                     <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Next</span>*/
/*                   </a>*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/* */
/*             <div class="row topCarrousel">*/
/*               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                 <p class="titleGroups">Tecnología Hip</p>*/
/*                 <a href="catalogo/catalogoFull"><p class="moreTitleGroups">Ver más</p></a>*/
/*                 <div id="TecnologiaHip" class="carousel slide" data-ride="carousel">*/
/*                 <!-- indicators -->*/
/*                   <ol class="carousel-indicators">*/
/*                     <li data-target="#TecnologiaHip" data-slide-to="0" class="active"></li>*/
/*                     <li data-target="#TecnologiaHip" data-slide-to="1"></li>*/
/*                   </ol>*/
/*                   <!-- carrousel -->*/
/*                   <div class="carousel-inner" role="listbox">*/
/*                     <div class="item active">*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselTwo/slide1.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Anki Kozmo</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogo/catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Anki Kozmo</p>*/
/*                           <p>Tecnología I Niños</p>*/
/*                           <p>$ 43.80 USD I 100 pz min.</p>*/
/*                           <p>China Muestra Express Personalizable</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselTwo/slide2.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>XYZ Printing da Vinci Mini 3D Printer</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogo/catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>XYZ Printing da Vinci Mini 3D Printer</p>*/
/*                           <p>Impresión I Tecnología</p>*/
/*                           <p>$ 218.24 EUR I 50 pz min.</p>*/
/*                           <p>Holanda Inventario</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselTwo/slide3.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Bocina Mass Fidelity Core</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogo/catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Bocina Mass Fidelity Core</p>*/
/*                           <p>Audio I Electronicos</p>*/
/*                           <p>$ 725.72 MXN I 50 pz min.</p>*/
/*                           <p>China Personalizable</p>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/* */
/*                   </div>*/
/*                   <!-- controls -->*/
/*                   <a class="left carousel-control" href="#TecnologiaHip" role="button" data-slide="prev">*/
/*                     <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Previous</span>*/
/*                   </a>*/
/*                   <a class="right carousel-control" href="#TecnologiaHip" role="button" data-slide="next">*/
/*                     <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Next</span>*/
/*                   </a>*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/* */
/*             <div class="row topCarrousel">*/
/*               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                 <p class="titleGroups">Decor</p>*/
/*                 <a href="catalogo/catalogoFull"><p class="moreTitleGroups">Ver más</p></a>*/
/*                 <div id="Decor" class="carousel slide" data-ride="carousel">*/
/*                 <!-- indicators -->*/
/*                   <ol class="carousel-indicators">*/
/*                     <li data-target="#Decor" data-slide-to="0" class="active"></li>*/
/*                     <li data-target="#Decor" data-slide-to="1"></li>*/
/*                   </ol>*/
/*                   <!-- carrousel -->*/
/*                   <div class="carousel-inner" role="listbox">*/
/*                     <div class="item active">*/
/*                       <div class="row">*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide1.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogo/catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Banca Orgánica Madera</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 4,190.00 MXN I 50 pz min.</p>*/
/*                           <p>México Personalizable</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide2.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogo/catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Mesa Geometrica de Acero</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 2,764.00 MXN I 10 pz min.</p>*/
/*                           <p>México Inventario</p>*/
/*                         </div>*/
/*                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">*/
/*                           <div class="hovereffect">*/
/*                               <img class="imgCarrouselOne" src="{{ base_url() }}public/assets/img/carrouselThree/slide3.png" alt="step1">*/
/*                               <div class="overlay overlay-small">*/
/*                                  <p>Banca Orgánica Madera</p>*/
/*                                  <p><strong>Excelente opción para estudiantes y principiantes</strong></p>*/
/*                                  <p>*/
/*                                   Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...*/
/*                                  </p>*/
/*                                  <a>*/
/*                                    <center>*/
/*                                      <div class="boxFavoritos">*/
/*                                         <ul>*/
/*                                           <li>*/
/*                                             <center>¡Regístrate para guardar este producto!</center>*/
/*                                           </li>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos iconFavo" src="{{ base_url() }}public/assets/img/iconFavoritas.png" alt="iconFavoritas">*/
/*                                             Favoritos*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li>*/
/*                                             <img class="iconFavoritos nuevaLista" src="{{ base_url() }}public/assets/img/iconPlus.png" alt="iconPlus">*/
/*                                             Nueva lista*/
/*                                           </li>*/
/*                                           <hr>*/
/*                                           <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>*/
/*                                         </ul>*/
/*                                      </div>*/
/*                                    </center>*/
/*                                  </a>*/
/*                                  <a href="catalogo/catalogoProducto" class="read_more"><p>Leer más</p></a>*/
/*                                  <a href="#" class="p_comparar">*/
/*                                    <p>*/
/*                                       <img class="icono-comparar" src="{{ base_url() }}public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar*/
/*                                    </p>*/
/*                                  </a>   */
/*                                  <a>*/
/*                                    <p>*/
/*                                     <img class="icono-like" src="{{ base_url() }}public/assets/img/icono-like.png" alt="icono-like.png">*/
/*                                    </p>*/
/*                                  </a>                 */
/*                               </div>*/
/*                           </div>*/
/*                           <p>Silla armada en 2 piezas madera</p>*/
/*                           <p>Muebles I Hogar</p>*/
/*                           <p>$ 3,899.00 MXN I 80 pz min.</p>*/
/*                           <p>México</p>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/* */
/*                   </div>*/
/*                   <!-- controls -->*/
/*                   <a class="left carousel-control" href="#Decor" role="button" data-slide="prev">*/
/*                     <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Previous</span>*/
/*                   </a>*/
/*                   <a class="right carousel-control" href="#Decor" role="button" data-slide="next">*/
/*                     <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*                     <span class="sr-only">Next</span>*/
/*                   </a>*/
/*                 </div>*/
/*               </div>*/
/*             </div>              */
/*             </main>*/
/* <!--             <div class="row search-products">*/
/*               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*                 */
/*               </div>*/
/*             </div>   -->        */
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <!-- End contend --> */
/* */
/*       <!-- Pre-Footer -->*/
/*       <div class="row anchoRow">*/
/*         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">*/
/*           <center>*/
/*             <div id="top-footer">*/
/*               <p>*/
/*               Selecciona los productos que quieras para solicitar un pedido <br>Tame Traffic no es una tienda en linea*/
/*               </p>*/
/*               <a href="#">¿Como se compra en Tame Traffic?</a><br>*/
/*               <a href="#">¿Como selecciono productos?</a><br>*/
/*               <a href="#">¿Como se hace un pedido?</a>*/
/*             </div>*/
/*           </center>*/
/*         </div>*/
/*       </div>*/
/* */
/*     </div>*/
/*     <!-- End box content -->*/
/* {% endblock %}*/
