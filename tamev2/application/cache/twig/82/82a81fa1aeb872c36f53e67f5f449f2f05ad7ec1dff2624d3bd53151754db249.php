<?php

/* dashboard/admin/catalog-preview.html */
class __TwigTemplate_a89ba2946c23f1e5917f801277f2d57b1f56193be2c29b39030e56c68f3a2798 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("common/base-admin.html", "dashboard/admin/catalog-preview.html", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "common/base-admin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-6\">
            <h3 class=\"title-dashboard\">Catálogo</h3>
        </div>
        <div class=\"col-sm-6 text-right\">
            <a href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "dashboard/admin/catalog/add\" class=\"btn btn-sm btn-success\">
                <i class=\"fa fa-plus-circle fa-right--10\"></i>
                Agregar
            </a>
        </div>
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <h4 class=\"margin-clear\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "name", array()), "html", null, true);
        echo "</h4>
                    <p class=\"text-muted\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "description", array()), "html", null, true);
        echo "</p>
                </div>
                <table class=\"table\">
                    <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Calificación</th>
                    </tr>
                    ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 29
            echo "                        <tr>
                            <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 5, array(), "array"), "html", null, true);
            echo "</td>
                            <td>
                                <img src=\"";
            // line 33
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "public/uploads/supplier/catalog/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supplier"]) ? $context["supplier"] : null), "directory", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 5, array(), "array"), "html", null, true);
            echo ".jpg\" alt=\"\" height=\"50\">
                            </td>
                            <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 2, array(), "array"), "html", null, true);
            echo "</td>
                            <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], 0, array(), "array"), "html", null, true);
            echo "</td>
                        </tr>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                </table>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "dashboard/admin/catalog-preview.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 39,  109 => 36,  105 => 35,  96 => 33,  91 => 31,  87 => 30,  84 => 29,  67 => 28,  54 => 18,  50 => 17,  39 => 9,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends 'common/base-admin.html' %}*/
/* {% block body%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-6">*/
/*             <h3 class="title-dashboard">Catálogo</h3>*/
/*         </div>*/
/*         <div class="col-sm-6 text-right">*/
/*             <a href="{{ base_url() }}dashboard/admin/catalog/add" class="btn btn-sm btn-success">*/
/*                 <i class="fa fa-plus-circle fa-right--10"></i>*/
/*                 Agregar*/
/*             </a>*/
/*         </div>*/
/*         <div class="col-sm-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <h4 class="margin-clear">{{ supplier.name }}</h4>*/
/*                     <p class="text-muted">{{ supplier.description }}</p>*/
/*                 </div>*/
/*                 <table class="table">*/
/*                     <tr>*/
/*                         <th>#</th>*/
/*                         <th>Item</th>*/
/*                         <th>Imagen</th>*/
/*                         <th>Nombre</th>*/
/*                         <th>Calificación</th>*/
/*                     </tr>*/
/*                     {% for product in products %}*/
/*                         <tr>*/
/*                             <td>{{ loop.index }}</td>*/
/*                             <td>{{ product[5] }}</td>*/
/*                             <td>*/
/*                                 <img src="{{ base_url() }}public/uploads/supplier/catalog/{{ supplier.directory }}/{{ product[5] }}.jpg" alt="" height="50">*/
/*                             </td>*/
/*                             <td>{{ product[2] }}</td>*/
/*                             <td>{{ product[0] }}</td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
