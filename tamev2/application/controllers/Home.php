<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{	//For used layouts
		$this->load->view('home-user');
		// For used twig
		//$this->twig->display("dashboard/admin/home-user");
	}

}
