<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogo extends CI_Controller {

	public function __construct(){

        parent::__construct();

        $this->load->database();
		
        $this->load->model("category_model", "category");		
		$this->load->model("subcategory_model", "subcategory");
		$this->load->model("Product_model", "product");

        //$this->load->helper('text');
        //$this->load->helper('info');		
	}

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{	
		$subcategories = $this->subcategory->findAll();
		$order = array();

		foreach($subcategories as $val){
			$str = (string) $val->cid;

			if (!array_key_exists($str, $order)) {
				$order[$str] = array();
			}
			array_push($order[$str],$val->sname);
		}

		$data['title'] = 'Catalogo :: Productos';
		$data['categorys'] = $this->category->findAll();
		$data['subcategories'] = $order;
		$data['qualities'] = $this->category->findQuality();
		$data['countries'] = $this->category->findCountries();

		$this->twig->display('shop/catalogo',$data);
		
	}

	public function action($action){
		switch($action){
			case 'search': 

				$groups = $this->input->post('groups');
				$categories = $this->input->post('categories');
				$qualities = $this->input->post('qualities');
				$countries = $this->input->post('origin');
				$coast = $this->input->post('coast');

				$prods = $this->product->findFrontFilters($groups,$categories,$qualities,$countries,$coast);
				//header("HTTP/1.0 404 Not Found");
				//var_dump($prods);
				echo json_encode($prods);
			break;
		}
	}

	public function catalogoFull()
	{	
		$data = ['title'=>'Catalogo :: Productos Completos'];
		$this->twig->display('shop/catalogo-full',$data);
	}

	public function catalogoProducto()
	{	
		$data = ['title'=>'Catalogo :: Producto'];
		$this->twig->display('shop/catalogo-producto',$data);
	}

	public function comparacionProducto()
	{	
		$data = ['title'=>'Catalogo :: Comparacion Producto'];
		$this->twig->display('shop/comparacion-producto',$data);
	}

	public function contenedorProducto()
	{	
		$data = ['title'=>'Catalogo :: Contenedor Producto'];
		$this->twig->display('shop/contenedor-producto',$data);
	}

	public function diseno()
	{	
		$data = ['title'=>'Catalogo :: diseno'];
		$this->twig->display('shop/diseno',$data);
	}

	public function empaque()
	{	
		$data = ['title'=>'Catalogo :: empaque'];
		$this->twig->display('shop/empaque',$data);
	}

	public function otros()
	{	
		$data = ['title'=>'Catalogo :: otros'];
		$this->twig->display('shop/otros',$data);
	}

	public function search(){

		var_dump($_POST);

		//echo json_encode($this->input->post('hola'));

	}

}