<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->model("supplier_model", "supplier");
        $this->load->model("product_model", "product");

        $this->load->helper('text');
        $this->load->helper('info');
    }
    
    public function edit($id) {

        $product = $this->product->find($id);

        $data = [];
        $data["page"]["title"] = "Dashboard :: Producto";
        $data["product"] = $product[0];

        $this->twig->display("dashboard/admin/product-edit", $data);
    }

    public function action($action)
    {
        switch ($action) {
            case 'edit':
                $this->edit_action();
                break;
            case 'add':
                $this->add_action();
                break;
            case 'editSupplier';
                $this->updateSupplier();
                break;
			case 'upload_img':
				$this->replacement();
				break;
			case 'new_upload_img':
				$this->aggre_img();
				break;	
			case 'del_img':
				$this->delete_img();
				break;
			case 'get_all_images':
				$this->gets_img();
				break;				
            default:
                break;
        }
    }
    private function gets_img() {
		echo "hola :)";
	}
    private function updateSupplier(){
        $data = [
            'name' => $_POST['p-name'],
            'legal_name' => $_POST['p-name'],
            'description' => $_POST['p-description'],
            'category' => $_POST['p-giro'],
            'office_address' => $_POST['p-oficina'],
            'office_warehouse' => $_POST['p-bodega'],
            'designee' => $_POST['p-asignadaname'],
            'designee_phone' => $_POST['p-asignadaoficina'],
            'designee_mobile' => $_POST['p-asignadamovil'],
            'designee_email' => $_POST['p-asignadaemail'],
            'manager' => $_POST['p-altoname'],
            'manager_phone' => $_POST['p-altooficina'],
            'manager_mobile' => $_POST['p-altomovil'],
            'manager_email' => $_POST['p-altoemail']
        ];

        if($this->supplier->update($data,7)){
            echo "Update correcto";
        }else{
            echo "Update falló";
        }
    }
	private function aggre_img()
	{			
		$CountImg = $this->conteoFiles("public/uploads/supplier/catalog/".$_POST['origen'],$_POST['prefijo']);
		//$nameImagen = $_POST['prefijo'].'('.$CountImg.')';
		$nameImagen = file_exists("public/uploads/supplier/catalog/".$_POST['origen']."/".$_POST['prefijo'].".jpg")?$_POST['prefijo'].'('.$CountImg.')':$_POST['prefijo'];
		if(move_uploaded_file($_FILES['imagen']['tmp_name'],"public/uploads/supplier/catalog/".$_POST['origen']."/".$nameImagen.".jpg"))
		{
			$data=[
					'status'=>1,
					'name'=>$nameImagen];
			echo json_encode($data);
		}
		else
		{
			echo 0;
		}		
	}
	function conteoFiles($ruta,$item)
	{
		//echo $item;
		// comprobamos si lo que nos pasan es un direcotrio
		if (is_dir($ruta))
		{
			// Abrimos el directorio y comprobamos que
			if ($aux = opendir($ruta))
			{
				$contador=0;
				$existe=0;
				while (($archivo = readdir($aux)) !== false)
				{
					if ($archivo!="." && $archivo!="..")
					{
					  $pos = strpos($archivo,$item );
					  if($pos !== FALSE){
						$existe++;
					  }
					  else{
					  }
					  $contador++;						  
					}
				}
				return $existe+1;
				closedir($aux);
			}
		}
		else
		{
		   return $result = 0;
		}
	}	
	private function delete_img()
	{
			$ruta  = "./public/uploads/supplier/catalog/".$_POST["origen"]."/".$_POST["imagen"];
			if(unlink($ruta)){
				echo 1;
			}
			else{
				echo 2;
			}			
	}		
	private function replacement()
	{
		if(isset($_FILES["imagen"]))
		{
			$file = $_FILES["imagen"];
			$nombre = $file["name"];
			$tipo = $file["type"];
			
			$ruta_provisional = $file["tmp_name"];
			$url = trim($_POST['url']);
			$mover  = "./public/uploads/supplier/catalog/".$url;
			if(unlink($mover."/".$_POST["ultimo"]))
			{
				if(move_uploaded_file($ruta_provisional,$mover."/".$_POST["ultimo"]))
				{
					echo 3;
				}
				else
				{
					echo 2;
				}
				
			}
			else
			{
				echo 1;
			}			
		}
		else{
			echo 0;
		}
		
	}
    private function edit_action()
    {
        $id = $this->input->post('pId');
        $product = [
            "score" => $this->input->post('pScore'),
            "name" => $this->input->post('pName'),
            "header" => $this->input->post('pHeader'),
            "description" => $this->input->post('pDescription'),
            "item" => $this->input->post('pItem'),
            "categoryA" => $this->input->post('pCategory'),
            "subcategoryA" => $this->input->post('pSubcategory'),
            "categoryB" => $this->input->post(''),
            "subcategoryB" => $this->input->post(''),
            "categoryC" => $this->input->post(''),
            "subcategoryC" => $this->input->post(''),
            "quality" => $this->input->post('pQuality'),
            "origin" => $this->input->post('pOrigin'),
            "origin2" => $this->input->post('pOrigin2'),
            "colors" => $this->input->post('pColors'),
            "presentation" => $this->input->post('pPresentation'),
            "characteristics" => $this->input->post('pCharacteristics'),
            "especifications" => $this->input->post('pSpecifications'),
            "dimensions" => $this->input->post('pDimensions'),
            "weight" => $this->input->post('´pWeight'),
            "minimumOrder" => $this->input->post('pMinimumOrder'),
            "dateExpiry" => $this->input->post('pDateExpiry'),
            "inventory_numPieces" => $this->input->post('pInvNumPieces'),
            "boutique_maximumOrder" => $this->input->post('pBoutiqueMaxOrder'),
            "price_withoutTax" => $this->input->post('pPriceWithoutTax'),
            "price_currency" => $this->input->post('pPriceCurrency'),
            "price_suggested" => $this->input->post('pPriceSuggested'),
            "price_tt" => $this->input->post('pPriceTT'),
            "packing_numPieces" => $this->input->post('pPackingNumPieces'),
            "packing_especifications" => $this->input->post('pPackingSpecifications'),
            "packing_height" => $this->input->post('pPackingHeight'),
            "packing_width" => $this->input->post('pPackingWidth'),
            "packing_large" => $this->input->post('pPackingLarge'),
            "packing_weight" => $this->input->post('pPackingWeight'),
            "logistics_productionTime" => $this->input->post('pLogProductionTime'),
            "logistics_fob" => $this->input->post('pLogFob'),
            "logistics_fobDeliveryTime" => $this->input->post('pLogFobDeliveryTime'),
            "sample" => $this->input->post('pSample'),
            "user_id" => 1
        ];

        $this->product->update($product, $id);

        print 1;
    }
    
    private function add_action() {
        if($this->input->post("pItem") != "" && $this->input->post("categoryA") != "" && $this->input->post("subcategoryA") != ""){
                $product = [
                    "score" => $this->input->post('pScore'),
                    "name" => $this->input->post('pName'),
                    "header" => $this->input->post('pHeader'),
                    "description" => $this->input->post('pDescription'),
                    "item" => $this->input->post('pItem'),
                    "categoryA" => $this->input->post('pCategory'),
                    "subcategoryA" => $this->input->post('pSubcategory'),
                    "categoryB" => $this->input->post(''),
                    "subcategoryB" => $this->input->post(''),
                    "categoryC" => $this->input->post(''),
                    "subcategoryC" => $this->input->post(''),
                    "quality" => $this->input->post('pQuality'),
                    "origin" => $this->input->post('pOrigin'),
                    "origin2" => $this->input->post('pOrigin2'),
                    "colors" => $this->input->post('pColors'),
                    "presentation" => $this->input->post('pPresentation'),
                    "characteristics" => $this->input->post('pCharacteristics'),
                    "especifications" => $this->input->post('pSpecifications'),
                    "dimensions" => $this->input->post('pDimensions'),
                    "weight" => $this->input->post('´pWeight'),
                    "minimumOrder" => $this->input->post('pMinimumOrder'),
                    "dateExpiry" => $this->input->post('pDateExpiry'),
                    "inventory_numPieces" => $this->input->post('pInvNumPieces'),
                    "boutique_maximumOrder" => $this->input->post('pBoutiqueMaxOrder'),
                    "price_withoutTax" => $this->input->post('pPriceWithoutTax'),
                    "price_currency" => $this->input->post('pPriceCurrency'),
                    "price_suggested" => $this->input->post('pPriceSuggested'),
                    "price_tt" => $this->input->post('pPriceTT'),
                    "packing_numPieces" => $this->input->post('pPackingNumPieces'),
                    "packing_especifications" => $this->input->post('pPackingSpecifications'),
                    "packing_height" => $this->input->post('pPackingHeight'),
                    "packing_width" => $this->input->post('pPackingWidth'),
                    "packing_large" => $this->input->post('pPackingLarge'),
                    "packing_weight" => $this->input->post('pPackingWeight'),
                    "logistics_productionTime" => $this->input->post('pLogProductionTime'),
                    "logistics_fob" => $this->input->post('pLogFob'),
                    "logistics_fobDeliveryTime" => $this->input->post('pLogFobDeliveryTime'),
                    "sample" => $this->input->post('pSample'),
                    "supplier_id" => $this->input->post('supplierid'),
                    "user_id" => 1
                ];

                $insertProduct = $this->product->save($product);

                if($insertProduct){
                    echo 1;
                }else{
                    echo 0;
                }
        }else{
            echo 0;
        }
        //print 1;
        //$product = $this->product->find($this->input->post('supplierid'));
        //echo 'public/uploads/supplier/catalog/'.$product[0]->s_directory
        ;
    }

    public function delete($id) {
        $this->product->delete($id);
        redirect("dashboard/admin/catalog");
    }

            public function tcpdf($id) {
                session_start();

                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);

                require_once APPPATH . 'third_party/pdf/tcpdf.php';                

                $pdf =  new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

                // pdf information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('TT');
                $pdf->SetTitle('Productos');
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);

                $product = $this->product->find($id);

                $data = [];
                $data["page"]["title"] = "Dashboard :: Producto";
                $data["product"] = $product[0];

                $colors = $product[0]->colors;
                $colorsExplode = explode(",", $colors);
                $colorsList = [];

                foreach ($colorsExplode as $k => $color) {

                    $color = trim($color);
                    
                    switch ($color) {
                        case "amarillo":
                            $colorsList[$k] = "#fdd835";
                            break;
                        case "rojo":
                            $colorsList[$k] = "#e53935";
                            break;
                        case "azul":
                            $colorsList[$k] = "#1e88e5";
                            break;
                        case "naranja":
                            $colorsList[$k] = "#fb8c00";
                            break;
                        case "morado":
                            $colorsList[$k] = "#673ab7";
                            break;
                        case "negro":
                            $colorsList[$k] = "#000";
                            break;
                        case "gris":
                            $colorsList[$k] = "#ccc";
                            break;
                    }
                }

                $gallery = [];
                $galleryCount = 0;

                while(true) {

                    //echo 'public/assets/uploads/supplier/catalog/'.$product[0]->s_directory.'/'.$product[0]->item.'('.($galleryCount + 1).').jpg';
                    if (file_exists('public/uploads/supplier/catalog/'.$product[0]->s_directory.'/'.$product[0]->item.'('.($galleryCount + 1).').jpg')) {
                        $gallery[$galleryCount]["name"] = $product[0]->item . "(" . ($galleryCount + 1) . ").jpg";
                        $gallery[$galleryCount]["num"] = $galleryCount + 1;
                    } else {
                        break;
                    }

                    $galleryCount++;
                }
    
        // function leer_archivos_y_directorios($ruta,$item)
        // {
        //     //echo $item;
        //     // comprobamos si lo que nos pasan es un direcotrio
        //     if (is_dir($ruta))
        //     {
        //         // Abrimos el directorio y comprobamos que
        //         if ($aux = opendir($ruta))
        //         {
        //             $contador=0;
        //             while (($archivo = readdir($aux)) !== false)
        //             {
        //                 if ($archivo!="." && $archivo!="..")
        //                 {
        //                   //echo $archivo;
        //                    $archivo."<br>";
        //                    $item."<br>";
        //                   $pos = strpos($archivo,$item );
        //                   if($pos !== FALSE){
        //                     $imagenes [$contador]= $archivo;
        //                   }
        //                   else{
        //                   }
        //                   $contador++;                        
        //                 }
        //                 else{
                            
        //                     $imagenes =false; 
        //                 }
        //             }
        //             return $imagenes;
        //             closedir($aux);
        //         }
        //     }
        //     else
        //     {
        //        return $result = 0;
        //     }
        // }
        //     $img_carpeta = leer_archivos_y_directorios("public/uploads/supplier/catalog/".$product[0]->s_directory,$product[0]->item);
    
                /*pruebas */
                $data["gallery"]= $gallery;
                $data["colors"] = $colorsList;
                // $data["getImages"] = $img_carpeta;

                $html = $this->twig->render('dashboard/admin/product-pdf', $data);

                // add new page
                $pdf->AddPage();
                $pdf->writeHTML($html, true, false, true, false, '');
                $pdf->lastPage();

                $pdf->Output('example_006.pdf', 'I');
                
                $this->load->view('template', $data, true);   
            }

}