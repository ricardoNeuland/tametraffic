<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends MY_AuthController {

    public function __construct() {
        parent::__construct();

        $this->load->database();

        $this->load->model("supplier_model", "supplier");
        $this->load->model("product_model", "product");

        $this->load->helper('text');
        $this->load->helper('info');
    }

    /**
     * Index Page for this controller.
     *
     */
    public function index() {
        $data = [];
        $data["page"]["title"] = "Dashboard :: Supplier";
		$data["categories"] = get_categories();
        $data["suppliers"] = $this->supplier->findAll();
        $data["suppliers_count"] = count($data["suppliers"]);

        $this->twig->display("dashboard/admin/supplier", $data);
    }

	public function search() {
		
		$category = ($this->input->get("category") != "") ? $this->input->get("category") : "";
		$subcategory = ($this->input->get("subcategory") != "") ? $this->input->get("subcategory") : "";
        $word = ($this->input->get("q") != "") ? $this->input->get("q") : "";
		
        $data = [];
		$data["page"]["title"] = "Dashboard :: Supplier";
		$data["category"] = $category;
		$data["categories"] = get_categories();
        if($word != null){
            $data["suppliers"] = $this->supplier->findbyKeyUp($word);
            
        }else{
            $data["suppliers"] = $this->supplier->findSupplier($category,$subcategory);
            
        }
        $data["suppliers_count"] = count($data["suppliers"]);

		$this->twig->display("dashboard/admin/supplier", $data);
	}

    public function subcategory($id) {
        $subcategories = get_subcategories($id);

        header('Content-Type: application/json');
        echo json_encode($subcategories);
    }	
    public function add() {
        $data = [];
        $data["page"]["title"] = "Dashboard :: Agregar catálogo";
        $data["error"]["validation"] = ($this->session->has_userdata("error.validation")) ? $this->session->userdata("error.validation") : "";

        $this->twig->display("dashboard/admin/supplier-add", $data);
    }

    public function products($id) {

        $supplier = $this->supplier->find($id);

        $word = ($this->input->get("q") != "") ? $this->input->get("q") : "";

        if($word != null){
            $products = $this->product->findAll("", "", "", $word);
            
        }else{
            $products = $this->product->findBySupplier($id);
            
        }

        $totalProducts = count($products);

        $totalSearch = $totalProducts > 0? $totalProducts: "No se ha encontrado ningún producto de esta busqueda.";

        $data = [];
        $data["page"]["title"] = "Dashboard :: Productos";
        $data["categories"] = get_categories();
        $data["idSupplier"] = $id;
        $data["supplier"] = $supplier;
        $data["products"] = $products;

        if($this->input->get("category") != ""){

        $category = 
        $subcategory = 

        $data["totalQ"] = $totalSearch;
        $data["category"] = $this->input->get("category");
            if($this->input->get("subcategory") != ""){
                $data["subcategory"] = $this->input->get("subcategory");
            }

        }


        $this->twig->display("dashboard/admin/supplier-products", $data);
    }


    public function upload() {

        $this->load->library('excel');
        $this->load->library('unzip');

        $directory = md5(uniqid());

        if (!mkdir('public/uploads/supplier/catalog/' . $directory)) {
            $this->session->set_flashdata('error.validation', 'Ocurrio un error en el servidor');
        }

        $file = $_FILES['excel']['tmp_name'];
        $zip = $_FILES['zip']['tmp_name'];

        $fileName = $_FILES['excel']['name'];
        $zipName = $_FILES['zip']['name'];

        $fileAllowed =  array('xlsx','cvs' ,'xls');
        $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);

        $zipAllowed =  array('zip');
        $zipExt = pathinfo($zipName, PATHINFO_EXTENSION);

        $validation = false;

        if (empty($file)) {

            $this->session->set_flashdata('error.validation', 'Debes elegir un archivo');
        }  else if(!in_array($fileExt, $fileAllowed) ) {

            $this->session->set_flashdata('error.validation', 'Extensión no valida, solo se permite un archivo excel (.xlsx)');
        } else if (empty($zip)) {

            $this->session->set_flashdata('error.validation', 'Debes elegir una carpeta de imagenes');

        } else if(!in_array($zipExt, $zipAllowed) ) {

            $this->session->set_flashdata('error.validation', 'Extensión no valida, solo se permite carpetas comprimidas en zip (.zip)');
        } else {

            $this->unzip->allow(array('css', 'js', 'png', 'gif', 'jpeg', 'jpg', 'tpl', 'html', 'swf'));
            $this->unzip->extract($zip, 'public/uploads/supplier/catalog/' . $directory, false);

            $objPHPExcel = PHPExcel_IOFactory::load($file);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = 2;
            $highestColumn = $sheet->getHighestColumn();

            $sheet1 = $objPHPExcel->getSheet(1);
            $highestRow1 = $sheet1->getHighestRow();
            $highestColumn1 = $sheet1->getHighestColumn();

            $productsSheet = [];
            $supplierSheet = [];

            $count = 0;

            for ($row = 2; $row <= $highestRow; $row++) {

                $supplierSheet = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                foreach ($supplierSheet[0] as $k => $v) {
                    if (count($v) == 0) continue;
                }
            }

            $countError = 0;

            $isEmpty = 0;
            $findEmpty = [];
            $findEmptyCount = 0;
            $validateData = [];

            for ($row = 3; $row <= $highestRow1; $row++) {

                $rowData = $sheet1->rangeToArray('A' . $row . ':' . $highestColumn1 . $row);

                $addData = [];
                $isEmpty = 0;

                foreach ($rowData[0] as $k => $v) {
                    //if (count($v) == 0) continue;

                    if ($k + 1 == 39) {
                        break;
                    }
                    //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
                    $addData[$k] = $v;

                }
                $productsSheet[$count] = $addData;
                $count++;
            }

            /*echo "<pre>";
            print_r($productsSheet);
            echo "</pre>";
            die();*/

                // calificacion [0]
                // imagen [1]
                // name [2]
                // item [5]
                // Supplier
                // name [0]
                // legalName [1]
                // idCustome [2]
                // category [4]
                // description[5]
                // officeAddress [6]
                // officeWarehous [7]
                // dessigner [8]
                // designer phone [9]
                // designer mobile [10]
                // designer email [11]
                // manager [12]
                // manager Phone [13]
                // manager Mobile [14]
                // manager email [15]

                $supplier = [
                    "idcustome" => $supplierSheet[0][2],
                    "name" => $supplierSheet[0][0],
                    "legal_name" => $supplierSheet[0][1],
                    "description" => $supplierSheet[0][5],
                    "category" => $supplierSheet[0][4],
                    "office_address" => $supplierSheet[0][6],
                    "office_warehouse" => $supplierSheet[0][7],
                    "designee" => $supplierSheet[0][8],
                    "designee_phone" => $supplierSheet[0][9],
                    "designee_mobile" => $supplierSheet[0][10],
                    "designee_email" => $supplierSheet[0][11],
                    "manager" => $supplierSheet[0][12],
                    "manager_phone" => $supplierSheet[0][13],
                    "manager_mobile" => $supplierSheet[0][14],
                    "manager_email" => $supplierSheet[0][15],
                    "directory" => $directory
                ];

                $this->supplier->save($supplier);

                // Calificación [0] DB
                // Foto [1] BY REFERENCE
                // Nombre del producto [2] DB
                // Encabezado [3]
                // Descripción [4]
                // Item [5]
                // CategoriaA [6]
                // SubcategoriaA [7]
                // CategoriaB [8]
                // SubcategoriaB [9]
                // CategoriaC [10]
                // SubcategoriaC [11]
                // Calidad [12]
                // Origen Producción [13]
                // Colores [14]
                // Presentación [15]
                // Caracteristicas [16]
                // Especificaciones [17]
                // Dimensiones [18]
                // Peso [19]
                // Pedido Cantidad minima [20]
                // Pedido Fecha de caducidad [21]
                // Inventario [22]
                // Boutique Cantidad Maxima [23]
                // Precio Sin IVA [24]
                // Precio Moneda [25]
                // Precio sugerido [26]
                // Precio TT [27]
                // Empaque Num Piezas [28]
                // Empaque Especidficaciones [29]
                // Empaque Alto [30]
                // Empaque Ancho [31]
                // Empaque Largo [32]
                // Empaque Peso [33]
                // Logistica tiempo de producción [34]
                // Logistica FOB [35]
                // Logistica FOB Tiempo entrega [36]
                // Logistica Muestra fisica [37]


                for ($i = 0; $i < count($productsSheet); $i++) {

                    $products = [
                        "score" => $products[$i]['data'][0],
                        "imagen" => $products[$i]['data'][1],                        
                        "name" => $products[$i]['data'][2],
                        "header" => $products[$i]['data'][3],
                        "description" => $products[$i]['data'][4],
                        "item" => $products[$i]['data'][5],
                        "categoryA" => $products[$i]['data'][6],
                        "subcategoryA" => $products[$i]['data'][7],
                        "categoryB" => $products[$i]['data'][8],
                        "subcategoryB" => $products[$i]['data'][9],
                        "categoryC" => $products[$i]['data'][10],
                        "subcategoryC" => $products[$i]['data'][11],
                        "quality" => $products[$i]['data'][12],
                        "origin" => $products[$i]['data'][13],
                        "colors" => $products[$i]['data'][14],
                        "presentation" => $products[$i]['data'][15],
                        "characteristics" => $products[$i]['data'][16],
                        "especifications" => $products[$i]['data'][17],
                        "dimensions" => $products[$i]['data'][18],
                        "weight" => $products[$i]['data'][19],
                        "minimumOrder" => $products[$i]['data'][20],
                        "dateExpiry" => $products[$i]['data'][21],
                        "inventory_numPieces" => $products[$i]['data'][22],
                        "boutique_maximumOrder" => $products[$i]['data'][23],
                        "price_withoutTax" => $products[$i]['data'][24],
                        "price_currency" => $products[$i]['data'][25],
                        "price_suggested" => $products[$i]['data'][36],
                        "price_tt" => $products[$i]['data'][27],
                        "packing_numPieces" => $products[$i]['data'][28],
                        "packing_especifications" => $products[$i]['data'][29],
                        "packing_height" => $products[$i]['data'][30],
                        "packing_width" => $products[$i]['data'][31],
                        "packing_large" => $products[$i]['data'][32],
                        "packing_weight" => $products[$i]['data'][33],
                        "logistics_productionTime" => $products[$i]['data'][34],
                        "logistics_fob" => $products[$i]['data'][35],
                        "logistics_fobDeliveryTime" => $products[$i]['data'][36],
                        "sample" => $products[$i]['data'][37],                        
                        "supplier_id" => $this->supplier->insertedId(),
                        "user_id" => 1
                    ];

                    $this->product->save($products);

                    $validation = true;

                }
        }

        if (!$validation) {
            redirect("dashboard/admin/supplier/add");
        } else {

            $data = [];
            $data["page"]["title"] = "Dashboard :: Agregar catálogo";
            $data["supplier"] = $supplier;
            $data["products"] = $productsSheet;
            $this->twig->display("dashboard/admin/supplier-preview", $data);
        }


    }

    public function upload_test() {

        $this->load->library('excel');
        $this->load->library('unzip');

        $directory = md5(uniqid());

        if (!mkdir('public/uploads/supplier/catalog/' . $directory)) {
            $this->session->set_flashdata('error.validation', 'Ocurrio un error en el servidor');
        }

        $file = $_FILES['excel']['tmp_name'];
        $zip = $_FILES['zip']['tmp_name'];

        $fileName = $_FILES['excel']['name'];
        $zipName = $_FILES['zip']['name'];

        $fileAllowed =  array('xlsx','cvs' ,'xls');
        $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);

        $zipAllowed =  array('zip');
        $zipExt = pathinfo($zipName, PATHINFO_EXTENSION);

        $validation = false;

        if (empty($file)) {

            $this->session->set_flashdata('error.validation', 'Debes elegir un archivo');
        }  else if(!in_array($fileExt, $fileAllowed) ) {

            $this->session->set_flashdata('error.validation', 'Extensión no valida, solo se permite un archivo excel (.xlsx)');
        } else if (empty($zip)) {

            $this->session->set_flashdata('error.validation', 'Debes elegir una carpeta de imagenes');

        } else if(!in_array($zipExt, $zipAllowed) ) {

            $this->session->set_flashdata('error.validation', 'Extensión no valida, solo se permite carpetas comprimidas en zip (.zip)');
        } else {

            $this->unzip->allow(array('css', 'js', 'png', 'gif', 'jpeg', 'jpg', 'tpl', 'html', 'swf'));
            $this->unzip->extract($zip, 'public/uploads/supplier/catalog/' . $directory, false);

            $objPHPExcel = PHPExcel_IOFactory::load($file);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = 2;
            $highestColumn = $sheet->getHighestColumn();

            $sheet1 = $objPHPExcel->getSheet(1);
            $highestRow1 = $sheet1->getHighestRow();
            $highestColumn1 = $sheet1->getHighestColumn();

            $productsSheet = [];
            $supplierSheet = [];

            $count = 0;

            

            for ($row = 2; $row <= $highestRow; $row++) {

                $supplierSheet = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                foreach ($supplierSheet[0] as $k => $v) {
                    if (count($v) == 0) continue;
                }
            }

            /*$countError = 0;

            $findEmpty = [];
            $findEmptyCount = 0;
            $validateData = [];

            for ($row = 3; $row <= $highestRow1; $row++) {

                $rowData = $sheet1->rangeToArray('A' . $row . ':' . $highestColumn1 . $row);

                $addData = [];
                $isEmpty = 0;



                foreach ($rowData[0] as $k => $v) {
                    //if (count($v) == 0) continue;

                    if ($k + 1 == 39) {
                        break;
                    }

                    if ($v == '') {
                        continue;
                    }

                    //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
                    $addData[$k] = $v;

                }

                $productsSheet[$count] = $addData;
                $count++;
            }*/

            /*echo "<pre>";
            print_r($productsSheet);
            echo "</pre>";
            die();*/

            $products = [];
            $countProducts = 0;

            foreach ($sheet1->getRowIterator(3, $highestRow1) as $row) {

                if (!$this->isRowEmpty($row)) {

                    $countCell = 0;
                    $countCellEmpty = 0;

                    foreach ($row->getCellIterator('A', 'AL') as $i => $cell) {

                        if ($cell->getValue() == '') {
                            $dataError[$countCellEmpty] = $countCell;
                            $countCellEmpty++;
                        }

                        $dataCell[$countCell] = $cell->getValue();
                        $countCell++;
                    }

                    $products[$countProducts]['errors'] = $dataError;
                    $products[$countProducts]['data'] = $dataCell;
                    $countProducts++;
                }
            }

            // calificacion [0]
            // imagen [1]
            // name [2]
            // item [5]
            // Supplier
            // name [0]
            // legalName [1]
            // idCustome [2]
            // category [4]
            // description[5]
            // officeAddress [6]
            // officeWarehous [7]
            // dessigner [8]
            // designer phone [9]
            // designer mobile [10]
            // designer email [11]
            // manager [12]
            // manager Phone [13]
            // manager Mobile [14]
            // manager email [15]

            $supplier = [
                "idcustome" => $supplierSheet[0][2],
                "name" => $supplierSheet[0][0],
                "legal_name" => $supplierSheet[0][1],
                "description" => $supplierSheet[0][5],
                "category" => $supplierSheet[0][4],
                "office_address" => $supplierSheet[0][6],
                "office_warehouse" => $supplierSheet[0][7],
                "designee" => $supplierSheet[0][8],
                "designee_phone" => $supplierSheet[0][9],
                "designee_mobile" => $supplierSheet[0][10],
                "designee_email" => $supplierSheet[0][11],
                "manager" => $supplierSheet[0][12],
                "manager_phone" => $supplierSheet[0][13],
                "manager_mobile" => $supplierSheet[0][14],
                "manager_email" => $supplierSheet[0][15],
                "directory" => $directory
            ];

            $this->supplier->save($supplier);

            for ($i = 0; $i < count($products); $i++) {

                $productsArray[$i]['errors'] = $products[$i]['errors'];
                $productsArray[$i]['data'] = [
                    "score" => $products[$i]['data'][0],
                    "name" => $products[$i]['data'][2],
                    "header" => $products[$i]['data'][3],
                    "description" => $products[$i]['data'][4],
                    "item" => $products[$i]['data'][5],
                    "categoryA" => $products[$i]['data'][6],
                    "subcategoryA" => $products[$i]['data'][7],
                    "quality" => $products[$i]['data'][8],
                    "origin" => $products[$i]['data'][9],
                    "colors" => $products[$i]['data'][10],
                    "presentation" => $products[$i]['data'][11],
                    "characteristics" => $products[$i]['data'][12],
                    "especifications" => $products[$i]['data'][13],
                    "dimensions" => $products[$i]['data'][14],
                    "weight" => $products[$i]['data'][15],
                    "minimumOrder" => $products[$i]['data'][16],
                    "dateExpiry" => $products[$i]['data'][17],
                    "inventory_numPieces" => $products[$i]['data'][18],
                    "boutique_maximumOrder" => $products[$i]['data'][19],
                    "price_withoutTax" => $products[$i]['data'][20],
                    "price_currency" => $products[$i]['data'][21],
                    "price_suggested" => $products[$i]['data'][22],
                    "price_tt" => $products[$i]['data'][23],
                    "packing_numPieces" => $products[$i]['data'][24],
                    "packing_especifications" => $products[$i]['data'][25],
                    "packing_height" => $products[$i]['data'][26],
                    "packing_width" => $products[$i]['data'][27],
                    "packing_large" => $products[$i]['data'][28],
                    "packing_weight" => $products[$i]['data'][29],
                    "logistics_productionTime" => $products[$i]['data'][30],
                    "logistics_fob" => $products[$i]['data'][31],
                    "logistics_fobDeliveryTime" => $products[$i]['data'][32],
                    "sample" => $products[$i]['data'][33],
                    "origin2" => $products[$i]['data'][34],
                    "supplier_id" => $this->supplier->insertedId(),
                    "user_id" => 1
                ];

                $this->product->save($productsArray[$i]['data']);

                $validation = true;

            }
        }

        if (!$validation) {
            redirect("dashboard/admin/supplier/add");
        } else {
            redirect("dashboard/admin/supplier/preview/".$this->supplier->insertedId());
        }
    }

    public function preview($id)
    {
        $supplier = $this->supplier->find($id);
        $products = $this->product->findBySupplier($id);

        $data = [];
        $data["page"]["title"] = "Dashboard :: Previo";
        $data["supplier"] = $supplier;
        $data["products"] = $products;

        $this->twig->display("dashboard/admin/supplier-preview-test", $data);
    }

    public function action($action)
    {
        switch ($action) {
            case 'add':
                $this->new_supplier();
                break;
            case 'edit':
                $this->action_edit();
                break;
            default:
                break;
        }
    }

    private function new_supplier()
    {
        $supplier = [
            "idcustome" => $this->input->post(''),
            "name" => $this->input->post(''),
            "legal_name" => $this->input->post(''),
            "description" => $this->input->post(''),
            "category" => $this->input->post(''),
            "office_address" => $this->input->post(''),
            "office_warehouse" => $this->input->post(''),
            "designee" => $this->input->post(''),
            "designee_phone" => $this->input->post(''),
            "designee_mobile" => $this->input->post(''),
            "designee_email" => $this->input->post(''),
            "manager" => $this->input->post(''),
            "manager_phone" => $this->input->post(''),
            "manager_mobile" => $this->input->post(''),
            "manager_email" => $this->input->post(''),
            "directory" => ''
        ];

        $this->supplier->save($supplier);
    }

    private function action_edit()
    {
        $id = $this->input->post('sId');

        $supplier = [
            "idcustome" => $this->input->post('sCustomeId'),
            "name" => $this->input->post('sName'),
            "legal_name" => $this->input->post('sLegalName'),
            "description" => $this->input->post('sDescription'),
            "category" => $this->input->post('sCategory'),
            "office_address" => $this->input->post('sOfficeAddress'),
            "office_warehouse" => $this->input->post('sWarehouseAddress'),
            "designee" => $this->input->post('sDesignee'),
            "designee_phone" => $this->input->post('sDesigneePhone'),
            "designee_mobile" => $this->input->post('sDesigneeMobile'),
            "designee_email" => $this->input->post('sDesigneeEmail'),
            "manager" => $this->input->post('sManager'),
            "manager_phone" => $this->input->post('sManagerPhone'),
            "manager_mobile" => $this->input->post('sManagerMobile'),
            "manager_email" => $this->input->post('sManagerEmail')
        ];

        $this->supplier->update($supplier, $id);

        print 1;
    }

    public function delete($id) {
        
        $supplier = $this->supplier->find($id);
        $this->supplier->delete($id);

        function delete_files($target) {
            if(is_dir($target)){
                $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

                foreach( $files as $file )
                {
                    delete_files( $file );
                }

                rmdir( $target );
            } elseif(is_file($target)) {
                unlink( $target );
            }
        }

        //rmdir("public/uploads/supplier/catalog/" . $supplier->directory. "/");

        delete_files("public/uploads/supplier/catalog/" . $supplier->directory. "/");

        redirect("dashboard/admin/supplier");

    }

    private function isRowEmpty($row)
    {
        $count = 1;

        foreach ($row->getCellIterator() as $cell) {
          
            $value = trim($cell->getValue());
          
            if ($value) {
                return false;
            }
        }

        return true;
    }

}
