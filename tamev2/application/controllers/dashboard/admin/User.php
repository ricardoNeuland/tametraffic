<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_AuthController {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     *
     */
    public function index() {
        $data = [];
        $data["page"]["title"] = "Dashboard :: Inicio";
        $this->twig->display("dashboard/admin/index", $data);
    }

    public function login() {
        $data = [];
        $data["page"]["title"] = "Areá de administración";
        $this->twig->display("dashboard/admin/login", $data);
    }

    public function login_action() {
        
        
        
        redirect("/dashboard/admin/home");
    }

}
