<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_AuthController {

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
		$data = [];
		$data["page"]["title"] = "Dashboard :: Inicio";
		$this->twig->display("dashboard/admin/index", $data);
	}
}
