<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();

        $this->load->model("supplier_model", "supplier");
        $this->load->model("product_model", "product");
		
        $this->load->model("category_model", "category");		
		$this->load->model("subcategory_model", "subcategory");

        $this->load->helper('text');
        $this->load->helper('info');
    }

    /**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
		$data = [];
		$data["page"]["title"] = "Dashboard :: Catálogo";
		$page = ($this->input->get("page")!='')? $this->input->get("page") : 1;
		$contador = ($this->input->get("page")!='')? $page+1 : 1;
		$data["contador"]= $contador;
        $data["products"] = $this->product->findByLimitProduct($page);
		$data["products_total_search"] = $this->product->findAll();
        $data["products_count"] = count($data["products_total_search"]);

		$data["subcategories"] = $this->subcategory->findAll();;
		$data["subcategories_count"] = count($data["subcategories"]);
		
        $data["categories"] = $this->category->findAll();
        $data["categories_count"] = count($data["categories"]);		
		
        $data["qualities"] = get_qualities();

		$this->twig->display("dashboard/admin/catalog", $data);
	}
	
	/**
	 * Index Page for this controller.
	 *
	 */
	public function search() {
		
		$category = ($this->input->get("category") != "") ? $this->input->get("category") : "";
		$subcategory = ($this->input->get("subcategory") != "") ? $this->input->get("subcategory") : "";
		$quality = ($this->input->get("quality") != "") ? $this->input->get("quality") : "";
		$general = ($this->input->get("q") != "") ? $this->input->get("q") : "";
		
		$data = [];
		$data["page"]["title"] = "Dashboard :: Catálogo";

        $data["products"] = $this->product->findAll($category, $subcategory, $quality, $general);
        $data["products_count"] = count($data["products"]);

		$data["subcategories"] = $this->subcategory->findAll();;
		$data["subcategories_count"] = count($data["subcategories"]);
		
        $data["categories"] = $this->category->findAll();
        $data["categories_count"] = count($data["categories"]);	
        
        $data["qualities"] = get_qualities();
        
        $data["category"] = $category;
        $data["subcategory"] = $subcategory;
        $data["quality"] = $quality;

		$this->twig->display("dashboard/admin/catalog", $data);
	}
	
	public function add() {
		$data = [];
		$data["page"]["title"] = "Dashboard :: Agregar catálogo";
		$this->twig->display("dashboard/admin/catalog-add", $data);
	}
    public function product($id) {

        $product = $this->product->find($id);

        $data = [];
        $data["page"]["title"] = "Dashboard :: Producto";
        $data["product"] = $product[0];

        $colors = $product[0]->colors;
        $colorsExplode = explode(",", $colors);
        $colorsList = [];

        foreach ($colorsExplode as $k => $color) {

			$color = trim($color);
			
            switch ($color) {
                case "amarillo":
                    $colorsList[$k] = "#fdd835";
                    break;
                case "rojo":
                    $colorsList[$k] = "#e53935";
                    break;
                case "azul":
                    $colorsList[$k] = "#1e88e5";
                    break;
                case "naranja":
                    $colorsList[$k] = "#fb8c00";
                    break;
                case "morado":
                    $colorsList[$k] = "#673ab7";
                    break;
                case "negro":
                    $colorsList[$k] = "#000";
                    break;
                case "gris":
                    $colorsList[$k] = "#ccc";
                    break;
            }
        }

        $gallery = [];
        $galleryCount = 0;

        while(true) {

            //echo 'public/assets/uploads/supplier/catalog/'.$product[0]->s_directory.'/'.$product[0]->item.'('.($galleryCount + 1).').jpg';
            if (file_exists('public/uploads/supplier/catalog/'.$product[0]->s_directory.'/'.$product[0]->item.'('.($galleryCount + 1).').jpg')) {
                $gallery[$galleryCount]["name"] = $product[0]->item . "(" . ($galleryCount + 1) . ").jpg";
                $gallery[$galleryCount]["num"] = $galleryCount + 1;
            } else {
                break;
            }

            $galleryCount++;
        }
	
		
		$count=0;

		function leer_archivos_y_directorios($ruta,$item,$count)
		{

			//echo $item;
			// comprobamos si lo que nos pasan es un direcotrio
			if (is_dir($ruta))
			{
				// Abrimos el directorio y comprobamos que
				if ($aux = opendir($ruta))
				{
					
					$imgs = [];
					while (($archivo = readdir($aux)) !== false)
					{
						
						if ($archivo!="." && $archivo!="..")
						{
						  
						   $archivo."<br>";
						   $item."<br>";
						  $pos = strpos($archivo,$item );
						  if($pos === false ){
						  }
						  else{
								
								$imgs[] = $archivo;
								$count++;		
						  }					  
						}
						else{
						}
					}
					closedir($aux);
					return $imgs;
				}
			}
			else
			{
			   return $result = 0;
			}
		}
		function reading_Files($ruta,$item,$count)
		{

			//echo $item;
			// comprobamos si lo que nos pasan es un direcotrio
			if (is_dir($ruta))
			{
				// Abrimos el directorio y comprobamos que
				if ($aux = opendir($ruta))
				{
					
					$imgs = [];
					while (($archivo = readdir($aux)) !== false)
					{
						
						if ($archivo!="." && $archivo!="..")
						{
						  
						   $archivo."<br>";
						   $item."<br>";
						  $pos = strpos($archivo,$item );
						  if($pos === false ){
						  }
						  else{
												
								$imgs[]["name"] = $archivo;
								$count++;		
						  }					  
						}
						else{
						}
					}
					closedir($aux);
					return $imgs;
				}
			}
			else
			{
			   return $result = 0;
			}
		}
		
		$img_carpeta = leer_archivos_y_directorios("public/uploads/supplier/catalog/".$product[0]->s_directory,$product[0]->item,$count);
		$gall = reading_Files("public/uploads/supplier/catalog/".$product[0]->s_directory,$product[0]->item,$count);
		
		
		/**********************/
		
		//var_dump($img_carpeta);
		rsort($img_carpeta);
		rsort($gall);
		/***********************/
		
		
        $data["gallery"]= $gall;
        $data["colors"] = $colorsList;
		$data["getImages"] = $img_carpeta;

        $this->twig->display("dashboard/admin/catalog-product", $data);
    }

    public function subcategory($id) {
        $subcategories = get_subcategories($id);

        header('Content-Type: application/json');
        echo json_encode($subcategories);
    }
	
	public function upload() {
		$this->load->library('excel');
        $this->load->library('unzip');
        
        $directory = md5(uniqid());

        if (!mkdir('public/uploads/supplier/catalog/' . $directory)) {
            print "ERror";
        }
        
		$file = $_FILES['excel']['tmp_name'];
        $zip = $_FILES['zip']['tmp_name'];

        $this->unzip->allow(array('css', 'js', 'png', 'gif', 'jpeg', 'jpg', 'tpl', 'html', 'swf'));
        $this->unzip->extract($zip, 'public/uploads/supplier/catalog/' . $directory, false);

		$objPHPExcel = PHPExcel_IOFactory::load($file);
		
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
        $rowData = [];
		
		$sheet1 = $objPHPExcel->getSheet(1);
		$highestRow1 = $sheet1->getHighestRow();
		$highestColumn1 = $sheet1->getHighestColumn();
		$arrayData = [];

		//  Loop through each row of the worksheet in turn
		for ($row = 2; $row <= 2; $row++) {
			//  Read a row of data into an array

			//print 'A' . $row . ':' . $highestColumn . $row;
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

			foreach($rowData[0] as $k=>$v) {
				if (count($v) == 0) continue;
	        	//echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
	        }
		}

		//print_r($rowData);


        $count = 0;

        for ($row = 3; $row <= $highestRow1; $row++) {
            //  Read a row of data into an array


            //print 'A' . $row . ':' . $highestColumn1 . $row;
            $rowData1 = $sheet1->rangeToArray('A' . $row . ':' . $highestColumn1 . $row);

            $addData = [];

            foreach($rowData1[0] as $k=>$v) {
                //if (count($v) == 0) continue;
                if ($k + 1 == 43) break;
                //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
                $addData[$k] = $v;

            }

            $arrayData[$count] = $addData;
            $count++;
        }

        /*echo "<pre>";
        print_r($rowData);
        echo "</pre>";
        die();*/

        // calificacion [0]
        // imagen [1]
        // name [2]
        // item [5]

        // Supplier
        // name [0]
        // legalName [1]
        // idCustome [2]
        // category [4]
        // description[5]
        // officeAddress [6]
        // officeWarehous [7]
        // dessigner [8]
        // designer phone [9]
        // designer mobile [10]
        // designer email [11]
        // manager [12]
        // manager Phone [13]
        // manager Mobile [14]
        // manager email [15]

        $supplier = [
            "idcustome" => $rowData[0][2],
            "name" => $rowData[0][0],
            "legal_name" => $rowData[0][1],
            "description" => $rowData[0][5],
            "category" => $rowData[0][4],
            "office_address" => $rowData[0][6],
            "office_warehouse" => $rowData[0][7],
            "designee" => $rowData[0][8],
            "designee_phone" => $rowData[0][9],
            "designee_mobile" => $rowData[0][10],
            "designee_email" => $rowData[0][11],
            "manager" => $rowData[0][12],
            "manager_phone" => $rowData[0][13],
            "manager_mobile" => $rowData[0][14],
            "manager_email" => $rowData[0][15],
            "directory" => $directory
        ];

        $this->supplier->save($supplier);

        for ($i = 0; $i < count($arrayData); $i++) {

            $products = [
                "name" => $arrayData[$i][2],
                "item" => $arrayData[$i][5],
                "ranking" => $arrayData[$i][0],
                "supplier_idSupplier" => $this->supplier->insertedId(),
                "user_idUser" => 1
            ];

            $this->product->save($products);
        }

        $data = [];
        $data["page"]["title"] = "Dashboard :: Agregar catálogo";
        $data["supplier"] = $supplier;
        $data["products"] = $arrayData;
        $this->twig->display("dashboard/admin/catalog-preview", $data);
		
		/*  Loop through each row of the worksheet in turn
		for ($row = 2; $row <= $highestRow1; $row++) {
			//  Read a row of data into an array
   
			print 'A' . $row . ':' . $highestColumn1 . $row;
			$rowData = $sheet1->rangeToArray('A' . $row . ':' . $highestColumn1 . $row);
			
			foreach($rowData[0] as $k=>$v) {
				if (count($v) == 0) continue;
	        	echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
	        }
		}*/
		
		
	}
}
