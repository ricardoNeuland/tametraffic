<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("category_model", "category");
        $this->load->model("subcategory_model", "subcategory");
    }

    /**
     * Index Page for this controller.
     *
     */
    public function index() {
        $data = [];
        $data["page"]["title"] = "Dashboard :: Categoría";

        $data["categories"] = $this->category->findAll();
        $data["categories_count"] = count($data["categories"]);

        $this->twig->display("dashboard/admin/category-add", $data);
    }

    public function subcategory() {
        $data = [];
        $data["page"]["title"] = "Dashboard :: Categoría";

        $data["categories"] = $this->category->findAll();
        $data["subcategories"] = $this->subcategory->findAll();
        $data["subcategories_count"] = count($data["subcategories"]);

        $this->twig->display("dashboard/admin/subcategory-add", $data);
    }

    public function add() {
        $data = [
            "name" => $this->input->post("name"),
            "slug" => $this->input->post("slug")
        ];
		echo $affects = $this->category->save($data);		
		/*if( $affects != 0){
			echo 1;		
		}else{
			echo 0;		
		}	*/

    }

    public function add_subcategory() {
        $data = [
            "name" => $this->input->post("name-sub"),
            "slug" => $this->input->post("slug-sub"),
            "category_id" => $this->input->post("category")
        ];
		
		echo $affects = $this->subcategory->save($data);
		
        //$this->subcategory->save($data);

        //redirect("/dashboard/admin/category/subcategory");

    }

    public function update() {		
		$id_name = $this->input->post('id_name');
        $id_slug = $this->input->post('id_slug');
		if( $id_name == $id_slug ){	
		
			$data = [
					"name" => $this->input->post('name'),				
					"slug" => $this->input->post('slug')			
					];			
					
			$affects = $this->category->update($data, $id_name);			
			if( $affects == 1 ){
				echo 1;			
				}else{
					echo 2;	
				}								
		}else{
			echo 0;		
		}
    }

    public function update_subcategory() {
		
		$id_name = $this->input->post('id_name');
		$id_slug = $this->input->post('id_slug');
		$indice = $this->input->post('indice');
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$slug = $this->input->post('slug');
		if($id_name == $id_slug){
			$data = [
				"name" => $name,
				"slug" => $slug,
				"category_id" => $id,
			];			
			$affects = $this->subcategory->update($data, $indice);
			if( $affects == 1 ){
				echo 1;			
				}else{
					echo 2;	
				}								
		}else{
			echo 0;		
		}

		

        /*$id = $this->input->post('id');
		
        $data = [
            "name" => $this->input->post("editCatName"),
            "slug" => $this->input->post("editCatSlug"),
            "category_id" => $this->input->post('editCategory'),
        ];

        $this->subcategory->update($data, $id);

        redirect("/dashboard/admin/category/subcategory");*/

    }

    public function delete($id) {


        echo $this->category->delete($id);


    }

    public function delete_subcategory($id) {


        echo $this->subcategory->delete($id);

    }
}
