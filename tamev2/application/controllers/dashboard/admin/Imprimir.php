<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Imprimir extends Controller {

        function pdf_test()
        {
            parent::Controller();
        }

        // function tcpdf()
        // {
        //      $this->load->helper(array('dompdf', 'file'));
        //      // page info here, db calls, etc.     
        //     session_start();

        //     ini_set('display_errors', 1);
        //     ini_set('display_startup_errors', 1);
        //     error_reporting(E_ALL);

        //     require_once 'third_party/pdf/tcpdf.php';
        //     require_once 'helpers.php';

        //     $pdf =  new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        //     // pdf information
        //     $pdf->SetCreator(PDF_CREATOR);
        //     $pdf->SetAuthor('TT');
        //     $pdf->SetTitle('Productos');
        //     $pdf->setPrintHeader(false);
        //     $pdf->setPrintFooter(false);

        //     $data = [
        //         'title' => 'Catalogo de productos',
        //         'model' => htmlentities($_SESSION["result"]->cotizacion->sModelo, ENT_QUOTES, "UTF-8"),
        //         'version' => htmlentities($_SESSION["result"]->cotizacion->sVersion, ENT_QUOTES, "UTF-8"),
        //         'price' => htmlentities($_SESSION["result"]->cotizacion->sPrecioUnidad, ENT_QUOTES, "UTF-8"),
        //         'personType' =>  htmlentities($_SESSION["result"]->cotizacion->sTipoPersona, ENT_QUOTES, "UTF-8"),
        //         'plan' => htmlentities($_SESSION["result"]->cotizacion->sPlan, ENT_QUOTES, "UTF-8"),
        //         'deposit' => htmlentities($_SESSION["result"]->cotizacion->sEnganche, ENT_QUOTES, "UTF-8"),
        //         'monthlyPayment' => htmlentities(number_format(substr($_SESSION["result"]->cotizacion->mensualidad,0,9), 2), ENT_QUOTES, "UTF-8"),
        //         'info' => $_SESSION['result']->cotizacion->leyenda,
        //     ];

        //     $html = fetchTemplate('views/templates/pdf.php', $data);

        //     // add new page
        //     $pdf->AddPage();
        //     $pdf->writeHTML($html, true, false, true, false, '');
        //     $pdf->lastPage();

        //     $pdf->Output('example_006.pdf', 'I');
            
        //     $this->load->view('template', $data, true);   
        // }

            public function tcpdf($id) {

                $this->load->helper(array('dompdf', 'file'));
                 // page info here, db calls, etc.     
                session_start();

                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);

                require_once 'third_party/pdf/tcpdf.php';
                require_once 'helpers.php';

                $pdf =  new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                // pdf information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('TT');
                $pdf->SetTitle('Productos');
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);

                $product = $this->product->find($id);

                $data = [];
                $data["page"]["title"] = "Dashboard :: Producto";
                $data["product"] = $product[0];

                $colors = $product[0]->colors;
                $colorsExplode = explode(",", $colors);
                $colorsList = [];

                foreach ($colorsExplode as $k => $color) {

                    $color = trim($color);
                    
                    switch ($color) {
                        case "amarillo":
                            $colorsList[$k] = "#fdd835";
                            break;
                        case "rojo":
                            $colorsList[$k] = "#e53935";
                            break;
                        case "azul":
                            $colorsList[$k] = "#1e88e5";
                            break;
                        case "naranja":
                            $colorsList[$k] = "#fb8c00";
                            break;
                        case "morado":
                            $colorsList[$k] = "#673ab7";
                            break;
                        case "negro":
                            $colorsList[$k] = "#000";
                            break;
                        case "gris":
                            $colorsList[$k] = "#ccc";
                            break;
                    }
                }

                $gallery = [];
                $galleryCount = 0;

                while(true) {

                    //echo 'public/assets/uploads/supplier/catalog/'.$product[0]->s_directory.'/'.$product[0]->item.'('.($galleryCount + 1).').jpg';
                    if (file_exists('public/uploads/supplier/catalog/'.$product[0]->s_directory.'/'.$product[0]->item.'('.($galleryCount + 1).').jpg')) {
                        $gallery[$galleryCount]["name"] = $product[0]->item . "(" . ($galleryCount + 1) . ").jpg";
                        $gallery[$galleryCount]["num"] = $galleryCount + 1;
                    } else {
                        break;
                    }

                    $galleryCount++;
                }

                $html = fetchTemplate('views/templates/pdf.php', $data);

                // add new page
                $pdf->AddPage();
                $pdf->writeHTML($html, true, false, true, false, '');
                $pdf->lastPage();

                $pdf->Output('example_006.pdf', 'I');
                
                $this->load->view('template', $data, true);   
            }
    }