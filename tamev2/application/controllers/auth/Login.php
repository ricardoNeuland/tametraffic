<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This class is to send alert emails 
 * from the contacts of the hermer page
 * Author ricardo_date@hotmail.com
 * 
 */

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	
		$this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->model('Login_model','auth');

	}

	/**
     *
	 * Connection
	 * 
	 * @param $username
	 * @return object PDO
	 */

	public function index()
	{
		$data['title'] = 'Login Tametraffic';
		$data['keywords'] = 'Venta al mayoreo, venta, compra';
		$data['description'] = 'Sitio de comercio electronico venta al mayoreo en toda la republica mexicana';
		$data['error'] = $this->session->flashdata('message');

		$this->twig->display('auth/login',$data);
	}

	public function signRules(){

		$this->form_validation->set_rules('username', 
										  'Username', 
										  'required|valid_email',
									array('required' => 'Proporciona un %s.',
										  'valid_email'=>'Proporciona un %s válido.'));

		$this->form_validation->set_rules('password', 
										  'Password',
										  'required|min_length[8]',
								    array('required' => 'Proporciona un %s.',
										  'min_length'=>'%s minimo de 8 caracteres.'));

	}

	public function validate()
	{
		$this->signRules();
	    if ($this->form_validation->run() == FALSE)
	    {

	    	$this->session->set_flashdata('message', validation_errors());
	    	redirect('/auth/login','refresh');

	    }
	    else
	    {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$if_user_exist = $this->auth->checkUser($username,$password);
			
			if($if_user_exist != NULL){

				if(password_verify($password, $if_user_exist->password) ){

					$data = ['identifier' => $if_user_exist->idUser,
							 'user' => $if_user_exist->email,
							 'pass' => $if_user_exist->password,
							 'locked' => $if_user_exist->locked,
							 'level_access' => $if_user_exist->access,
							 'is_logged_in' => true
							];;


					switch ($if_user_exist->access) {
						case  0:
							//redirect('/catalogo','refresh');
							echo "Tu estas logeado como acceso nivel 0";
							break;
						case 1:
							//redirect('/dashboard/admin','refresh');
							echo "Tu estas logeado como acceso nivel 0";
							break;
						default:
							redirect('/auth/login','refresh');
							break;
					}
					

				}else{

			    	$this->session->set_flashdata('message', 'Password incorrecta, intenta nuevamente.');
			    	redirect('/auth/login','refresh');

				}

			}else{
				$this->session->set_flashdata('message', 'Username no esta registrado, intenta nuevamente.');
			    redirect('/auth/login','refresh');
			}
	    }

	}

}