<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function findAll($id = null) {
        $this->db->select("s.idSubcategory as sid, s.name as sname, s.slug as sslug, c.idCategory as cid,c.name as cname, c.slug as cslug");
        $this->db->from("subcategory as s");
        $this->db->join("category as c", "c.idCategory = s.category_id");
        $this->db->order_by("idSubcategory", "DESC");

        if ($id != null) {
            $this->db->where("category_id", $id);
        }

        $data = $this->db->get();

        return $data->result();
    }

    public function findBySupplier($id) {
        $this->db->select("s.directory, p.*");
        $this->db->from("product as p");
        $this->db->join("supplier as s", "p.supplie_id = s.idSupplier", "left");
        $this->db->where("idSupplier", $id);

        $data = $this->db->get();

        return $data->result();
    }

    public function save($data) {
        $this->db->insert("subcategory", $data);
		
		$last = $this->db->insert_id();
		if($last != 0 || $last != false ){
			return $last;
		}
		else{
			return false;
		} 		
    }

    public function update($data, $id) {
        $this->db->where('idSubcategory', $id);
        $this->db->update("subcategory", $data);
		
		return $this->db->affected_rows();		
    }

    public function delete($id) {
        $this->db->where('idSubcategory', $id);
        $this->db->delete("subcategory");				return $this->db->affected_rows();
    }

}
