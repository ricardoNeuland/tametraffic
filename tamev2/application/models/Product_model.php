<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function findFrontFilters($groups = null, $categories = null, $qualities = null, $countries = null, $coast = null){

     
        $result['data'] = $this->db->select("p.*, s.directory as s_directory, s.idcustome as s_custome, s.idSupplier as s_id, s.name as s_name")
                ->from("product as p")
                ->join("supplier as s", "s.idSupplier = p.supplier_id", "left")
                ->where_in("LOWER(p.categoryA)", $categories)->get()->result();
        
        $result['count'] = $this->db->select("p.*, s.directory as s_directory, s.idcustome as s_custome, s.idSupplier as s_id, s.name as s_name")
                ->from("product as p")
                ->join("supplier as s", "s.idSupplier = p.supplier_id", "left")
                ->where_in("LOWER(p.categoryA)", $categories)->count_all_results();

        return $result; //$data->result();

    }
    public function find($id) {
        // $this->db->select("p.*, s.directory as s_directory, s.idcustome as s_custome, c.name as c_name");
        $this->db->select("p.*, s.directory as s_directory, s.idcustome as s_custome, c.name as c_name, s.idSupplier as s_id, s.name as s_name");
        $this->db->from("product as p");
        $this->db->join("supplier as s", "s.idSupplier = p.supplier_id", "left");
        $this->db->join("category as c", "c.slug = p.categoryA", "left");
        $this->db->where("p.idProduct", $id);
        
        
        
        $data = $this->db->get();

        return $data->result();
    }

    public function findAll($category = null, $subcategory = null, $quality = null, $general = null) {
        $this->db->select("p.*, s.directory as directory, s.idcustome as supplier_customeid, s.idSupplier as id");
        $this->db->from("product as p");
        $this->db->join("supplier as s", "s.idSupplier = p.supplier_id");

		if ($category != null) {
	        $this->db->where("p.categoryA", $category);
        }
        
        if ($subcategory != null) {
	        $this->db->where("p.subcategoryA", $subcategory);
        }
        
        if ($quality != null) {
	        $this->db->where("p.quality", $quality);
        }
        
        if ($general != null) {		
	        $this->db->like("p.name", $general);
			$this->db->or_where("p.item", $general);
			$this->db->or_where("s.idcustome", $general);			
        }

        $data = $this->db->get();

        return $data->result();
    }

    public function findByLimitProduct($page) {	
        $this->db->select("p.*, s.directory as directory, s.idcustome as supplier_customeid");
        $this->db->from("product as p");
        $this->db->join("supplier as s", "s.idSupplier = p.supplier_id");
		if($page!=''){
			$start = $page*9;
			$this->db->limit(9, $start);
		}else{
			$this->db->limit(9,0);
		}		

        $data = $this->db->get();

        return $data->result();
    }	

    public function findBySupplier($id) {

        $this->db->select("s.directory, p.*");
        $this->db->from("product as p");

        if($this->input->get("category") != ""){

            $category = $this->input->get("category");

            $this->db->join("category as cat","cat.name = p.categoryA");
            $this->db->where("p.categoryA", $category);

            if($this->input->get("subcategory") != ""){

                $subcategory = $this->input->get("subcategory");

                $this->db->join("subcategory as sub","sub.slug = p.subcategoryA");
                $this->db->where("p.subcategoryA", $subcategory);

            }

        }
        $this->db->join("supplier as s", "p.supplier_id = s.idSupplier", "left");
        $this->db->where("idSupplier", $id);

        $data = $this->db->get();

        return $data->result();
    }
	

    public function save($data) {
        $this->db->insert("product", $data);

        return true;
    }

    public function update($data, $id) {
        $this->db->where("idProduct", $id);
        $this->db->update("product", $data);
    }

    public function delete($id) {
        $this->db->where('idProduct', $id);
        $this->db->delete("product");
        
    }

}
