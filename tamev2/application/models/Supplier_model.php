<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Supplier_model extends CI_Model {



    /**

     * @var int

     */

    public $insertedId = 0;



    public function __construct()

    {

        parent::__construct();

        $this->load->database();

    }



    public function find($id)

    {

        $this->db->select("s.*, COUNT(p.idProduct) as number_products");

        $this->db->from("supplier as s");

        $this->db->join("product as p", "s.idSupplier = p.supplier_id");

        $this->db->where("idSupplier", $id);



        $data = $this->db->get();



        return $data->row();

    }


    public function findSupplier($category = null, $subcategory = null)

    {


        $this->db->select("t3.*,             
                          (SELECT COUNT(p.idProduct) FROM product as p WHERE p.supplier_id = t3.idSupplier) as number_products,

                            (SELECT SUM(p.score) FROM product as p WHERE p.supplier_id = t3.idSupplier) as total_score");

        $this->db->from("category AS t1");

        $this->db->join("product as t2", "t1.slug = t2.categoryA");

        $this->db->join("supplier as t3", "t2.supplier_id = t3.idSupplier");

        if($category != null){
            $this->db->where("t1.name", $category);
            $this->db->or_where("t1.slug", $category);
        }

        if($subcategory != null){

            $this->db->join("subcategory as t4", "t1.idCategory = t4.category_id");
            $this->db->where("t4.name", $subcategory);
            $this->db->or_where("t4.slug", $subcategory);
        }
        

        $this->db->group_by("t3.idSupplier"); 



        $data = $this->db->get();



        return $data->result_array();

    }

   public function findbyKeyUp($q)

    {


        $this->db->select("
                            t1.*,
                            (SELECT COUNT(p.idProduct) FROM product as p WHERE p.supplier_id = t1.idSupplier) as number_products,
                            (SELECT SUM(p.score) FROM product as p WHERE p.supplier_id = t1.idSupplier) as total_score
                        ");

        $this->db->from("supplier as t1");

        $this->db->like("LOWER(t1.name)", $q,"both");

        $this->db->or_where("t1.idcustome", $q);
        



        $data = $this->db->get();



        return $data->result_array();

    }

    public function findAll()

    {

        $this->db->select("s.*, 

            (SELECT COUNT(p.idProduct) FROM product as p WHERE p.supplier_id = s.idSupplier) as number_products,

            (SELECT SUM(p.score) FROM product as p WHERE p.supplier_id = s.idSupplier) as total_score");

        $this->db->from("supplier as s");



        $data = $this->db->get();



        return $data->result();

    }



    public function save($data) {

        $this->db->insert('supplier', $data);

        $this->insertedId = $this->db->insert_id();

    }



    public function insertedId() {

        return $this->insertedId;

    }



    public function delete($id) {

        $this->db->where('idSupplier', $id);

        $delete = $this->db->delete("supplier");



        $this->db->where('supplier_id', $id);

        $this->db->delete("product");

    }



    public function update($data, $id) {

        $this->db->where('idSupplier', $id);

        $this->db->update("supplier", $data);

        return true;

    }



}