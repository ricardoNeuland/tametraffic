<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    /**
     * @var int
     */
    public $insertedId = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function exists()
    {
        $this->db->select("*");
        $this->db->where("");
    }

    public function find($id)
    {
        $this->db->select("s.*, COUNT(p.idProduct) as number_products");
        $this->db->from("supplier as s");
        $this->db->join("product as p", "s.idSupplier = p.supplier_id");
        $this->db->where("idSupplier", $id);

        $data = $this->db->get();

        return $data->row();
    }

    public function findAll()
    {
        $this->db->select("s.*, (SELECT COUNT(p.idProduct) FROM product as p WHERE p.supplier_id = s.idSupplier) as number_products");
        $this->db->from("supplier as s");

        $data = $this->db->get();

        return $data->result();
    }

    public function save($data) {
        $this->db->insert('supplier', $data);
        $this->insertedId = $this->db->insert_id();
    }

    public function insertedId() {
        return $this->insertedId;
    }

}