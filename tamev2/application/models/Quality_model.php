<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Quality_model extends CI_Model {

    private $table = "quality";

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function findAll() {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->order_by("position", "ASC");

        $data = $this->db->get();

        return $data->result();
    }

    public function save($data) {
        $this->db->insert($this->table, $data);
    }

}
