<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function findAll() {
        $this->db->select("*");
        $this->db->from("category");

        $data = $this->db->get();

        return $data->result();
    }

    public function findQuality() {

        $this->db->select("name");
        $this->db->from("quality");

        $data = $this->db->get();

        return $data->result();
    }

    public function findCountries() {
        
        $this->db->select("country");
        $this->db->from("country");

        $data = $this->db->get();

        return $data->result();
    }

    public function findBySupplier($id) {
        $this->db->select("s.directory, p.*");
        $this->db->from("product as p");
        $this->db->join("supplier as s", "p.supplier_id = s.idSupplier", "left");
        $this->db->where("idSupplier", $id);

        $data = $this->db->get();

        return $data->result();
    }

    public function save($data) {
        $this->db->insert("category", $data);
		$last = $this->db->insert_id();
		if($last != 0 || $last != false ){
			return $last;
		}
		else{
			return false;
		} 
    }

    public function update($data, $id) {
        $this->db->where('idCategory', $id);
        $this->db->update("category", $data);
		
		return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('idCategory', $id);
        $this->db->delete("category");				
		
		return $this->db->affected_rows();
    }
}
