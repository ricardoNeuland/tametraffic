<?php

defined('BASEPATH') OR exit('No direct script access allowed');



if ( ! function_exists('get_categories'))
{

    /**
     * Get Categores - Fetch all the categories from the data base
     *
     * @return	array   contain slug and name for the category
     */
    function get_categories()
    {
        $CI =& get_instance();
        $CI->load->model('category_model', 'category');
        return  $CI->category->findAll();
    }
}

if ( ! function_exists('get_subcategories'))
{

    /**
     * Get Subcategories - Fetch all the subcategories from the data base
     *
     * @return	array   containr slug, name and the parent category
     */
    function get_subcategories($id)
    {
        $CI =& get_instance();
        $CI->load->model('subcategory_model', 'subcategory');
        return  $CI->subcategory->findAll($id);
    }
}

if ( ! function_exists('get_qualities'))
{

    /**
     * Get Subcategories - Fetch all the subcategories from the data base
     *
     * @return	array   containr slug, name and the parent category
     */
    function get_qualities()
    {
        $CI =& get_instance();
        $CI->load->model('quality_model', 'quality');
        return  $CI->quality->findAll();
    }
}