<!DOCTYPE html>
<html lang="en" ng-app="root">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="http://yankuserver.com/tamev2/public/assets/img/favicon.ico" type="image/png">
    <title>Tame Traffic V2</title>
    <!-- Bootstrap -->
    <link href="http://yankuserver.com/tamev2/public/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-awesome -->
    <link href="http://yankuserver.com/tamev2/public/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS/SASS -->
    <link href="http://yankuserver.com/tamev2/public/assets/css/estilos.css" rel="stylesheet">
    <!-- Angular -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <!-- Fuente -->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,500,500i,700,700i" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-default" id="nobaNav">
      <a href="index.html"><img id="imgLogo" src="http://yankuserver.com/tamev2/public/assets/img/logo_tt.png" alt="logoTT"></a>
      <div class="container-fluid anchoContainer">
        <div class="navbar-header grosorNav">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <center>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a href="catalogo" class="nobaMenu">PRODUCTOS</a></li>
                  <li><a href="#" class="nobaMenu">NOSOTROS</a></li>
                  <li><a href="#" class="nobaMenu">BLOG</a></li>
                </ul>    
            </div>
        </center>
      </div>
    </nav>

    <section id="homeUser">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="datosHome">
                    <center>
                        <h2>Encuentra los productos que están cambiando el mundo.</h2>
                        <form> 
                            <input type="text" class="form-control" id="findHome" placeholder="¿Qué tienes en mente?">
                            <p>Sé parte de la nueva revolución industrial en México</p>
                            <button type="submit" class="btn btn-default" id="btnHome" onclick="contactar()">REGÍSTRATE</button>
                        </form>
                    </center>
                </div>
            </div>
        </div>
    </section>

    <!--Modals-->
     <center>
         <div  id="winContactar" class="modalEstilos text-justify"> 
            <p style="color: black; margin-top: 0px;">¿Para que voy a utilizar TT *?</p>
            <form action="#" method="POST">
              <input type="checkbox" name="1" value="Bike"> Ya tengo un producto pero lo quiero mejorar<br>
              <input type="checkbox" name="2" value="Car"> Necesito u producto para mi negocio<br>
              <input type="checkbox" name="3" value="Car"> Quiero hace un negocio alrededor de un producto<br>
              <input type="checkbox" name="4" value="Car"> Necesito u producto para mi negocio<br>
              <input type="checkbox" name="5" value="Car"> Quiero importar un producto pero no se como<br>
              <center><input type="submit" value="CONTINUAR" id="btnModal"></center>
            </form>
        </div>
     </center>
        
      <script>
          function contactar()
          {
              var ventana = document.getElementById("winContactar");
              ventana.style.marginTop = "100px";
              ventana.style.display = "block";
          }
      </script>
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://yankuserver.com/tamev2/public/assets/js/bootstrap.min.js"></script>
    <!-- operaciones -->
    <script src="http://yankuserver.com/tamev2/public/assets/js/operaciones.js"></script>
    <!-- modules -->
    <script src="http://yankuserver.com/tamev2/public/assets/js/modules/root.js"></script>
  </body>
</html>
