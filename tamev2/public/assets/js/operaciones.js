
$(document).ready(function(){

	$("#btnHome").click(function(){
	    $("#homeUser").css("background-image", "url('http://yankuserver.com/tamev2/public/assets/img/homeUserModal.jpg')");
	    $("#nobaNav").hide();
	    $("#datosHome").hide();
	    $("#miVentana").show();
	});

	$('.filters').hide();
	$('.progress-search').hide();
	$('.label-success').hide();
	//$('.contain-filt').hide();
	$('.submenu').hide();
	$('.submenu').perfectScrollbar('destroy');
	$('.submenu').perfectScrollbar();
	
	

	$('.big-l').on('click',function(){
		var data = $(this).data('special');
		if( data =='category' ){
			$(this).next().next('.filters').slideToggle('slow');
		}else{
			$(this).next('.filters').slideToggle('slow');
		}
		
		$('.var1').perfectScrollbar('destroy');
		$('.var1').perfectScrollbar();
	});

	$('.effect').on('click ', function(e) {
		e.preventDefault();
		$ele = $(this);
		$img = $(this).find('img');
		$src = $img.attr('src');
		$word = $(this).find('span').text();
		$type= $(this).parent().data('type');
		
		switch(e.type){
			case 'click':
				changeImage($ele,$img,$src,$word,$type);
			break;

		}
	});


	$('.sidenav').on('click','.close-tag',function(){
		console.log('eliminando tags');
		$word = $(this).parent().find('.tag').text();
		removeTags($word);
	});

	var filters = $('.var1');

	$('.subcategory').hover(
		function(){
			$('.itm').removeClass('hidden-tmp-none');
			$('.itm').addClass('hidden-tmp');
			
			var id = $(this).data('id');
			var name = $(this).data('name');
			var token_class = $('.sub-m-'+id);
			var contain = $(token_class);
				contain.removeClass('hidden-tmp');
				contain.addClass('hidden-tmp-none');
			$('.submenu').show();
			$('.name-sub').text(name);
		},function(){
		}
	);

	$('#boxContent').hover(
		function(){
			var sidenav = $('.sidenav').hasClass('sidenav-appear');
			if(sidenav){
				hideSubmenu();
			}else{ 
			}
			
		},function(){		}
	);

});

function hideSubmenu(){
	$('.itm').removeClass('hidden-tmp-none');
	$('.itm').addClass('hidden-tmp');
	$('.submenu').hide();	
}

function addTags($string,$type){
	$contain = $('.container-tags');
	$tag = `
			<div class="tags" data-type=${ $type }>
				<div class="close-tag">x</div>
				<div class="tag text-uppercase"> ${ $string }</div>
			</div>
			`;
	$contain.append($tag);
}

function progress(e){

	var progressBar = document.getElementById("progress");

	if(e.lengthComputable){
		
		progressBar.max = e.total;
		progressBar.value = e.loaded;
		
		var current = e.loaded / e.total; 
		var Percentage = current * 100;
		
		var percentReal = parseInt(Percentage) + ' %';
		
		if(Percentage >= 100)
		{
			console.log('Proceso completo');
			$('.label-success').text('Espera un momento');						
		}
		else{
			$('.label-success').text(Percentage);
			console.log(Percentage);
		}
	}  
 }
function displayProducts($product){

}
function searchProduct($type){

	var groups = [];
	var categories = [];
	var qualities = [];
	var origin = [];
	var coast = [];

	$('.tags').each(function(){
		var type = $(this).data('type');
		var value =  $(this).children('.tag').text().toLowerCase().trim();
		console.log(value);
		switch(type){
			case 'groups':
				groups.push(value);
			break;
			case 'categorys':
				categories.push(value);
			break;
			case 'quality':
				qualities.push(value);
			break;
			case 'country':
				origin.push(value)
			break;
			case 'coast':
				coast.push(value);
			break;
			case 'q':
				console.log('abcdefgh');
			break;
		}
	});


	$.ajax({
		method: "POST",
		url: "http://yankuserver.com/tamev2/catalogo/action/search",
		data: { groups: groups, categories : categories , qualities: qualities ,origins: origin, coast:coast  },
		xhr: function() {
				var myXhr = $.ajaxSettings.xhr();
				if(myXhr.upload){
					myXhr.upload.addEventListener('progress',progress, false);
				}
				return myXhr;
		},
		beforeSend: function() {
			$('.progress-search').show();
			$('main').hide();
			$('.label-success').show();
		},
	})
  .error(function( msg ) {
  	$('.progress-search').hide();
	swal(
	  'Ups',
	  'Lo sentimos intenta nuevamente!',
	  'error'
	)
  })
  .done(function( msg ) {
  	console.log( msg );
  	$('main').show();
  	$('.label-success').hide();
  	$('.progress-search').hide();

    var parse = JSON.parse(msg);
    console	.log(parse);
    $('main').empty();
    	var pagination = `
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <select name="" id="selectAgregados">
                    <option value="#">RECIÉN AGREGADOS</option>
                    <option value="#">HACE UN MES</option>
                    <option value="#">HACE UNA SEMANA</option>
                    <option value="#">HACE UN DÍA</option>
                  </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <nav aria-label="Page navigation" id="navPagination">
                    <ul class="pagination">
                      <li>
                        <a href="#" aria-label="Previous">
                          <span aria-hidden="true"><</span>
                        </a>
                      </li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li>
                        <a href="#" aria-label="Next">
                          <span aria-hidden="true">></span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                  <div class="form-group inputPagina">
                    <label for="">PÁGINA</label>
                    <input type="text" value="1">
                  </div>
                  <div class="form-group inputPagina">
                    <label for="">VER</label>
                    <input type="text" value="48">
                    <input type="text" value="96">
                  </div>
                </div>
            </div>
    	`;
    $('main').append(pagination);
    for(var x=0; x < parse.data.length; x++){
    	var prod = `
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 groupTitles">
			  <div class="hovereffect">
			      <img class="imgCarrouselOne" src="http://yankuserver.com/tamev2/public/assets/img/carrouselTwo/slide2.png" alt="step1">
			      <div class="overlay overlay-small">
			         <p>${ parse.data[x].name }</p>
			         <p><strong>Excelente opción para estudiantes y principiantes</strong></p>
			         <p>
			          Compatible con los productos VisiJet® M2 Projet2500 produce piezas robustas y versátiles de...
			         </p>
			         <a>
			           <center>
			             <div class="boxFavoritos">
			                <ul>
			                  <li>
			                    <center>¡Regístrate para guardar este producto!</center>
			                  </li>
			                  <li>
			                    <img class="iconFavoritos iconFavo" src="http://yankuserver.com/tamev2/public/assets/img/iconFavoritas.png" alt="iconFavoritas">
			                    Favoritos
			                  </li>
			                  <hr>
			                  <li>
			                    <img class="iconFavoritos nuevaLista" src="http://yankuserver.com/tamev2/public/assets/img/iconPlus.png" alt="iconPlus">
			                    Nueva lista
			                  </li>
			                  <hr>
			                  <li><center>Crea una lista para organizar tus <br>productos en grupos personalizados</center></li>
			                </ul>
			             </div>
			           </center>
			         </a>
			         <a href="catalogo/catalogoProducto" class="read_more"><p>Leer más</p></a>
			         <a href="#" class="p_comparar">
			           <p>
			              <img class="icono-comparar" src="http://yankuserver.com/tamev2/public/assets/img/icono-comparar.png" alt="icono-comparar.png"> Comparar
			           </p>
			         </a>   
			         <a>
			           <p>
			            <img class="icono-like" src="http://yankuserver.com/tamev2/public/assets/img/icono-like.png" alt="icono-like.png">
			           </p>
			         </a>                 
			      </div>
			  </div>
			  <p>${ parse.data[x].name }</p>
			  <p>${ parse.data[x].subcategoryA }  I ${ parse.data[x].categoryA }</p>
			  <p>$ ${ parse.data[x].price_suggested } ${ parse.data[x].price_currency } I ${ parse.data[x].minimumOrder } pz min.</p>
			  <p>${ parse.data[x].origin } Inventario</p>
			</div>
    				`;
    	$('main').append(prod);
    }

  });	

}

function pushElement($name,$value){
	$name.push($value);
}

function removeTags($word){
	$ele = $(".tags .tag:contains('"+$word+"')");
	$ele.parent().remove();
}

function changeImage($ele,$img,$src,$word,$type){
	var $diamant = 'http://yankuserver.com/tamev2/public/assets/img/';
	if($ele.hasClass('selected')){
		$ele.removeClass('selected');
		removeTags($word);
		$img.attr('src',$diamant+"diamant.png");
	}else{
		$ele.addClass('selected');
		addTags($word,$type);
		$img.attr('src',$diamant+"diamant2.png");
		searchProduct($type);
	}	
}

function oNav() {
	// Get width window
	var ancho_window = $(window).width();
	 $('#boxMenu').show();
  	if(ancho_window > 768){
  		// console.log('width desktop');
		document.getElementById("boxMenu").style.width = "250px";
		document.getElementById("boxContent").style.marginLeft = "250px";
	} else{
		// console.log('width movil');
		document.getElementById("boxMenu").style.width = "100%";
  		document.getElementById("boxContent").style.marginLeft = "100%";
	}
}

function cNav() {
  //$('#boxMenu').hide();
  document.getElementById("boxMenu").style.width = "0";
  document.getElementById("boxContent").style.marginLeft= "0";
}