
$(document).ready(function(){

	// Cantidad contenedor producto
	$(".plus", this).click(function() {
	    var item = $(this).parent().parent().find('.numbertype');
	    var selectBoxMinimo = $(this).parent().parent().parent().find('.pMinimo');
	    item.val(Number(item.val()) + 1);
	    if(item.val() >= 50){
			item.css('color', 'black');
			selectBoxMinimo.hide();
		}
	});
	$(".minus", this).click(function() {
		var item = $(this).parent().parent().find('.numbertype');
		var selectBoxMinimo = $(this).parent().parent().parent().find('.pMinimo');
		item.val(Number(item.val()) - 1); 
		if(item.val() < 50){
			item.css('color', 'red');
			selectBoxMinimo.show();
		}
	});

	// Replace click favorites contenedor Producto
	$('.iconFavII').click(function(){
		if($(this).attr('src') == 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritasB.png'){
			$(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas_h.png');
		}
		else {
			$(this).attr('src','http://yankuserver.com/tamev2/public/assets/img/iconFavoritasB.png');
		}
	});

	// Delete row content checkout
	$('.btnContenEliminar').click(function(){
		$(this).parent().parent().remove();
	});

	// Replace hover icons
    $('.p_comparar').hover(function(){
    	$(this).children().children().attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-comparar-h.png');
	 	$(this).children().css('color', '#05aa9a');
	 	$(this).children().css('transition', 'none');
    }, function(){
    	$(this).children().children().attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-comparar.png');
    	$(this).children().css('color', 'black');
    	$(this).children().css('transition', 'none');
    });

    $('.icono-like').hover(function(){
       $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-like-h.png');
    }, function(){
       $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-like.png');
    });

    $('.icono-like').click(function(){
    	$(this).parent().parent().parent().find('.boxFavoritos').fadeIn();
    	$(this).parent().parent().parent().find('.boxFavoritos').show();
	    $('.read_more, .p_comparar').hide();
	    $(this).css('margin-top', '80px');
	    $(this).parent().parent().parent().find('.iconFavo').click(function(){
	    	if($(this).attr('src') == 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas.png'){
			   $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas_h.png');
			} else {
			   $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas.png');
			}
	    });
	});

	$('.icono-like-navRight img').click(function(){
		$(this).parent().parent().parent().find('.boxFavoritos').fadeIn();
		$(this).parent().parent().parent().find('.boxFavoritos').show();
		$('.icono-comparar-navRight, .pdf-navRight').hide();
		$(this).parent().parent().parent().find('.iconFavo').click(function(){
	    	if($(this).attr('src') == 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas.png'){
			   $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas_h.png');
			} else {
			   $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas.png');
			}
	    });
	});

	$('.boxFavoritos').mouseleave(function(){
		$(this).parent().parent().parent().find('.boxFavoritos').fadeOut();
		$(this).parent().parent().parent().find('.boxFavoritos').hide();
		$('.read_more, .p_comparar, .icono-comparar-navRight, .pdf-navRight').fadeIn();
		$('.icono-like').css('margin-top', '-25px');
	});


	// Replace hover icons navRight
	$('.icono-like-navRight img').hover(function(){
       $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-like-h.png');
       $(this).css('cursor', 'pointer');
    }, function(){
       $(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-like.png');
    });

    $('.icono-comparar-navRight img').hover(function(){
    	$(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-comparar-h.png');
	 	$(this).next().css('color', '#05aa9a');
	 	$(this).next().css('transition', 'none');
    }, function(){
    	$(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/icono-comparar.png');
    	$(this).css('cursor', 'pointer');
    	$(this).next().css('color', 'black');
    	$(this).next().css('transition', 'none');
    });

    // Boton sidenav
	$('#btnSidenav').click(function(){
		$(this).hide();
	});

	$('.closebtn').click(function(){
		$('#btnSidenav').show();
	});

	// Modals
	$('#registrar').click(function(){
		$('#modal_registrar').modal('show');
	});

	$('#ingresar').click(function(){
		$('#modal_ingresar').modal('show');
	});

	$('.nuevaLista').click(function(){
		$('#modal_lista').modal('show');
	});

	$('.btnPersonalizar').click(function(){
		$('#modal_personalizar').modal('show');
	});

	$('#btnPedido').click(function(){
		$('#modal_pedido').modal('show');
	});

	$('#saveNavR').click(function(){
		$('#modal_guardar_contenedor').modal('show');
	});

	$('.comentarioContenedor').click(function(){
		if ( $(this).attr('src') == 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas_h.png' ) {
			$('#modal_comentario').modal('show');
		}
	});

	// Slide navRight texts
	$('.slide_tc').click(function(){
		$(this).prev().slideToggle();
	});

	$('.slide_eo').click(function(){
		$(this).prev().slideToggle();
	});

	// Slide Content Pics
	$('#btnShowPic').click(function(){
		$('#contenidoFotos').slideToggle();
	});

	// Remove elements VS
	$('.cerrarComparar').click(function(){
		$(this).parent().parent().remove();
	});

	// Sortable columns vs 
	$(".sortable").sortable({connectWith: ".sortable"}).disableSelection();

	// Calendar
    $('.calendar').datepicker({
        inline: true,
        firstDay: 1,
        showOtherMonths: true,
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        monthNames: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'],
        onSelect: function(dateText, inst) {
	        var date = $(this).datepicker('getDate'),
	            day  = date.getDate(),  
	            month = date.getMonth() + 1,              
	            year =  date.getFullYear();

	        $('.dateColumInfo').click(function(){
	        	var temporal = $(this).next();
				if( $(this).attr('src') == 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas_h.png' ){
					$("#btnSave").click(function(){
				        temporal.html(day + '/' + month + '/' + year);
				        $('#modal_calendar').modal('hide');
				    });
				} else {
					$(this).next().html('Fecha de entrega');
				}
			});

			// $("#btnSave").click(function(){
		 //        $(".dateReplace").html(day + '/' + month + '/' + year);
		 //        $('#modal_calendar').modal('hide');
		 //    });

		    $('.btnNavC').click(function(){
		    	$("#in_calendar").val(day + '/' + month + '/' + year);
		    	$('.modal_calendar_right').modal('hide');
		    });
	    }
    });

	$('.dateColumInfo').click(function(){
		alert('Fallo al obtener fecha');
		if( $(this).attr('src') == 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas_h.png' ){
			$('#modal_calendar').modal('show');
		} else {
			$(this).next().html('Fecha de entrega');
		}
	});

	$('#in_calendar').click(function(){
    	$('.modal_calendar_right').modal('show');
    });


	$('.entregaColumnInfo').click(function(){
		var rowContent = $(this).parent().parent().parent().parent();
		if ( $(this).attr('src') == 'http://yankuserver.com/tamev2/public/assets/img/iconFavoritas_h.png' ) {
			rowContent.after(`
								<div class="rowEntrega">
					              <div class="row">
					                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					                  <center>
					                    <label for="">LUGAR DE ENTREGA</label>
					                  </center>
					                </div>
					                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					                  <center>
					                    <label for="">DIRECCIÓN</label>
					                  </center>
					                </div>
					              </div>
					              <div class="row">
					                <form action="" class="form_entrega_row">
					                  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					                    <center>
					                      <select name="" id="">
					                        <option value="Estado">ESTADO</option>
					                        <option value="1">Valor 1</option>
					                        <option value="2">Valor 2</option>
					                        <option value="3">Valor 3</option>
					                      </select>
					                    </center>
					                  </div>
					                  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					                    <center>
					                      <select name="" id="">
					                        <option value="CiudadMunicipio">CIUDAD O MUNICIPIO</option>
					                        <option value="1">Valor 1</option>
					                        <option value="2">Valor 2</option>
					                        <option value="3">Valor 3</option>
					                      </select>
					                    </center>
					                  </div>
					                  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					                    <center>
					                      <textarea name="" id="" rows="4"></textarea>  
					                    </center>
					                  </div>
					                </form>
					              </div>
					            </div>
							`);
		} else {
			rowContent.next().remove();
		}
	});

    

});

// Funciones

function urlSet(){
	var pathname = window.location.pathname;
	// console.log(pathname);
	switch (pathname){
		case '/tamev2/catalogo/catalogoProducto':
			$('#boxContent').css('margin-right', '310px');
			$('#boxContent').css('border-right', '1px solid #c7c1c1');
		break;
		case '/tamev2/catalogo/contenedorProducto':
			$('#boxContent').css('margin-right', '310px');
			$('#boxContent').css('border-right', '1px solid #c7c1c1');
			$('#boxContent').css('padding', '80px 0px');
			// $('#modal_calendar').modal('show');
		break;
	}
}
urlSet();

function sandwichMovil(x) {
  x.classList.toggle("change");
}

function openNav() {
	// Get width window
	var ancho_window = $(window).width();
	
  	if(ancho_window > 768){
  		// console.log('width desktop');
  		$('#boxMenu').removeClass('sidenav-hide');
  		$('#boxMenu').addClass('sidenav-appear');
		// document.getElementById("boxMenu").style.width = "250px";
		document.getElementById("boxContent").style.marginLeft = "250px";
	} else{
		// console.log('width movil');
		// document.getElementById("boxMenu").style.width = "100%";
  		document.getElementById("boxContent").style.marginLeft = "50%";
  		$('#boxMenu').removeClass('sidenav-hide');
  		$('#boxMenu').addClass('sidenav-appear-mov');
	}
}

function closeNav() {
  $('#boxMenu').addClass('sidenav-hide');
  $('#boxMenu').removeClass('sidenav-appear');
  $('#boxMenu').removeClass('sidenav-appear-mov');
  //document.getElementById("boxMenu").style.width = "0";
  document.getElementById("boxContent").style.marginLeft= "0";
}

function highlightStar(obj) {
	removeHighlight();		
	$('.starsClass').each(function(index) {
		$(this).addClass('highlight');
		if(index == $(".starsClass").index(obj)) {
			return false;	
		}
	});
}

function removeHighlight() {
	$('.starsClass').removeClass('selected');
	$('.starsClass').removeClass('highlight');
}

function addRating(obj) {
	$('.starsClass').each(function(index) {
		$(this).addClass('selected');
		$('#rating').val((index+1));
		if(index == $(".starsClass").index(obj)) {
			return false;	
		}
	});
}

function resetRating() {
	if($("#rating").val()) {
		$('.starsClass').each(function(index) {
			$(this).addClass('selected');
			if((index+1) == $("#rating").val()) {
				return false;	
			}
		});
	}
}
