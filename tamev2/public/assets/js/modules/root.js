
// Routing templates

var app = angular.module("appTame", ["ngRoute"]);
app.config(function($routeProvider) {
   $routeProvider
   .when("/", {
        templateUrl : "/tamev2/catalogo/diseno"
    })
    .when("/diseno", {
        templateUrl : "/tamev2/catalogo/diseno"
    })
    .when("/empaque", {
        templateUrl : "/tamev2/catalogo/empaque"
    })
    .when("/otros", {
        templateUrl : "/tamev2/catalogo/otros"
    });
});
