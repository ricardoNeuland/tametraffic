
$(function(){
	$('#inline-demo').colorpicker({
		color:'#31859b',
		defaultPalette:'web'
	});
	
	// COLORS WHITE TO BLACK
	// $('.evo-sep').next().hide();
	// $('.evo-sep').next().next().hide();

	// CHOOSE MORE
	$('.evo-more').hide();

	// SHAPE CIRCLE
	$('.evo-palette2 tbody tr td').css('border-radius', '50px');
});

$(document).ready(function(){

	// Color Picker settings
	$('.evo-palette2 tbody tr td').click(function(){
		$(this).toggleClass('bgCirclePicker');
	});

	$('.circle_color').click(function(){
		$(this).toggleClass('bgCircle');
	});

	// Modal Comentarios
	$('.upFile').click(function(){
		$('#modal_comentarios').modal('show');
	});

	$('.upFile').hover(function(){
		$(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/up_file_h.png');
	}, function(){
		$(this).attr('src', 'http://yankuserver.com/tamev2/public/assets/img/up_file.png');
	});

});

var href = window.location.href;
// console.log(href);
switch (href){
	case 'http://yankuserver.com/tamev2/catalogo/catalogoProducto#/diseno':
		$('#navPersonalizar a:nth-child(1)').css('color', '#05aa9a');
		$('#navPersonalizar a:nth-child(2)').css('color', 'white');
		$('#navPersonalizar a:nth-child(3)').css('color', 'white');
	break;
	case 'http://yankuserver.com/tamev2/catalogo/catalogoProducto#/empaque':
		$('#navPersonalizar a:nth-child(2)').css('color', '#05aa9a');
		$('#navPersonalizar a:nth-child(1)').css('color', 'white');
		$('#navPersonalizar a:nth-child(3)').css('color', 'white');
	break;
	case 'http://yankuserver.com/tamev2/catalogo/catalogoProducto#/otros':
		$('#navPersonalizar a:nth-child(3)').css('color', '#05aa9a');
		$('#navPersonalizar a:nth-child(1)').css('color', 'white');
		$('#navPersonalizar a:nth-child(2)').css('color', 'white');
	break;
}
